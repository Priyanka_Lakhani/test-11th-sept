<apex:page tabStyle="Case" controller="TakeCall_Callback_Cntrl">

    <apex:stylesheet value="{!URLFOR($Resource.jquery,'jquery-ui.css')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jquery,'jquery-1.10.2.js')}"/>
    <apex:includeScript value="{!URLFOR($Resource.jquery,'jquery-ui.js')}"/>
    <apex:includeScript value="https://maps-api-ssl.google.com/maps/api/js?libraries=places,geometry{!IF(NOT(ISBLANK($Setup.CA__c.Google_Key__c)), '&client='&$Setup.CA__c.Google_Key__c, '')}"/>
    <apex:includeScript value="/support/console/26.0/integration.js"/>
    <apex:includeScript value="/soap/ajax/29.0/connection.js"/>
    <apex:includeScript value="/soap/ajax/29.0/apex.js"/>

    <script type="text/javascript">
    	$(function() { 
            $("body").prepend('<div class="loading loadingStyle">Loading...<br/><img src="/img/loading.gif"/></div>'); 
            stopLoading();
        });

        function startLoading() {
            $("[class*='loading']").show();
        }

        function stopLoading() {
            $("[class*='loading']").hide();
        }
    </script>

    <style type="text/css">
	    	.nav_buttons { 
	    		float: right; 
	    	}

	    	.loadingStyle { 
	            z-index:999; 
	            width: 100%; 
	            height: 100%;
	            position: fixed;        
	            display: block;
	            background-color: #DDDDDD; 
	            opacity: 0.6; 
	            filter: alpha(opacity=60);
	        }

	        .over {
	            color: #CC3333;
	            font-weight: bold;
	        }
	        
	        .label-highlight {
	            color: #000099;
	        }
	        
	        .priority-highlight {
	            color: #990000;
	        }

        	.flat-table {
                margin-bottom: 20px;
                border-collapse:collapse;
                font-family: 'Lato', Calibri, Arial, sans-serif;
                border: none;
                        border-radius: 3px;
                       -webkit-border-radius: 3px;
                       -moz-border-radius: 3px;
        	}
            
        	.flat-table th, .flat-table td {
                box-shadow: inset 0 -1px rgba(0,0,0,0.25), 
                    inset 0 1px rgba(0,0,0,0.25);
      	    }
            
            .flat-table th {
                font-weight: normal;
                -webkit-font-smoothing: antialiased;
                padding: 1em;
                color: rgba(0,0,0,0.45);
                text-shadow: 0 0 1px rgba(0,0,0,0.1);
                font-size: 1.5em;
            }
            .flat-table td {
                color: #f7f7f7;
                padding: 0.7em 1em 0.7em 1.15em;
                text-shadow: 0 0 1px rgba(255,255,255,0.1);
                font-size: 1.4em;
            }
            .flat-table tr {
                -webkit-transition: background 0.3s, box-shadow 0.3s;
                -moz-transition: background 0.3s, box-shadow 0.3s;
                transition: background 0.3s, box-shadow 0.3s;
            }

            .flat-table-1 {
                background: #336ca6;
            }
            .flat-table-1 tr:hover {
                background: rgba(0,0,0,0.19);
            }
            .program-message { min-height: 60px; }
            .faded {
                opacity: 0.6;
                filter: alpha(opacity=60);
            }
            .btn {
                text-decoration: none;
            }
    </style>

    <apex:outputPanel rendered="{!$CurrentPage.Parameters.ShowCTIParameters='1'}">
        <strong>CTI Parameters</strong><br/><br/>
        <strong>Call Type:</strong> {!$CurrentPage.Parameters.Type}<br/>
        <strong>Client Code:</strong> {!$CurrentPage.Parameters.Client_Code__c}<br/>
        <strong>Caller ANI:</strong> {!$CurrentPage.Parameters.ANI__c}<br/>
        <strong>Caller Language:</strong> {!$CurrentPage.Parameters.Language__c}<br/>
        <strong>Caller VIN / Member ID:</strong> {!$CurrentPage.Parameters.VIN_Member_ID__c}<br/>
        <strong>Caller City:</strong> {!$CurrentPage.Parameters.City__c}<br/>
        <strong>Caller Province:</strong> {!$CurrentPage.Parameters.Province__c}<br/>
        <strong>Interaction ID:</strong> {!$CurrentPage.Parameters.Interaction_ID__c}
    </apex:outputPanel>

    <apex:outputPanel id="page_header">
        <apex:variable var="subtitle" value="{!redirectedCase.Name__c&' - '&redirectedCase.Phone__c&' '&redirectedCase.Trouble_Code__c}" />
        <apex:variable var="title" value="{!'VIN: '&redirectedCase.Vehicle_Year__c&' '&redirectedCase.Vehicle_Make__c&' '&redirectedCase.Vehicle_Model__c&' '&redirectedCase.VIN_Member_ID__c}" />
        <apex:sectionHeader title="{!title}" subtitle="{!subtitle}" />
        <apex:pageMessage severity="error" strength="2" title="{!errorString}" rendered="{!NOT(ISBLANK(errorString))}" />
        <script>
        	$("[class*='loading']").hide();
        </script>
    </apex:outputPanel>

    <apex:form >

    	<div class="the_whole_page">
			<apex:pageBlock id="new_call_page">
				<apex:pageBlockButtons location="top">
	                <apex:outputPanel id="nav_buttons" styleClass="nav_buttons">
	                    <apex:commandButton value="{!$Label.Button_Save}" action="{!saveAndClose}" onclick="startLoading();" />
	                    <apex:commandButton value="{!$Label.Button_Next}" action="{!next}" onclick="startLoading();" />
	                </apex:outputPanel>
	            </apex:pageBlockButtons>
	            <apex:pageBlockButtons location="bottom">
	                <apex:outputPanel id="nav_buttons2" styleClass="nav_buttons">
	                    <apex:commandButton value="{!$Label.Button_Save}" action="{!saveAndClose}" onclick="startLoading();" />
	                    <apex:commandButton value="{!$Label.Button_Next}" action="{!next}" onclick="startLoading();" />
	                </apex:outputPanel>
	            </apex:pageBlockButtons>

	            <apex:variable var="pName" value="{!IF(redirectedCase.Language__c!='French',	IF(isLGMProject,$Label.LGM_Greeting_English,NULLVALUE(selectedProgram.Client_Name_English__c, selectedProgram.Client_s_Name__c)),IF(isLGMProject,$Label.LGM_Greeting_English,NULLVALUE(selectedProgram.Client_Name_French__c, selectedProgram.Client_s_Name__c)))}" />

	            <apex:outputPanel id="call_steps">

	            	<table width="100%">
					    <apex:variable var="u" value="{!0}">
					        <tr>
					            <td colspan="3">
					                <table class="flat-table flat-table-1" width="100%" style="margin-bottom:0px;">
					                    <tbody>
					                        <tr>
					                            <td width="100%" style="text-align:center;">
					                                <span style="font-size: 1.2em;"><strong>{!redirectedCase.Type} Call</strong></span><br/>
					                                <apex:inputField value="{!redirectedCase.Type}" styleClass="faded" style="font-size: 0.6em;">
					                                    <apex:actionSupport onsubmit="startLoading()" oncomplete="stopLoading()" event="onchange" rerender="call_steps,call_type,refresh_console_links" /> &nbsp;&nbsp;&nbsp;
					                                </apex:inputField>
					                                <apex:inputField value="{!redirectedCase.Language__c}" styleClass="faded" style="font-size: 0.6em;">
					                                    <apex:actionSupport onsubmit="startLoading()" oncomplete="stopLoading()" event="onchange" rerender="new_call_page" />
					                                </apex:inputField>
					                                <apex:inputField value="{!redirectedCase.Origin}" styleClass="faded" style="font-size: 0.6em;" />
					                            </td>
					                        </tr>
					                    </tbody>
					                </table>
					            </td>
					        </tr>
					    </apex:variable>
					    <tr>
					        <td valign="top" width="49%">
					            <apex:pageMessage severity="info" strength="3" id="program_details">
					                <div style="margin-top: -25px; margin-left: 10px; font-size:1.15em;" class="program-message">

					                    <strong>
					                        <apex:outputText escape="false" rendered="{!redirectedCase.Language__c='English'}">
											    <apex:variable var="u1" value="{!0}">
											        <!-- Close the loop start-->
											        <apex:outputText escape="false" value="{!SUBSTITUTE(SUBSTITUTE($Label.Heading_Roadside_Greeting, '[pName]', pName), '[user]', $User.FirstName)}" rendered="{!AND(caseNumberParam == '',selectedProgram.Program_Code__c !='599',selectedProgram.Program_Code__c !='546')}" />
											        <apex:outputText escape="false" value="{!SUBSTITUTE(SUBSTITUTE($Label.Heading_Roadside_Greeting_JLR_CTL, '[pName]', pName), '[user]', $User.FirstName)}" rendered="{!AND(caseNumberParam != '',selectedProgram.Program_Code__c !='599',selectedProgram.Program_Code__c !='546')}" />
											        <!-- Close the loop end-->
											        <apex:outputText escape="false" value="{!SUBSTITUTE($Label.Heading_Roadside_Greeting_Waze, '[user]', $User.FirstName)}" rendered="{!AND(selectedProgram.Program_Code__c =='599')}" />
											        <apex:outputText escape="false" value="{!SUBSTITUTE($Label.Heading_Roadside_Greeting_GVC, '[user]', $User.FirstName)}" rendered="{!AND(selectedProgram.Program_Code__c =='546')}" />
											    </apex:variable>
											</apex:outputText>
											<apex:outputText escape="false" rendered="{!redirectedCase.Language__c='French'}">
											    <apex:variable var="u1" value="{!0}">
											        <!-- Close the loop start-->
											        <apex:outputText escape="false" value="{!SUBSTITUTE(SUBSTITUTE($Label.Heading_Roadside_Greeting_French, '[pName]', pName), '[user]', $User.FirstName)}" rendered="{!AND(caseNumberParam == '',selectedProgram.Program_Code__c !='599',selectedProgram.Program_Code__c !='546')}" />
											        <apex:outputText escape="false" value="{!SUBSTITUTE(SUBSTITUTE($Label.Heading_Roadside_Greeting_JLR_CTL_FR, '[pName]', pName), '[user]', $User.FirstName)}" rendered="{!AND(caseNumberParam != '',selectedProgram.Program_Code__c !='599',selectedProgram.Program_Code__c !='546')}" />
											        <!-- Close the loop end-->
											        <apex:outputText escape="false" value="{!SUBSTITUTE($Label.Heading_Roadside_Greeting_Waze_French, '[user]', $User.FirstName)}" rendered="{!AND(selectedProgram.Program_Code__c =='599')}" />
											        <apex:outputText escape="false" value="{!SUBSTITUTE($Label.Heading_Roadside_Greeting_GVC_French, '[user]', $User.FirstName)}" rendered="{!AND(selectedProgram.Program_Code__c =='546')}" />
											    </apex:variable>
											</apex:outputText>
											<apex:outputText escape="false" rendered="{!redirectedCase.Language__c='No Option'}">
											    <apex:variable var="u1" value="{!0}">
											        {!SUBSTITUTE(SUBSTITUTE($Label.Heading_Roadside_Greeting_No_Option, '[pName]', pName), '[user]', $User.FirstName)}
											    </apex:variable>
											</apex:outputText>
					                    </strong>

					                    <div style="font-size: 1em; color:#696969; margin-top: 10px;" class="faded">
					                        For full program details
					                        <a href="/{!redirectedCase.Program__c}" class="not-console" target="_blank" style="display:{!IF(NOT(ISBLANK(redirectedCase.Program__c)), 'inline', 'none')};">click here</a>
					                        <a href="javascript:void(0);" class="is-console" style="display:{!IF(NOT(ISBLANK(redirectedCase.Program__c)), 'inline', 'none')};" onclick="openConsoleSubTab('{!selectedProgram.Id}', '{!selectedProgram.Name}');return false;">click here</a>&nbsp;
					                        <apex:selectList value="{!redirectedCase.Program__c}" multiselect="false" size="1" style="max-width:400px;font-size:1.2em;" styleClass="faded">
					                            <apex:selectOptions value="{!programs}" />
					                            <apex:actionSupport onsubmit="startLoading()" oncomplete="stopLoading()" event="onchange" action="{!getSelectedProgram}" rerender="refresh_vehicle_section,travel_intro,eligibility,tc_label,tc_results,vin_details,mileage_details,flip_chart,caller_details,exp_details,vehicle_details,program_details,program_promotions,hidden_details,refresh_console_links,refresh_policy_vin,autocomplete_filter,trouble_options,caller_history,recall_assessment,RecallCall,jlrTriagePnl,call_steps,recall_faq" />-->
					                        </apex:selectList>
					                        <apex:outputText value="This is a Spanish call,contact language link" rendered="{!(redirectedCase.CTI_Term__c='LincolnSpanish')}" style="font-size:16px; color:red" />
					                    </div>
					                </div>
					            </apex:pageMessage>
					        </td>
					        <td>
					            <apex:pageMessage severity="info" strength="3" id="program_promotions">
					                <div style="margin-top: -25px; margin-left: 10px;" class="program-message">
					                    <strong>Program Notes:</strong>
					                    <apex:outputPanel rendered="{!NOT(ISBLANK(selectedProgram.Special_Offers__c))}">
					                        <br/>
					                        <apex:outputField value="{!selectedProgram.Special_Offers__c}" />
					                    </apex:outputPanel>
					                    <apex:outputPanel rendered="{!(selectedProgram.Program_Code__c =='566' || selectedProgram.Program_Code__c =='567')}">
					                        <br/>
					                        <apex:outputText value="If the customer is asking for a higher level of Triage or any inquiries outside of what you know, please contact an Acura NSX Specialist at __________" />
					                    </apex:outputPanel>
					                </div>
					            </apex:pageMessage>
					        </td>
					    </tr>
					</table>

					<apex:outputPanel id="nonRecallCall">
						<apex:outputPanel >
					        <apex:pageMessages />
					        <table class="flat-table flat-table-1" width="100%">
					            <tbody>
					                <tr>
					                    <td width="100%">
					                        <apex:outputText escape="false" value="{!$Label.Heading_Needs_Assessment}" rendered="{!redirectedCase.Language__c!='French'}" />
					                        <apex:outputText escape="false" value="{!$Label.Heading_Needs_Assessment_French}" rendered="{!redirectedCase.Language__c='French'}" />
					                    </td>
					                </tr>
					            </tbody>
					        </table>

					        <apex:outputPanel id="roadside_assessment2">
		                        
		                        <apex:pageBlockSection columns="1" collapsible="false">

		                            <apex:pageBlockSectionItem >
		                                <apex:outputLabel >
		                                    <apex:outputText escape="false" styleClass="over" value="{!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE($Label.Question_JLR_CTL_Callback_ENG, '[vModel]', redirectedCase.Vehicle_Model__c), '[vYear]', redirectedCase.Vehicle_Year__c),'[vVin]',IF(NOT(ISBLANK(redirectedCase.VIN_Member_ID__c)), '(VIN: '&redirectedCase.VIN_Member_ID__c&')',''))}" rendered="{!redirectedCase.Language__c != 'French'}"/>
		                                    <apex:outputText escape="false" styleClass="over" value="{!SUBSTITUTE(SUBSTITUTE(SUBSTITUTE($Label.Question_JLR_CTL_Callback_FR, '[vModel]', redirectedCase.Vehicle_Model__c), '[vYear]', redirectedCase.Vehicle_Year__c),'[vVin]',IF(NOT(ISBLANK(redirectedCase.VIN_Member_ID__c)), '(VIN: '&redirectedCase.VIN_Member_ID__c&')',''))}" rendered="{!redirectedCase.Language__c == 'French'}"/>
		                                </apex:outputLabel>    
		                                <apex:selectList value="{!isYourVehicleJLR}" size="1" multiselect="false">
		                                    <apex:selectOption itemValue="--None--" itemLabel="--None--"/>
		                                    <apex:selectOption itemValue="Yes" itemLabel="Yes"/>
		                                    <apex:selectOption itemValue="No" itemLabel="No"/>
		                                    <apex:actionSupport event="onchange" onsubmit="startLoading()" oncomplete="stopLoading()" rerender="roadside_assessment2"/>
		                                </apex:selectList>   
		                            </apex:pageBlockSectionItem>
		                    <!-- TR 534 start -->
		                            <apex:pageBlockSectionItem rendered="{!AND(caseNumberParam == '',isYourVehicleJLR == 'Yes')}"> 
		                                <apex:outputLabel >
		                                    <apex:outputText escape="false" styleClass="over" value="{!'Are you calling for an update or to cancel?'}" />
		                                </apex:outputLabel>
		                                <apex:selectList value="{!CallbackToCancelOrUpdate}" size="1" multiselect="false">
		                                    <apex:selectOption itemValue="--None--" itemLabel="--None--"/>
		                                    <apex:selectOption itemValue="Cancel" itemLabel="Cancel"/>
		                                    <apex:selectOption itemValue="Update" itemLabel="Update"/>
		                                    <apex:actionSupport event="onchange" onsubmit="startLoading()" oncomplete="stopLoading()" rerender="roadside_assessment2"/>
		                                </apex:selectList>
		                            </apex:pageBlockSectionItem>
		                    <!-- TR 534 end -->
		                            <apex:pageBlockSectionItem rendered="{!AND(caseNumberParam != '',isYourVehicleJLR == 'Yes')}">
		                                <apex:outputLabel >
		                                    <apex:outputText escape="false" styleClass="over" value="{!$Label.Question_You_are_waiting_for_Service_ENG}" rendered="{!redirectedCase.Language__c != 'French'}"/>
		                                    <apex:outputText escape="false" styleClass="over" value="{!$Label.Question_You_are_waiting_for_Service_FR}" rendered="{!redirectedCase.Language__c == 'French'}"/>
		                                </apex:outputLabel>                             
		                                <apex:selectList value="{!isYouWaitingJLR}" size="1" multiselect="false">
		                                    <apex:selectOption itemValue="--None--" itemLabel="--None--"/>
		                                    <apex:selectOption itemValue="Yes" itemLabel="Yes"/>
		                                    <apex:selectOption itemValue="No" itemLabel="No"/>
		                                    <apex:actionSupport event="onchange" onsubmit="startLoading()" oncomplete="stopLoading()" rerender="roadside_assessment2"/>
		                                </apex:selectList>
		                            </apex:pageBlockSectionItem>
		                            <apex:pageBlockSectionItem rendered="{!isYourVehicleJLR == 'No'}">
		                                <apex:outputPanel >
		                                    <apex:outputText escape="false" styleClass="over" value="{!$Label.Question_Provide_Phone_Number_or_VIN_ENG}" rendered="{!redirectedCase.Language__c != 'French'}"/>
		                                    <apex:outputText escape="false" styleClass="over" value="{!$Label.Question_Provide_Phone_Number_or_VIN_FR}" rendered="{!redirectedCase.Language__c == 'French'}"/>
		                                </apex:outputPanel>
		                            </apex:pageBlockSectionItem>
		                            <apex:pageBlockSectionItem rendered="{!AND(caseNumberParam != '',isYourVehicleJLR == 'Yes',isYouWaitingJLR == 'No')}">
		                                <apex:outputLabel >
		                                    <apex:outputText escape="false" value="Since the service is complete, do you want to Close the Loop?" />
		                                </apex:outputLabel>                             
		                                <!--TR 673 start-->      
		                                <apex:selectList value="{!doYouWantCloseTheLoop}" size="1" multiselect="false">
		                                    <apex:selectOption itemValue="--None--" itemLabel="--None--"/>
		                                    <apex:selectOption itemValue="Yes" itemLabel="Yes"/>
		                                    <apex:selectOption itemValue="No" itemLabel="No"/>
		                                </apex:selectList>
		                                <!--TR 673 end-->
		                            </apex:pageBlockSectionItem>
		                            <!--close the loop call back end-->
		                            
		                            <apex:pageBlockSectionItem rendered="{!redirectedCase.Callback_To_Cancel__c='No'}">
		                                <apex:outputLabel >
		                                    <apex:outputText escape="false" value="{!'Are you calling for an update?'}" />
		                                </apex:outputLabel>                             
		                                <apex:inputField value="{!redirectedCase.Callback_To_Update__c}">
		                                    <apex:actionSupport event="onchange" rerender="roadside_assessment2,refresh_console_links"/>
		                                </apex:inputField>
		                            </apex:pageBlockSectionItem>

		                        </apex:pageBlockSection>
		                    </apex:outputPanel>

					    </apex:outputPanel>

					</apex:outputPanel>

	            </apex:outputPanel>

	            <apex:outputText escape="false" >
                    <div style="font-weight: bold;float: right;">
                                    JLR AAA service tracker is located <a href="http://rap.national.aaa.com:8080/TruckTracker/TruckTracker.html" target="_blank">[here]</a>
                                    If the call is located in Canada, use the service tracker map located in the next page or call CAA dispatch to have an updated ETA
                    </div>
                </apex:outputText>

			</apex:pageBlock>
		</div>

    </apex:form>
</apex:page>