public class JLR_CasesForEscalationCntrl {
    
    public List<Case> jlrEscList {get;set;}
    public Map<Id,Integer> delayCaseTime {get;set;}
    
    public JLR_CasesForEscalationCntrl () {
    
        jlrEscList = new List<Case>();
        delayCaseTime = new Map<Id,Integer>();
        getCaseInfo();
        
    }
    
    public void getCaseInfo(){
    
        jlrEscList.clear();
        delayCaseTime.clear();
        for(Case cs: [SELECT id,CaseNumber,Status,First_Name__c,Last_Name__c,DI_Status_Date_Time__c,Trouble_Code__c,Club__c,Club_Call_Number__c,AAAClub__c,Vehicle_Make__c,Vehicle_Model__c,ETA__c,OwnerId,Owner.Name,Escalation_Modified_By__c,Escalation_Modified_Date__c
                      FROM Case 
                      WHERE Program__r.Call_Flow_JLR__c = TRUE 
                            AND Club_Code__c IN ('999','272','273','275','285','288','290','284','286','282')
                            AND DI_Status_Date_Time__c != null
                            AND Status IN ('Dispatched','Received','Driver En Route','Driver On Location','Vehicle in Tow')
                            AND ETA__c > 89
                            AND Trouble_Code__c IN ('Tow','Accident Tow','Winch','Flatbed Tow')
                      ORDER BY DI_Status_Date_Time__c DESC]){
                               
            Integer delayInMin = getDuration(cs.DI_Status_Date_Time__c,DateTime.now());
                                
            jlrEscList.add(cs);
            /*if (30-delayInMin < 0){
                delayInMin = 0;
            }
            else {
                delayInMin = 30 - delayInMin;
            }*/
            delayCaseTime.put(cs.id,delayInMin);
            
        }
        
    }
    
    public Integer getDuration(DateTime start_date_time, DateTime end_date_time){
    
        Integer start_year_as_int = start_date_time.year();
        Integer start_day_as_int = start_date_time.dayOfYear();
        Integer start_hour_as_int = start_date_time.hour();
        Integer start_minute_as_int = start_date_time.minute();
        Integer start_second_as_int = start_date_time.second();
        Integer start_in_seconds = (start_year_as_int * 31556926) + (start_day_as_int * 86400) + (start_hour_as_int * 3600) + (start_minute_as_int * 60) + (start_second_as_int * 1);

        Integer end_year_as_int = end_date_time.year();
        Integer end_day_as_int = end_date_time.dayOfYear();
        Integer end_hour_as_int = end_date_time.hour();
        Integer end_minute_as_int = end_date_time.minute();
        Integer end_second_as_int = end_date_time.second();
        Integer end_in_seconds = (end_year_as_int * 31556926) + (end_day_as_int * 86400) + (end_hour_as_int * 3600) + (end_minute_as_int * 60) + (end_second_as_int * 1); //convert the end date to a value in seconds
        Integer total_duration_in_seconds = end_in_seconds - start_in_seconds;
        Integer minute_result = math.floor(total_duration_in_seconds/60).intValue();
        
        return minute_result;
    }
    
}