@RestResource(urlMapping='/api/v1/policy/*')
global class CommonApiPolicy {

    @HttpPost
    global static String doPost(Policy__c policy){
        insert policy;
        return policy.Id;
    }
}