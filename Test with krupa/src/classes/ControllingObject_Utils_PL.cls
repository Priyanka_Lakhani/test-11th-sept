/*************************************************************************************************
* @author Alex Parsin   <alex.parsin@integrate2cloud.com>
*
* @description Utility class
*/
public class ControllingObject_Utils_PL {
    
    /*************************************************************************************************
     * @description Constructor.
    */
    public ControllingObject_Utils_PL(ControllingObjectTestPageController_PL controller) {}

    /*************************************************************************************************
     * @description Retrieves the Vehicle or the Policy record for the Lookup entities
     * 
     * @param idSearch The Id of the Lookup Entity that triggered the search
     * 
     * @param value The value provided by the agent
     * 
     * @param client The Client Id to be used as a filter
     * 
     * @param programCode The Program code to be used as a filter
     * 
     * @return String, a JSON string containing all the SOQL results
    */
    @RemoteAction public static String getLookUpMethod2(String idSearch,String value, String client, String programCode) {
        List<Flow_Entity__c> FlowEntityRecord = Database.query('SELECT Id, Controlled_Object__c, Controlled_Field__c, Lookup_Object__c, Lookup_Field__c, Lookup_Filter_Field__c, Lookup_Filter_Value__c FROM Flow_Entity__c WHERE Id = :idSearch');
        //System.debug('[AR] Value = ['+FlowEntityRecord+']');

        String soQl;
        String soSl;
        String soslQuery;

        String returnString = '';
        final String specialChars = '& | ! ( ) { } [ ] ^ " ~ * ? : -';
        
        for (String sc: specialChars.split(' ')) {
        value = value.replaceAll('\\'+sc, '');
        }
        
        List<Map<String, String>> resultsList = new List<Map<String, String>>();
        
        if((FlowEntityRecord[0].Lookup_Object__c == 'Vehicle__c'  || FlowEntityRecord[0].Lookup_Object__c == 'Policy__c') &&  (FlowEntityRecord[0].Lookup_Field__c == 'Name' || FlowEntityRecord[0].Lookup_Field__c == 'License__c')){
            List<string> soslResults = new List<string>();
            
            if(FlowEntityRecord[0].Lookup_Object__c == 'Vehicle__c' && FlowEntityRecord[0].Lookup_Field__c == 'Name'){
                soSl = 'FIND {' +value+ '*} IN ALL FIELDS RETURNING Vehicle_Policy__c(Policy__c where AccountID__c = \''+client+'\' and Vehicle_Policy_External__c like \'%'+value+'%\'), Contact(Name, Policy__c where AccountID = \''+client+'\' and (Name like \'%'+value+'%\' or Phone like \'%'+value.substring(0,3)+'%\' )) LIMIT 15';        
                
                List<List <sObject>> searchResult = search.query(soSl);
                 
                List<Vehicle_Policy__c> vps = ((List<Vehicle_Policy__c>)searchResult[0]);
                List<Contact> cts = ((List<Contact>)searchResult[1]);
                
                for (Vehicle_Policy__c vp : vps) { 
                    soslResults.add(vp.Policy__c); 
                } 
                for (Contact ct : cts) { 
                    soslResults.add(ct.Policy__c); 
                }
                
                soQl = 'Select Vehicle_Number__c,Policy__c, Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c FROM Vehicle_Policy__c WHERE  Vehicle_Policy_External__c Like \'%'+value+'%\' and  Vehicle_Number__c Like \'%'+value+'%\' and AccountID__c = \''+client+'\'  LIMIT 15';
            } 
            else if(FlowEntityRecord[0].Lookup_Object__c == 'Policy__c' && FlowEntityRecord[0].Lookup_Field__c == 'Name'){
                soSl = 'FIND {' +value+ '*} IN ALL FIELDS RETURNING Policy__c(ID where program__r.Account__c = \''+client+'\'), Contact(Name, Policy__c where AccountID = \''+client+'\' and (Name like \'%'+value+'%\' or Phone like \'%'+value.substring(0,3)+'%\' )) LIMIT 15'; 
                List<List <sObject>> searchResult = search.query(soSl);
                 
                List<Policy__c> ps = ((List<Policy__c>)searchResult[0]);
                List<Contact> cts = ((List<Contact>)searchResult[1]);
                
                for (Policy__c p : ps) { 
                    soslResults.add(p.Id); 
                } 
                for (Contact ct : cts) { 
                    soslResults.add(ct.Policy__c); 
                }
                
                soQl = 'Select Vehicle_Number__c,Policy__c,Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c, Policy_Number__c  FROM Vehicle_Policy__c WHERE  Vehicle_Policy_External__c Like \'%'+value+'%\' and  Policy_Number__c Like \'%'+value+'%\' and AccountID__c = \''+client+'\' LIMIT 15';   
            } 
            else if(FlowEntityRecord[0].Lookup_Object__c == 'Vehicle__c' &&  FlowEntityRecord[0].Lookup_Field__c == 'License__c'){
                soQl = 'Select Vehicle_License__c, Vehicle_Number__c, Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c, Policy__c FROM Vehicle_Policy__c WHERE  Vehicle_License__c Like \'%'+value+'%\' AND Vehicle_License__c !=\'\' and AccountID__c = \''+client+'\' LIMIT 15';
            }
            
            System.debug('[VC] soSl ['+soSl+']'); 
            System.debug('[VC] soQl ['+soQl+']');
            
            
            List<vehicle_policy__c> listRecord = new List<vehicle_policy__c>();
            try{
            if(!soslResults.isEmpty()){
             soslQuery = 'Select Vehicle_Number__c, Policy__c, Policy_Number__c, Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c FROM Vehicle_Policy__c WHERE  policy__c in :soslResults  LIMIT 15';
             System.debug('[VC] soslQuery ['+soslQuery+']');
             listRecord = Database.query(soslQuery);
            } else{
             listRecord = Database.query(soQl);   
            }
            }catch(Exception e){system.debug(e);}
            Map<String, String> policyContactsMap = new Map<String, String>();
            Map<String, String> vehiclePolicyMap = new Map<String, String>();
            
            for (Integer i = 0; i < listRecord.size(); i++) {
                    Vehicle_Policy__c vehiclePolicy = (Vehicle_Policy__c)listRecord[i];
                    
                        vehiclePolicyMap.put(vehiclePolicy.Id, vehiclePolicy.Policy__c);
                        policyContactsMap.put(vehiclePolicy.Policy__c, '');
                     
            }
            for(Contact currContact : [SELECT Id, Policy__c, Name FROM Contact WHERE Policy__c IN :policyContactsMap.keySet()]){
                if(policyContactsMap.containsKey(currContact.Policy__c)){
                    policyContactsMap.put(currContact.Policy__c, currContact.Name);
                }
            }
            for (Integer i = 0; i < listRecord.size(); i++) {
                Map<String, String> objectsMap = new Map<String, String>();
     
                objectsMap.put('Id', (String)listRecord[i].get(FlowEntityRecord[0].Lookup_Object__c));
                if(FlowEntityRecord[0].Lookup_Object__c == 'Policy__c'){
                objectsMap.put('Text', (String)listRecord[i].get('Policy_Number__c'));
                }
                else if(FlowEntityRecord[0].Lookup_Object__c == 'Vehicle__c' && FlowEntityRecord[0].Lookup_Field__c == 'License__c'){
                objectsMap.put('Text', (String)listRecord[i].get('Vehicle_License__c'));
                }else {
                    objectsMap.put('Text', (String)listRecord[i].get('Vehicle_Number__c'));
                }
                objectsMap.put('RelatedContact', '');
                objectsMap.put('Car', '');
     
     
                    objectsMap.put('Car', (listRecord[i].Vehicle__r.get('Year__c') != null ? (String)listRecord[i].Vehicle__r.get('Year__c') : '') + ' ' + (listRecord[i].Vehicle__r.get('Make__c') != null ? (String)listRecord[i].Vehicle__r.get('Make__c') : '') + ' ' + (listRecord[i].Vehicle__r.get('Model__c') != null ? (String)listRecord[i].Vehicle__r.get('Model__c') : ''));
                    if(vehiclePolicyMap.containsKey((String)listRecord[i].get('Id')) && policyContactsMap.containsKey(vehiclePolicyMap.get((String)listRecord[i].get('Id')))){
                        objectsMap.put('RelatedContact', policyContactsMap.get(vehiclePolicyMap.get((String)listRecord[i].get('Id'))));
                    } else {
                        objectsMap.put('RelatedContact', '');
                    }
     
            
                resultsList.add(objectsMap);
            }
        }
        else if(FlowEntityRecord[0].Lookup_Object__c == 'Contact' &&  FlowEntityRecord[0].Lookup_Field__c == 'Phone'){
            soSl = 'FIND \'*'+value+'*\' IN PHONE FIELDS RETURNING '+FlowEntityRecord[0].Lookup_Object__c+'(Id, Policy__c, Name, Phone  where Policy__r.Program__r.Account__c=\''+client+'\') limit 15';
            List<List<SObject>> searchList = search.query(soSl);
            
            Map<String, String> policyContactsMap = new Map<String, String>();
            Map<String, Vehicle_Policy__c> vehiclePolicyMap = new Map<String, Vehicle_Policy__c>();
            List<Contact> contactList = new List<Contact>();
            
            contactList = ((List<Contact>)searchList[0]);
            
            for (Contact ctc: contactList) {                    
                policyContactsMap.put(ctc.Policy__c, ctc.Name);                     
            }
            List<Vehicle_Policy__c> vehiclePolicyList = [SELECT Id, Vehicle__c,Vehicle__r.Name,Vehicle__r.Year__c, Vehicle__r.Make__c, Vehicle__r.Model__c,Policy__c FROM Vehicle_Policy__c WHERE Policy__c IN :policyContactsMap.keySet()];
            for(Vehicle_Policy__c vehiclePolicy : vehiclePolicyList){
                if(policyContactsMap.containsKey(vehiclePolicy.Policy__c)){
                    vehiclePolicyMap.put(vehiclePolicy.Policy__c, vehiclePolicy);
                }
            }
            for (Integer i = 0; i < contactList.size(); i++) {
                Map<String, String> objectsMap = new Map<String, String>();
     
                objectsMap.put('Id', (String)contactList[i].get('Id'));
                objectsMap.put('Text', (String)contactList[i].get('Phone'));
                objectsMap.put('RelatedContact', (String)contactList[i].get('Name'));
                objectsMap.put('Car', '');
        
                objectsMap.put('Car', (((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Name') != null ? (String)(((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Name')) : '') + ' ' + (((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Year__c') != null ? (String)(((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Year__c')) : '') + ' ' + (((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Make__c') != null ? (String)(((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Make__c')) : '') + ' ' + (((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Model__c') != null ? (String)(((Vehicle_Policy__c)vehiclePolicyMap.get((String)contactList[i].get('Policy__c'))).Vehicle__r.get('Model__c')) : ''));     
            
                resultsList.add(objectsMap);
            }
        }
        else {        
            soQl = 'Select '+ FlowEntityRecord[0].Lookup_Field__c+' FROM '+FlowEntityRecord[0].Lookup_Object__c+' WHERE '+FlowEntityRecord[0].Lookup_Field__c+' Like \'%'+value+'%\'';
        

            if(FlowEntityRecord[0].Lookup_Filter_Field__c != null){
                soQl += ' AND ' + FlowEntityRecord[0].Lookup_Filter_Field__c + ' = \'' + FlowEntityRecord[0].Lookup_Filter_Value__c + '\'';
            }
       
            if(FlowEntityRecord[0].Controlled_Object__c == 'Case' && FlowEntityRecord[0].Controlled_Field__c == 'Club__c' && FlowEntityRecord[0].Lookup_Object__c == 'Account'){ // The query should be filtered to Clubs only
                soQl += ' AND RecordType.Name = \'Dispatch Club\'';
            }
            soQL += ' LIMIT 15';
    
            List<sObject> listRecord = Database.query(soQl);
            for (Integer i = 0; i < listRecord.size(); i++) {
                Map<String, String> objectsMap = new Map<String, String>();
     
                objectsMap.put('Id', (String)listRecord[i].get('Id'));
                objectsMap.put('Text', (String)listRecord[i].get(FlowEntityRecord[0].Lookup_Field__c));
                resultsList.add(objectsMap);
            }
        }
        //return JSON.serialize(Database.query(query));
        returnString = JSON.serialize(resultsList);
        System.debug('[AP] [getLookUpMethod2] returnString = ' + returnString);
        return returnString;
    }
   
    /*************************************************************************************************
     * @description Transforms the dependency JSON string into a Map of values
     * 
     * @param jsonString String, the string to be transformed into map
     * 
     * @return Map of Maps of available values for the dependent picklists
    */
    public static Map<String, Map<String, Boolean>> deserializePicklistDependencies(String jsonString){
        //FlodRegion
        Map<String, Map<String, Boolean>> jsonMap = new Map<String, Map<String, Boolean>>();
        if(jsonString != null){
            Map<String, Object> tempMap = (Map<String, Object>)JSON.deserializeUntyped(jsonString);
            for(String currentIndex : tempMap.keySet()){
                Map<String, Boolean> subMap = new Map<String, Boolean>();
                Map<String, Object> tempSubmap = (Map<String, Object>)tempMap.get(currentIndex);
                for(String currentSubIndex : tempSubmap.keySet()){
                    subMap.put(currentSubIndex, (Boolean)tempSubmap.get(currentSubIndex));
                }
                jsonMap.put(currentIndex, subMap);
            }
        }
        return jsonMap;
    }
    
    /*************************************************************************************************
     * @description Find dispatch Club based on breakdown location
     * 
     * @param currentCase The Case record to be updated with the Dispatch Club
     * 
     * @param coverageMap A Map of territory coverages
     * 
     * @param mtclub The Id of the Mobile Technician Club
     * 
     * @param aaaclub The Id of the AAA Club
     * 
     * @return The updated Case record
    */
    public static Case findDispatchClub(Case currentCase, Map<String, Territory_Coverage__c> coverageMap, Id mtclub, Id aaaclub){
        //FlodRegion
        System.debug('[PL] [findDispatchClub] currentCase : '+ currentCase);
        System.debug('[PL] [findDispatchClub] coverageMap : '+ coverageMap);
        System.debug('[PL] [findDispatchClub] mtclub : '+ mtclub);
        System.debug('[PL] [findDispatchClub] aaaclub : '+ aaaclub);
        boolean success = false;
        System.debug('[PL] [clubAssignment] currentCase.JLRTriageMTRequired__c=' + currentCase.JLRTriageMTRequired__c + ' currentCase.JLRTriageAgent__c:'+currentCase.JLRTriageAgent__c+' currentCase.JLRMTFound__c:'+currentCase.JLRMTFound__c+' currentCase.MobTechDenied__c:'+currentCase.MobTechDenied__c);
        //JLR Mobile Technician club already set in CheckJLRMobileTech() method
        if (currentCase.JLRTriageMTRequired__c=='Yes' && currentCase.JLRTriageAgent__c =='Yes' && currentCase.JLRMTFound__c && currentCase.MobTechDenied__c != 'Yes'){
            currentCase.Club__c = mtclub;
            success = true;
        }
        System.debug('[PL] [findDispatchClub] currentCase.Club__c: '+ currentCase.Club__c+'currentCase.Country__c:'+currentCase.Country__c+'currentCase.sponsorId__c:'+currentCase.sponsorId__c);   
        // set AAA club    
        if (!success && aaaclub!=null && (currentCase.Country__c == 'United States' || currentCase.Country__c == 'US')&& (currentCase.sponsorId__c != null && currentCase.sponsorId__c!='') && (currentCase.programId__c != null && currentCase.programId__c!=''))
        {
            currentCase.Club__c = aaaclub;
            success = true;
        }
        System.debug('[PL] [findDispatchClub] currentCase.Club__c: '+ currentCase.Club__c);  
        //set CAA club
        if (!success)
        {
            currentCase.Club__c=null;
            Territory_Coverage__c temp = coverageMap.get(currentCase.Coverage_Key__c);
            if(currentCase.Club__c==NULL && temp!=NULL) { 
                boolean isOffHours = false; 
                if(temp.Club__c!=NULL && temp.Club__r.Off_Hours_Club__c!=NULL){
                    try{
                        datetime dt = system.datetime.now();
                        String timezone = 'EST';
                        if(temp.Club__r.timezone__c != null)
                            timezone= temp.Club__r.timezone__c;
                        if(TimeZone_Abbreviation__c.getAll() != null && TimeZone_Abbreviation__c.getAll().get(timezone) != null && TimeZone_Abbreviation__c.getAll().get(timezone).TimeZone_ID__c != null)
                            timezone = TimeZone_Abbreviation__c.getAll().get(timezone).TimeZone_ID__c;                      
                        string hour = dt.format('HH:mm:ss', timezone);
                        String day = dt.format('EEEE', timezone); 
                        string[] hr;
                        if (day=='Monday' && String.isNotBlank(temp.Club__r.monday__c)){
                            hr = temp.Club__r.monday__c.split('-');
                        } else if (day=='Tuesday' && String.isNotBlank(temp.Club__r.Tuesday__c)){
                            hr = temp.Club__r.Tuesday__c.split('-');
                        } else if (day=='Wednesday' && String.isNotBlank(temp.Club__r.Wednesday__c)){
                            hr = temp.Club__r.Wednesday__c.split('-');
                        }  else if (day=='Thursday' && String.isNotBlank(temp.Club__r.Thursday__c)){
                            hr = temp.Club__r.Thursday__c.split('-');
                        } else if (day=='Friday' && String.isNotBlank(temp.Club__r.Friday__c)){
                            hr = temp.Club__r.Friday__c.split('-');
                        } else if (day=='Saturday' && String.isNotBlank(temp.Club__r.Saturday__c)){
                            hr = temp.Club__r.Saturday__c.split('-');
                        } else if (day=='Sunday' && String.isNotBlank(temp.Club__r.Sunday__c)){
                            hr = temp.Club__r.Sunday__c.split('-');
                        } else{
                            isOffHours=true;
                        }
                        if(hr != null && hr.size()>0){
                            string[] shr = hr[0].split(':');
                            string[] ehr =hr[1].split(':');
                            string[] hrNow = hour.split(':');
                            datetime startdt = datetime.newInstance(integer.valueof(dt.format('yyyy')),integer.valueof(dt.format('MM')),integer.valueof(dt.format('dd')),integer.valueof(shr[0]),integer.valueof(shr[1]),0);
                            datetime enddt =  datetime.newInstance(integer.valueof(dt.format('yyyy')),integer.valueof(dt.format('MM')),integer.valueof(dt.format('dd')),integer.valueof(ehr[0]),integer.valueof(ehr[1]),0);
                            datetime nowdt = datetime.newInstance(integer.valueof(dt.format('yyyy')), integer.valueof(dt.format('MM')), integer.valueof(dt.format('dd')), integer.valueof(hrNow[0]),integer.valueof(hrNow[1]),0);
                            system.debug(startdt);
                            system.debug(enddt);
                            system.debug(nowdt);
            
                            if (nowdt<startdt || nowdt>enddt)
                                isOffHours=true;
                        }
                    }catch(System.Exception ex){
                        System.debug('ex:'+ex.getMessage());
                        isOffHours=false;
                    }     
                }
                if(temp.Club__c!=NULL && isOffHours && temp.Club__r.Off_Hours_Club__c!=NULL) {
                    currentCase.Club__c=temp.Club__r.Off_Hours_Club__c;
                    currentCase.Red_Flag__c=temp.Club__r.Off_Hours_Club__r.Code_Red__c;
                } else {
                    currentCase.Club__c=temp.Club__c; 
                    currentCase.Red_Flag__c=temp.Club__r.Code_Red__c;
                }
            }
        }
        System.debug('[PL] [findDispatchClub] currentCase.Club__c: '+ currentCase.Club__c);
        return currentCase;
    }

    /*************************************************************************************************
     * @description Used for detailed Exception debugging
     * 
     * @param e The Exception to be debuged
    */
    public static void debugSFException(Exception e) {
        //FlodRegion
        System.debug(LoggingLevel.ERROR, e.getTypeName() + ': ' + '[' + e.getMessage() + ']; StackTrace: [' + JSON.serialize(e.getStackTraceString()) + ']');
    }

    /*************************************************************************************************
     * @description Updates the Case record in background
     * 
     * @param flowEntityMap The JSON string containing the controller flowEntityMap
     * 
     * @param businessPhoneExtention String, the extension for business phone numbers
     * 
     * @param vehicleId The Id of the current Vehicle
     * 
     * @param defaultMake The default Make for the current Program
     * 
     * @param currentCaseId The Id of the current Case record
     * 
     * @param currentProgramId The Id of the current Program
     * 
     * @return The Id of the updated Case record
    */
    @RemoteAction public static String autoSave(String flowEntityMap,String businessPhoneExtention,String vehicleId,String defaultMake,String currentCaseId, String currentProgramId) {
        Map<String, Map<String, Schema.DisplayType>> objectFieldType = new Map<String, Map<String, Schema.DisplayType>>();
        CA__c settings = CA__c.getOrgDefaults();

        Case autoSaveCase = new Case();
        Schema.DescribeSobjectResult[] results = Schema.describeSObjects(new List<String>{'Case'});
        for(Schema.DescribeSobjectResult res : results){
            objectFieldType.put(res.name, new Map<String, Schema.DisplayType>());
            Map<String, SObjectField> fldMap = res.fields.getMap();
            for(String fieldName : fldMap.keySet()){
                objectFieldType.get(res.name).put(fieldName, fldMap.get(fieldName).getDescribe().getType());
            }
        }
        Map<String, Map<String, String>> deserializeFlowEntityMap = (Map<String, Map<String, String>>)JSON.deserialize(flowEntityMap, Map<String, Map<String, String>>.class);

         for(String fieldId : deserializeFlowEntityMap.keySet()){

            System.debug(deserializeFlowEntityMap.get(fieldId).get('ControlledField'));
            System.debug(deserializeFlowEntityMap.get(fieldId).get('FieldValue'));
            System.debug(deserializeFlowEntityMap.get(fieldId).get('FieldType'));



            if((deserializeFlowEntityMap.get(fieldId).get('FieldValue') != null && deserializeFlowEntityMap.get(fieldId).get('FieldValue') != 'null' && deserializeFlowEntityMap.get(fieldId).get('FieldValue')!= '' && deserializeFlowEntityMap.get(fieldId).get('FieldValue') != '--None--' && deserializeFlowEntityMap.get(fieldId).get('FieldValue') != '-- None --' && deserializeFlowEntityMap.get(fieldId).get('FieldValue') != '- None -' && deserializeFlowEntityMap.get(fieldId).get('FieldValue') != '-None-' && deserializeFlowEntityMap.get(fieldId).get('FieldValue') != 'None') || deserializeFlowEntityMap.get(fieldId).get('ControlledField') == 'Vehicle_Make__c'){
                    Schema.DisplayType fldType = objectFieldType.get('Case').get(deserializeFlowEntityMap.get(fieldId).get('ControlledField').toLowerCase());
                    switch on fldType {
                        when Boolean {
                            try{
                                autoSaveCase.put(deserializeFlowEntityMap.get(fieldId).get('ControlledField'), Boolean.valueOf(deserializeFlowEntityMap.get(fieldId).get('FieldValue')));
                            }
                            catch(Exception e){
                                System.debug(e);
                            }
                        }
                        when Double {
                            try{
                                autoSaveCase.put(deserializeFlowEntityMap.get(fieldId).get('ControlledField'), Double.valueOf(deserializeFlowEntityMap.get(fieldId).get('FieldValue')));
                            }
                            catch(Exception e){
                                System.debug(e);
                            }
                        }
                        when else {
                            if(deserializeFlowEntityMap.get(fieldId).get('ControlledField') == 'Vehicle_Make__c' && (deserializeFlowEntityMap.get(fieldId).get('FieldValue') == null || deserializeFlowEntityMap.get(fieldId).get('FieldValue')== '' || deserializeFlowEntityMap.get(fieldId).get('FieldValue') == '--None--' || deserializeFlowEntityMap.get(fieldId).get('FieldValue') == '-- None --' || deserializeFlowEntityMap.get(fieldId).get('FieldValue') == '- None -' || deserializeFlowEntityMap.get(fieldId).get('FieldValue') == '-None-' )){
                               if(defaultMake != ''){
                                    autoSaveCase.put(deserializeFlowEntityMap.get(fieldId).get('ControlledField'), String.valueOf(defaultMake));
                               }
                            } else if(deserializeFlowEntityMap.get(fieldId).get('ControlledField') == 'Vehicle__c'){
                                if(vehicleId != null && vehicleId!= '' && vehicleId!= 'null'){
                                    autoSaveCase.put(deserializeFlowEntityMap.get(fieldId).get('ControlledField'), String.valueOf(vehicleId));
                                    autoSaveCase.put('VIN_Member_ID__c', String.valueOf(deserializeFlowEntityMap.get(fieldId).get('FieldValue')));
                                } else {
                                    autoSaveCase.put('VIN_Member_ID__c', String.valueOf(deserializeFlowEntityMap.get(fieldId).get('FieldValue')));
                                }
                            } else if(deserializeFlowEntityMap.get(fieldId).get('ControlledField') == 'Contact_Policy__c' && (vehicleId == null || vehicleId == '' || vehicleId == 'null')){
                                    autoSaveCase.put('Contact_Policy__c', '');
                            }else if(deserializeFlowEntityMap.get(fieldId).get('ControlledField') =='Is_your_vehicle_an_electric_vehicle__c'|| deserializeFlowEntityMap.get(fieldId).get('ControlledField') =='Would_you_like_to_use_LBS__c' || deserializeFlowEntityMap.get(fieldId).get('ControlledField') =='Tow_Exchange__c'){ 
                                    autoSaveCase.put(deserializeFlowEntityMap.get(fieldId).get('ControlledField'), Boolean.valueOf(deserializeFlowEntityMap.get(fieldId).get('FieldValue')) ? 'Yes' : 'No');
                            } else {
                                autoSaveCase.put(deserializeFlowEntityMap.get(fieldId).get('ControlledField'), String.valueOf(deserializeFlowEntityMap.get(fieldId).get('FieldValue')));
                            }

                            //System.debug('[AP] [updateExistingRecord] currentValue = ' + currentValue);
                            //autoSaveCase.put(currentField.ControlledField, String.valueOf(currentField.FieldValue));
                        }
                    }

            }
            
            
         }

        if(businessPhoneExtention !='' && businessPhoneExtention != null && businessPhoneExtention != 'null'){
            autoSaveCase.Phone__c = autoSaveCase.Phone__c + businessPhoneExtention;
        }
        if (autoSaveCase.Trouble_Code__c != settings.Non_Dispatch_Call_Types__c && (autoSaveCase.Tow_Reason__c != settings.Call_Flow_Vehicle_Defect__c || autoSaveCase.Tow_Reason__c != settings.Call_Flow_Vehicle_Recovery__c)) {
            autoSaveCase.Miscellaneous_Call_Reason__c = autoSaveCase.Tow_Reason__c;
        }
        if(autoSaveCase.Breakdown_Location__Longitude__s != null){
            autoSaveCase.Breakdown_Location__Longitude__s = (autoSaveCase.Breakdown_Location__Longitude__s != null && autoSaveCase.Breakdown_Location__Longitude__s != 0) ? (autoSaveCase.Breakdown_Location__Longitude__s).setScale(7) : autoSaveCase.Breakdown_Location__Longitude__s;     
        }
        if(autoSaveCase.Breakdown_Location__Latitude__s != null){
            autoSaveCase.Breakdown_Location__Latitude__s= (autoSaveCase.Breakdown_Location__Latitude__s!=null && autoSaveCase.Breakdown_Location__Latitude__s!= 0) ? (autoSaveCase.Breakdown_Location__Latitude__s).setScale(7) : autoSaveCase.Breakdown_Location__Latitude__s;
        }
        if(currentProgramId != '' && currentProgramId != null && currentProgramId != 'null'){
            autoSaveCase.Program__c = currentProgramId;
        }
            

        autoSaveCase.Auto_save__c = true;    


        String errorTryCatch = '';
        if(currentCaseId != null && currentCaseId != '' && currentCaseId!= 'null'){
            autoSaveCase.Id = currentCaseId;
        }
         
        upsert autoSaveCase;

           
        
        System.debug(autoSaveCase);
        if(autoSaveCase.Id != null){
            return autoSaveCase.Id;
        }else{
            return '';
        }
    }
}