@isTest
public class Test_ClaimsPortal {
    /*@testSetup 
    static void setup() {
        List<Group> grouplist = new List<Group>();
        List<QueuesObject> queuelist = new List<QueuesObject>();
        Group g1 = new Group(Name='Claims_Verification_Queue', type='Queue');
        Group g2 = new Group(Name='AccountingTeam_Claim', type='Queue');
        grouplist.add(g1);
        grouplist.add(g2);
        upsert grouplist;
        
        QueuesObject q1 = new QueueSObject(QueueID = g1.id, SobjectType = 'Expense_Claim__c');
        QueuesObject q2 = new QueueSObject(QueueID = g2.id, SobjectType = 'Expense_Claim__c');
        queuelist.add(q1);
        queuelist.add(q2);
        upsert queuelist;
        
    }*/
    @testSetup 
    static void setup1() {
        Id rtAccount = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer' LIMIT 1].Id;
        Id rtClaim = [SELECT Id FROM RecordType WHERE SobjectType = 'Expense_Claim__c' AND DeveloperName = 'Club_Auto_Claims' LIMIT 1].Id;
        
        Account acc = new Account();
        acc.name = 'TestAccount1';
        acc.recordtypeId = rtAccount;
        acc.type = 'Customer';
        insert acc;
        
        Contact con = new Contact();
        con.firstname = 'TestContact1';
        con.lastname = 'TestContact1';
        con.accountid = acc.Id;
        con.phone = '1111112222';
        con.Status__c = 'Active';
        con.email = 'TestContact1@gmail.com';
        insert con;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Club Auto Customer Community Plus Login User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com', contactid = con.Id);
        insert u;
        
        Expense_Claim__c claim = new Expense_Claim__c();
        claim.recordtypeId = rtClaim;
        claim.Claimant__c = con.Id;
        claim.Claim_Status__c = 'Submitted';
        claim.Claims_Type__c = 'Own service expense';
        claim.VIN_Member_ID__c = '1ABCD23456E789012';
        claim.Claimants_First_Name__c = 'FN';
        claim.Claimants_Last_Name__c = 'LN';
        claim.Claimants_Phone__c = '2342342343';
        claim.Claimant_Postal_Code__c = 'M2M0A9';
        claim.Claimant_Province__c = 'ON';
        claim.Claimants_Street__c = 'Asleton blvd';
        claim.Claimants_Email__c = 'sd@sd.com';
        claim.Claimant_City__c = 'Toronto';
        claim.Claimant_Country__c = 'CA';
        claim.Claimant_Apartment_No__c = '324';
        claim.Cheque_Payee_Diff_from_Claimant__c = true;
        claim.Payee_First_Name__c = 'FN';
        claim.Payee_Last_Name__c = 'LN';
        claim.Payee_Phone__c = '2342342343';
        claim.Payee_Postal_Code__c = 'M2M0A9';
        claim.Payee_Province__c = 'ON';
        claim.Payee_Street__c = 'Asleton blvd';
        claim.Payee_s_Email__c = 'sd@sd.com';
        claim.Payee_City__c = 'Toronto';
        claim.Payee_Country__c = 'CA';
        claim.Payee_Apartment_No__c = '324';
        claim.Payment_Method__c = 'Cheque';
        claim.Service_Type__c = 'Tow';
        claim.Service_Date__c = Date.Today();
        claim.Accommodation__c = 100;
        claim.Alternative_Transportation__c = 100;
        claim.Total_Amount_Paid__c = 100;
        claim.Base_Amount_Paid__c = 80;
        claim.Expense_Currency__c = 'USD';
        claim.GST__c = 100;
        claim.HST__c = 100;
        claim.Meals__c = 80;
        claim.Other_Expenses__c = 80;
        claim.QST__c = 80;
        claim.Total_Paid__c = 30;
        claim.VIN_Available__c = true;
        insert claim;
        
        ContentVersion CV = new ContentVersion(
          Title = 'Penguins',
          PathOnClient = 'Penguins.jpg',
          VersionData = Blob.valueOf('Test Content'),
          IsMajorVersion = true
        );
        insert CV;
        
        ContentVersion CV1 = new ContentVersion(
          Title = 'Penguins',
          PathOnClient = 'Penguins.jpg',
          VersionData = Blob.valueOf('Test Content'),
          IsMajorVersion = true
        );
        insert CV1;
        
        ContentVersion CVQuery = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :CV.Id LIMIT 1];
        ContentVersion CVQuery1 = [SELECT Id, Title, ContentDocumentId FROM ContentVersion WHERE Id = :CV1.Id LIMIT 1];
        
        ContentDocumentLink CDLink = new ContentDocumentLink();
        CDLink.ContentDocumentId = CVQuery.ContentDocumentId;
        CDLink.LinkedEntityId = claim.Id;
        CDLink.ShareType = 'V';
        insert CDLink;
        
        
        ClubAuto_Consent_Attributes__c CA = new ClubAuto_Consent_Attributes__c();
        CA.Consent_Type__c = 'Club Auto Claims Portal';
        CA.Consent_SubType__c = 'Claim Submission';
        CA.Consent_Message__c = 'I hereby certify that the above statements are true and correct to the best of my knowledge. Incomplete or incorrect information may result delayed or rejected claims.';
        insert CA;
        
        ContentDocumentLink CDLink1 = new ContentDocumentLink();
        CDLink1.ContentDocumentId = CVQuery1.ContentDocumentId;
        CDLink1.LinkedEntityId = claim.Id;
        CDLink1.ShareType = 'V';
        insert CDLink1;
        
        claim.Claim_Status__c = 'Approved';
        update claim;
    }
    static testMethod void ClubAuto_LtngSelfRegisterControllerTest() {
        Test.startTest();
            Expense_Claim__c claim = [select id from Expense_Claim__c limit 1];
            ContentDocument CD = [Select id from ContentDocument limit 1];
            ClubAuto_LtngSelfRegisterController.isValidPassword('Welcome01','Welcome01');
            ClubAuto_LtngSelfRegisterController.setExperienceId('ExpId');
            try{
            ClubAuto_LtngSelfRegisterController.siteAsContainerEnabled('CommunityURL');
            }catch(Exception ex){
            }
            ClubAuto_LtngSelfRegisterController cs = new ClubAuto_LtngSelfRegisterController();
            ClubAuto_LtngSelfRegisterController.selfRegister('FirstName','LastName','abc@abc.com','Welcome01','Welcome01',true,'en_US');
      ClubAuto_LtngClaimsSubController.getselectOptions('Claims_Type__c');
            ClubAuto_LtngClaimsSubController.instPage('Club Auto Claims Portal','Claim Submission',claim.Id);
            ClubAuto_LtngClaimsSubController.instPage('TestA','TestA',claim.Id);
      ClubAuto_LtngClaimsSubController.instPage('TestA','TestA',null);
            ClubAuto_LtngClaimsSubController.retrieveDoc(claim.Id);
            ClubAuto_LtngClaimsSubController.updAttachment(CD);
            try{
                throw new TriggerException('Test the coverage');
            }catch(TriggerException ex){
            }
            Claims_CVOwnerAssignment.Claims_AssignOwnerCV(new List<String>{claim.Id});
            ClaimsOwnerAssignment.assignSelf(claim.Id);
            ClubAuto_LtngClaimsSubController.deleteAttachment(CD.Id, claim.Id);
            ClubAuto_LtngClaimsSubController.subitClaim(JSON.Serialize(claim), true, 'Consent Provided', claim.Id, 'Screen1', 'New');
            ClubAuto_LtngClaimsSubController.getDependentOptions('Claimant_Province__c', 'Claimant_Country__c', 'CA');
        Test.stopTest();
    }
}