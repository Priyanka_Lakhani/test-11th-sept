/*
Created Date: Jan 17 2019
Last Modified Date: Jan 17 2019
Last Modified By: Thamizh
Description: Helper Class for Flow Entity Trigger
*/
Public without sharing Class FE_TriggerHelper{

    public static Boolean isFirstTime = true;

    Public Static Void FE_DeleteFlowChildRecords(Set<Id> FEIds){
        system.debug('***FEIds***'+FEIds);
        List<Id> feIdLst = new List<Id>(); //Assigns the Id of the parent flow record ids whose children are to be deleted
        Map<Id, Flow_Entity__c> delFERecsMap = new Map<Id, Flow_Entity__c>();
        List<Flow_Entity_Dependency__c> delFEDRecslst = new List<Flow_Entity_Dependency__c>();
        try{
            if(FEIds != null && FEIds.size() > 0){
                feIdLst.addAll(FEIds);
            }
            if(feIdLst.size() > 0){
                for(Flow_Entity__c FE:[Select id, Parent_Flow__c, Parent_Entity__c from Flow_Entity__c where Parent_Flow__c IN:feIdLst]){
                    delFERecsMap.put(FE.Id, FE);
                }
            }
            if(delFERecsMap.size() > 0){
                for(Flow_Entity_Dependency__c FED:[Select id, Dependency_Parent__c, Dependency_Child__c from Flow_Entity_Dependency__c where Dependency_Parent__c in:delFERecsMap.keyset() OR Dependency_Child__c in:delFERecsMap.keyset() OR Marker_Dependency_Parent__c in:delFERecsMap.keyset() OR Marker_Dependency_Child__c in:delFERecsMap.keyset()]){
                    delFEDRecslst.add(FED);
                }
            }
            if(delFERecsMap.size() > 0){
                delete delFERecsMap.values();
            }
            if(delFEDRecslst.size() > 0){
                delete delFEDRecslst;
            }
        }catch(Exception ex){
        
        }
    }
    
    Public Static Void FE_DeleteEntityChildRecords(Set<Id> FEIds){
        system.debug('***FEIds***'+FEIds);
        List<Id> feIdLst = new List<Id>(); //Assigns the Id of the parent flow record ids whose children are to be deleted
        List<Id> tmpfeIdLst = new List<Id>(); //Assigns the Id of the parent flow record ids whose children are to be deleted
        List<Id> extfeIdLst = new List<Id>(); //Assigns the Id of the parent flow record ids whose children are to be deleted
        Map<Id, Flow_Entity__c> delFERecsMap = new Map<Id, Flow_Entity__c>();
        List<Flow_Entity_Dependency__c> delFEDRecslst = new List<Flow_Entity_Dependency__c>();
        try{
            if(FEIds != null && FEIds.size() > 0){
                feIdLst.addAll(FEIds);
            }
            if(feIdLst.size() > 0){
                tmpfeIdLst.addAll(feIdLst);
                while (tmpfeIdLst.size() > 0){
                
                    for(Flow_Entity__c FE:[
                    Select id, Parent_Flow__c, 
                        Parent_Entity__c, 
                        Parent_Entity__r.Parent_Entity__c, 
                        Parent_Entity__r.Parent_Entity__r.Parent_Entity__c, 
                        Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c, 
                        Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c,
                        Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c
                    from Flow_Entity__c where 
                        Parent_Entity__c IN:tmpfeIdLst 
                        OR Parent_Entity__r.Parent_Entity__c IN:tmpfeIdLst 
                        OR Parent_Entity__r.Parent_Entity__r.Parent_Entity__c IN:tmpfeIdLst 
                        OR Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c IN:tmpfeIdLst
                        OR Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c IN:tmpfeIdLst
                        OR Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c IN:tmpfeIdLst
                    ])
                    {
                        delFERecsMap.put(FE.Id, FE);
                        feIdLst.add(FE.Id);
                        If(FE.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__r.Parent_Entity__c != null){
                            extfeIdLst.add(FE.Id);
                        }
                    }
                    
                    if(extfeIdLst.size() > 0){
                        tmpfeIdLst = new List<Id>();
                        tmpfeIdLst.addAll(extfeIdLst);
                        extfeIdLst = new List<Id>();
                    }else{
                        tmpfeIdLst = new List<Id>();
                    }
                    
                }
            }
            if(feIdLst.size() > 0){
                for(Flow_Entity_Dependency__c FED:[Select id, Dependency_Parent__c, Dependency_Child__c from Flow_Entity_Dependency__c where Dependency_Parent__c in:feIdLst OR Dependency_Child__c in:feIdLst OR Marker_Dependency_Parent__c in:feIdLst OR Marker_Dependency_Child__c in:feIdLst]){
                    delFEDRecslst.add(FED);
                }
            }
            system.debug('***delFERecsMap***'+delFERecsMap.size());
            system.debug('***delFEDRecslst***'+delFEDRecslst.size());
            if(delFEDRecslst.size() > 0){
                delete delFEDRecslst;
            }
            if(delFERecsMap.size() > 0){
                delete delFERecsMap.values();
            }
        }catch(Exception ex){
            system.debug('**EXCEPTION***'+ex.getMessage());
        }
    }

}