@isTest
private class TEST_sendCaseDataAfterInsertUpdate{
    static Account club {get;set;}
    static Account client {get;set;}
    static Account client2 {get;set;}
    static Account tow {get;set;}
    static Contact cont {get;set;}
    static Territory_Coverage__c terr {get;set;}
    static Program__c prog {get;set;}
    static Program__c prog2 {get;set;}
    static VIN__c vin {get;set;}
    static VIN__c vin2 {get;set;}
    static CA__c settings {get;set;}
    static String accountFields='';
    static String programFields='';
    static String vinFields='';
    static String terrFields='';
    @testSetup 
    static void setup() {
        CA__c settings = new CA__c(
            Call_Flow_Vehicle_Year_Start__c = 1920,
            Non_Dispatch_Call_Types__c = 'Miscellaneous (Non Dispatch)',
            Last_Case_Number__c = '00000002',
            Call_Flow_Priority_Required__c = 'Unlock',
            Call_Flow_Always_Hazardous__c = 'Anywhere On Highway,Highway Shoulder,On/Off Ramp,Ditch,Car Wash,N\'importe où sur l\'autoroute,Bord de l\'autoroute,Entrée/Sortie de l\'autoroute,Fossé,Lave-auto,',
            Call_Flow_Potentially_Hazardous__c = 'Alley,Side of Road,Parking Lot,Gas Station/At the Pump,"In the back"/Courtyard,Ruelle,Bord de la rue,Stationnement,Station d\'essence/a la pompe d\'essence,A l\'arrière/arrière-cours,Leak / Smoke / Noise, Tow to Safety,',
            Call_Flow_Fire_Hazard__c = 'Leak / Smoke / Noise',CL_Status__c = 'Closed (Call Cleared)', XX_Status__c = 'Call Cancelled', DI_Status__c = 'Dispatched', RE_Status__c = 'Received', SP_Status__c = 'Spotted',
            Location_Codes_French__c = 'Accotement Droit,Accotement Gauche,Collectrice - Accotement Droit,Collectrice - Accotement Gauche,Express- Accotement Droit,Express - Accotement Gauche,Bord de la Rue,Banc de Neige,Entrée,Fossé,Garage,Lave Auto,Milieu de la Rue,Pompe Essence,Rampe',
            Location_Codes_English__c = 'Airport Terminal,Alley,Car Port,Car Wash,Driveway,Ditch,Collectors Right Shoulder,Collectors Left Shoulder,Express Right Shoulder,Express Left Shoulder,HWY Right Shoulder,HWY Left Shoulder,Garage,Gas Pump,Middle of the Road,Open Parking Lot,Ramp',
            Location_Codes_French_Ext__c = 'Ruelle,Stationement Ouvert,Tempo,Terminal Aéroport,Voie de Service,Stationnement,Station d\'essence/a la pompe d\'essence,A l\'arrière/arrière-cours,N\'importe où sur l\'autoroute,Bord de l\'autoroute,Entrée/Sortie de l\'autoroute,Souterrain,Maison',
            Location_Codes_English_Ext__c = 'Side of the Road,Service Road,Snowbank,Underground/Parkade,Gas Station/At the Pump,Anywhere On Highway,Highway Shoulder,Airport',
            Call_Flow_Personal_Update_Request__c = 'Personal Information Update Requests',
            Call_Flow_Vehicle_Defect__c = 'Vehicle Defect Claim',
            T6F__c = 'Flatbed Tow', T8__c= 'Accident Tow',T1__c = 'Install Spare',
            Call_Flow_Tow_Reason_Required__c = 'Tow,Flatbed Tow,Heavy Duty Tow',
            EHILostKeyEmail__c = 'abc@mail.com,xyz@mail.com',
            EHITowExchangeEmail__c = 'abc@mail.com,xyz@mail.com',
            EHIDriveInExchangeEmail__c = 'abc@mail.com,xyz@mail.com',
            API_User_IDs__c='asd',
            Light_Services_MT__c ='Gas,Install Spare,Boost,Unlock',
            Google_Key__c ='gme-clubautoroadside',
            Google_Private_Key__c ='VzYkgzw6H0qJYuvLhqb-s7P64LI='        
        );
        insert settings;
        
        ClientServicesSetting__c csSettings1 = new ClientServicesSetting__c(Name='mercedes-benz',EndPoint_URL__c='test.com',UserName__c='test',Password__c='test');
        insert csSettings1;
        ClientServicesSetting__c csSettings2 = new ClientServicesSetting__c(Name='JLR',EndPoint_URL__c='test.com',UserName__c='test',Password__c='test',Is_Trigger_On_SendData_To_MiddleTier__c=true);
        insert csSettings2;
        
        List<Account> accList = new List<Account>();
        List<Program__c> progList = new List<Program__c>();
        List<VIN__c> vinList = new List<VIN__c>();
        
        Account club = new Account(Name='club CAA', Account_Number__c='999000',ETA__c= 60, RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        accList.add(club);
        Account client = new Account(
            Name='Mercedes-Benz', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='960'
        );
        accList.add(client);
        Account client2 = new Account(
            Name='JLR', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='858'
        );
        accList.add(client2);
        Account tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Account_Number__c='960-123',
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        accList.add(tow);
        insert accList;
        
        Contact cont = new Contact(LastName='Test',Email='abc@mail.com',Contact_External__c='DevelopmentContact_DoNotDelete007');
        insert cont;
        
        Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Florida',
            Province_Code__c='FL'
        );
        insert terr;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='960',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            Exclude_Phone_From_History_Check__c=false,
            sponsorid__c= '000001',
            programid__c = '000012'
        );
        progList.add(prog);
        Program__c prog2 = new Program__c(
            Account__c=client2.Id,
            Program_Code__c='858',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            Exclude_Phone_From_History_Check__c=false,
            call_flow_JLR__c = true,
            sponsorid__c= '000001',
            programid__c = '000012'
        );
        progList.add(prog2); 
        insert progList;
        
        VIN__c vin = new VIN__c(
            Name='VINxxxxxxxxxxxxxx1',
            UID__c='VINxxxxxxxxxxxxxx1',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        vinList.add(vin);
        VIN__c vin2 = new VIN__c(
            Name='VINxxxxxxxxxxxxxx2',
            UID__c='VINxxxxxxxxxxxxxx2',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog2.Id
        );
        vinList.add(vin2);
        insert vinList;
    }
    static void Utility()
    {
        accountFields='';
        for(Schema.SObjectField f : Account.SObjectType.getDescribe().fields.getMap().values()) {
            accountFields+= (f.getDescribe().getName() + ',');
        }
        accountFields=accountFields.left(accountFields.length()-1);
        
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);
        
        vinFields='';
        for(Schema.SObjectField f : VIN__c.SObjectType.getDescribe().fields.getMap().values()) {
            vinFields += (f.getDescribe().getName() + ',');
        }
        vinFields=vinFields.left(vinFields.length()-1);
        
        terrFields='';
        for(Schema.SObjectField f : Territory_Coverage__c.SObjectType.getDescribe().fields.getMap().values()) {
            terrFields+= (f.getDescribe().getName() + ',');
        }
        terrFields=terrFields.left(terrFields.length()-1);
        
        String query1 = 'Select ' + accountFields+ '  FROM Account WHERE Account_number__c = \'999000\' LIMIT 1';
        club = database.query(query1);

        String query2 = 'Select ' + accountFields+ '  FROM Account WHERE Account_number__c = \'960\' LIMIT 1';
        client = database.query(query2);
        
        String query3 = 'Select ' + accountFields+ '  FROM Account WHERE Account_number__c = \'858\' LIMIT 1';
        client2 = database.query(query3);

        String query4 = 'Select ' + accountFields+ '  FROM Account WHERE Account_number__c = \'960-123\' LIMIT 1';
        tow = database.query(query4);
        
        String query5 = 'Select ' + programFields+ ' FROM Program__c WHERE Program_Code__c = \'960\' LIMIT 1';
        prog = database.query(query5);   
           
        String query6 = 'Select ' + programFields+ ' FROM Program__c WHERE Program_Code__c = \'858\' LIMIT 1';
        prog2 = database.query(query6);      
        
        String query7 = 'Select ' + vinFields+ ' FROM VIN__c WHERE Name = \'VINxxxxxxxxxxxxxx1\' LIMIT 1';
        vin = database.query(query7);
        
        String query8 = 'Select ' + vinFields+ ' FROM VIN__c WHERE Name = \'VINxxxxxxxxxxxxxx2\' LIMIT 1';
        vin2 = database.query(query8);
        
        String query9 = 'Select ' + terrFields+ ' FROM Territory_Coverage__c WHERE Province_Code__c=\'FL\' LIMIT 1';
        terr = database.query(query9);
        
        settings = CA__c.getOrgDefaults();

    }
    static testMethod void myUnitTest1() {
        Utility();

        GlobalClass.firstRun = true;
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c='abc',
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Call_ID__c='ABC4',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c ='Lost Keys',
            Miscellaneous_Call_Reason__c ='Lost Keys',
            Tow_Exchange__c ='Yes',
            Street__c='1000 AAA dr',
            PickupStreet__c = 'test',
            Destination_Name__c = 'test',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            Exchange_Pickup_Location_Email__c ='abcxyz@gmail.com;test1@mail.com',
            clientcasenumber__c = '1111111-abc',
            Program__c = prog.id,
            First_Name__c= 'priyanka',
            Last_Name__c= 'lakhani'
        );
        insert c;
        GlobalClass.firstRun = true;
        c.club__c = club.id;
        update c;
        GlobalClass.firstRun = true;
        c.Status =settings.ER_Status__c;
        update c;
    }
    static testMethod void myUnitTest2() {
        Utility();

        GlobalClass.firstRun = true;
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Call_ID__c='ABC5',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c ='Lost Keys',
            Miscellaneous_Call_Reason__c ='Lost Keys',
            Tow_Exchange__c ='Yes',
            Street__c='1000 AAA dr',
            PickupStreet__c = 'test',
            Destination_Name__c = 'test',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            Exchange_Pickup_Location_Email__c ='abcxyz@gmail.com;test1@mail.com',
            clientcasenumber__c = '1111111-abc',
            Program__c = prog.id,
            Club__c = club.id,
            First_Name__c= 'priyanka',
            Last_Name__c= 'lakhani'
        );
        insert c;

        GlobalClass.firstRun = true;
        c.Status =settings.CL_Status__c;
        update c;
    }
    static testMethod void myUnitTest3() {
        Utility();
    
        GlobalClass.firstRun = true;
        Case c = new Case(
            Type= 'Roadside Assistance',
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin2.Name,
            Interaction_ID__c='INTERACTION',
            Call_ID__c='ABC5',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c ='Lost Keys',
            Miscellaneous_Call_Reason__c ='Lost Keys',
            Tow_Exchange__c ='Yes',
            Street__c='1000 AAA dr',
            PickupStreet__c = 'test',
            Destination_Name__c = 'test',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            Exchange_Pickup_Location_Email__c ='abcxyz@gmail.com;test1@mail.com',
            clientcasenumber__c = '1111111-abc',
            Program__c = prog2.id,
            Club__c = club.id,
            First_Name__c= 'priyanka',
            Last_Name__c= 'lakhani'
        );
        insert c;

        GlobalClass.firstRun = true;
        c.Status =settings.DI_Status__c;
        update c;
        RestCallouts.JLRCallout('http://testurl.com','test','test123','jsonString',c.Id,'POST');
    }
}