global class JLR_CloseTheLoopBatchable implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Schedulable
{   
    global Database.QueryLocator start(Database.BatchableContext bc) 
    {   
        String query = ' SELECT Id, Name, CreatedDate, ETA__c, Request_Info__c, Request_Has_Been_Sent__c, ' +
                                ' Case__r.CaseNumber, Case__r.Phone__c, Case__r.Client_Code__c,ETA_total__c, ' +
                                ' Case__r.Vehicle_Make__c, Case__r.Language__c, Case__r.Close_the_loop_option__c ' +
                         ' FROM Close_the_loop__c ' +
                         ' WHERE ETA_total__c != 0 AND ' +
                               ' ETA_total__c != null AND ' +
                               ' Close_the_loop_Parent__c = null AND ' +
                               ' Response__c = null AND ' +
                               ' Request_Has_Been_Sent__c = false AND ' +
                               ' Case__r.Close_the_loop_option__c = \'Yes\' ' +
                         ' ORDER BY CreatedDate DESC ';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<SObject> batch) 
    {
    
        List<Close_the_loop__c> closeLoops = new List<Close_the_loop__c>();
        
        for(Close_the_loop__c cllp : (List<Close_the_loop__c>)batch) {
            
            if(cllp.CreatedDate.addMinutes(Integer.ValueOf(cllp.ETA_total__c)) <= system.now()){
            
                cllp.Request_Has_Been_Sent__c = true;
                cllp.Request_Info__c = JLR_CloseTheLoop.sendCaseToNuvaxx(cllp);
                closeLoops.add(cllp); 
                
            }
            
        }
        
        update closeLoops;
        
    }
    
    global void execute(SchedulableContext sc) {
        JLR_CloseTheLoopBatchable b = new JLR_CloseTheLoopBatchable(); 
        database.executebatch(b,20);
        system.abortJob(sc.getTriggerId());
    }
    
    global void finish(Database.BatchableContext bc) 
    {        
        
        datetime nextScheduleTime = system.now().addMinutes(1);
        string minute = string.valueof(nextScheduleTime.minute());
        string second = string.valueof(nextScheduleTime.second ());
        string cronvalue = second+' '+minute+' * * * ?' ;
        string jobName = 'JLR_CloseTheLoopBatchable ' + nextScheduleTime.format('hh:mm');
 
        JLR_CloseTheLoopBatchable p = new JLR_CloseTheLoopBatchable();
        if(!Test.isRunningTest()){
            system.schedule(jobName, cronvalue , p);
        }
    
    }  
}