global class BatchUpdateVIN implements Database.Batchable<SObject>, Database.Stateful {

    private Id jobId;
    private String query;
    global BatchUpdateVIN(String query) {
        this.query = query;
        if (this.query == null) {
            this.query = 'SELECT Id, PIN__c, Year__c, Make__c, Model__c, License__c, Plan_Name__c, First_Name__c, Last_Name__c, Personal_First_Name__c, Personal_Last_Name__c, Name, Program__r.Call_Flow_Autocomplete_Type__c FROM VIN__c ORDER BY Program__c ASC';
        }
    }

    global Database.QueryLocator start(Database.BatchableContext bc) {
        try {
            jobId = bc.getJobID();
        } catch (Exception e) {
            jobId = '0';
        }
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext bc, List<SObject> batch) {
        Database.update(refreshVINs((List<VIN__c>) batch).values());
    }
    public static Map<Id,VIN__c> refreshVINs(List<VIN__c> v) {
        System.debug('REFRESHING VINS: ' + v);
        Map<Id,VIN__c> vMap = new Map<Id,VIN__c>();
        for (VIN__c c : v) {
            String f_name = c.Personal_First_Name__c!=NULL?c.Personal_First_Name__c:c.First_Name__c;
            String l_name = c.Personal_Last_Name__c!=NULL?c.Personal_Last_Name__c:c.Last_Name__c;
            String vin_person = (c.Name + ((f_name!=NULL&&f_name!='')?(' ' + f_name):'') + ((l_name!=NULL&&l_name!='')?(' ' + l_name):''));
            
            if(c.Program__r.Call_Flow_Autocomplete_Type__c!=NULL) {
                if(c.Program__r.Call_Flow_Autocomplete_Type__c.containsIgnoreCase('PIN__c') && c.PIN__c!=NULL) {
                    vin_person += (' ['+c.PIN__c+']');
                }
                if(c.Program__r.Call_Flow_Autocomplete_Type__c.containsIgnoreCase('License__c') && c.License__c!=NULL) {
                    vin_person += (' ['+c.License__c+']');
                }
                if(c.Program__r.Call_Flow_Autocomplete_Type__c.containsIgnoreCase('Plan_Name__c') && c.Plan_Name__c!=NULL) {
                    vin_person += (' ['+c.Plan_Name__c+']');
                }
            }
            
            vMap.put(c.Id, new VIN__c(
                Id=c.Id,
                VIN_Person__c = vin_person,
                Car__c = ((c.Year__c!=NULL&&c.Year__c!=''?c.Year__c:'') + (c.Make__c!=NULL&&c.Make__c!=''?(' ' + c.Make__c):'') + (c.Model__c!=NULL&&c.Model__c!=''?(' ' + c.Model__c):'')),
                Person__c = ((f_name!=NULL&&f_name!=''?f_name:'') + (l_name!=NULL&&l_name!=''?(' ' + l_name):''))
            ));
        }

        return vMap;
    }
    global void finish(Database.BatchableContext bc) { }
    
}