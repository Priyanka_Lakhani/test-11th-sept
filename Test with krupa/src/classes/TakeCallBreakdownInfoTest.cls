@isTest
private class TakeCallBreakdownInfoTest {
	
	@testSetup static void setup() {
		 CA__c settings = new CA__c(
            Call_Flow_Vehicle_Year_Start__c = 1920,
            Non_Dispatch_Call_Types__c = 'Miscellaneous (Non Dispatch)',
            Call_Flow_Personal_Update_Request__c = 'Service Request Cancellations',
            Last_Case_Number__c = '00000002',
            Call_Flow_Tow_Reason_Include__c = 'FlatBed Tow',
            Call_Flow_Priority_Required__c = 'Unlock',
            Call_Flow_Always_Hazardous__c = 'Anywhere On Highway,Highway Shoulder,On/Off Ramp,Ditch,Car Wash,N\'importe où sur l\'autoroute,Bord de l\'autoroute,Entrée/Sortie de l\'autoroute,Fossé,Lave-auto,',
            Call_Flow_Potentially_Hazardous__c = 'Alley,Side of Road,Parking Lot,Gas Station/At the Pump,"In the back"/Courtyard,Ruelle,Bord de la rue,Stationnement,Station d\'essence/a la pompe d\'essence,A l\'arrière/arrière-cours,Leak / Smoke / Noise, Tow to Safety,',
            Call_Flow_Fire_Hazard__c = 'Leak / Smoke / Noise',
            Location_Codes_French__c = 'Accotement Droit,Accotement Gauche,Collectrice - Accotement Droit,Collectrice - Accotement Gauche,Express- Accotement Droit,Express - Accotement Gauche,Bord de la Rue,Banc de Neige,Entrée,Fossé,Garage,Lave Auto,Milieu de la Rue,Pompe Essence,Rampe',
            Location_Codes_English__c = 'Airport Terminal,Alley,Car Port,Car Wash,Driveway,Ditch,Collectors Right Shoulder,Collectors Left Shoulder,Express Right Shoulder,Express Left Shoulder,HWY Right Shoulder,HWY Left Shoulder,Garage,Gas Pump,Middle of the Road,Open Parking Lot,Ramp',
            Location_Codes_French_Ext__c = 'Ruelle,Stationement Ouvert,Tempo,Terminal Aéroport,Voie de Service,Stationnement,Station dessence/a la pompe dessence,A larrière/arrière-cours,Nimporte où sur lautoroute,Bord de lautoroute,Entrée/Sortie de lautoroute,Souterrain,Maison',
            Location_Codes_English_Ext__c = 'Side of the Road,Service Road,Snowbank,Underground/Parkade,Gas Station/At the Pump,Anywhere On Highway,Highway Shoulder,Airport',
            Light_Services_MT__c = 'Install Spare,Gas,Boost,Unlock',
            Google_Key__c = 'test',
            Google_Private_Key__c = 'test',
            T6F__c = 'Flatbed Tow'
        );
        insert settings;
        Map<String, Id> recordTypeMap = new Map<String, Id>();
        List<RecordType> recordTypes = [SELECT Id, Developername FROM RecordType WHERE SobjectType='Account' OR SobjectType='Case' ];
        for(RecordType r : recordTypes){
        	recordTypeMap.put(r.Developername, r.Id);
        }
        
        List<Account> accList = new List<Account>();
        List<Program__c> progList = new List<Program__c>();
        List<VIN__c> vinList = new List<VIN__c>();
        List<Case> caseList = new List<Case>();
        
        Account mtclub = new Account(
            RecordTypeId = recordTypeMap.get('Dispatch_Club'),
            Name = 'JLR Mobile Technician',
            Account_Number__c = '998',
            Status__c = 'Active'
        );
        accList.add(mtclub);
        Account client560 = new Account(
            RecordTypeId = recordTypeMap.get('Client'),
            Name = 'Honda Canada Inc.',
            Account_Number__c = '560',
            Status__c = 'Active',
            Type = 'Customer'
        );
        Account tow560 = new Account(
            Name='Honda dealer', 
            RecordTypeId=recordTypeMap.get('Dealership'), 
            Client__c=client560.Id,
            Account_number__c='Honda560Dealer',
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE,
            Tow_Exchange_Email_Address__c = 'abc@clubautoltd.com',
            gpbr__c = 'aaazzz',
            status__c = 'Active'
        );
        accList.add(tow560);       
       
        Account client858 = new Account(
            RecordTypeId = recordTypeMap.get('Client'),
            Name = 'Jaguar Land Rover ULC',
            Account_Number__c = '858',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client858);
        
        Account Mt1 = new Account(
            Name='MT test1', 
            RecordTypeId=recordTypeMap.get('Mobile_Tech'), 
            Location__Longitude__s=45.59971620, 
            Location__Latitude__s=-73.78835360,
            Dispatch_Email__c= 'abc@clubautoltd.com',
            account_number__c = 'MT-J111PRI',
            milecoverage__c = 25,
            monday__c = '00:00-23:59',
            Tuesday__c='00:00-23:59',
            Wednesday__c='00:00-23:59',
            thursday__c='00:00-23:59',
            Friday__c='00:00-23:59',
            Saturday__c='00:00-23:59',
            Sunday__c='00:00-23:59',
            timezone__c= 'EST',
            status__c = 'Active',
            islightservicemt__c = true,
            Type = 'Mobile Tech'
        );
        accList.add(Mt1);
        
        Account Mt2 = new Account(
            Name='MT test2', 
            RecordTypeId=recordTypeMap.get('Mobile_Tech'), 
            Location__Longitude__s=45.59971620, 
            Location__Latitude__s=-73.78835360,
            Dispatch_Email__c= 'abc@clubautoltd.com',
            account_number__c = 'MT-J222PRI',
            SecondAccountNumber__c = 'MT-R222PRI',
            milecoverage__c = 25,
            monday__c = '00:00-23:59',
            Tuesday__c='00:00-23:59',
            Wednesday__c='00:00-23:59',
            thursday__c='00:00-23:59',
            Friday__c='00:00-23:59',
            Saturday__c='00:00-23:59',
            Sunday__c='00:00-23:59',
            timezone__c= 'EST',
            status__c = 'Active',
            islightservicemt__c = true,
            Type = 'Mobile Tech'
        );
        accList.add(Mt2);
        
        Account Ma1 = new Account(
            Name='MA test1', 
            RecordTypeId=recordTypeMap.get('Mobile_Assist'), 
            Location__Longitude__s=45.59971620, 
            Location__Latitude__s=-73.78835360,
            Dispatch_Email__c= 'abc@clubautoltd.com',
            account_number__c = 'MA-J111PRI',
            milecoverage__c = 25,
            monday__c = '00:00-23:59',
            Tuesday__c='00:00-23:59',
            Wednesday__c='00:00-23:59',
            thursday__c='00:00-23:59',
            Friday__c='00:00-23:59',
            Saturday__c='00:00-23:59',
            Sunday__c='00:00-23:59',
            timezone__c= 'EST',
            status__c = 'Active',
            islightservicemt__c = true,
            Type = 'Mobile Assist'
        );
        accList.add(Ma1);
                              
        Account client582 = new Account(
            RecordTypeId = recordTypeMap.get('Client'),
            Name = 'Garantie Nationale',
            Account_Number__c = '582',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client582);
      
        Account client569 = new Account(
            RecordTypeId = recordTypeMap.get('Client'),
            Name = 'Honda Motorcycle',
            Account_Number__c = '569',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client569);
        
        Account client599 = new Account(
            RecordTypeId = recordTypeMap.get('Client'),
            Name = 'Waze',
            Account_Number__c = '599',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client599);
        
        Account tow599 = new Account(
            Name='Waze dealer', 
            RecordTypeId=recordTypeMap.get('Dealership'), 
            Client__c=client599.Id,
            Account_number__c='Waze599Dealer',
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE,
            Tow_Exchange_Email_Address__c = 'abc@clubautoltd.com',
            gpbr__c = 'aaazzz',
            status__c = 'Active',
            BillingState='Ontario'
        );
        accList.add(tow599);
       
        Account limoCompany = new Account(
          Name = 'test',
          Phone= '111111111111',
          Website = 'test.test',
          Services__c = 'Limo'
          );
        accList.add(limoCompany);
        
        insert accList;
        
        Program__c prog560 = new Program__c(
            Account__c = client560.Id,
            Description__c = 'Base',
            Type__c = 'Underwritten',
            Status__c = 'Active',
            Name = '560 - Honda Canada Inc. [Base]',
            Program_Code__c = '560',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            CTI_Search_Terms__c = 'Honda',
            Client_Name_for_Club__c = 'Honda',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'Honda Canada Roadside Assistance',
            Client_Name_French__c = 'Honda Canada Roadside Assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2'
        );
        progList.add(prog560);
        
        Program__c prog858 = new Program__c(
            Account__c = client858.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = '858 - Jaguar US Base',
            Program_Code__c = '858',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T4_Limit__c = 3,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            T11__c = true,
            CTI_Search_Terms__c = 'JaguarUS,JagTele',
            Client_Name_for_Club__c = '',
            VIN_Help_Text__c = 'SAJ',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'JLR Roadside Assistance',
            Client_Name_French__c = 'JLR Roadside Assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'Reunite,Re-Acquired Vehicle',
            Call_Flow_JLR__c = true,
            Base_Warranty_Service_Limit_Term__c = 5
        );
        progList.add(prog858);
        
        Program__c prog582 = new Program__c(
            Account__c = client582.Id,
            Description__c = 'Base',
            Type__c = 'Underwritten',
            Status__c = 'Active',
            Name = '582 - Garantie Nationale [Base]',
            Program_Code__c = '582',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            CTI_Search_Terms__c = 'Garantie Nationale',
            Client_Name_for_Club__c = 'Garantie Nationale',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 25,
            Max_Search_Results_Extra__c = 35,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 25,
            Max_Tow_Distance_Absolute__c = 50,
            Client_Name_English__c = 'Garantie Nationale Roadside Assistance',
            Client_Name_French__c = 'Garantie Nationale Roadside Assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Call_Flow_EHI__c = true
        );
        progList.add(prog582);
        
        Program__c prog569 = new Program__c(
            Account__c = client569.Id,
            Description__c = 'Base',
            Type__c = 'Underwritten',
            Status__c = 'Active',
            Name = '569 - Honda Motorcycle [Base]',
            Program_Code__c = '569',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            CTI_Search_Terms__c = 'Honda Motorcyle',
            Client_Name_for_Club__c = 'Honda',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 25,
            Max_Search_Results_Extra__c = 35,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 25,
            Max_Tow_Distance_Absolute__c = 50,
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Call_Flow_EHI__c = true
        );
        progList.add(prog569);
        
        Program__c prog599 = new Program__c(
            Account__c = client599.Id,
            Description__c = 'Base',
            Type__c = 'Underwritten',
            Status__c = 'Active',
            Name = '599 - Waze Canada [Base]',
            Program_Code__c = '599',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            CTI_Search_Terms__c = 'Waze',
            Client_Name_for_Club__c = 'Waze',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 25,
            Max_Search_Results_Extra__c = 35,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 25,
            Max_Tow_Distance_Absolute__c = 50,
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Call_Flow_EHI__c = true
        );
        progList.add(prog599);
        
        insert progList;
        
        VIN__c vin560 = new VIN__c(
            Name = 'Honda560111111111',
            UID__c = 'Honda560111111111',
            Program__c = prog560.Id,
            Year__c = '2018',
            Make__c = 'HONDA',
            Model__c = 'ODYSSEY',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            EV__c = true,
            Extra_Coverage__c = 'OIL CHANGE'
        );
        vinList.add(vin560);
        
        VIN__c vin858 = new VIN__c(
            Name = 'JLR85811111111111',
            UID__c = 'JLR85811111111111',
            Program__c = prog858.Id,
            Year__c = '2018',
            Make__c = 'Jaguar',
            Model__c = '111',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            JLREngineeringVehicle__c = true
        );
        vinList.add(vin858);
        
        VIN__c vin582 = new VIN__c(
            Name = 'GN582111111111111',
            UID__c = 'GN582111111111111',
            Program__c = prog582.Id,
            Year__c = '2018',
            Make__c = 'NISSAN',
            Model__c = 'PATHFINDER',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123'
        );
        vinList.add(vin582);
        
        VIN__c vin569 = new VIN__c(
            Name = 'HM569111111111111',
            UID__c = 'HM569111111111111',
            Program__c = prog569.Id,
            Year__c = '2018',
            Make__c = 'M/C',
            Model__c = 'ON ROAD',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123'
        );
        vinList.add(vin569);
        
        VIN__c vin599 = new VIN__c(
            Name = 'Waze5991111111111',
            UID__c = 'Waze5991111111111',
            Program__c = prog599.Id,
            Year__c = '2018',
            Make__c = 'M/C',
            Model__c = 'ON ROAD',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123'
        );
        vinList.add(vin599);
        
        insert vinList;
        
        Case existCase560 = new Case(
            Program__c = prog560.Id,
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin560.Id,
            VIN_Member_ID__c = 'Honda560111111111',
            Trouble_Code__c = 'Flatbed Tow',
            Tow_Reason__c = 'Engine Stalled',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            DealerShip__c= tow560.Id,
            Exchange_Pickup_Location__c= 'aaazzz',
            GPBR__c = 'aaazzz',
            Location_Code__c = 'Driveway',
            Phone__c='1234567890',
            Phone_type__c='Cellular',
            Is_your_vehicle_an_electric_vehicle__c = 'Yes',
            Vehicle_Model__c = 'phev',
            Sirius_XM_ID__c = '123456'
        );
        caseList.add(existCase560);

        Case existCase858 = new Case(
            Program__c = prog858.Id,
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin858.Id,
            VIN_Member_ID__c = 'JLR85811111111111',
            Trouble_Code__c = 'Boost',
            //Tow_Reason__c = 'Engine Stalled',
            Club_Call_Number__c = '123',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            JLR__c= true,
            JLRMTFound__c = true,
            MobTechDenied__c='No',
            JLRTriageMTRequired__c = 'Yes',
            Country__c='US',
            Postal_code__c= '33169',
            JLRTriageWork__c = 'No'
        );
        caseList.add(existCase858);

        Case exist1Case858 = new Case(
            Program__c = prog858.Id,
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin858.Id,
            VIN_Member_ID__c = 'JLR85811111111111',
            Trouble_Code__c = 'Flatbed Tow',
            Tow_Reason__c = 'Engine Stalled',
            Club_Call_Number__c = '123',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            JLR__c= true,
            JLRMTFound__c = true,
            MobTechDenied__c='No',
            JLRTriageMTRequired__c = 'Yes',
            JLRTriageWork__c = 'No',
            Country__c='US'
        );
        caseList.add(exist1Case858);

        Case exist2Case858 = new Case(
            Program__c = prog858.Id,
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin858.Id,
            VIN_Member_ID__c = 'JLR85811111111111',
            Trouble_Code__c = 'Gas',
            Club_Call_Number__c = '123',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            JLR__c= true,
            JLRMTFound__c = true,
            MobTechDenied__c='No',
            JLRTriageMTRequired__c = 'Yes',
            Country__c='US',
            taxidest__c = 'test'
        );
        caseList.add(exist2Case858);

        Case existCase582 = new Case(
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin582.Id,
            VIN_Member_ID__c = 'GN582111111111111',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c = 'Engine Stalled',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            Program__c = prog582.Id,
            Selected_distance__c= 30
        );
        caseList.add(existCase582);
        
        Case existCase569 = new Case(
            Program__c = prog569.Id,
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin569.Id,
            VIN_Member_ID__c = 'HM569111111111111',
            Trouble_Code__c = 'Accident Tow',
            Tow_Reason__c = 'Engine Stalled',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            Location_Code__c = 'Driveway',
            Province__c='US'
        );
        caseList.add(existCase569);

        Case existCase599 = new Case(
            RecordTypeId = recordTypeMap.get('Roadside_Assistance'),
            VIN__c = vin599.Id,
            VIN_Member_ID__c = 'Waze5991111111111',
            Trouble_Code__c = 'Accident Tow',
            Tow_Reason__c = 'Engine Stalled',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now(),
            Breakdown_Location__Longitude__s=45.59971620, 
            Breakdown_Location__Latitude__s=-73.78835360,
            Program__c = prog599.Id,
            Location_Code__c = 'Driveway',
            Province__c='CA',
            DealerShip__c= tow599.Id
        );
        caseList.add(existCase599);
        insert caseList;
        
        MACoverage__c maCov1 = new MACoverage__c(
            mobileAssist__c = Ma1.id,
            zipcode__c = '33169'
        );
        insert maCov1;
        
        MTCoverage__c mtCov1 = new MTCoverage__c(
            mobileTech__c = Mt1.id,
            zipcode__c = '33169'
        );
        insert mtCov1;
        
        Area_Of_Responsibility__c aor1 = new Area_Of_Responsibility__c(
            Mobile_Tech__c = Mt1.id,
            MT_Brand__c = 'Land Rover',
            Zip_Code__c = '33169'
        );
        insert aor1;

        Area_Of_Responsibility__c aor2 = new Area_Of_Responsibility__c(
            Mobile_Tech__c = Mt2.id,
            MT_Brand__c = 'Jaguar',
            Zip_Code__c = '33169'
        	);
        insert aor2;
    }

     @isTest static void testmethod_one(){
        Case c = [SELECT Id,DealerShip__c,Program__c FROM Case WHERE VIN_Member_ID__c = 'Honda560111111111' LIMIT 1];
        Account d = [Select id from account where account_number__c='Honda560Dealer' limit 1];
        c.DealerShip__c = d.Id;
        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        ctrl.showVip = true;
        ctrl.limoCompanies[0].selectLimo = true;
        ctrl.current_call.Is_Request_Turned_Down__c='Yes';
        ctrl.turnDowns.add(new EHI_Turn_Down__c(name='e123',reason__c='test'));
        ctrl.turnDowns.add(new EHI_Turn_Down__c(name='e124',reason__c='test'));
        ctrl.requestCCInfo();
        ctrl.CalcTaxiFee();
        ctrl.ValidateJLRMobAssist();
        ctrl.refreshAfterSendLocatioonText();
        ctrl.showCloseTheLoopPopup();
        ctrl.closeCloseTheLoopPopup();
        ctrl.getDriveInGPBR();
        ctrl.setDealership();
        ctrl.setDealershipByName();
        ctrl.GetTaxiCompanyDenyCC();
        //ctrl.sendLocationText();
        ctrl.AcceptCreditCard();
        ctrl.getNearbyAccounts();
        ctrl.current_call.Is_your_vehicle_an_electric_vehicle__c = 'No';
        ctrl.getNearbyAccounts();
        ctrl.showMoreAccounts();
        ctrl.saveAndRefresh();
        ctrl.saveAndClose();
        ctrl.next();
        ctrl.prev();

	}

    @isTest static void test_method_twoJLR(){
        Case c = [SELECT Id,Program__c FROM Case WHERE VIN_Member_ID__c = 'JLR85811111111111' and Trouble_Code__c = 'Gas' LIMIT 1];
        Program__c prog = [select Id,Program_Code__c,Account__c,Call_Flow_JLR__c,Call_Flow_EHI__c from Program__c where Program_code__c='858' LIMIT 1];

        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'OK',' {  "destination_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ], "origin_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ],"rows" : [{"elements" : [{"distance" : { "text" : "1 ft","value" : 0}, "duration" : {"text" : "1 min","value" : 0}, "status" : "OK"}]}],"status" : "OK"}',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        test.startTest();
        ctrl.next();       
        ctrl.getNearbyAccounts();
        ctrl.GetTaxiDistance();
        test.stopTest();
    }
        
    @isTest static void test_method_twoJLR_FT(){
        Case c = [SELECT Id,Program__c FROM Case WHERE VIN_Member_ID__c = 'JLR85811111111111' and Trouble_Code__c = 'Flatbed Tow' LIMIT 1];
        Program__c prog = [select Id,Program_Code__c,Account__c,Call_Flow_JLR__c,Call_Flow_EHI__c from Program__c where Program_code__c='858' LIMIT 1];
        
        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'OK',' {  "destination_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ], "origin_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ],"rows" : [{"elements" : [{"distance" : { "text" : "1 ft","value" : 0}, "duration" : {"text" : "1 min","value" : 0}, "status" : "OK"}]}],"status" : "OK"}',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        test.startTest();
        ctrl.next();
        ctrl.getNearbyAccounts();

        test.stopTest();
    }
        
    @isTest static void test_method_twoJLR_MT(){
        Case c = [SELECT Id,Program__c FROM Case WHERE VIN_Member_ID__c = 'JLR85811111111111' and Trouble_Code__c = 'Boost' LIMIT 1];
        Program__c prog = [select Id,Program_Code__c,Account__c,Call_Flow_JLR__c,Call_Flow_EHI__c from Program__c where Program_code__c='858' LIMIT 1];
        
        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'OK',' {  "destination_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ], "origin_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ],"rows" : [{"elements" : [{"distance" : { "text" : "1 ft","value" : 0}, "duration" : {"text" : "1 min","value" : 0}, "status" : "OK"}]}],"status" : "OK"}',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        test.startTest();
        ctrl.next();
        ctrl.getNearbyAccounts();
        ctrl.current_call.JLRTriageAgent__c='No';
        ctrl.next();
        test.stopTest();
    }
        
     @isTest static void test_method_twoJLRMA(){
        Case c = [SELECT Id,Program__c FROM Case WHERE VIN_Member_ID__c = 'JLR85811111111111' and Trouble_Code__c = 'Flatbed Tow' LIMIT 1];
        Program__c prog = [select Id,Program_Code__c,Account__c,Call_Flow_JLR__c,Call_Flow_EHI__c from Program__c where Program_code__c='858' LIMIT 1];

        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'OK',' {  "destination_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ], "origin_addresses" : [ "900 2nd St, Ellsworth, KS 67439, USA" ],"rows" : [{"elements" : [{"distance" : { "text" : "1 ft","value" : 0}, "duration" : {"text" : "1 min","value" : 0}, "status" : "OK"}]}],"status" : "OK"}',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        test.startTest();
        ctrl.getNearbyAccounts();
        ctrl.current_call.JLRTriageAgent__c = 'Yes';
        ctrl.current_call.JLRMAFound__c = true;
        ctrl.next();
        ctrl.current_call.TowtoNearestDealer__c = 'Yes';
        ctrl.next();
        ctrl.current_call.LoanerVehicleAvailable__c = 'Yes';
        ctrl.next();
        ctrl.current_call.IsRegisterOwner__c = 'Yes';
        ctrl.next();
        ctrl.current_call.RetailertoProvideVehicle__c = 'No';
        ctrl.next();
        ctrl.current_call.JLRMAETALessClubETA__c= 'Yes';
        ctrl.next();
        ctrl.current_call.ExcNotes__c = 'test';
        ctrl.current_call.jlrtriageagent__c='No';
        ctrl.next();        
        ctrl.current_call.LoanerVehicleAvailable__c = 'No';
        ctrl.current_call.Postal_code__c= '33169';
        ctrl.next();
        ctrl.getNearbyAccounts();        

        test.stopTest();
	}

    @isTest static void test_method_three(){
        
        Case c = [SELECT Id,Program__c,program__r.id,Club__c,VIN__c,Vin__r.id,vin__r.name FROM Case WHERE VIN_Member_ID__c = 'GN582111111111111' LIMIT 1];
        Program__c prog = [select Id,Program_Code__c,Max_Tow_Distance__c from Program__c where Program_code__c='582' LIMIT 1];
        
        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        ctrl.next();
        ctrl.current_call.Is_Customer_Pay_Extra_cost_For_Tow__c = 'Yes';
        ctrl.current_call.Selected_distance__c=20;
        ctrl.next();
        ctrl.getNearbyAccounts();
        ctrl.current_call.Unattended_tow__c= true;
        ctrl.current_call.Unattended_Tow_Authorization_Name__c = 'test';
        ctrl.next();
    }
    @isTest static void test_method_four(){

        Case c = [SELECT Id,Program__c FROM Case WHERE VIN_Member_ID__c = 'HM569111111111111' LIMIT 1];
        System.debug('p1:'+c.Program__c);
        Program__c prog = [select Id,Program_Code__c from Program__c where Program_code__c='569' LIMIT 1];

        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        ctrl.next();
        ctrl.current_call.Unattended_tow__c= true;
        ctrl.next();
        ctrl.current_call.Unattended_Tow_Authorization_Name__c = 'test';
        ctrl.next();

	}

	@isTest static void test_method_five(){

        Case c = [SELECT Id FROM Case WHERE VIN_Member_ID__c = 'Waze5991111111111' LIMIT 1];
        Program__c prog = [select Id,Program_Code__c,Call_Flow_JLR__c,Call_Flow_EHI__c,Account__c from Program__c where Program_code__c='599' LIMIT 1];

        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        ctrl.next();
        ctrl.current_call.Waze_Review_Charges__c = 'Yes';
        ctrl.next();
        ctrl.current_call.Payment_Approved__c = 'Yes';
        ctrl.next();
        ctrl.current_call.Auth_Code__c = 'test';
        ctrl.next();
        ctrl.setDealerShip();
        ctrl.next();
	}

	@isTest static void test_method_sixHonda(){

        Case c = [SELECT Id FROM Case WHERE VIN_Member_ID__c = 'Honda560111111111' LIMIT 1];
        Program__c prog = [select Id,Account__c,Program_Code__c,Call_Flow_JLR__c,Call_Flow_EHI__c from Program__c where Program_code__c='560' LIMIT 1];

        PageReference pageRef = Page.TakeCall; 
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('sirius_xm_status', 'false');
        System.currentPageReference().getParameters().put('id', c.Id);
        TakeCall_BreakdownInfo_Cntrl ctrl = new  TakeCall_BreakdownInfo_Cntrl();
        ctrl.sendLocationText();
        ctrl.terminateSiriusXM_call();
	}

}