@isTest
private class PromoController_Test {

    public class LoginReponse
    {
        public string status {get;set;}
        public string token {get;set;}
        public boolean reset {get;set;}
        public string dealername {get;set;}
        public string dealerphone {get;set;}
        public string dealeraddress {get;set;}
    }
    
    /* Forgot Password and Username Changes : Start */
    public class GetTokenReponse
    {
        public string status {get;set;}
        public string dealerId {get;set;}
        public string token {get;set;}
    }
    /* Forgot Password and Username Changes : End */
    
    static Account client{get;set;}
    static Account dealer{get;set;}
    static VIN__c vin{get;set;}
    static Program__c prog{get;set;}
    static String accountFields='';
    static String programFields='';
    static String vinFields='';
    @testSetup
    static void setup() {
        Account clientAcc = new Account(
            Name='Audi', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='511',
            Tow_Exchange_Email_Address__c = 'dzhu@clubautoltd.com',     
            gpbr__c = 'gpbr1'
        );
        insert clientAcc;
        Account dealerAcc = new Account(
            Name='Audi Dealer', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Account_Number__c='511-X9999999',
            client__c = clientAcc.id,
            shippingStreet = '100csvd',
            shippingcity = 'toronto',
            shippingstate = 'Ontario',
            shippingpostalcode = 'M5J3W2',
            phone = '4166666666',
            status__c = 'Active',
            WEBLOGINCODE__C ='1234',
            type = 'Dealership / Tow Destination',
            business_email__c = 'abcTest@abc.com'        // Forgot UserName and Password Functionality           
        );
        insert dealerAcc;
        Program__c progAudi = new Program__c(
            Account__c=clientAcc.Id,
            Program_Code__c='511',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='Audi',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            OEM__c=true
        );
        insert progAudi;
        Program__c progAudiEx = new Program__c(
            Account__c=clientAcc.Id,
            Program_Code__c='541',
            Description__c='Extended',
            Type__c='Passthrough',
            CTI_Search_Terms__c='Audi',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            OEM__c=true
        );
        insert progAudiEx;
        VIN__c vinAudi = new VIN__c(
            Name='VIN12345678904567',
            UID__c='VIN12345678904567',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=progAudi.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vinAudi;
    }
    static void Utility()
    {        
        accountFields='';
        for(Schema.SObjectField f : Account.SObjectType.getDescribe().fields.getMap().values()) {
            accountFields+= (f.getDescribe().getName() + ',');
        }
        accountFields=accountFields.left(accountFields.length()-1);
        
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);
        
        vinFields='';
        for(Schema.SObjectField f : VIN__c.SObjectType.getDescribe().fields.getMap().values()) {
            vinFields += (f.getDescribe().getName() + ',');
        }
        vinFields=vinFields.left(vinFields.length()-1);
        
        String query1 = 'Select ' + accountFields+ '  FROM Account WHERE Account_number__c = \'511\' LIMIT 1';
        client  = database.query(query1);
        //client = [select id from account where account_number__c = '511'];
     
        //insert client;
        String query2 = 'Select ' + accountFields+ '  FROM Account WHERE Account_number__c = \'511-X9999999\' LIMIT 1';
        dealer = database.query(query2);
        /* dealer = new Account(
            Name='Audi Dealer', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Account_Number__c='511-X9999999',
            client__c = client.id,
            shippingStreet = '100csvd',
            shippingcity = 'toronto',
            shippingstate = 'Ontario',
            shippingpostalcode = 'M5J3W2',
            phone = '4166666666',
            status__c = 'Active',
            WEBLOGINCODE__C ='1234',
            type = 'Dealership / Tow Destination',
            business_email__c = 'abcTest@abc.com'        // Forgot UserName and Password Functionality           
        );
        insert dealer;*/
        
        String query3 = 'Select ' + programFields+ ' FROM Program__c WHERE Program_Code__c = \'511\' LIMIT 1';
        prog = database.query(query3);        

        // prog = [select id from program__c where program_code__c = '511'];
        
        String query4 = 'Select ' + vinFields+ ' FROM VIN__c WHERE Name = \'VIN12345678904567\' LIMIT 1';
        vin = database.query(query4);
        
        /* vin = new VIN__c(
            Name='VIN12345678904567',
            UID__c='VIN12345678904567',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        insert vin;*/
      
    }

    static testMethod void myUnitTest() {
        
        Utility();

        string loginstring1 = '{"Type":"Login","UserName":"X9999999xxx","Password":"1234","Program":"Audi"}';
        ApexPages.currentPage().getParameters().put('Promo', loginString1);
        
        PromoController pc2 = new PromoController();
        pc2.Init();

        loginstring1 = '{"Type":"Login","UserName":"X9999999xxx","Password":"1234","Program":"Audi"}';
        ApexPages.currentPage().getParameters().put('Promo', loginString1);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        
        pc2= new PromoController();
        pc2.Init();
        
        string loginstring = '{"Type":"Login","UserName":"X9999999","Password":"1234","Program":"Audi"}';
        ApexPages.currentPage().getParameters().put('Promo', loginString);
        
        PromoController pc = new PromoController();
        pc.Init();
        
        LoginReponse resp = (LoginReponse)JSON.deserialize(pc.Response,LoginReponse.class);
        string token = '';
        if (resp.Status == 'Success')
         token = resp.token; 
         system.debug('resp:' + resp);

        string rpString = '{"Type":"ResetPassword","UserName":"X9999999","OldPassword":"1234","NewPassword":"12345","Program": "Audi","Token":"' + token + '"}';     
         ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        pc2 = new PromoController();
        pc2.Init();

        rpString = '{"Type":"ResetPassword","UserName":"X9999999x","OldPassword":"1234","NewPassword":"12345","Program": "Audi","Token":"' + token + '"}';   
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        PromoController pc3 = new PromoController();
        pc3.Init();
         
        rpString = '{"Type":"GETVINInfo","VIN": "VIN12345678904567","Program": "Audi","Token": "' + token + 'xx"}';  
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        PromoController pc8 = new PromoController();
        pc8.Init();  
         
        
        rpString = '{"Type" : "SubmitRequest","VIN" : "VIN12345678904567","FirstName" : "John","LastName" : "Smith","Phone" : "416-999-9999","Email" : "abc@abc.com","PromoStartDate" : "2017-03-10","PromoEndDate" : "2017-09-10","Reason" : "reason ","Cost" : 100,"Program": "Audi","Token": "' + token + '","MaxOdo":"109999"}';     
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        PromoController pc4 = new PromoController();
        pc4.Init();
         
        rpString = '{"Type" : "SubmitRequest","VIN" : "VIN12345678904997","FirstName" : "John","LastName" : "Smith","Phone" : "416-999-9999","Email" : "abc@abc.com","PromoStartDate" : "2017-03-10","PromoEndDate" : "2017-09-10","Reason" : "reason ","Cost" : 100,"Program": "Audi","Token": "' + token + '","MaxOdo":"109999"}';     
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        PromoController pc5 = new PromoController();
        pc5.Init();
         
        rpString = '{"Type" : "SubmitRequest","VIN" : "VIN12345678904997","FirstName" : "John","LastName" : "Smith","Phone" : "416-999-9999","Email" : "abc@abc.com","PromoStartDate" : "2017-03-10","PromoEndDate" : "2017-09-10","Reason" : "reason ","Cost" : 100,"Program": "Audi","Token": "' + token + 'xx","MaxOdo":"109999"}';   
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        pc5 = new PromoController();
        pc5.Init();
        
        
        rpString = '{"Type" : "SubmitRequest","VIN" : "VIN12345678904997","FirstName" : "John","LastName" : "Smith","Phone" : "416-999-9999","Email" : "abc@abc.com","PromoStartDate" : "2017-03-10","PromoEndDate" : "2017-09-10","Reason" : "reason ","Cost" : 100,"Program": "Audix","Token": "' + token + '","MaxOdo":"109999"}';    
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        pc5 = new PromoController();
        pc5.Init();
        
        rpString = '{"Type" : "GetAllHistory","Program": "Audi","Token": "' + token + '"}';  
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        pc5 = new PromoController();
        pc5.Init();

        rpString = '{"Type" : "SearchUser","Program": "Audi","Token": "' + token + '","Keyword":"John"}';    
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        pc5 = new PromoController();
        pc5.Init();

        rpString = '{"Type" : "SearchUser","Program": "Audi","Token": "' + token + '","Keyword":""}';    
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        pc5 = new PromoController();
        pc5.Init();

        
        rpString = '{"Type" : "SubmitRequest1","VIN" : "VIN12345678904997","FirstName" : "John","LastName" : "Smith","Phone" : "416-999-9999","Email" : "abc@abc.com","PromoStartDate" : "2017-03-10","PromoEndDate" : "2017-09-10","Reason" : "reason ","Cost" : 100,"Program": "Audix","Token": "' + token + '","MaxOdo":"109999"}';   
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        pc5 = new PromoController();
        pc5.Init();

        rpString = '{"Type":"GETVINInfo","VIN": "WA1EFCFS9GR012585","Program": "Audi","Token": "' + token + '"}';    
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        PromoController pc6 = new PromoController();
        pc6.Init();
         
        rpString = '{"Type":"GETVINInfo","VIN": "VIN12345678904997","Program": "Audi","Token": "' + token + '"}';    
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        PromoController pc7 = new PromoController();
        pc7.Init();
         
        rpString = '{"Type":"GETVINInfo","VIN": "VIN12345678904567","Program": "Audi","Token": "' + token + 'xx"}';  
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        pc8 = new PromoController();
        pc8.Init();  
         
       
       rpString = '';    
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        
        pc8 = new PromoController();
        pc8.Init();  
        
        rpString = '{"Type1":"GETVINInfo","VIN1": "VIN12345678904567","Program1": "Audi","Token1": "' + token + 'xx"}';  
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        pc8 = new PromoController();
        pc8.Init();  
 
 
        Account audi = [select id,WEBLOGINCODE__C from account where account_number__c = '511' limit 1];
        audi.WEBLOGINCODE__C = '1234';
        update audi;
 
        loginstring = '{"Type":"Login","UserName":"5419999","Password":"1234","Program":"Audi"}';
        ApexPages.currentPage().getParameters().put('Promo', loginString);
        
        pc = new PromoController();
        pc.Init();
        
        LoginReponse resp1 = (LoginReponse)JSON.deserialize(pc.Response,LoginReponse.class);
        string token1 = '';
        if (resp1.Status == 'Success')
         token1 = resp1.token; 
         system.debug('resp:' + resp1);
 

        rpString = '{"Type" : "GetAllHistory","Program": "Audi","Token": "' + token1 + '"}';     
        ApexPages.currentPage().getParameters().put('Promo', rpString);
        ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
        pc5 = new PromoController();
        pc5.Init();     
    }
    
    /* Forgot Password and Username Changes : Start */
    static testMethod void myUnitTestForExtendedMethods() {
      Utility();
      
      string fuString = '{"Type" : "ForgotUserName","Program": "Audi","Email":"abcTest@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', fuString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      PromoController pc9 = new PromoController();
      pc9.Init();
      
      fuString = '{"Type1" : "ForgotUserName","Program": "Audi","Email":"abcTest@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', fuString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc9 = new PromoController();
      pc9.Init();
      
      fuString = '{"Type" : "ForgotUserNamexx","Program": "Audi","Email":"abcTest@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', fuString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc9 = new PromoController();
      pc9.Init();
      
      fuString = '{"Type" : "ForgotUserName","Program": "Audi","Email":"est@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', fuString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc9 = new PromoController();
      pc9.Init();
      
      fuString = '{"Type" : "ForgotUserName","Program": "Audi","EmailId":"est@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', fuString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc9 = new PromoController();
      pc9.Init();
      
      string loginstring = '{"Type":"Login","UserName":"X9999999","Password":"1234","Program":"Audi"}';
      ApexPages.currentPage().getParameters().put('Promo', loginString);
      
      PromoController pc10 = new PromoController();
      pc10.Init();
      
      LoginReponse resp = (LoginReponse)JSON.deserialize(pc10.Response,LoginReponse.class);
      string token = '';
      if (resp.Status == 'Success')
       token = resp.token; 
      system.debug('resp:' + resp);
      
      string tfpString = '{"Type" : "GetTokenForForgotPassword" , "Program": "Audi", "Email" :"email@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', tfpString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      PromoController pc11 = new PromoController();
      pc11.Init();
      
      tfpString = '{"Type" : "GetTokenForForgotPasswordXX" , "Program": "Audi", "Email" :"email@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', tfpString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc11 = new PromoController();
      pc11.Init();
      
      tfpString = '{"Type" : "GetTokenForForgotPassword" , "Program": "Audi12", "Email" :"email@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', tfpString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc11 = new PromoController();
      pc11.Init();
      
      tfpString = '{"Type" : "GetTokenForForgotPassword" , "Program": "Audi", "Email" :"abcTest@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', tfpString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc11 = new PromoController();
      pc11.Init();
      
      tfpString = '{"Type" : "GetTokenForForgotPassword" , "Program": "Audi", "EmailId" :"abcTest@abc.com"}';
      ApexPages.currentPage().getParameters().put('Promo', tfpString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc11 = new PromoController();
      pc11.Init();
      
      GetTokenReponse resp1 = (GetTokenReponse)JSON.deserialize(pc11.Response,GetTokenReponse.class);
      string token1 = '';
      if (resp1.Status == 'Success')
       token1 = resp1.token; 
       system.debug('resp:' + resp1);  
       
      string upString = '{"Type" : "UpdatePassword","Program": "Audi","UserName":"X9999999","NewPassword":"12345","Token":"' + token1  + 'xx"}';
      ApexPages.currentPage().getParameters().put('Promo', upString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      PromoController pc12 = new PromoController();
      pc12.Init();
      
      upString = '{"Type1" : "UpdatePassword","Program": "Audi","UserName":"X9999999","NewPassword":"12345","Token":"' + token1  + '"}';
      ApexPages.currentPage().getParameters().put('Promo', upString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc12 = new PromoController();
      pc12.Init();
      
      upString = '{"Type" : "UpdatePassword","Program": "Audi12","UserName":"X9999999","NewPassword":"12345","Token":"' + token1  + '"}';
      ApexPages.currentPage().getParameters().put('Promo', upString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc12 = new PromoController();
      pc12.Init();
      
      upString = '{"Type" : "UpdatePasswordXX","Program": "Audi","UserName":"X9999999","NewPassword":"12345","Token":"' + token1  + '"}';
      ApexPages.currentPage().getParameters().put('Promo', upString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc12 = new PromoController();
      pc12.Init();
      
      upString = '{"Type" : "UpdatePassword","Program": "Audi","UserName":"X9899679","NewPassword":"12345","Token":"' + token1  + '"}';
      ApexPages.currentPage().getParameters().put('Promo', upString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc12 = new PromoController();
      pc12.Init();
      
      upString = '{"Type" : "UpdatePassword","Program": "Audi","UserName":"X9999999","NewPassword":"12345","Token":"' + token1  + '"}';
      ApexPages.currentPage().getParameters().put('Promo', upString);
      ApexPages.currentPage().getHeaders().put('Content-Type', 'application/x-www-form-urlencoded');
      pc12 = new PromoController();
      pc12.Init();
      
            
      loginstring = '{"Type":"Login","UserName":"X9999999","Password":"1234","Program":"Audi"}';
      ApexPages.currentPage().getParameters().put('Promo', loginString);
      
      PromoController pc13 = new PromoController();
      pc13.Init();
      
      loginstring = '{"Type":"Login","UserName":"X9999999","Password":"12345","Program":"Audi"}';
      ApexPages.currentPage().getParameters().put('Promo', loginString);
      
      pc13 = new PromoController();
      pc13.Init(); 
    }
    
    /* Forgot Password and Username Changes : End */
}