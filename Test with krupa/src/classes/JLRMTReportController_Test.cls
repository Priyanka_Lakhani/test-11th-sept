@isTest
private class JLRMTReportController_Test {
    static Account a1 {get;set;}
    static Account a2 {get;set;}
    static Account club {get;set;}
    static Contact cont {get;set;}
    static Case c1 {get;set;}
    static Case c2 {get;set;}
    static Case c3 {get;set;}
    static Case caseTowRequired {get;set;}
    static Case caseCompleted {get;set;}
    static string SelectedDealer {get;set;}
    static string fromdate {get;set;}
    static string todate {get;set;}
    static CA__c settings {get;set;}
    static Survey__c survey {get;set;}
    static Survey_Question__c sq1 {get;set;}
    static Survey_Question__c sq2 {get;set;}
    static Survey_Question__c sq3 {get;set;}
    static Survey_Question__c sq4 {get;set;}
    static SurveyResponse__c sr1 {get;set;}
    static SurveyResponse__c sr2 {get;set;}
    static SurveyResponse__c sr3 {get;set;}
    static SurveyResponse__c sr4 {get;set;}

    static Integration__c intgration1 {get;set;}
	static Integration__c intgration2 {get;set;}
	static Integration__c intgration3 {get;set;}
	static Integration__c intgration4 {get;set;}
	static Integration__c intgration5 {get;set;}
	static list<MTBilling__c> MTBillingSettings {get;set;}
	
	static Integration__c intgrationAck1 {get;set;}
 	static Integration__c intgrationAck2 {get;set;}
 	static Integration__c intgrationAck3 {get;set;}

	static void TowReqandCloseCasesUtility()
	{
		  caseTowRequired = new Case(       
          Trouble_Code__c='Tow',
          //Final_Status__c = 'Dispatched',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',
          MobTechDenied__c = 'Yes',
          MTDenyReason__c ='Tow Required',
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwu',
          postal_code__c='K178J9',
          damage__c=true,
          ETA__c=15);
        insert caseTowRequired;  
        caseTowRequired.DI_Status_Date_Time__c = DateTime.now();
        caseTowRequired.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        update caseTowRequired;
        
		intgration4 = new Integration__c();
        intgration4.Case__c = caseTowRequired.id;
        intgration4.IP__c = '49.213.53.37';
        intgration4.Message__c = 'Dispatch';
        intgration4.Parameters__c = 'DispatchCall MT-123';
        intgration4.Reprocess_Date_Time__c = DateTime.now();
        insert intgration4;
        
        
        /* caseCompleted = new Case(       
          Trouble_Code__c='Gas',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',
          MobTechDenied__c = 'Yes',
          MTDenyReason__c ='Closed (Call Cleared)',
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwv',
          postal_code__c='K178J9',
          damage__c=true,
          ETA__c=15);
        insert caseCompleted;  
        caseCompleted.DI_Status_Date_Time__c = DateTime.now();
        caseCompleted.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        update caseCompleted;
		
        intgration5 = new Integration__c();
        intgration5.Case__c = caseCompleted.id;
        intgration5.IP__c = '49.213.53.37';
        intgration5.Message__c = 'Dispatch';
        intgration5.Parameters__c = 'DispatchCall';
        intgration5.Reprocess_Date_Time__c = DateTime.now(); 
        insert intgration5;
		*/
	}

	static void AccountUtility()
	{
	    a1 = new Account(
          name='MT1',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-123',
          SecondAccountNumber__c='R0123',
          Status__c ='Active');
        insert a1;
        a2 = new Account(
          name='MT3',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-333',
          SecondAccountNumber__c='R0333',
          Status__c ='Active');
        insert a2;	
        
        cont = new Contact(LastName='Test',Email='abc@mail.com',Contact_External__c='DevelopmentContact_DoNotDelete001');
        insert cont;
        
        club = new Account(Name='Club', Account_Number__c='998', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
	}

	static void SurveyUtility()
	{
	    survey = new Survey__c(
            name ='JLR Survey'
        );
        insert survey;
                
        sq1 = new Survey_Question__c();
        sq1.question__c = 'How would you rate the competency of the responder who provided assistance?';
        sq1.QuestionShort__c = 'How would you rate the competency of the responder who provided assistance?';
        sq1.order__c = 1;
        sq1.survey__c = survey.id;
        sq1.type__c = 'Single Select';
        sq1.Language__c = 'English';
        upsert sq1;

        sq2 = new Survey_Question__c();
        sq2.question__c = 'How would you rate the courtesy of the responder who provided assistance?';
        sq2.QuestionShort__c = 'How would you rate the courtesy of the responder who provided assistance?';
        sq2.order__c = 2;
        sq2.survey__c = survey.id;
        sq2.type__c = 'Single Select';
        sq2.Language__c = 'French';
        upsert sq2;
        
        sq3 = new Survey_Question__c();
        sq3.question__c = 'How would you rate the courtesy of the person who answered your call?';
        sq3.QuestionShort__c = 'How would you rate the courtesy of the person who answered your call?';
        sq3.order__c = 3;
        sq3.survey__c = survey.id;
        sq3.type__c = 'Single Select';
        sq3.Language__c = 'English';
        upsert sq3;

        sq4 = new Survey_Question__c();
        sq4.question__c = 'How would you rate the competency of the person who answered your call?';
        sq4.QuestionShort__c = 'How would you rate the competency of the person who answered your call?';
        sq4.order__c = 4;
        sq4.survey__c = survey.id;
        sq4.type__c = 'Single Select';
        sq4.Language__c = 'French';
        upsert sq4;
	
	}

	static void CasewithSurveyUtility()
	{
        c1 = new Case(       
          Trouble_Code__c='Gas',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',         
          MTCloseReason__c='GOA',
          kill_code__c = 'GOA',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABC',
          postal_code__c='K178J8',
          surveysent__c ='test',
          club__c = club.id
          );
        insert c1;
        c2 = new Case(       
          Trouble_Code__c='Install Spare',
          MobileTech__c= a1.id, 
          JLRTriageAgent__c = 'Yes',        
          ETA__c = 30,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtyy',
          postal_code__c='K178J9',
          complaint__c=true
          );
        insert c2;
        c3 = new Case(       
          Trouble_Code__c='Gas',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',
          MobTechDenied__c = 'Yes',
          MTDenyReason__c ='Rejected by Customer',
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwt',
          postal_code__c='K178J8',
          damage__c=true,
          ETA__c=15);
        insert c3;
        
        
        c1.DI_Status_Date_Time__c = DateTime.now();
        c2.DI_Status_Date_Time__c = DateTime.now();
        c3.DI_Status_Date_Time__c = DateTime.now();
        c1.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        c2.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        c3.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        System.debug('c1.final_status__c:'+c1.final_status__c);
        update c1;
        update c2;
        update c3;
        System.debug('c1.final_status__c:'+c1.final_status__c); 
        
        sr1 = new SurveyResponse__c();
        sr1.Case__c = c1.id;
        sr1.Phone__c = '123456';
        sr1.Response__c = '9';
        sr1.ResponseDate__c = DateTime.now();
        sr1.SubmissionDate__c = DateTime.now();
        sr1.SurveyQuestion__c = sq1.id;
        sr1.Callback__c = 'Yes';
        insert sr1;

        sr2 = new SurveyResponse__c();
        sr2.Case__c = c1.id;
        sr2.Phone__c = '123456';
        sr2.Response__c = '9';
        sr2.ResponseDate__c = DateTime.now();
        sr2.SubmissionDate__c = DateTime.now();
        sr2.SurveyQuestion__c = sq2.id;
        sr2.Callback__c = 'Yes';
        insert sr2;  
        
        sr3 = new SurveyResponse__c();
        sr3.Case__c = c1.id;
        sr3.Phone__c = '123456';
        sr3.Response__c = '10';
        sr3.ResponseDate__c = DateTime.now();
        sr3.SubmissionDate__c = DateTime.now();
        sr3.SurveyQuestion__c = sq3.id;
        sr3.Callback__c = 'Yes';
        insert sr3;

        sr4 = new SurveyResponse__c();
        sr4.Case__c = c1.id;
        sr4.Phone__c = '123456';
        sr4.Response__c = '10';
        sr4.ResponseDate__c = DateTime.now();
        sr4.SubmissionDate__c = DateTime.now();
        sr4.SurveyQuestion__c = sq4.id;
        sr4.Callback__c = 'Yes';
        insert sr4;

        intgration1 = new Integration__c();
        intgration1.Case__c = c1.id;
        intgration1.IP__c = '49.213.53.37';
        intgration1.Message__c = 'Dispatch';
        intgration1.Parameters__c = 'DispatchCall MT-123';
        intgration1.Reprocess_Date_Time__c = DateTime.now();
        insert intgration1;

        intgration2 = new Integration__c();
        intgration2.Case__c = c2.id;
        intgration2.IP__c = '49.213.53.37';
        intgration2.Message__c = 'Dispatch';
        intgration2.Parameters__c = 'DispatchCall MT-123';
        intgration2.Reprocess_Date_Time__c = DateTime.now();
        insert intgration2;

        intgration3 = new Integration__c();
        intgration3.Case__c = c3.id;
        intgration3.IP__c = '49.213.53.37';
        intgration3.Message__c = 'Dispatch';
        intgration3.Parameters__c = 'DispatchCall MT-123';
        intgration3.Reprocess_Date_Time__c = DateTime.now();
        insert intgration3;

		intgrationAck1 = new Integration__c();
       intgrationAck1.Case__c = c1.id;
       intgrationAck1.IP__c = '49.213.53.37';
       intgrationAck1.Message__c = 'MTAck';
       intgrationAck1.Parameters__c = 'Acknowledgment';
       intgrationAck1.Reprocess_Date_Time__c = DateTime.now();
       insert intgrationAck1;

       intgrationAck2 = new Integration__c();
       intgrationAck2.Case__c = c2.id;
       intgrationAck2.IP__c = '49.213.53.37';
       intgrationAck2.Message__c = 'MTAck';
       intgrationAck2.Parameters__c = 'Acknowledgment';
       intgrationAck2.Reprocess_Date_Time__c = DateTime.now();
       insert intgrationAck2;

       intgrationAck3 = new Integration__c();
       intgrationAck3.Case__c = c3.id;
       intgrationAck3.IP__c = '49.213.53.37';
       intgrationAck3.Message__c = 'MTAck';
       intgrationAck3.Parameters__c = 'Acknowledgment';
       intgrationAck3.Reprocess_Date_Time__c = DateTime.now();
       insert intgrationAck3;
	
	}

    static void Utility()
    {
    	AccountUtility();

    	
        settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';        
        upsert settings;
        
        Sites__c sites = new Sites__c();
        sites.name = 'Survey';
        sites.value__c = 'http://www.salesforce.com';
        upsert sites;
        
        Sites__c sites1 = new Sites__c();
        sites1.name = 'JLRLogo';
        sites1.value__c = '015m0000001BLhX';
        upsert sites1;
        
        Sites__c sites2 = new Sites__c();
        sites2.name = 'Instance';
        sites2.value__c = 'cs20';
        upsert sites2;
                
        
        MTBillingSettings = new list<MTBilling__c>();        
        MTBillingSettings.add(new MTBilling__c(name = 'GOA',Amount__c = 40.00));
        MTBillingSettings.add(new MTBilling__c(name = 'MRS',Amount__c = 85.00));
        MTBillingSettings.add(new MTBilling__c(name = 'PCAN',Amount__c = 40.00));
        MTBillingSettings.add(new MTBilling__c(name = 'SCP',Amount__c = 85.00));
        MTBillingSettings.add(new MTBilling__c(name = 'UNS',Amount__c = 85.00));
        upsert MTBillingSettings;
        
        //MTBillingSettings = MTBilling__c.getOrgDefaults();
        //MTBillingSettings.name = 'UNS';
        //MTBillingSettings.Amount__c = 85.00;
        //upsert MTBillingSettings;
       
    }
    static void FilterUtility(){
        SelectedDealer = 'MT1';
        fromDate= '15/08/2017';
        todate = '31/08/2017';
    }
    
    static testMethod void myUnitTest() {
      SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.show();
      mt1.updatePage();
      
      //mt1 = new JLRMTReportController(sdate,edate);
      //mt1.prevBtn();     
   } 
   /* static testMethod void myUnitTest11() {
      SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.prevBtn();     
   } */
   static testMethod void myUnitTest1() {
      SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      FilterUtility();
      JLRMTReportController mt2 = new JLRMTReportController(sdate,edate);
      mt2.filterReport();
      
   }
   
   static testMethod void myUnitTest2() {
      SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.FirstBtn();      
    
      //mt1 = new JLRMTReportController(sdate,edate);
      //mt1.nextBtn();
   }
   static testMethod void myUnitTest21() {
      SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.nextBtn();
   }
   
   static testMethod void myUnitTest3() {
      SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.lastBtn();     
      
      boolean isNext = mt1.getNext();
      boolean isPrev = mt1.getPrev();
   }
   static testMethod void myUnitTest4() {
   	  SurveyUtility();
      Utility();
      CasewithSurveyUtility();
      //TowReqandCloseCasesUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.SelectedDealerToFilter = null;
      mt1.setFilterDates();
      mt1.ExportAll();  
   }
  static testMethod void myUnitTest5() {
      Utility();
      //CasewithSurveyUtility();
      TowReqandCloseCasesUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.SelectedDealerToFilter = null;
      mt1.setFilterDates();
      mt1.ExportAll();  
   } 
      static testMethod void JLRMTR_SendJLRReportEmail_Test() {
      Utility();
      //CasewithSurveyUtility();
      TowReqandCloseCasesUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.SelectedDealerToFilter = null;
      mt1.setFilterDates();
      mt1.SendMTRetailerReportEmail();  
   } 
     static testMethod void JLRMTR_SendMTReportEmail_Test() {
      Utility();
      //CasewithSurveyUtility();
      TowReqandCloseCasesUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.SelectedDealerToFilter = null;
      mt1.setFilterDates();
      mt1.SendJLRReportEmail();  
   } 
     static testMethod void JLRMTR_SendEmailToAllRetailers_Test() {
      Utility();
      //CasewithSurveyUtility();
      TowReqandCloseCasesUtility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      JLRMTReportController mt1 = new JLRMTReportController(sdate,edate);
      mt1.SelectedDealerToFilter = null;
      //mt1.setFilterDates();
      mt1.SendEmailToAllRetailers();  
   } 
}