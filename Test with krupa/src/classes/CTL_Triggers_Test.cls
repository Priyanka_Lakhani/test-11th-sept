@isTest(SeeAllData=true) private class CTL_Triggers_Test{    
    static testMethod void testETA_baseOnInitialAndCTL_Create() {
        Account club = new Account(Name='JLR Mobile Technician', Account_Number__c='999000', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        update club;
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        Account tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE,
            Tow_Exchange_Email_Address__c = 'abc@clubautoltd.com',
            gpbr__c = 'aaazzz',
            status__c = 'Active'
        );
        insert tow;
        
        Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Florida',
            Province_Code__c='FL'
        );
        insert terr;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            call_flow_JLR__c = true,
            sponsorid__c= '000001',
            programid__c = '000012',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        insert prog;
        
        VIN__c vin = new VIN__c(
            Name='VINxxxxxxxxxxxxxxx12',
            UID__c='VINxxxxxxxxxxxxxxx12',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Jaguar',
            Program__c=prog.Id
        );
        insert vin;
        
        CA__c settings = CA__c.getOrgDefaults();
        
        Case c = new Case(
            Status = 'Dispatched',
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Miscellaneous_Call_Reason__c=settings.Call_Flow_Personal_Update_Request__c,
            Call_ID__c='ABCCTL',
            Trouble_Code__c = 'FlatBed Tow',
            Tow_Reason__c ='Snowbank',
            Street__c='1000 AAA dr',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Phone__c = '1234567891',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            Club_Call_Number__c = '111111',
            servicingclub__c = '014',
            first_name__c = 'daviddaviddaviddaviddaviddavid',
            last_name__c = 'daviddaviddaviddaviddaviddavid',
            gpbr__c = 'aaazzz'
        );
        insert c;
        
        test.startTest();
        Close_the_loop__c ctl = new Close_the_loop__c(
            ETA__c = 5,
            ETA_total__c = 5,
            Case__c = c.id
        );
        insert ctl;
        
        Close_the_loop__c ctl2 = new Close_the_loop__c(
            ETA__c = 5,
            ETA_total__c = 5,
            Case__c = c.id,
            Close_the_loop_Parent__c = ctl.id
        );
        insert ctl2;
        
        delete ctl2;
        
        c.Close_the_loop_option__c = 'No';
        update c;
        c.Close_the_loop_option__c = 'Yes';
        update c;
        test.stopTest();
    }
}