/*
Created Date: Nov 7 2018
Last Modified Date: Nov 7 2018
Last Modified By: Thamizh
Description: Contains invocable method which is called from Claims object process builder to create apex sharing for the applicants.
This provides sharing for the applicant even when the ownership of claims is modified.
*/
Public without sharing Class Claims_TriggerHelper{

    Public Static Void Claims_ApplicantShareCreation(Set<Id> claimsId){
        Set<Id> createdClaimsIds = new Set<Id>();//List of created claim Ids
        Map<Id,Id> ApplicantClaimMap = new Map<Id,Id>();//Map of Claim applicant contact & User Ids
        Map<Id,Id> claimsUsrMap = new Map<Id,Id>();
        createdClaimsIds.addAll(claimsId);
        system.debug('****createdClaimsIds****'+createdClaimsIds);
        List<Expense_Claim__share> claimShares = new List<Expense_Claim__share>();
        for(Expense_Claim__c claim:[Select Id, Claimant__c from Expense_Claim__c where Id in :createdClaimsIds]){
            ApplicantClaimMap.put(claim.Claimant__c, claim.Id);
        }
        system.debug('****ApplicantClaimMap****'+ApplicantClaimMap);
        for(User usr:[Select Id, ContactId from User where ContactId in :ApplicantClaimMap.keyset()]){
            if(ApplicantClaimMap.keyset().contains(usr.ContactId)){
                claimsUsrMap.put(ApplicantClaimMap.get(usr.ContactId), usr.Id);
            }
        }
        system.debug('****claimsUsrMap****'+claimsUsrMap);
        if(claimsUsrMap.size() > 0){
            for(Id conId:claimsUsrMap.keyset()){
                Expense_Claim__share expShare = new Expense_Claim__share();
                expShare.ParentId = conId;
                expShare.UserOrGroupId = claimsUsrMap.get(conId);
                expShare.AccessLevel = 'Edit';
                expShare.RowCause = Schema.Expense_Claim__share.RowCause.Community_Applicant__c;
                claimShares.add(expShare);
            }
        }
        system.debug('****claimShares****'+claimShares);
        if(claimShares.size() > 0){
            insert claimShares;
        }
    }
    
    Public Static Void Claims_ApplicantShareUpdation(Set<Id> claimsId){
        Set<Id> updatedClaimsIds = new Set<Id>();//List of updated claim Ids
        updatedClaimsIds.addAll(claimsId);
        system.debug('****updatedClaimsIds****'+updatedClaimsIds);
        List<Expense_Claim__share> claimShares = new List<Expense_Claim__share>();
        for(Expense_Claim__share claimShare:[Select Id, ParentId, AccessLevel, RowCause from Expense_Claim__share where ParentId in :updatedClaimsIds AND RowCause =:Schema.Expense_Claim__share.RowCause.Community_Applicant__c 
        AND AccessLevel = 'Edit']){
            claimShare.AccessLevel = 'Read';
            claimShares.add(claimShare);
        }system.debug('****claimShares****'+claimShares);
        if(claimShares.size() > 0){
            update claimShares;
        }
    }

}