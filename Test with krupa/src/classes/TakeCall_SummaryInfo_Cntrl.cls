global without sharing class TakeCall_SummaryInfo_Cntrl{
	
    public Case current_call { get; set; }
    public CA__c settings {get;set;}
    public Program__c selectedProgram { get; set; }
    public String siriusXMclienCode {get;set;}

    public String errorString { get; set; }
    public boolean isHondaProject {get;set;}
    public boolean boostToFTAcura {get;set;}
    public String errorMsgDebug {get;set;}
    public Account selectedGPBR { get; set; }
    public boolean isLGMProject {get;set;}
    public String cid;
    public boolean redirectPage {get;set;}
    public Boolean disconectCall_SXM {get;set;}
    public Boolean jlrNHSTA {get; set;}
    public String isNHSTA {get; set;}    
    public String nHSTAreason {get; set;}
    public String exitLink { get; set; }
	public boolean dispAccNSX{get;set;}

    transient String caseFields;
    transient String programFields;
    public Map<String, String> parameterMap;

    public TakeCall_SummaryInfo_Cntrl(){

        parameterMap = ApexPages.currentPage().getParameters();

    	cid = ApexPages.currentPage().getParameters().get('Id');
        
        try{
            disconectCall_SXM = Boolean.valueOf(parameterMap.get('sirius_xm_status'));
        }
        catch(Exception ex){
        
        }

    	settings = CA__c.getOrgDefaults();

    	isLGMProject = false;
    	isHondaProject = false;
    	boostToFTAcura = false;
    	redirectPage = false;
		dispAccNSX = false;
    	jlrNHSTA = false;
        isNHSTA = '--None--';
        nHSTAreason = '--None--';

    	init();

    }

    public PageReference init() {

    	current_call = new Case(Id = cid);
        refresh(); 

        getSelectedProgram();
        getSelectedGPBR();
            

        return null;
    }

    public void refresh() {

            getCaseFields();
            String query = 'Select ' + caseFields + ' CreatedBy.Name, Program__r.Program_Code__c, Dealership__r.Name from Case where Id=\''+current_call.Id+'\' LIMIT 1';
            
            System.debug('REFRESH QUERY: ' + query);
            System.debug('QUERY length: ' + query.length());
            current_call = database.query(query);
        
    }

    public void save() {
        
        errorString=null;
        
        try {
            
            upsert current_call;
            
        } catch(DMLException e) {
            System.debug('Cannot Save Case: ' + e);
            errorString = e.getDmlMessage(0);
        } catch(Exception e) {
            System.debug('Cannot Save Case: ' + e);
            errorString = e.getMessage();
        }

        exitLink=('/'+current_call.Id);

    }

    public PageReference prev() {
    	//redirect to Breakdown Information page
        save();
		if(dispAccNSX == false){
        String redirectLink = '/apex/TakeCall_BreakdownInfo?id=' + current_call.id;

        for(String key: parameterMap.keySet()){
            redirectLink+= '&'+ key + '=' + parameterMap.get(key);
        }

        PageReference pageRef = new PageReference(redirectLink);

        return pageRef;
		}else{
            String redirectLink = '/apex/TakeCall_CustomerInfo?id=' + current_call.id;

            for(String key: parameterMap.keySet()){
                redirectLink+= '&'+ key + '=' + parameterMap.get(key);
            }

            PageReference pageRef1 = new PageReference(redirectLink);

            return pageRef1;
        }
	}

    public void saveAndClose() {

		save();
		if (errorString == null) {
			jlrNHSTA = false;
			exitLink=('/apex/CloseCase?Id='+current_call.Id);
            
			//Redirect to Close Case
			redirectPage = true;
		}

	}

	public void next() { 

        if(selectedProgram.Call_Flow_EHI__c && current_call.Miscellaneous_Call_Reason__c != settings.Call_Flow_US_Call_Transferred__c){
            
        	if ((current_call.GPBR__c == null || current_call.GPBR__c == '') && (current_call.No_GPBR_Reason__c == null || current_call.No_GPBR_Reason__c == '')) {
			    current_call.GPBR__c.adderror('Please enter GPBR or Why is there no GPBR');
			    return;
			}
			if (current_call.Roadside_Ticket__c == null || current_call.Roadside_Ticket__c == '') {
			    current_call.Roadside_Ticket__c.adderror('Please enter Roadside Ticket');
			    return;
			}

        }

        if (current_call.Trouble_Code__c != null && selectedProgram.Call_Flow_JLR__c)
        {

            if (current_call.Trouble_Code__c.containsignorecase('tow')) {
                jlrNHSTA = true;
            }
            else {
                jlrNHSTA = false;
            }
            
        }
        
        if(selectedProgram!=null && selectedProgram.Program_Code__c=='503'){
        	if (current_call.Would_you_like_to_provide_an_email__c == null){
            	current_call.Would_you_like_to_provide_an_email__c.adderror('Please select Yes or No');
			   	return;
            }else if(current_call.Would_you_like_to_provide_an_email__c != null && current_call.Would_you_like_to_provide_an_email__c == 'No'){
            	current_call.Email__c = ''; 				
        	}
        }
        
        save();

        if (((current_call.Program__r.Program_Code__c == '566' || current_call.Program__r.Program_Code__c == '567') && dispAccNSX == false) || Test.isRunningTest()) {
            sendEmailForAcuraNSX();
        }/*else if((current_call.Program__r.Program_Code__c == '566' || current_call.Program__r.Program_Code__c == '567') && dispAccNSX == true){
            sendEmailForAcuraNSXCampaign();
        }*/

        redirectPage = true;
    }

    public void jlrNHSTAsaveSent() {
        if (jlrNHSTA) {
            if (isNHSTA == 'No') {
                current_call.NHSTA_prompt__c = 'No';
            }
            else if (isNHSTA == 'Yes' && nHSTAreason != '--None--') {
                current_call.NHSTA_prompt__c = 'Yes ' + nHSTAreason;
            }
            jlrNHSTA = false;
            update current_call;
        }
    }

    public void getSelectedProgram() {
        isHondaProject = false;

        selectedProgram = new Program__c();
        if(current_call != null && current_call.Program__c != null) {
            getProgramFields();
            String query = 'Select ' + programFields + ', Account__r.Name from Program__c where Id=\''+current_call.Program__c+'\' LIMIT 1';
            
            selectedProgram = database.query(query); 

            if(selectedProgram.Program_Code__c =='560'  || selectedProgram.Program_Code__c =='801'){
        		siriusXMclienCode = 'honda';
        	}
            else if(selectedProgram.Program_Code__c =='563' || selectedProgram.Program_Code__c =='804'){
        		siriusXMclienCode = 'acura';
        	}

            if(selectedProgram.Name.contains('LGM')){
            	isLGMProject = true;
            }
			system.debug('***selectedProgram.Program_Code__c***'+selectedProgram.Program_Code__c);
            system.debug('***current_call.Trouble_Code__c***'+current_call.Trouble_Code__c);
            system.debug('***current_call.Miscellaneous_Call_Reason__c***'+current_call.Miscellaneous_Call_Reason__c);
            if(selectedProgram.Program_Code__c =='566' || selectedProgram.Program_Code__c =='567') {
	        
	            if(current_call.Trouble_Code__c == 'Boost') {
	                boostToFTAcura = true;
	            }else if(current_call.Trouble_Code__c == 'Miscellaneous (Non Dispatch)' && current_call.Miscellaneous_Call_Reason__c == 'NSX campaign'){
                    dispAccNSX = true;
                    //redirectPage = true;
                }
	        }
                
        }
        
        if(selectedProgram.Program_Code__c =='560' || selectedProgram.Program_Code__c =='561' || selectedProgram.Program_Code__c =='562' || selectedProgram.Program_Code__c =='563' || selectedProgram.Program_Code__c =='564' || selectedProgram.Program_Code__c =='565' || selectedProgram.Program_Code__c =='569') {
            isHondaProject = true;  
        }
            
    }

    public void getSelectedGPBR() {

        System.debug('Selecting GPBR: ' + current_call.GPBR__c);
        selectedGPBR = new Account();
        
        if(current_call != null && current_call.GPBR__c != null) {
            String query = 'Select Id, Name, Tow_Exchange_Email_Address__c, Tow_Exchange_Notes__c, Tow_Exchange_Valid_Email__c, Airport_Code__c, Account_Number__c, GPBR__c, Station_ID__c from Account where GPBR__c=\''+current_call.GPBR__c+'\' LIMIT 1';
            try { 
                selectedGPBR = database.query(query); 
                current_call.GPBR__c=selectedGPBR.GPBR__c;
                current_call.Station_ID__c=selectedGPBR.Station_ID__c;
            } catch(Exception e) { 
            }
        }
    }

    public void getProgramFields() {
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);
    }

    public void getCaseFields() {
        caseFields='';
        Set<String> excludeSet = new Set<String>();
        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.Call_Flow_Field_Inclusions.getFields()) {
            caseFields += (f.getFieldPath() + ',');
        }
    }

    public void createRSA_Login() {

        CaseServicesSetting__c css = CaseServicesSetting__c.getOrgDefaults();

        HttpRequest req = new HttpRequest(); 
        req.setEndpoint('https://'+css.Endpoint_Url__c+'/token');
        req.setMethod('POST');
        req.setHeader('Content-Type','application/x-www-form-urlencoded');
        String Body = 'grant_type=password&UserName='+css.token_User_Name__c+'&Password='+ css.token_Password__c; 
        req.setBody(Body); 
        req.setHeader('Content-Length', String.valueOf(Body.length())); 
        Http http = new Http();
        HTTPResponse res;

        if(!test.IsRunningTest()){
            res = http.send(req); 
        } 

        String sessionId;

        if(!test.IsRunningTest()){
            TokenResp r = (TokenResp)JSON.deserialize(res.getBody(), TokenResp.class);
            sessionId = r.access_token; 
        }

        CreateRSALoginJson cls = new CreateRSALoginJson();        
        cls.FirstName = current_call.First_Name__c;
        cls.LastName = current_call.Last_Name__c;
        cls.MobileNumber = current_call.Phone__c;
        cls.Email = current_call.Email__c;
        cls.ProgramCode = current_call.Program__r.Program_Code__c;
        if(current_call.VIN__r.Name == null) {
            cls.Vin = '';
        } else {
            cls.Vin = current_call.VIN__r.Name;
        }
        cls.CaseID = current_call.CaseNumber;
        cls.CaseUniqueID = current_call.id;              

        HttpRequest req2 = new HttpRequest(); 
        req2.setEndpoint('https://'+css.Endpoint_Url__c+'/api/External/CreateUserAccount'); 
        req2.setMethod('POST'); 
        req2.setHeader('Content-Type','application/json');
        req2.setHeader('Authorization','bearer ' + sessionId); 
        req2.setBody(JSON.serialize(cls)); 
        Http http2 = new Http(); 
        HTTPResponse res2;

        if(!test.IsRunningTest()){ 
            res2 = http2.send(req2); 
        } 
        if (res2 != null) {
            errorMsgDebug = res2.getBody();
        }
        if(res2 != null && res2.getBody().contains('Success')) {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'RSA login was created successfully.'));
        } else { 
            if(res2 !=null && res2.getBody().contains('userModel.')) {
                if(res2.getBody().contains('FirstName')) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'- First Name is Required')); 
                }
                if(res2.getBody().contains('LastName')) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'- Last Name is Required'));
                }
                if(res2.getBody().contains('MobileNumber')) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'- Phone is Required'));
                }
                if(res2.getBody().contains('Email')) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'- Email field is Required'));
                }
                if(res2.getBody().contains('ProgramCode')) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'- ProgramCode field is Required'));
                } 
            } else {
                if(res2 !=null && res2.getBody().contains('Invalid ProgramCode')) {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Cannot create RSA login at this time. Invalid Program Code.'));
                } else {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Cannot create RSA login at this time. Try again later or contact your system administrator.'));
                }
            }
        } 
    }

    public void sendEmailForAcuraNSX(){
    	
	    //OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'Melody Razavitoussi'];
	    String msgBody2 = '';
	    //msgBody2 = '<p>Hello Ron,<br/>Service has been dispatched for an engineering vehicle.</p>';

	    if (current_call.PickupStreet__c != NULL || current_call.PickupCity__c != NULL || current_call.PickupProvince__c != NULL || current_call.PickupCountry__c != NULL) {
	        msgBody2 += '<b>Pick up location: </b> ' + (current_call.PickupStreet__c != NULL ? current_call.PickupStreet__c : '') + (current_call.PickupCity__c != NULL ? ', ' + current_call.PickupCity__c : '') + (current_call.PickupProvince__c != NULL ? ', ' + current_call.PickupProvince__c : '') + (current_call.PickupCountry__c != NULL ? ', ' + current_call.PickupCountry__c : '') + '<br/>';
	    }
	    if (current_call.Destination_Name__c != NULL || current_call.Destination_Street__c != NULL || current_call.Destination_City__c != NULL || current_call.Destination_Province__c != NULL || current_call.Destination_Country__c != NULL) {
	        msgBody2 += '<b>Drop off location: </b> ' + (current_call.Destination_Name__c != NULL ? current_call.Destination_Name__c : '') + (current_call.Destination_Street__c != NULL ? ', ' + current_call.Destination_Street__c : '') + (current_call.Destination_City__c != NULL ? ', ' + current_call.Destination_City__c : '') + (current_call.Destination_Province__c != NULL ? ', ' + current_call.Destination_Province__c : '') + (current_call.Destination_Country__c != NULL ? ', ' + current_call.Destination_Country__c : '') + '<br/>';
	    }
	    msgBody2 += '<b>Case: </b>' + current_call.CaseNumber + '<br/>';
	    if (current_call.Tow_Reason__c != '') {
	        msgBody2 += '<b>Issue reported: </b>' + current_call.Tow_Reason__c + '</br>';
	    }
	    if (current_call.VIN_Member_ID__c != '') {
	        msgBody2 += '<b>VIN: </b>' + current_call.VIN_Member_ID__c + '</br>';
	    }
	    if (current_call.First_Name__c != '') {
	        msgBody2 += '<b>First Name: </b>' + current_call.First_Name__c + '</br>';
	    }
	    if (current_call.Last_Name__c != '') {
	        msgBody2 += '<b>Last Name: </b>' + current_call.Last_Name__c + '</br>';
	    }
	    if (current_call.Phone__c != '') {
	        msgBody2 += '<b>Phone Number: </b>' + current_call.Phone__c + '</br>';
	    }
	    if (current_call.Breakdown_Location__latitude__s != null && current_call.Breakdown_Location__longitude__s != null) {
	        msgBody2 += '<b>Breakdown Location: </b>' + current_call.Breakdown_Location__latitude__s + ':' + current_call.Breakdown_Location__longitude__s + '</br>';
	    }
	    if (current_call.City__c != '') {
	        msgBody2 += '<b>City: </b>' + current_call.City__c + '</br>';
	    }
	    if (current_call.Province__c != '') {
	        msgBody2 += '<b>Province: </b>' + current_call.Province__c + '</br>';
	    }
	    if (current_call.Street__c != '') {
	        msgBody2 += '<b>Street: </b>' + current_call.Street__c + '</br>';
	    }
	    if (current_call.Dealership__c != null) {
	        msgBody2 += '<b>Dealership / Tow Destination: </b>' + current_call.Dealership__r.Name + '</p>';
	    }

	    //msgBody2 += '<p>Have a great day, <br/>Jaguar Land Rover roadside assistance.</p>';

	    system.debug('msgBody2:' + msgBody2);
	    Automatic_email_recipients__c emails_addr = Automatic_email_recipients__c.getOrgDefaults();

	    Messaging.SingleEmailMessage mailRon = new Messaging.SingleEmailMessage();
	    String[] toAddresses = new String[] {};
	    emails_addr.Acura_NSX_Emails__c = emails_addr.Acura_NSX_Emails__c == null ? '' : emails_addr.Acura_NSX_Emails__c;
	    for (String em: emails_addr.Acura_NSX_Emails__c.split(',')) {
	        toAddresses.add(em);
	    }
	    mailRon.setToAddresses(toAddresses);
	    mailRon.setSubject('Acura NSX Roadside Assistance Case');
	    mailRon.setPlainTextBody('');
	    mailRon.setHtmlBody(msgBody2);
	    /*if(owea.size() > 0) 
	    {
	    mailRon.setOrgWideEmailAddressId(owea.get(0).Id);
	    } */
	    if (!Test.isRunningTest()) {
	        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
	            mailRon
	        });
	    }

    }
	
	
	public void sendEmailForAcuraNSXCampaign(){
        List<OrgWideEmailAddress> orgwide = [select id, address, displayname from OrgWideEmailAddress where displayname = 'noreply@xperigo.com'];
        String msgBody2 = 'Hello,';
        msgBody2 += '<br/><br/>'+'This is to advise you of that an Acura NSX Recall/Campaign case has been created';
        msgBody2 += '<br/><br/>';
        if (current_call.caseNumber != '') {
            msgBody2 += '<b>Case Number: </b>' + current_call.CaseNumber + '<br/>';
        }
        if (current_call.First_Name__c != '') {
            msgBody2 += '<b>Customer\'s First Name: </b>' + current_call.First_Name__c + '<br/>';
        }
        if (current_call.Last_Name__c != '') {
            msgBody2 += '<b>Customer\'s Last Name: </b>' + current_call.Last_Name__c + '<br/>';
        }
        if (current_call.Phone__c != '') {
            msgBody2 += '<b>Phone: </b>' + current_call.Phone__c + '<br/>';
        }
        if (current_call.VIN_Member_ID__c != null) {
            msgBody2 += '<b>VIN: </b>' + current_call.VIN_Member_ID__c + '<br/>';
        }
        if (current_call.Best_Time_to_Reach__c != null) {
            msgBody2 += '<b>Best Time to reach: </b>' + current_call.Best_Time_to_Reach__c + '<br/>';
        }
        
        msgBody2 += '<br/><br/>'+'If there are any questions, please contact Shannon Simpson at ssimpson@xperigo.com or by phone at 416-988-3884';
        msgBody2 += '<br/><br/>'+'Thank you for your assistance with this customer,';
        msgBody2 += '<br/><br/>'+'Acura NSX Roadside Assistance';
        
        system.debug('msgBody2:' + msgBody2);
        Automatic_email_recipients__c emails_addr = Automatic_email_recipients__c.getOrgDefaults();

        Messaging.SingleEmailMessage mailRon = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {};
        emails_addr.Acura_NSX_Campaign_Emails__c = emails_addr.Acura_NSX_Campaign_Emails__c == null ? '' : emails_addr.Acura_NSX_Campaign_Emails__c;
        for (String em: emails_addr.Acura_NSX_Campaign_Emails__c.split(',')) {
            toAddresses.add(em);
        }
        mailRon.setToAddresses(toAddresses);
        if(orgwide.size() > 0){
            mailRon.setOrgWideEmailAddressId(orgwide[0].Id);
        }
        String[] ccAddresses = new String[] {};
        emails_addr.Acura_NSX_Campaign_CC_Emails__c = emails_addr.Acura_NSX_Campaign_CC_Emails__c == null ? '' : emails_addr.Acura_NSX_Campaign_CC_Emails__c;
        for (String em: emails_addr.Acura_NSX_Campaign_CC_Emails__c.split(',')) {
            ccAddresses.add(em);
        }
        mailRon.setCCAddresses(ccAddresses);
        mailRon.setSubject('Acura NSX Campaign Alert');
        mailRon.setPlainTextBody('');
        mailRon.setHtmlBody(msgBody2);

        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                mailRon
            });
        }

    }
	
    //TB SiriusXM start
    public void terminateSiriusXM_call(){

        if(!disconectCall_SXM){
            String msgerror = '';
                    
            if(current_call.Sirius_XM_Id__c != null){
                    
                String siriusXMclienCode = '';
                        
                if(selectedProgram.Program_Code__c == '560' || selectedProgram.Program_Code__c =='801'){
                        
                    siriusXMclienCode = 'honda';
                            
                }
                else if(selectedProgram.Program_Code__c == '563' || selectedProgram.Program_Code__c =='804'){
                        
                    siriusXMclienCode = 'acura';
                            
                }
                
                if(current_call.Terminate_info__c == null){
                    current_call.Terminate_info__c = '';
                }

                try{
                    
                    SiriusXM_Service.terminate(current_call.Sirius_XM_Id__c,'ReasonCode',siriusXMclienCode,selectedProgram.Program_Code__c);
                    current_call.Terminate_info__c += ' Terminate success';        
                }catch(exception ex){
                    current_call.Terminate_info__c += ' Terminate exception: ' + ex.getMessage() + siriusXMclienCode + current_call.Sirius_XM_Id__c;
                    msgerror = 'Sirius XM Exceptiom :' + ex.getMessage();
                            
                }
                if(cid != null){
                    update current_call;
                }
            }
        }
    }
    //TB SiriusXM end

    @RemoteAction global static List<Account> findGPBRs(string term, String client, String typeOfCase) {

        String searchTerm = '%'+term+'%';
        String searchTerm2 = '*'+term+'*'; 
        System.debug('SPLIT TEST 1: ' + term.toUpperCase().split('[0-9]*[0-9]'));
        System.debug('SPLIT TEST 2: ' + term.toUpperCase().split('[a-zA-Z]*[a-zA-Z]'));
        
        if(term!=null&&term!='') {
            try {
                return [SELECT Id, Name, Account_Number__c, GPBR__c, Station_ID__c from Account where Type='GPBR' AND (GPBR__c LIKE : searchTerm OR Station_ID__c LIKE : searchTerm)];
            } catch(Exception e) {
                System.debug('ERROR FINDING GPBR: ' + e);
            }
        }
        return new List<Account>();
    }

    public class CreateRSALoginJson {
        public String FirstName {get; set;}
        public String ProgramCode {get; set;}
        public String LastName {get; set;}
        public String MobileNumber {get; set;}
        public String Email {get; set;}
        public Boolean IsMarketingOptIn = true;
        public String CaseID {get; set;}
        public String CaseUniqueID {get; set;}     
        public String Vin {get; set;}  
    } 

    public class TokenResp {
        public String access_token {get; set;}
        public String token_type {get; set;}
        public String expires_in {get; set;}
    }
}