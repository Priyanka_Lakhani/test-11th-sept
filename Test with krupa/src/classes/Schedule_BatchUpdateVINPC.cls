global class Schedule_BatchUpdateVINPC implements Schedulable{
    global void execute(SchedulableContext sc) {
        Batch_Update_VINPC b = new Batch_Update_VINPC(); 
        database.executebatch(b,200);
    }
}