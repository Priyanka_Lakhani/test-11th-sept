global without sharing class ClubAuto_LtngSelfRegisterController {

    public ClubAuto_LtngSelfRegisterController() {

    }

    @TestVisible 
    private static boolean isValidPassword(String password, String confirmPassword) {
        return password == confirmPassword;
    }
    
    @TestVisible 
    private static boolean siteAsContainerEnabled(String communityUrl) {
        Auth.AuthConfiguration authConfig = new Auth.AuthConfiguration(communityUrl,'');
        return authConfig.isCommunityUsingSiteAsContainer();
    }
    
    @TestVisible 
    private static void validatePassword(User u, String password, String confirmPassword) {
        if(!Test.isRunningTest()) {
        Site.validatePassword(u, password, confirmPassword);
        }
        return;
    }
    
    @AuraEnabled
    public static String selfRegister(String firstname ,String lastname, String email, String password, String confirmPassword, Boolean includePassword, String language) {
        String startUrl = '';
        String regConfirmUrl = '';
        List<User> usrlist = [Select id , name from user where name = 'API' limit 1];
        List<Network> nwlist = [select id , name from Network where name = 'Claims Portal' limit 1];
        if(nwlist.size() > 0){
            String comURL = '';
            comURL = Network.getLoginUrl(nwlist[0].Id);
            if(comURL.contains('login')){
                comURL = comURL.remove('login');
            }
            startUrl = comURL;
            regConfirmUrl = comURL;
        }
        System.debug('**startUrl**' + startUrl);
        System.debug('**regConfirmUrl**' + regConfirmUrl);
        regConfirmUrl = regConfirmUrl + 'clubautoreg';
        Savepoint sp = null;
        Id rtAccount = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Customer' LIMIT 1].Id;
        String lang = '';//Parameter for the language
        if(language != null){
            lang = language;
            startUrl = startUrl +'?language='+lang;
        }
        //Account acc = [select id, name from Account where name like '%customerGrouping%' order by createddate desc limit 1];
        String accountId = '';
        try {
            sp = Database.setSavepoint();
            
            if (lastname == null || String.isEmpty(lastname)) {
                return Label.Site.lastname_is_required;
            }
            
            if (email == null || String.isEmpty(email)) {
                return Label.Site.email_is_required;
            }
            Account acc = new Account(
                name = firstName + ' ' + lastName,
                recordtypeid = rtAccount,
                type = 'Customer',
                ownerid = usrlist[0].Id
            );
            insert acc;
            accountId = acc.ID;
            system.debug('***AccountID***'+accountId);
            User u = new User();
            u.Username = email;
            u.put('Email',email);
            //u.userroleid = '00Eq0000000qJYl';
            u.FirstName = firstname;
            u.LastName = lastname;
            u.PortalRole = 'PersonAccount';
            u.LanguageLocaleKey = 'en_US';
            u.LocaleSidKey = 'en_CA';
            /*if(lang == 'fr_CA'){
                u.LanguageLocaleKey = 'fr';
                u.LocaleSidKey = 'fr_CA';
            }else if(lang == 'en_US'){
                u.LanguageLocaleKey = 'en_US';
                u.LocaleSidKey = 'en_CA';
            }*/
            String userId = '';
            
            String networkId = Network.getNetworkId();

            // If using site to host the community the user should not hit s1 after logging in from mobile.
            if(networkId != null && siteAsContainerEnabled(Network.getLoginUrl(networkId))) {
                u.put('UserPreferencesHideS1BrowserUI',true);
            }
            
            String nickname = ((firstname != null && firstname.length() > 0) ? firstname.substring(0,1) : '' ) + lastname.substring(0,1);
            nickname += String.valueOf(Crypto.getRandomInteger()).substring(1,7);
            u.put('CommunityNickname', nickname);
                     
                        
            if (includePassword) {    
                if (!isValidPassword(password, confirmPassword)) {
                    return Label.site.passwords_dont_match;
                }
             validatePassword(u, password, confirmPassword);
             
             
            }
            else {
                password = null;
            }
            
            // lastName is a required field on user, but if it isn't specified, we'll default it to the username
            //try{
                userId = Site.createExternalUser(u, accountId, password);
                /*user usr = new user();
                usr.id = userId;
                usr.PortalRole = 'PersonAccount';
                update usr;
                List<Id> usrids = new List<ID>();
                usrids.add(userid);
                if(usrids.size() > 0){
                    FutureMethodRecordProcessing.processRecords(usrids);
                }
            }catch(Exception ex){
                system.debug('***ex***'+ex.getMessage());
                throw new AuraHandledException(ex.getMessage());   
            }*/
            // create a fake userId for test.
            if (Test.isRunningTest()) {
                userId = 'fakeUserId';           
            }
            if (userId != null) { 
                if (password != null && password.length() > 1) {
                    ApexPages.PageReference lgn = Site.login(email, password, startUrl);
                    if(!Test.isRunningTest()) {
                     aura.redirect(lgn);
                    }
                }
                else {
                    ApexPages.PageReference confirmRef = new PageReference(regConfirmUrl);
                    if(!Test.isRunningTest()) {
                    aura.redirect(confirmRef);
                   }

                }
            }
            return null;
        }
        catch (Exception ex) {
            Database.rollback(sp);
            system.debug('***ex***'+ex.getMessage());
            throw new AuraHandledException(ex.getMessage());            
        }
    }

    
    @AuraEnabled
    global static String setExperienceId(String expId) {
        // Return null if there is no error, else it will return the error message 
        try {
            if (expId != null) {
                Site.setExperienceId(expId);   
            }
            return null; 
        } catch (Exception ex) {
            return ex.getMessage();            
        }        
    }  
}