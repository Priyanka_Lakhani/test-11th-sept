public with sharing class LabelTranslator_Controller {
	public String language { get; set; }
	public String label { get; set; }
	
	public LabelTranslator_Controller() {
       Map<String, String> reqParams = ApexPages.currentPage().getParameters(); 
       language = reqParams.get('language');
       label = reqParams.get('label');
	}
}