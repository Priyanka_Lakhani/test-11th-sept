/*
Created Date: Feb 4 2019
Last Modified Date: Feb 4 2019
Last Modified By: I2C Thamizh
Description: Redirects to the dispatch call page
*/
public without sharing class CaseDetailsLtng{

    @AuraEnabled
    public Case cs{get;set;}
    
    @AuraEnabled
    public CA__c CA{get;set;}
    
    @AuraEnabled
    public Boolean Err{get;set;}
    
    @AuraEnabled
    public String ErrMsg{get;set;}
    
    /*private static string getAllFields(String objectAPI){ //Retrieves all field api names for the object
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objectAPI).getDescribe().fields.getMap();
        String strFields = 'Program__r.Call_Flow_EHI__c';
        for(String fieldName : fieldMap.keyset()){
          if(strFields == null || strFields == ''){
            strFields = fieldName;
          }else{
            strFields = strFields + ' , ' + fieldName;
          }
        }
        return strFields;
    }*/
    
    @AuraEnabled
    public static CaseDetailsLtng getCaseDetails(String csId){
        CaseDetailsLtng cd1 = new CaseDetailsLtng();
        cd1.Err = false;
        cd1.ErrMsg = '';
        cd1.cs = new Case();
        cd1.CA = CA__c.getinstance();
        String csQuery = '';
        system.debug('***csId***'+csId);
        system.debug('***cd1.CA***'+cd1.CA);
        try{
            if(csId != null && csId !=''){
                //csQuery = 'Select ' + getAllFields('Case') + ' from Case where Id =:csId';
                csQuery = 'Select id , Authorization_Code__c, Authorization_Required__c , Club_Call_Number__c, Club__c, VIN_Member_ID__c, Current_Odometer__c, programID__c , Country__c, Client_Code__c, First_Name__c, Last_Name__c, Phone__c, Street__c, City__c, Service_call__c, Vehicle_Year__c, Vehicle_Make__c, Vehicle_Model__c, Vehicle_Colour__c , Trouble_Code__c, Wait__c, ETA__c, ETA_Date__c, Call_ID__c, Program__c, Program__r.Call_Flow_EHI__c, Service_is_completed__c, Currency__c, Customer_purchased_RAP_RSP__c, Total_Charges_ve_been_applied_in_EHI_sys__c, Name_of_dealership_contacted__c, Name_of_Advisor_Contacted__c, Number_of_Advisor_Contacted__c, Key_Cost__c, Fob_Cost__c, Key_and_Fob_Duo_Cost__c, Key_Programming_Cost__c, Key_Cutting_Cost__c, Coverage_validatednoted_in_EHI_system__c, Transaction_fee__c, Projected_cost_based_on_km_travel__c, Club_Code__c, JLR_MT_Status__c, Club_Email__c  from Case where Id =:csId';
                cd1.cs = Database.query(csQuery);
                system.debug('***cd1.cs***'+cd1.cs);
            }
        }catch(Exception ex){
            system.debug('***ex***'+ex.getMessage());
            cd1.Err = true;
            cd1.ErrMsg = ex.getMessage();
        }
        return cd1;
    }
    
    @AuraEnabled
    public static CaseDetailsLtng saveKF(string CaseDetailsLtngStr){
        system.debug('*****CaseDetailsLtngStr********'+CaseDetailsLtngStr);
        CaseDetailsLtng cd = (CaseDetailsLtng)JSON.deserialize(CaseDetailsLtngStr, CaseDetailsLtng.class);
        system.debug('***cd.cs***'+cd.cs);
        try{
            if(cd.cs != null && cd.cs.Id !=null){
                update cd.cs;
            }
        }catch(Exception ex){
            system.debug('***ex***'+ex.getMessage());
            cd.Err = true;
            cd.ErrMsg = ex.getMessage();
        }
        return cd;
    }
    
}