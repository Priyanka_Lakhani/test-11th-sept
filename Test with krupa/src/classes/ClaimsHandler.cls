/**
    Created Date: Nov 7 2018
    Last Modified Date: Nov 7 2018
    Last Modified By: Thamizh
    Description: Handler for Claims object
 */
public without sharing class ClaimsHandler implements ITrigger
{   
    // Member variable to hold the Id's of Claims 'in use'
    public Set<Id> m_inUseIds = new Set<Id>();
    public Set<Id> m_updIds = new Set<Id>();
    public Recordtype cpClaimRT{get;set;}
    
    
    // Constructor
    public ClaimsHandler()
    {
        cpClaimRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Club_Auto_Claims' AND SobjectType = 'Expense_Claim__c'];
    }

    public void bulkBefore()
    {
        QueueSObject QueueID = [Select Queue.Id, Queue.Name, Queue.Type from QueueSObject WHERE Queue.Type ='Queue' AND Queue.DeveloperName = 'Claims_Verification_Queue' Limit 1];
        if(Trigger.isInsert){
            for(sObject sobj : Trigger.new){
                Expense_Claim__c claim = (Expense_Claim__c)sobj;
                if(claim.RecordTypeId == cpClaimRT.Id){
                    claim.ownerid = QueueID.Queue.Id;//Assign to verification Queue
                }
            }
        }else if(Trigger.isUpdate){
            for(sObject sobj : Trigger.new){
                Expense_Claim__c claim = (Expense_Claim__c)sobj;
                Expense_Claim__c oldClaim = (Expense_Claim__c)(Trigger.oldMap.get(claim.Id));
                if(claim.ownerId != oldClaim.ownerId && (claim.RecordTypeId == cpClaimRT.Id)){
                    claim.Claim_Status__c = 'Processing';
                }
            }
        }
    }
    
    public void bulkAfter()
    {
        if(Trigger.isInsert){
            for(sObject sobj : Trigger.new){
                Expense_Claim__c claim = (Expense_Claim__c)sobj;
                m_inUseIds.add(claim.Id);
            }
            if(m_inUseIds.size() > 0){
                Claims_TriggerHelper.Claims_ApplicantShareCreation(m_inUseIds);
            }
        }else if(Trigger.isUpdate){
            for(sObject sobj : Trigger.new){
                Expense_Claim__c claim = (Expense_Claim__c)sobj;
                Expense_Claim__c oldClaim = (Expense_Claim__c)(Trigger.oldMap.get(claim.Id));
                if(claim.Claim_Status__c != oldClaim.Claim_Status__c && (claim.RecordTypeId == cpClaimRT.Id)){
                    if(claim.Claim_Status__c != 'Submitted'){
                        m_updIds.add(claim.Id);
                    }
                }
            }
            if(m_updIds.size() > 0){
                Claims_TriggerHelper.Claims_ApplicantShareUpdation(m_updIds);
            }
        }
    }
        
    public void beforeInsert(SObject so)
    {
    }
    
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void beforeDelete(SObject so)
    {   
        // Cast the SObject to an Claim
    }
    
    public void afterInsert(SObject so)
    {
    }
    
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void afterDelete(SObject so)
    {
    }
    
    public void andFinally()
    {
        // insert any audit records
    }
}