public class FlowEntityDependencyExtension {
    public Flow_Entity_Dependency__c currentDependency;
    public Flow_Entity__c dependencyParent {get; set;}
    public Flow_Entity__c dependencyChild {get; set;}
    public List<String> parentValueList {get; set;}
    public List<String> childValueList {get; set;}
    public String jsonString;
    public Map<String, Map<String, Boolean>> jsonMap {get; set;}
    
    public FlowEntityDependencyExtension(ApexPages.StandardController stdController){
        jsonMap = new Map<String, Map<String, Boolean>>();
        Set<String> fieldIdSet = new Set<String>();
        currentDependency = (Flow_Entity_Dependency__c)stdController.getRecord();
        System.debug(currentDependency);

        this.jsonMap = ControllingObject_Utils.deserializePicklistDependencies(currentDependency.Value_Dependencies__c);
        System.debug(this.jsonMap);
        
        //Add related fields ids to the set
        fieldIdSet.add(currentDependency.Dependency_Parent__c);
        fieldIdSet.add(currentDependency.Dependency_Child__c);
        
        //Retrieve related fields for further rendering
        Map<Id, Flow_Entity__c> flowEntityMap = new Map<Id, Flow_Entity__c>();
        flowEntityMap = new Map<Id, Flow_Entity__c>([SELECT Id, Name, Display_Name__c, Picklist_Values__c FROM Flow_Entity__c WHERE Id IN :fieldIdSet]);
        System.debug('[AP] [FlowEntityDependencyExtension] [constructor] flowEntityList = [' + JSON.serialize(flowEntityMap) + ']');
        
        if(flowEntityMap.containsKey(currentDependency.Dependency_Parent__c)){
            dependencyParent = flowEntityMap.get(currentDependency.Dependency_Parent__c);
        }
        if(flowEntityMap.containsKey(currentDependency.Dependency_Child__c)){
            dependencyChild = flowEntityMap.get(currentDependency.Dependency_Child__c);
        }
        
        //Cast Picklist_Values__c field to list of values for further rendering
        parentValueList = dependencyParent.Picklist_Values__c.split('\r\n');
        childValueList = dependencyChild.Picklist_Values__c.split('\r\n');

        if(this.jsonMap.isEmpty()){
            Integer rowRunner = 0;
            for(String currentChildValue : childValueList){
                String rowRunnerString = String.valueOf(rowRunner);
                this.jsonMap.put(rowRunnerString, new Map<String, Boolean>());
                Integer colRunner = 0;
                for(String currentParentValue : parentValueList){
                    String colRunnerString = 'c' + String.valueOf(colRunner+1);
                    this.jsonMap.get(rowRunnerString).put(colRunnerString, false);
                    colRunner++;
                }
                rowRunner++;
            }
        }
    }

    public PageReference goBack() {
        PageReference pg = new PageReference('/' + this.currentDependency.Id);
        pg.setRedirect(true);
        return pg;
    }

    public PageReference saveDependency(){
        this.jsonString = Apexpages.currentPage().getParameters().get('jsonString');
        System.debug(this.jsonString);
        this.currentDependency.Value_Dependencies__c = this.jsonString;
        update currentDependency;
        PageReference pg = new PageReference('/' + this.currentDependency.Id);
        pg.setRedirect(true);
        return pg;
    }
}