@isTest
private class SetGeolocationTest {
    
    @isTest static void test_method_one() {
    
        Id recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Dealership / Tow Destination'].Id;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorGeolocation());
        
        Account acc = new Account(
            Name = 'test',
            RecordTypeId = recordTypeId,
            BillingStreet = '1221 Granville St',
            BillingCity = 'Toronto',
            BillingState = 'Ontario',
            BillingPostalCode = 'V6Z 1M6',
            BillingCountry = 'Canada',
            isLocationUpdated__c = false
        );
        insert acc;
        acc.BillingStreet = '2222 Granville St';
        update acc;
        LocationCallouts.getLocation(acc.id);
        
        SetLocationBatch b = new SetLocationBatch(null);
        Database.executeBatch(b, 10);
        
        
        
        Test.stopTest();
        
    }
    
    @isTest static void test_method_two() {
    
        Id recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Dealership / Tow Destination'].Id;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorGeolocation());
        
        Account acc = new Account(
            Name = 'test',
            RecordTypeId = recordTypeId,
            BillingStreet = '1221 Granville St',
            BillingCity = 'Toronto',
            BillingState = 'Ontario',
            BillingPostalCode = 'V6Z 1M6',
            BillingCountry = 'Canada',
            isLocationUpdated__c = false
        );
        insert acc;
        
        SetLocationBatch b = new SetLocationBatch(null);
        Database.executeBatch(b, 10);
        
        Test.stopTest();
        
    }
    
    @isTest static void three() {
    
        Id recordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Dealership / Tow Destination'].Id;
        
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGeneratorGeolocation());
        
        Account acc = new Account(
            Name = 'test',
            RecordTypeId = recordTypeId,
            BillingStreet = '1221 Granville St',
            BillingCity = 'Toronto',
            BillingState = 'Ontario',
            BillingPostalCode = 'V6Z 1M6',
            BillingCountry = 'Canada',
            isLocationUpdated__c = false
        );
        insert acc;
        acc.BillingStreet = '2222 Granville St';
        update acc;
        
        AccsWithIncorrectLocationBatch b = new AccsWithIncorrectLocationBatch(null);
        Database.executeBatch(b, 70);
        
        Test.stopTest();
        
    }
    
}