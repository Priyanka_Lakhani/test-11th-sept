@isTest
private class TakeCall_SummaryInfo_Cntrl_TEST {

    @testSetup 
    static void setup() {
        CA__c settings = new CA__c(
            Call_Flow_Vehicle_Year_Start__c = 1920,
            Non_Dispatch_Call_Types__c = 'Miscellaneous (Non Dispatch)',
            Last_Case_Number__c = '00000002',
            Call_Flow_Priority_Required__c = 'Unlock',
            Call_Flow_Always_Hazardous__c = 'Anywhere On Highway,Highway Shoulder,On/Off Ramp,Ditch,Car Wash,N\'importe où sur l\'autoroute,Bord de l\'autoroute,Entrée/Sortie de l\'autoroute,Fossé,Lave-auto,',
            Call_Flow_Potentially_Hazardous__c = 'Alley,Side of Road,Parking Lot,Gas Station/At the Pump,"In the back"/Courtyard,Ruelle,Bord de la rue,Stationnement,Station d\'essence/a la pompe d\'essence,A l\'arrière/arrière-cours,Leak / Smoke / Noise, Tow to Safety,',
            Call_Flow_Fire_Hazard__c = 'Leak / Smoke / Noise',CL_Status__c = 'Closed (Call Cleared)', XX_Status__c = 'Call Cancelled'
        );
        insert settings;

        WarrantyOverride__c warrantyOverride = new WarrantyOverride__c(
            Name = 'WarrantyOverride',
            WarrantyEmail__c = 'TestEmail1,TestEmail2'
        );
        insert warrantyOverride;

        List<Account> accList = new List<Account>();
		List<Contact> conList = new List<Contact>();
        List<Program__c> prodList = new List<Program__c>();
        List<VIN__c> vinList = new List<VIN__c>();
        List<Case> caseList = new List<Case>();
        Id rtClient = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Client' LIMIT 1].Id;
        Id rtDealership = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Dealership' LIMIT 1].Id;

//EHI start
        Account client930 = new Account(
            RecordTypeId = rtClient,
            Name = 'Enterprise',
            Account_Number__c = '930',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client930);

        Account towDest930 = new Account(
            RecordTypeId = rtDealership,
            Name = 'test Dealership',
            Status__c = 'Active',
            Client__c = client930.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'Canada',
            BillingStreet = '266 LABELLE BOUL',
            BillingCity = 'SAINTE-ROSE',
            BillingState = 'Quebec',
            BillingPostalCode = 'H7L3A2',
            Location__Latitude__s = 45.59971620,
            Location__Longitude__s = -73.78835360
        );
        accList.add(towDest930);
//EHI end

//NAPA start
        Account client530 = new Account(
            RecordTypeId = rtClient,
            Name = 'Enterprise',
            Account_Number__c = '530',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client530);

        Account towDest530 = new Account(
            RecordTypeId = rtDealership,
            Name = 'test Dealership',
            Status__c = 'Active',
            Client__c = client530.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'Canada',
            BillingStreet = '266 LABELLE BOUL',
            BillingCity = 'SAINTE-ROSE',
            BillingState = 'Quebec',
            BillingPostalCode = 'H7L3A2',
            Location__Latitude__s = 45.59971620,
            Location__Longitude__s = -73.78835360
        );
        accList.add(towDest530);
//Napa end

//NSX Start
            Account client566 = new Account(
            RecordTypeId = rtClient,
            Name = 'AcuraNSX',
            Account_Number__c = '566',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client566);
//NSX End
//LGM start
        Account client503 = new Account(
            RecordTypeId = rtClient,
            Name = 'KIA Motors',
            Account_Number__c = '503',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client503);

        Account towDest503 = new Account(
            RecordTypeId = rtDealership,
            Name = 'test Dealership',
            Status__c = 'Active',
            Client__c = client503.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'Canada',
            BillingStreet = '266 LABELLE BOUL',
            BillingCity = 'SAINTE-ROSE',
            BillingState = 'Quebec',
            BillingPostalCode = 'H7L3A2',
            Location__Latitude__s = 45.59971620,
            Location__Longitude__s = -73.78835360
        );
        accList.add(towDest503);
//LGM end      

//Mitsubishi start
        Account client542 = new Account(
            RecordTypeId = rtClient,
            Name = 'Mitsubishi Motor Sales of Canada, Inc.',
            Account_Number__c = '542',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client542);

        Account towDest542 = new Account(
            RecordTypeId = rtDealership,
            Name = 'test Dealership',
            Status__c = 'Active',
            Client__c = client542.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'Canada',
            BillingStreet = '266 LABELLE BOUL',
            BillingCity = 'SAINTE-ROSE',
            BillingState = 'Quebec',
            BillingPostalCode = 'H7L3A2',
            Location__Latitude__s = 45.59971620,
            Location__Longitude__s = -73.78835360
        );
        accList.add(towDest542);
//Mitsubishi end   

//HONDA start
        Account client560 = new Account(
            RecordTypeId = rtClient,
            Name = 'Honda Canada Inc.',
            Account_Number__c = '560',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client560);

        Account towDest560 = new Account(
            RecordTypeId = rtDealership,
            Name = 'test Dealership',
            Status__c = 'Active',
            Client__c = client560.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'Canada',
            BillingStreet = '266 LABELLE BOUL',
            BillingCity = 'SAINTE-ROSE',
            BillingState = 'Quebec',
            BillingPostalCode = 'H7L3A2',
            Location__Latitude__s = 45.59971620,
            Location__Longitude__s = -73.78835360,
            GPBR__c = 'TestHonda'
        );
        accList.add(towDest560);
//HONDA end        

//JAGUAR start
        Account client861 = new Account(
            RecordTypeId = rtClient,
            Name = 'Jaguar Land Rover ULC',
            Account_Number__c = '861',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client861);

        Account towDest861 = new Account(
            RecordTypeId = rtDealership,
            Name = 'Jaguar Anaheim Hills',
            Status__c = 'Active',
            Client__c = client861.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'United States',
            BillingStreet = '5425 E La Palma Ave',
            BillingCity = 'Anaheim',
            BillingState = 'California',
            BillingPostalCode = '92807-2022',
            Location__Latitude__s = 33.85971,
            Location__Longitude__s = -117.79741
        );
        accList.add(towDest861);
//JAGUAR end        

        insert accList;
//NSX Start
		List<Account> acclist1 = [Select id, name from Account where name = 'AcuraNSX'];
        if(acclist1.size() > 0){
			Contact con566 = new Contact();
			con566.lastname = 'ACURA NSX';
			con566.firstname = 'client';
			con566.email = 'test@testxpg.com';
			con566.accountid = acclist1[0].Id;
			con566.Contact_External__c='DevelopmentContact_DoNotDelete006';
			conList.add(con566);
		}
		insert conList;
		
//NSX End

//EHI start
        Program__c prog930 = new Program__c(
            Account__c = client930.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = '930 - Enterprise [Base]',
            Program_Code__c = '930',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            Unattended_Tow__c = true,
            CTI_Search_Terms__c = 'Enterprise,ERACHomeCity',
            Client_Name_for_Club__c = 'Enterprise',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'LGM MP Financial Services Inc.',
            Client_Name_French__c = 'Groupe Financier LGM',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Call_Flow_License_Plate_Required__c = true,
            Call_Flow_Tow_Exchange_Required__c = true,
            Call_Flow_EHI__c = true
            //Base_Warranty_Service_Limit_Term__c
        );
        prodList.add(prog930);
//EHI end             

//NAPA start
        Program__c prog530 = new Program__c(
            Account__c = client930.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = '530 - (NAPA) UAP Inc [Base]',
            Program_Code__c = '530',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            Unattended_Tow__c = true,
            CTI_Search_Terms__c = 'Enterprise,ERACHomeCity',
            Client_Name_for_Club__c = 'Enterprise',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'NAPA roadside assistance',
            Client_Name_French__c = 'NAPA roadside assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Base_Warranty_Service_Limit_Term__c = 6,
            Base_Warranty_Service_Limit__c = 3,
            Call_Flow_EHI__c = false
        );
        prodList.add(prog530);
//NAPA end

//NSX start
        Program__c prog566 = new Program__c(
            Account__c = client566.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = '566 - (Acura) NSX Inc [Base]',
            Program_Code__c = '566',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            Unattended_Tow__c = true,
            CTI_Search_Terms__c = 'Enterprise,ERACHomeCity',
            Client_Name_for_Club__c = 'Enterprise',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'NAPA roadside assistance',
            Client_Name_French__c = 'NAPA roadside assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Base_Warranty_Service_Limit_Term__c = 6,
            Base_Warranty_Service_Limit__c = 3,
            Call_Flow_EHI__c = false
        );
        prodList.add(prog566);
//NSX end         

//LGM start
        Program__c prog503 = new Program__c(
            Account__c = client503.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = 'KIA Base',
            Program_Code__c = '503',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            Unattended_Tow__c = true,
            CTI_Search_Terms__c = 'LGM Kia,Kia Extended',
            Client_Name_for_Club__c = 'LGM - Kia Protect',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'LGM MP Financial Services Inc.',
            Client_Name_French__c = 'Groupe Financier LGM',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2'
            //Base_Warranty_Service_Limit_Term__c
        );
        prodList.add(prog503);

        Program__c prog585 = new Program__c(
            Account__c = client503.Id,
            Description__c = 'Extended',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = 'LGM - Kia MP',
            Program_Code__c = '585',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            Unattended_Tow__c = true,
            CTI_Search_Terms__c = 'LGM Kia,Kia Extended',
            Client_Name_for_Club__c = 'LGM - Kia Protect',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'LGM MP Financial Services Inc.',
            Client_Name_French__c = 'Groupe Financier LGM',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2'
            //Base_Warranty_Service_Limit_Term__c
        );
        prodList.add(prog585);
//LGM end        

//HONDA start
        Program__c prog560 = new Program__c(
            Account__c = client560.Id,
            Description__c = 'Base',
            Type__c = 'Underwritten',
            Status__c = 'Active',
            Name = '560 - Honda Canada Inc. [Base]',
            Program_Code__c = '560',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            CTI_Search_Terms__c = 'Honda',
            Client_Name_for_Club__c = 'Honda',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'Honda Canada Roadside Assistance',
            Client_Name_French__c = 'Honda Canada Roadside Assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2'
            //Base_Warranty_Service_Limit_Term__c
        );
        prodList.add(prog560);
//HONDA end

//Mitsubishi start
        Program__c prog542 = new Program__c(
            Account__c = client542.Id,
            Description__c = 'Base',
            Type__c = 'Underwritten',
            Status__c = 'Active',
            Name = '542 - Mitsubishi Motors Canada [Base]',
            Program_Code__c = '542',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            CTI_Search_Terms__c = 'Mitsubishi',
            Client_Name_for_Club__c = '',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'Mitsubishi Roadside Assistance',
            Client_Name_French__c = 'Mitsubishi Roadside Assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Power_Train_EV_Warranty_Mileage_Limit__c = 100000,
            Power_Train_EV_Warranty_Year_Limit__c = 5,
            Power_Train_Warranty_Mileage_Limit__c = 160000,
            Power_Train_Warranty_Year_Limit__c = 10,
            T4_Limit__c = 3,
            T4_Limit_Term__c = 0,
            Tow_Due_to_Recall_Option__c = true,
            NotifyExcessiveUsg__c = true
            //Base_Warranty_Service_Limit_Term__c
        );
        prodList.add(prog542);
//Mitsubishi end

//JAGUAR start
        Program__c prog858 = new Program__c(
            Account__c = client861.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = '858 - Jaguar US Base',
            Program_Code__c = '858',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T4_Limit__c = 3,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            T11__c = true,
            CTI_Search_Terms__c = 'JaguarUS,JagTele',
            Client_Name_for_Club__c = '',
            VIN_Help_Text__c = 'SAJ',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'JLR Roadside Assistance',
            Client_Name_French__c = 'JLR Roadside Assistance',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'Reunite,Re-Acquired Vehicle',
            Call_Flow_JLR__c = true,
            Base_Warranty_Service_Limit_Term__c = 5
        );
        prodList.add(prog858);
//JAGUAR end

        insert prodList;

//LGM start
        VIN__c vin503 = new VIN__c(
            Name = 'KIA11222223333344',
            UID__c = 'KIA11222223333344',
            Program__c = prog503.Id,
            Year__c = '2018',
            Make__c = 'KIA',
            Model__c = 'Rio',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            Extended_Warranty_Start__c = Date.today().addMonths(-4),
            Extended_Warranty_End__c = Date.today().addMonths(-1),
            Extend_Service_Limit__c = 6,
            Extended_Warranty_Max_Odometer__c = 100000,
            Promo_Start__c = Date.today().addMonths(-3),
            Promo_End__c = Date.today().addMonths(-1),
            Promo_Max_Odometer__c = 100000,
            Promo_Term__c = '6',
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123'
        );
        vinList.add(vin503);
//LGM end        

//Acura NSX
VIN__c vin566 = new VIN__c(
            Name = 'ACU11222223333344',
            UID__c = 'ACU11222223333344',
            Program__c = prog566.Id,
            Year__c = '2018',
            Make__c = 'ACURA',
            Model__c = 'Rio',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            Extended_Warranty_Start__c = Date.today().addMonths(-4),
            Extended_Warranty_End__c = Date.today().addMonths(-1),
            Extend_Service_Limit__c = 6,
            Extended_Warranty_Max_Odometer__c = 100000,
            Promo_Start__c = Date.today().addMonths(-3),
            Promo_End__c = Date.today().addMonths(-1),
            Promo_Max_Odometer__c = 100000,
            Promo_Term__c = '6',
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123'
        );
        vinList.add(vin566);
//ACURA NSX END
//HONDA start
        VIN__c vin560 = new VIN__c(
            Name = '11111222223333344',
            UID__c = '11111222223333344',
            Program__c = prog560.Id,
            Year__c = '2018',
            Make__c = 'HONDA',
            Model__c = 'ODYSSEY',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            EV__c = true,
            Extra_Coverage__c = 'OIL CHANGE'
        );
        vinList.add(vin560);
//HONDA end

//Mitsubishi start
        VIN__c vin542 = new VIN__c(
            Name = 'MIT11222223333344',
            UID__c = 'MIT11222223333344',
            Program__c = prog542.Id,
            Extended_Program__c = prog542.Id,
            Promo_Program__c = prog542.Id,
            Year__c = '2018',
            Make__c = 'HONDA',
            Model__c = 'phev',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-7),
            Base_Warranty_End__c = Date.today().addMonths(-1),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            Extended_Warranty_Start__c = Date.today().addMonths(-4),
            Extended_Warranty_End__c = Date.today().addMonths(-1),
            Extend_Service_Limit__c = 6,
            Extended_Warranty_Max_Odometer__c = 100000,
            Promo_Start__c = Date.today().addMonths(-3),
            Promo_End__c = Date.today().addMonths(-1),
            Promo_Max_Odometer__c = 100000,
            Promo_Term__c = '6',
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            EV__c = true
        );
        vinList.add(vin542);
//Mitsubishi end

//JAGUAR start
        VIN__c vin858 = new VIN__c(
            Name = 'JLR11222223333344',
            UID__c = 'JLR11222223333344',
            Program__c = prog858.Id,
            Year__c = '2018',
            Make__c = 'Jaguar',
            Model__c = '111',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            JLREngineeringVehicle__c = true
        );
        vinList.add(vin858);
//JAGUAR end

//EHI start
        VIN__c vin930 = new VIN__c(
            Name = 'EHI11222223333344',
            UID__c = 'EHI11222223333344',
            Program__c = prog930.Id,
            Year__c = '2018',
            Make__c = 'Jaguar',
            Model__c = '111',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            License__c = 'Test'
        );
        vinList.add(vin930);
//EHI end

//NAPA start
        VIN__c vin530 = new VIN__c(
            Name = 'NAPA1222223333344',
            UID__c = 'NAPA1222223333344',
            Program__c = prog530.Id,
            Year__c = '2018',
            Make__c = 'Jaguar',
            Model__c = '111',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addDays(-3),
            Base_Warranty_End__c = Date.today().addMonths(6),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123'
        );
        vinList.add(vin530);
//NAPA end

        insert vinList;

//LGM start
        VIN_Program_Policy__c policy503 = new VIN_Program_Policy__c(
            Name = 'Test Policy',
            Program__c = prog503.Id,
            VIN__c = vin503.Id,
            Policy_number__c = '1234567890',
            Policy_Status__c = true,
            Warranty_Start__c = Date.today().addMonths(-3),
            Warranty_End__c = Date.today().addMonths(3),
            Warranty_Term__c = 6,
            Warranty_Max_Odometer__c = 100000
        );    
        insert policy503;
//LGM end

        Id rtRoadsideAssistance = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'Roadside_Assistance' LIMIT 1].Id;

        Case existCase560 = new Case(
            Program__c = prog560.Id,
            RecordTypeId = rtRoadsideAssistance,
            VIN__c = vin560.Id,
            VIN_Member_ID__c = '11111222223333344',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c = 'Engine Stalled',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now()
        );
        caseList.add(existCase560);

        Case existCase858 = new Case(
            Program__c = prog858.Id,
            RecordTypeId = rtRoadsideAssistance,
            VIN__c = vin858.Id,
            VIN_Member_ID__c = 'JLR11222223333344',
            Trouble_Code__c = 'Winch',
            //Tow_Reason__c = 'Engine Stalled',
            Club_Call_Number__c = '123',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now()
        );
        caseList.add(existCase858);

        Case exist1Case858 = new Case(
            Program__c = prog858.Id,
            RecordTypeId = rtRoadsideAssistance,
            VIN__c = vin858.Id,
            VIN_Member_ID__c = 'JLR11222223333344',
            Trouble_Code__c = 'FlatBed Tow',
            Tow_Reason__c = 'Engine Stalled',
            Club_Call_Number__c = '123',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now()
        );
        caseList.add(exist1Case858);

        Case exist1Case503 = new Case(
            Program__c = prog503.Id,
            RecordTypeId = rtRoadsideAssistance,
            VIN__c = vin503.Id,
            VIN_Member_ID__c = 'KIA11222223333344',
            Trouble_Code__c = 'Gas',
            Club_Call_Number__c = '123',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now()
        );
        caseList.add(exist1Case503);
        Case exist1Case566 = new Case(
            Program__c = prog566.Id,
            RecordTypeId = rtRoadsideAssistance,
            VIN__c = vin566.Id,
            VIN_Member_ID__c = 'ACU11222223333344',
            Trouble_Code__c = 'Miscellaneous (Non Dispatch)',
            Club_Call_Number__c = '123',
            Miscellaneous_Call_Reason__c = 'NSX campaign',
            Status = 'Spotted',
            RE_Status_Date_Time__c = System.now()
        );
        
        caseList.add(exist1Case566);
        
        insert caseList;
    }
    
    
    @isTest static void takeCall_SummaryInfo_Cntrl_Honda() {
        Test.startTest();
            Id cid = [SELECT Id, VIN_Member_ID__c FROM Case WHERE VIN_Member_ID__c = '11111222223333344' LIMIT 1].Id;
            ApexPages.currentPage().getParameters().put('Id', cid);
            ApexPages.currentPage().getParameters().put('Ref_id', '1234567');
            ApexPages.currentPage().getParameters().put('Client_Code__c', 'siriusxm_honda');
            ApexPages.currentPage().getParameters().put('sirius_xm_status', 'false');

            TakeCall_SummaryInfo_Cntrl tsc = new TakeCall_SummaryInfo_Cntrl();
            tsc.createRSA_Login();
            tsc.current_call.Sirius_XM_Id__c = '1234';

            tsc.terminateSiriusXM_call();
            tsc.current_call.GPBR__c = 'TestHonda';
            tsc.getSelectedGPBR();
            tsc.saveAndClose();
        Test.stopTest();
    }

    static testMethod void takeCall_CustomerInfo_newCall_JLR() {    
        Test.startTest();
            Id cid = [SELECT Id, VIN_Member_ID__c FROM Case WHERE VIN_Member_ID__c = 'JLR11222223333344' AND Trouble_Code__c = 'Winch' LIMIT 1].Id;
            ApexPages.currentPage().getParameters().put('Id', cid);
            TakeCall_SummaryInfo_Cntrl tsc = new TakeCall_SummaryInfo_Cntrl();
            tsc.prev();
            tsc.next();
            tsc.jlrNHSTA = true;
            tsc.isNHSTA = 'No';
            tsc.jlrNHSTAsaveSent();
            tsc.jlrNHSTA = true;
            tsc.isNHSTA = 'Yes';
            tsc.nHSTAreason = 'Test';
            tsc.jlrNHSTAsaveSent();

            TakeCall_SummaryInfo_Cntrl.findGPBRs('term', 'client', 'typeOfCase');
        Test.stopTest();
    }
    static testMethod void takeCall_CustomerInfo_newCall_KIA() {    
        Test.startTest();
            Id cid = [SELECT Id, VIN_Member_ID__c FROM Case WHERE VIN_Member_ID__c = 'KIA11222223333344' LIMIT 1].Id;
            ApexPages.currentPage().getParameters().put('Id', cid);
            TakeCall_SummaryInfo_Cntrl tsc = new TakeCall_SummaryInfo_Cntrl();
            tsc.next();
            tsc.current_call.Email__c='abc@xyz.com';
            tsc.current_call.Would_you_like_to_provide_an_email__c = 'No';
            tsc.next();
        Test.stopTest();
    }
    static testMethod void takeCall_CustomerInfo_newCall_ACU() {    
        Test.startTest();
            Case c = [SELECT Id, VIN_Member_ID__c FROM Case WHERE VIN_Member_ID__c = 'ACU11222223333344' LIMIT 1];
            Id cid = [SELECT Id, VIN_Member_ID__c FROM Case WHERE VIN_Member_ID__c = 'ACU11222223333344' LIMIT 1].Id;
            ApexPages.currentPage().getParameters().put('Id', cid);
            TakeCall_SummaryInfo_Cntrl tsc = new TakeCall_SummaryInfo_Cntrl();
            
            tsc.next();
            tsc.current_call.Email__c='abc@xyz.com';
            tsc.current_call.Would_you_like_to_provide_an_email__c = 'No';
            tsc.next();
            tsc.sendEmailForAcuraNSXCampaign();
            ApexPages.currentPage().getParameters().put('Id', c.Id);
            CloseCase cc = new CloseCase(new ApexPages.StandardController(c));
            cc.save();
            cc.saveAndRoadside();
        Test.stopTest();
    }
}