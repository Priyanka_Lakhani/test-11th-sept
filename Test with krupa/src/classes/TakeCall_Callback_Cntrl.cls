public without sharing class TakeCall_Callback_Cntrl{
    
    public String errorString { get; set; }
	public String caseNumberParam {get;set;}
	public Case redirectedCase {get;set;}
	public String callbackToCancelOrUpdate {get;set;}
	public String isYourVehicleJLR {get;set;}
	public String isYouWaitingJLR {get;set;}
	public String cid;
	public String exitLink;
	public Program__c selectedProgram { get; set; }
	public List<SelectOption> programs { get; set; }
	public String program_code { get; set; }
	public boolean isLGMProject {get;set;}
    //TB TR 673 start
    public String doYouWantCloseTheLoop {get;set;}
    //TB TR 673 end
	transient String caseFields;

	transient String programFields;

    public TakeCall_Callback_Cntrl(){

    	isLGMProject = false;
    	callbackToCancelOrUpdate = '--None--';
    	isYourVehicleJLR = '--None--';
    	isYouWaitingJLR = '--None--';
        //TR 673 start
        doYouWantCloseTheLoop = '--None--';
        //TR 673 end
    	try { program_code = ApexPages.currentPage().getParameters().get('Client_Code__c'); } catch(Exception e) { program_code=null; }


    	caseNumberParam = ApexPages.currentPage().getParameters().get('case_number');
        cid = ApexPages.currentPage().getParameters().get('Id');

    	init();

    }

    public void init(){

    	getCaseFields();
        String query = 'Select ' + caseFields + ' CreatedBy.Name, Program__r.Program_Code__c, Dealership__r.Name from Case where Id=\''+ cid +'\' LIMIT 1';

        redirectedCase = database.query(query);

        getSelectedProgram();

    }

    public PageReference next() {

		if(callbackToCancelOrUpdate == 'Cancel'){

			exitLink = ('/apex/CloseCase?r=1&Id='+redirectedCase.Id); 
            
		}
		else{
			//close the loop call back start
		    if (caseNumberParam != '') {
                if(doYouWantCloseTheLoop == 'Yes'){
                    redirectedCase.Close_the_loop_option__c = 'No';
		            update redirectedCase;
                }
		    }
		    exitLink = '/' + redirectedCase.Id;
		    //close the loop call back end
		}
        
        PageReference pageRef = new PageReference(exitLink);

    	return pageRef;
    }

    public PageReference saveAndClose() {
        
        exitLink=('/apex/CloseCase?Id='+redirectedCase.Id);

        PageReference pageRef = new PageReference(exitLink);

        return pageRef;
        
    }

    public void getSelectedProgram() {

    	programs = new List<SelectOption>();

        selectedProgram = new Program__c();
        if(redirectedCase != null && redirectedCase.Program__c!=null) {
            getProgramFields();
            String query = 'Select ' + programFields + ', Account__r.Name from Program__c where Id=\''+redirectedCase.Program__c+'\' LIMIT 1';
            try { 

                selectedProgram = database.query(query); 
                
                programs.add(new SelectOption(selectedProgram.Id, selectedProgram.Call_Flow_Name__c));
                
            } catch(Exception e) {
            }

            if(selectedProgram.Name.contains('LGM')) {               
	            isLGMProject = true;  
        	}
        }
    }

    public void getProgramFields() {
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);
    }

    public void getCaseFields() {
        caseFields='';
        Set<String> excludeSet = new Set<String>();
        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.Call_Flow_Field_Inclusions.getFields()) {
            caseFields += (f.getFieldPath() + ',');
        }
    }
    
}