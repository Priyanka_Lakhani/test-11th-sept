public with sharing class EHIEmailDriveInExchange {
    private static list<string> getGPBREmails(string gpbr)
	{
	    list<string> emails = new list<string>();
        try
        {
            string bemail =  [select Tow_Exchange_Email_Address__c from account where gpbr__c = :gpbr and status__c != 'Inactive' limit 1].Tow_Exchange_Email_Address__c;
            string[] bemails = bemail.split(';');
            for (string st : bemails)
            {
            	if (st.contains('@'))
            	{
        			emails.add(st);
            	}
            }
        }
        catch(System.exception ex){}
		return emails;
	}
	
     @InvocableMethod(label='EmailDriveInExchange' description='EmailDriveInExchange')
    public static void EmailDriveInExchange(List<Case> cs)
    {
    	Case c = cs[0];

		try
		{    	
	        list<string> emails1 = getGPBREmails(c.gpbr__c);
	        list<string> emails2 = getGPBREmails(c.driveingpbr__c); //getNearestGPBREmail(c.Breakdown_Location__Latitude__s,c.Breakdown_Location__Longitude__s,getGPBRClientId(c.gpbr__c)); //getGPBREmails(c.VehRecoveryGPBR__c);
			string[] demails = CA__c.getOrgDefaults().EHIDriveInExchangeEmail__c.split(';');
        	for (string st : demails)
        		emails2.add(st);
	
			emails2.add(UserInfo.getUserEmail());
	
	 		Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
	 		
	        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
	
	        mail.setToAddresses(emails1);
	        mail.setCcAddresses(emails2);
	        
	        Id templateId;    
	        try {
	            templateId = [select id, name from EmailTemplate where developername = 'Miscellaneous_Calls_Drive_In_Exchange'].id;
	        }catch (Exception e) {}    
		    	
	    	mail.setReplyTo('noreply@salesforce.com');
		    mail.SetSenderDisplayName ('Xperigo');
			mail.setTemplateID(templateId);
		    mail.setWhatId(c.id);
			mail.setSaveAsActivity(true);
			mail.setTargetObjectId(ctc.id);
			mail.setTreatTargetObjectAsRecipient(false);  
		    
		    Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage> {mail};
		    if(!Test.isRunningTest()) {
		        Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
		    }
		}
		catch(System.exception ex){}
    }         
  
	
}