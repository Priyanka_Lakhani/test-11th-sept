@isTest
private class TEST_JLR_CTL_CalloutClass {
    @isTest static void testCloseTheLoopProcess() {
    
        CA__c settings = new CA__c();
        settings.Last_Case_Number__c = '12345';
        insert settings;
        
        JLR_Close_The_Loop__c jlrSet = new JLR_Close_The_Loop__c();
        jlrSet.Endpoint__c = 'http://dev-clientclosetheloop.azurewebsites.net/closetheloop/initialcall';
        jlrSet.User_Name__c = 'closetheloop';
        jlrSet.Password__c = 'CLOSETHELOOP123$';
        insert jlrSet;
    
        Account club = new Account(Name='Club', Account_Number__c='Club', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        Account tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1
        );
        insert tow;
        
        Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            Power_Train_EV_Warranty_Year_Limit__c = 5,
            Power_Train_EV_Warranty_Mileage_Limit__c = 100000,
            Power_Train_Warranty_Year_Limit__c = 10,
            Power_Train_Warranty_Mileage_Limit__c = 160000,
            T4_Limit__c = 3
        );
        insert prog;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-6),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        Case c = new Case(
            Status= 'Received',
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Phone__c='123',
            Call_ID__c='ABC',
            Close_the_loop_option__c = 'Yes'
            
        );
        insert c;
        
        Close_the_Loop__c ctl = new Close_the_Loop__c(
            Case__c = c.id,
            ETA__c = -1,
            ETA_total__c = -1
        );
        insert ctl;
        /*
        WHERE ETA_total__c != 0 AND ' +
                               ' ETA_total__c != null AND ' +
                               ' Close_the_loop_Parent__c = null AND ' +
                               ' Response__c = null AND ' +
                               ' Request_Has_Been_Sent__c = false AND ' +
                               ' Case__r.Close_the_loop_option__c = \'Yes\' ' +
                         ' ORDER BY CreatedDate DESC ';
        */
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());
            database.executebatch(new JLR_CloseTheLoopBatchable(),20); 
        Test.stopTest();
        
    }
}