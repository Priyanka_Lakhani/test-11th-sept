/**
    Created Date: Nov 7 2018
    Last Modified Date: Nov 7 2018
    Last Modified By: Thamizh
    Description: Methods to handle trigger exception.
*/
public class TriggerException extends Exception {}