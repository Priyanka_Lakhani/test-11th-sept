public class RestCallouts{
    public static Case c{get;set;}
    public static String getJLRBreakDownData(Id caseId) {
        try{
            system.debug('Case ID' +caseId);
            Case cc=[select CaseNumber, CreatedDate, Country__c, Breakdown_Location__latitude__s,Breakdown_Location__longitude__s, JLR_Breakdown_Notes__c , ETA__c, Arrival_Minutes__c, RE_Status_Date_Time__c, CL_Status_Date_Time__c, DI_Status_Date_Time__c, OL_Status_Date_Time__c,TW_Status_Date_Time__c, Program__r.Program_Code__c, Status, Vehicle_Make__c, Vehicle_Model__c, Current_Odometer__c, VIN_Member_ID__c, First_Name__c, Last_Name__c, Email__c, Trouble_code__c,T_Code__c,Tow_Reason__c,Club__c,Club__r.account_number__c,Club__r.name,Phone__c,Contact_Policy__c from case where id=:caseId];
            /*if(cc.Club__c != null && cc.Club__r.account_number__c != null){
                if(cc.Club__r.account_number__c == '998')
                    cc.Club__r.name = 'JLR Mobile Technician';
                else if(cc.Club__r.account_number__c == '999')
                    cc.Club__r.name = 'AAA';
                else{
                    cc.Club__r.name = 'CAA';
                }
            }*/
            if(cc.Contact_Policy__c != null){
	            List<Contact> listOfContacts = [select Salutation,FirstName,LastName,Email,Phone,MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,Street_2__c from Contact where Policy__c=:cc.Contact_Policy__c limit 1];
	            if(listOfContacts.size()>0)
	                cc.Contact = listOfContacts[0];
            }
            // update JSON - moved contact fields from inner contact list to case directly
               
            Map<String, Object> caseFieldsAsMap = cc.getPopulatedFieldsAsMap();
            Map<String, Object> contactMap = new Map<String, Object>();
            Map<String, Object> finalMapToSend = new Map<String, Object>();
            
            for (String fieldName : caseFieldsAsMap.keySet()){
                if(fieldName.equalsIgnoreCase('Contact')){
                    contactMap = ((Contact)caseFieldsAsMap.get(fieldName)).getPopulatedFieldsAsMap();
                }
                if(caseFieldsAsMap.get(fieldName) instanceOf DateTime){
                    String formattedDT = ((DateTime)caseFieldsAsMap.get(fieldName)).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                    finalMapToSend.put(fieldName,formattedDT);
                }else{
                    finalMapToSend.put(fieldName,caseFieldsAsMap.get(fieldName));
                }
            }
            if(finalMapToSend.containsKey('Contact'))
                finalMapToSend.remove('Contact');
            for (String fieldName : contactMap.keySet()){
                if(fieldName != 'Id' && fieldName != 'RecordTypeId')
                    finalMapToSend.put(fieldName,contactMap.get(fieldName)); 
            }
           
            system.debug('without pretty'+JSON.serialize(finalMapToSend));
            String JsonString=JSON.serializePretty(finalMapToSend); 
            return JsonString;
        }catch(System.Exception ex){System.debug(ex);}
        return null;
    }
    public static String getMercedesBenzBreakDownData(Id caseId) {
        try{
            system.debug('Case ID' +caseId);
            Case cc=[select CaseNumber, clientcasenumber__c, Current_Odometer__c, status, DI_Status_Date_Time__c, OL_Status_Date_Time__c, ER_Status_Date_Time__c, CL_Status_Date_Time__c,XX_Status_Date_Time__c,TW_Status_Date_Time__c, ETA__c,Trouble_code__c,Close_Reason_Non_Roadside__c,MBCProcessStep__c,Destination_Id__c,Destination_Name__c,Club_Code__c,Club__r.Name,country__c from case where id=:caseId];
            if(cc.country__c == 'US' && cc.Status == CA__c.getOrgDefaults().DI_Status__c){
                String dispatchedStatus = cc.Status + ' - ' + cc.Club_code__c + ' (' + cc.Club__r.Name + ')';
                cc.Status = dispatchedStatus;
            }
            system.debug('without pretty'+JSON.serialize(cc));
            String JsonString=JSON.serialize(cc);
            return JsonString;
        }catch(System.Exception ex){System.debug(ex);}
        return null;
    }
    public static void sendNotDispatchEmailToMBC(Case c){
        try{
            Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
            Automatic_email_recipients__c emails_addr = Automatic_email_recipients__c.getOrgDefaults();                     
            String[] toAddresses = new String[]{};
            emails_addr.MBC_Emails__c = emails_addr.MBC_Emails__c== null ? '' : emails_addr.MBC_Emails__c;
            for(String em : emails_addr.MBC_Emails__c.split(',')){
                toAddresses.add(em);
            }
            Id templateId;    
	        try {
	            templateId = [select id, name from EmailTemplate where developername = 'MBC_Automatic_Not_Dispatch_Email'].id;
	        }catch (Exception e) {}
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@xperigo.com'];
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    		
            mail.setToAddresses(toAddresses);
            mail.setTemplateId(templateId);
            mail.setWhatId(c.id);
            mail.setSaveAsActivity(true); 
            mail.setTargetObjectId(ctc.id);
            mail.setTreatTargetObjectAsRecipient(false);         
            mail.setUseSignature(false);
            if(owea.size() > 0) 
            {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }      
            List<Messaging.SingleEmailMessage> lstMsgs = new List<Messaging.SingleEmailMessage>();        
            lstMsgs.add(mail);               
                    
            Savepoint sp = Database.setSavepoint();
            
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(lstMsgs);
            }
            
            Database.rollback(sp);
            //have to do trick with email template. 
            //it has to set targetobjectid as a contact id, then case inforation case be merged with template.
    
            List<Messaging.SingleEmailMessage> lstMsgsToSend = new List<Messaging.SingleEmailMessage>();
            for (Messaging.SingleEmailMessage email : lstMsgs) {
                Messaging.SingleEmailMessage emailToSend = new Messaging.SingleEmailMessage();
                emailToSend.setToAddresses(email.getToAddresses());
                emailToSend.setHTMLBody(email.getHTMLBody());
                emailToSend.setSubject(email.getSubject());
                if(owea.size() > 0) 
                {
                    emailToSend.setOrgWideEmailAddressId(owea.get(0).Id);
                }  
                lstMsgsToSend.add(emailToSend);
            }
            
            if (!Test.isRunningTest()) {
                Messaging.sendEmail(lstMsgsToSend);
            }
        }catch(Exception e){
             System.debug('Exception:'+e.getMessage());
        }
    }
    @future(callout=true)
    public static void callout(String url, String username, String password, String content, ID caseId,String reqMethod) {
        System.debug('URL:'+url);
        System.debug('content:'+content);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod(reqMethod); 
        req.setTimeout(5000);      
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        System.debug('BASIC Auth:'+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(content);
        try {
            HTTPResponse res;
            if(!test.IsRunningTest()){
                res = http.send(req);
            }
        } catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    @future(callout=true)
    public static void JLRCallout(String url, String username, String password, String content, ID caseId,String reqMethod) {
        Integration__c integration;
        integration = new Integration__c(Parameters__c=('Time: '+System.now()+'\n\n'), Message__c='JLR-SVCRM Update');
        integration.Case__c=caseId; 
        
        c = [Select Id,casenumber,JLR_Svcrm_Breakdown_Id__c,Client_CRM_Sync_Failure_Reason__c,Client_CRM_Synchronization_Status__c from Case where id=:caseId LIMIT 1];
        //c.Client_CRM_Synchronization_Status__c = 'Submitted';
        
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod(reqMethod); 
        req.setTimeout(60000);      
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        System.debug('BASIC Auth:'+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(content);
        string rtnmsg = ''; 
        try {
            HTTPResponse res;
            if(!test.IsRunningTest()){
                res = http.send(req);
                rtnmsg = res.getbody();
            }
            
            if(res.GetStatus() == 'OK'){
                JLRBreakdownCase jlrEvent = (JLRBreakdownCase)JSON.deserialize(res.getbody(),JLRBreakdownCase.class);
                if(c.JLR_Svcrm_Breakdown_Id__c == null){
                    c.JLR_Svcrm_Breakdown_Id__c = jlrEvent.BrkdwnEvntId;                    
                }
                c.Client_CRM_Synchronization_Status__c = 'Successful';
                c.Client_CRM_Sync_Failure_Reason__c = '';
            } else{
                JLRBreakdownErrorResp jlrErrorResp = (JLRBreakdownErrorResp)JSON.deserialize(res.getbody(),JLRBreakdownErrorResp.class);
                c.Client_CRM_Sync_Failure_Reason__c= jlrErrorResp.ErrorDescription;
                c.Client_CRM_Synchronization_Status__c = 'Failed';
            }
            //System.debug(res.getBody());
        } catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
            rtnmsg += e.getMessage();   
            c.Client_CRM_Sync_Failure_Reason__c= e.getMessage() ;
            c.Client_CRM_Synchronization_Status__c = 'Failed';  
        }
        update c;
        integration.Parameters__c=(integration.Parameters__c+'\n\n'+'End Point: ' +  url+'\n\nRequest:' + content+ '\n\n Reponse: ' + rtnmsg);
        insert integration;
    }
    
    public void mbcCallout(String url, String username, String password, String content, ID caseId,String reqMethod) {
        System.debug('URL:'+url);
        System.debug('content:'+content);
        Http http = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(url);
        req.setMethod(reqMethod); 
        req.setTimeout(5000);      
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);
        System.debug('BASIC Auth:'+authorizationHeader);
        req.setHeader('Authorization', authorizationHeader);
        req.setHeader('Content-Type', 'application/json');
        req.setBody(content);
        try {
            HTTPResponse res;
            if(!test.IsRunningTest()){
                res = http.send(req);
            }
        } catch(Exception e) {
            System.debug('Exception:'+e.getMessage());
        }
    }
    
    @future(callout=true)
    public static void dispatchCase(ID caseId) {

           CA__c settings = CA__c.getOrgDefaults();
           System.debug('Dispatch Case ID ========= '+ settings.DI_Status__c);
           DispatchCallController dispatchCall = new DispatchCallController(caseId,settings.DI_Status__c,'20');
           dispatchCall.dispatchMBCCall();
   } 
   
    @future(callout=true)
    public static void updateServiceCall(ID caseId) {
           CA__c settings = CA__c.getOrgDefaults();
           System.debug('updateServiceCall ========= ');
           MBC_Integration mbc = new MBC_Integration(caseId,'23');
           mbc.mbcAuthorize();
    } 
    public class JLRBreakdownCase  {
        public String BrkdwnEvntId{get;set;}
    }
    public class JLRBreakdownErrorResp  {
        public String ErrorDescription{get;set;}
    }

}