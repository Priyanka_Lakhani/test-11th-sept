@isTest(SeeAllData=true)
private class TEST_ServiceCallCounter {
  
  @isTest static void test_method_one() {
    Account club = new Account(Name='Club', Account_Number__c='Club', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        update club;
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='12333311'
        );
        insert client;
        
        Account tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;

        Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='12312345',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Base_Warranty_Service_Limit_Term__c = 3,
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        insert prog;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        //Modified to create vehicle / Policy & Vehicle Policy Record
        Vehicle__c veh = new Vehicle__c(
            Name='VIN',
            Vehicle_External__c='VIN',
            Make__c='Audi',
            Model__c='R8'
        );
        insert veh;
        
        //Modified to create vehicle / Policy & Vehicle Policy Record
        Vehicle__c veh1 = new Vehicle__c(
            Name='VIN1',
            Vehicle_External__c='VIN1',
            Make__c='Audi',
            Model__c='R8'
        );
        insert veh1;
        
        //Modified to create vehicle / Policy & Vehicle Policy Record
        Vehicle__c veh2 = new Vehicle__c(
            Name='VIN2',
            Vehicle_External__c='VIN2',
            Make__c='Audi',
            Model__c='R8'
        );
        insert veh2;
        
        Policy__c pol = new Policy__c(
            Name='POL123',
            Policy_External__c='POL123',
            Warranty_Start__c=Date.today().addYears(-5),
            Warranty_End__c=Date.today().addYears(1),
            Warranty_Max_Odometer__c = 10000,
            Program__c=prog.Id,
            RecordTypeId=Schema.SObjectType.Policy__c.getRecordTypeInfosByName().get('Base Program Policy').getRecordTypeId()
        );
        insert pol;
        
        Policy__c pol1 = new Policy__c(
            Name='POL124',
            Policy_External__c='POL124',
            Warranty_Start__c=Date.today().addYears(-5),
            Warranty_End__c=Date.today().addYears(1),
            Warranty_Max_Odometer__c = 10000,
            Program__c=prog.Id,
            RecordTypeId=Schema.SObjectType.Policy__c.getRecordTypeInfosByName().get('Promo Program Policy').getRecordTypeId()
        );
        insert pol1;
        
        Policy__c pol2 = new Policy__c(
            Name='POL125',
            Policy_External__c='POL125',
            Warranty_Start__c=Date.today().addYears(-5),
            Warranty_End__c=Date.today().addYears(1),
            Warranty_Max_Odometer__c = 10000,
            Program__c=prog.Id,
            RecordTypeId=Schema.SObjectType.Policy__c.getRecordTypeInfosByName().get('Extended Program Policy').getRecordTypeId()
        );
        insert pol2;
        
        Vehicle_Policy__c vehPol = new Vehicle_Policy__c    (
            Policy__c=pol.Id,
            Vehicle__c=veh.Id,
            Vehicle_Policy_External__c='VIN-POL123'
        );
        insert vehPol;
        
        Vehicle_Policy__c vehPol1 = new Vehicle_Policy__c    (
            Policy__c=pol1.Id,
            Vehicle__c=veh1.Id,
            Vehicle_Policy_External__c='VIN-POL124'
        );
        insert vehPol1;
        
        Vehicle_Policy__c vehPol2 = new Vehicle_Policy__c    (
            Policy__c=pol2.Id,
            Vehicle__c=veh2.Id,
            Vehicle_Policy_External__c='VIN-POL125'
        );
        insert vehPol2;
        
        CA__c settings = CA__c.getOrgDefaults();
        
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=veh.Name,
            Vehicle__c = veh.Id,
            Club_Call_Number__c = '123',
            Interaction_ID__c='INTERACTION',
            Miscellaneous_Call_Reason__c=settings.Call_Flow_Personal_Update_Request__c,
            Phone__c='1233213',
            Call_ID__c='ABC123',
            Program__c=prog.Id,
            Non_Service_Call__c = false
        );
        insert c;
        


        ApexPages.currentPage().getParameters().put('Id', veh.Id);

        Apexpages.StandardController sc = new Apexpages.standardController(veh);

        Vehicle_ServiceCallCounterController scc = new Vehicle_ServiceCallCounterController(sc);
        scc.updateSelected();
        //scc.resetCounter();
        
        ApexPages.currentPage().getParameters().put('Id', veh1.Id);
		Apexpages.StandardController sc2 = new Apexpages.standardController(veh1);
		Vehicle_ServiceCallCounterController scc2 = new Vehicle_ServiceCallCounterController(sc2);
		
		ApexPages.currentPage().getParameters().put('Id', veh1.Id);
		Apexpages.StandardController sc3 = new Apexpages.standardController(veh2);
		Vehicle_ServiceCallCounterController scc3 = new Vehicle_ServiceCallCounterController(sc3);
        
        ApexPages.currentPage().getParameters().put('Id', vin.Id);

        Apexpages.StandardController sc1 = new Apexpages.standardController(vin);

        ServiceCallCounterController serv = new ServiceCallCounterController(sc1);
        serv.updateSelected();

  }
}