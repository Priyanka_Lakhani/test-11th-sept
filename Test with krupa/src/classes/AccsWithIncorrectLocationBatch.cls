global class AccsWithIncorrectLocationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
    global String query;
    
    global AccsWithIncorrectLocationBatch(String q) {
        if(q == null){
            query = 'SELECT Id,IncorrectLocation__c,Location__Latitude__s,(SELECT Field,CreatedDate FROM Histories WHERE Field IN (\'BillingStreet\',\'Location__Latitude__s\',\'BillingCountry\',\'BillingProvince\') ORDER BY CreatedDate DESC) FROM Account WHERE RecordType.Name = \'Dealership / Tow Destination\'';
        }
        else{
            query = q;
        }
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        List<Account>accToUpdate = new List<Account>();
        for(Account a : scope){
            if(a.Location__Latitude__s == null){
                a.IncorrectLocation__c = true;
                accToUpdate.add(a);
            }
            else if(a.Histories.size() != 0){

                for(AccountHistory ah: a.Histories){
                    system.debug('1111111111111 ' + ah.Field);
                    if(ah.Field.contains('Location')){
                        system.debug('222222222222222 ' + ah.Field);
                        break;
                    }
                    if(ah.Field.contains('Billing')){
                        system.debug('333333333333 ' + ah.Field);
                        a.IncorrectLocation__c = true;
                        accToUpdate.add(a);
                        break;
                    }
                    
                }
                
            }
        }
        update accToUpdate;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}