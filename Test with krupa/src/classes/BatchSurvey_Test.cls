@isTest
private class BatchSurvey_Test {

    static Account client{get;set;}
    static Account dealer{get;set;}
    static VIN__c vin{get;set;}
    static Program__c prog{get;set;}
    static VIN__c vin2{get;set;}
    static Program__c prog2{get;set;}
    static Program__c prog3{get;set;}
    static Case c {get;set;}
    static Survey__c survey {get;set;}
    static Survey__c survey2 {get;set;}
    static Survey__c survey3 {get;set;}
    static CA__c settings {get;set;}
    static Sites__c sites {get;set;}
    
    static void Utility()
    {
        sites = Sites__c.getOrgDefaults();
        sites.name = 'Survey';
        sites.value__c = 'http://www.salesforce.com';
        upsert sites;

        settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';
        upsert settings;
        
        client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        survey = new Survey__c(
            name ='JLR Survey'
        );
        insert survey;

        survey2 = new Survey__c(
            name ='Honda Survey'
        );
        insert survey2;
        
        survey3 = new Survey__c(
            name ='Kia Survey'
        );
        insert survey3;
        
        Survey_Question__c sq1 = new Survey_Question__c();
        sq1.question__c = 'how are your?';
        sq1.order__c = 1;
        sq1.survey__c = survey.id;
        sq1.Language__c = 'English';
        upsert sq1;

        Survey_Question__c sq2 = new Survey_Question__c();
        sq2.question__c = 'how old are you?';
        sq2.order__c = 2;
        sq2.survey__c = survey.id;
        sq2.Language__c = 'French';
        upsert sq2;
        
        List<Program__c> porgToIns = new List<Program__c>();
        prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='858',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='133',
            Status__c='Active',
            survey__c = survey.id
        );
        porgToIns.add(prog);
                
        List<VIN__c> vinToIns = new List<VIN__c>();        
        vin = new VIN__c(
            Name='VIN12345678944567',
            UID__c='VIN12335678904567',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        vinToIns.add(vin);
        
        prog2 = new Program__c(
            Account__c=client.Id,
            Program_Code__c = '560',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey2.id
            );

       porgToIns.add(prog2);
       
       prog3 = new Program__c(
            Account__c=client.Id,
            Program_Code__c = '503',
            Description__c='Base',
            Type__c='Underwritten',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey3.id
            );

       porgToIns.add(prog3);
       insert porgToIns;

         vin2 = new VIN__c(
            Name='VIN123456789045',
            UID__c='VIN123456789045',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog2.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
            );
        vinToIns.add(vin2);
        insert vinToIns;
            
            Survey_Question__c sq11 = new Survey_Question__c();
            sq11.question__c = 'how are your?';
            sq11.order__c = 1;
            sq11.survey__c = survey2.id;
            sq11.Language__c = 'English';
            upsert sq11;

            Survey_Question__c sq22 = new Survey_Question__c();
            sq22.question__c = 'how old are you?';
            sq22.order__c = 2;
            sq22.survey__c = survey2.id;
            sq22.Language__c = 'French';
            upsert sq22;

		    Survey_Question__c sq31 = new Survey_Question__c();
            sq31.question__c = 'how are your?';
            sq31.order__c = 1;
            sq31.survey__c = survey3.id;
            sq31.Language__c = 'English';
            upsert sq31;

            Survey_Question__c sq32 = new Survey_Question__c();
            sq32.question__c = 'how old are you?';
            sq32.order__c = 2;
            sq32.survey__c = survey3.id;
            sq32.Language__c = 'French';
            upsert sq32;
            
        
    }
    static testMethod void myUnitTest() {
            test.startTest();
           Utility();
  
            c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'JLR Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs:'+cs);        system.debug('cs:'+cs);
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'JLR Survey\''; // limit 100';
        
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'Jaguar Land Rover Roadside Assistance';
                b.surveyname = 'JLR Survey';
                database.executebatch(b,20);
            }
        }        
        
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        test.stoptest();
    }
    
    static testMethod void myUnitTest1() {

        test.startTest();
        System.schedule('S1', '0 0 0 3 9 ? 2022', new Schedule_BatchJLRSurvey());
        System.schedule('S2', '0 0 0 3 9 ? 2022', new Schedule_BatchHondaSurvey());
        System.schedule('S3', '0 0 0 3 9 ? 2022', new Schedule_BatchKIASurvey());
        test.stoptest();
    }
    
    static testMethod void myUnitTest2() {

           Utility();
           prog.Program_code__c='864';
           update prog;
           
           c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'English'
            
        );
        insert c;
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c from case WHERE SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'JLR Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs:'+cs);        system.debug('cs:'+cs);
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'JLR Survey\''; // limit 100';
       
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
            test.startTest();
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'Jaguar Land Rover Roadside Assistance';
                b.surveyname = 'JLR Survey';
                database.executebatch(b,20);
            test.stoptest();
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        //test.stoptest();
            }
        }
    }
    
        static testMethod void myUnitTest3() {
           test.startTest();
           Utility();
                       c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog.id,
            Vehicle_Make__c ='Land Rover',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
            
        );
        insert c;
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'JLR Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs:'+cs);        system.debug('cs:'+cs);
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'JLR Survey\''; // limit 100';
        
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'Jaguar Land Rover Roadside Assistance';
                b.surveyname = 'JLR Survey';
                database.executebatch(b,20);
            }
        }        
        
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        test.stoptest();
    }
    
       static testMethod void myUnitTest4() {
           test.startTest();
            Utility();
            prog.Program_code__c='864';
            update prog;
           
            c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog.id,
            Vehicle_Make__c ='Land Rover',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'English'
          
        );
        insert c;
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'JLR Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs:'+cs);        system.debug('cs:'+cs);
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'JLR Survey\''; // limit 100';
       
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'Jaguar Land Rover Roadside Assistance';
                b.surveyname = 'JLR Survey';
                database.executebatch(b,20);
        
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        //test.stoptest();
            }
        }
        test.stopTest();
    }
    
    
    static testMethod void myUnitTest5() {
       test.startTest();
           Utility();
          prog.Program_code__c='864';
           update prog;
           
           c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog.id,
            Vehicle_Make__c ='Land Rover',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'English'
            
        );
        insert c;
        
        SurveyUnsubscriber__c su = new SurveyUnsubscriber__c();
        su.email__c = 'abc@clubautoltd.com';
        upsert su;
        
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'JLR Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs:'+cs);        system.debug('cs:'+cs);
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'JLR Survey\''; // limit 100';
       
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'Jaguar Land Rover Roadside Assistance';
                b.surveyname = 'JLR Survey';
                database.executebatch(b,20);
        
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        //test.stoptest();
            }
        }
        test.stopTest();
    }


    static testMethod void myUnitTest6() {
       test.startTest();
        Utility();

        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin2.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog2.id,
            Vehicle_Make__c ='Land Rover',
            first_name__c='test378',
            last_name__c='test378',
            email__c = 'abc123@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'French'
            
        );
        insert c;
        
        SurveyUnsubscriber__c su = new SurveyUnsubscriber__c();
        su.email__c = 'abc@clubautoltd.com';
        upsert su;
        
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'Honda Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs378:' + cs);
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'Honda Survey\''; // limit 100';
       
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'Jaguar Land Rover Roadside Assistance';
                b.surveyname = 'Honda Survey';
                database.executebatch(b,20);
        
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        //test.stoptest();
            }
        } 
        test.stopTest();   
    }
    
    static testMethod void myUnitTest7() {
        test.startTest();
        Utility();

        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin2.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog2.id,
            Vehicle_Make__c ='Land Rover',
            first_name__c='test378',
            last_name__c='test378',
            email__c = 'abc123@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'English'
            
        );
        insert c;
        
        SurveyUnsubscriber__c su = new SurveyUnsubscriber__c();
        su.email__c = 'abc@clubautoltd.com';
        upsert su;
        
        list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = 'Honda Survey'];
        //list<Case> cs = [select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate  from case];
        system.debug('cs378:' + cs);
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'Honda Survey\''; // limit 100';
       
        for(Case caseli: cs) {
            if(caseli.email__c!=null) {
                BatchSurvey b = new BatchSurvey(qry); 
                b.owa = 'No Reply';
                b.surveyname = 'Honda Survey';
                database.executebatch(b,20);
        
        //System.schedule('S1', '0 56 11 * * ?', new BatchSurvey());
        //test.stoptest();
            }
        } 
        test.stopTest();   
    }
    static testMethod void myUnitTest8() {
        test.startTest();
        Utility();

        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin2.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog3.id,
            Vehicle_Make__c ='Kia',
            first_name__c='test378',
            last_name__c='test378',
            email__c = 'abc123@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'French'
            
        );
        insert c;       
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'Kia Survey\'';

        BatchSurvey b = new BatchSurvey(qry); 
        b.owa = 'No Reply';
        b.surveyname = 'Kia Survey';
        database.executebatch(b,20);
    
        test.stopTest();   
    }
    
    static testMethod void myUnitTest9() {
        test.startTest();
        Utility();
           
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog3.id,
            Vehicle_Make__c ='Kia',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'CA',
            Language__c = 'English'
            
        );
        insert c;
        
        string qry = 'select id,casenumber,Vehicle_Make__c,first_name__c,last_name__c, email__c,program__r.survey__c,program__r.program_code__c,surveysent__c,CreatedDate,Language__c  from case where SurveySent__c = null and program__r.survey__c !=null and program__r.survey__r.name = \'Kia Survey\''; 
        
        BatchSurvey b = new BatchSurvey(qry); 
        b.owa = 'No Reply';
        b.surveyname = 'Kia Survey';
        database.executebatch(b,20);
        
        test.stopTest();
    }
}