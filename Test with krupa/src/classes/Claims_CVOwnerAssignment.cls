global without sharing class Claims_CVOwnerAssignment
{
    @invocableMethod(label='Assign Permissions for claims owner' description='Assign permissions for the claim owner to edit attachments')
    webservice static void Claims_AssignOwnerCV(List<String> claimIds)
    { 
        String retmsg = '';
        system.debug('*****ENTRY****'+retmsg);
        Set<String> cdids = new Set<String>();
        Map<Id,Id> claimownerMap = new Map<Id,Id>();
        Map<Id,Id> docClaimMap = new Map<Id,Id>();
        for(Expense_Claim__c claim:[Select id, ownerId from Expense_Claim__c where Id in:claimIds]){
            claimownerMap.put(claim.Id, claim.ownerId);
        }
        List<ContentVersion> cvlist = new List<ContentVersion>();
         for(ContentDocumentLink CD:[Select id, ContentDocumentId, LinkedEntityId from ContentDocumentLink WHERE LinkedEntityId in :claimIds]){
            cdids.add(CD.ContentDocumentId);
            docClaimMap.put(CD.ContentDocumentId,CD.LinkedEntityId);
         }
         if(cdids.size() > 0){
            for(ContentVersion cv:[SELECT ContentDocumentId,OwnerId,SharingOption,SharingPrivacy,Status__c,Title,VersionNumber FROM ContentVersion where ContentDocumentId in:cdids]){
                String claimId = '';
                if(docClaimMap.keyset().contains(cv.ContentDocumentId)){
                    claimId = docClaimMap.get(cv.ContentDocumentId);
                }
                if(claimId != ''){
                    if(claimownerMap.keyset().contains(claimId)){
                        cv.ownerid = claimownerMap.get(claimId);
                        cvlist.add(cv);
                    }
                }
            }
         }
         if(cvlist.size() > 0){
            try{
                update cvlist;
                retmsg = 'Success';
            }catch(Exception ex){
                retmsg = 'Failure';
            }
         }
         system.debug('*****EXIT****'+retmsg);
    }
}