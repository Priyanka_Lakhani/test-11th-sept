@isTest
private class TEST_SearchDealershipCC {
        static Account club {get;set;}
        static Account client{get;set;}
        static Account tow{get;set;}
        static Territory_Coverage__c terr{get;set;}
        static VIN__c vin{get;set;}
        //static CA__c settings{get;set;}
        static Case c{get;set;}
        static Program__c prog{get;set;}    
    
    
    @isTest static void test_method_one() {


        club = new Account(Name='Club', Account_Number__c='club', Status__c = 'Active',RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        club.Account_Number__c='club' + club.Id;
        update club;
        
        client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123' + club.Id
        );
        insert client;
        
        tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=-79.383214, 
            Location__Latitude__s=43.654226,
            R8_Dealership__c=false,
            status__c ='Active'
        );
        insert tow;
        
        terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='RichmondXHill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        
        insert terr;
        prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123' + client.Id,
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123' + client.Id,
            Status__c='Active'
        );
        insert prog;
        
        vin = new VIN__c(
            Name='VIN' + prog.Id,
            UID__c='VIN' + prog.Id,
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        CA__c settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';
        settings.Google_Key__c ='gme-clubautoroadside';
        settings.Google_Private_Key__c ='VzYkgzw6H0qJYuvLhqb-s7P64LI=';
        upsert settings;
        
         c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            //Miscellaneous_Call_Reason__c=settings.Call_Flow_Personal_Update_Request__c,
            Phone__c='123',
            Call_ID__c='ABC' + prog.Id,
            RE_Status_Date_Time__c = DateTime.newInstance(2015,10,5,9,0,0),
            Club_Call_Number__c = '2251' + prog.Id
        );
        insert c;

        ApexPages.currentPage().getParameters().put('Id', c.Id);
        SearchDealershipCC controller = new SearchDealershipCC();
        controller.accName = 'Tow';
        controller.accCity = 'RichmondXHill';
        controller.accStreet = 'test';
        controller.tempCase.Program__c = prog.Id;
        controller.getAccounts();
    }
}