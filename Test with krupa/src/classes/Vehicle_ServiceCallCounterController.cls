public with sharing class Vehicle_ServiceCallCounterController {
    public Integer service_call_count {get; set;}
    public Integer service_call_limit {get; set;}
    List<Case> caseHistory {get; set;}
    Vehicle__c selectedVIN;
    public Set<String> progPolicyIds{get;set;}//List of PROG POLICY Ids obtained from POLICY junction object
    public Policy__c basePP{get;set;}//Assign BASE policy record of the Vehicle
    public Policy__c extPP{get;set;}//Assign EXTENDED policy record of the Vehicle
    public Policy__c promoPP{get;set;}//Assign PROMO policy record of the Vehicle
    Decimal extservLimit = 0.0;
    Map<String, String> parameterMap;
    Id selectedProgramId;
    Program__c selectedProgram;
    String programFields;
    String vinFields;
    CA__c settings;
    public List<ServiceCall> casesServiceCall {get; set;}
    
    
    public Vehicle_ServiceCallCounterController(ApexPages.StandardController controller) {
        parameterMap = ApexPages.currentPage().getParameters();
        service_call_count = 0;
        settings = CA__c.getOrgDefaults();
        selectedVIN = (Vehicle__c)controller.getRecord();
        progPolicyIds = new Set<String>();

        getVINFields();
        String queryP = 'Select ' + vinFields + ' from Vehicle__c where Id=\''+ selectedVIN.Id +'\' LIMIT 1';
        selectedVIN = database.query(queryP); 
        
        for(Vehicle_Policy__c VP:[Select id, Vehicle__c, Policy__c from Vehicle_Policy__c where Vehicle__c = :selectedVIN.Id]){
            progPolicyIds.add(VP.Policy__c);
        }
        
        if(progPolicyIds.size() > 0 && (selectedVIN != null && selectedVin.Id != null)){
            for(Policy__c pp:[Select Id, Policy_number__c, Policy_Selling_Dealership__c, Policy_Status__c, Program__c, Warranty_End__c, Warranty_Start__c, Warranty_Max_Odometer__c, Warranty_Term__c, RecordType.DeveloperName, Extend_Service_Limit__c from Policy__c where Id in :progPolicyIds]){
                    if(pp.Program__c != NULL && pp.RecordType.DeveloperName == 'Base_Program_Policy') {
                        if(pp.Warranty_End__c != null && System.today() < pp.Warranty_End__c) { 
                            basePP = pp;
                        }
                    }

                    if(pp.Program__c != NULL && pp.RecordType.DeveloperName == 'Extended_Program_Policy') {
                        if(pp.Warranty_End__c != null && System.today() < pp.Warranty_End__c) { 
                            extPP = pp;
                        }
                    }

                    if(pp.Program__c != NULL && pp.RecordType.DeveloperName == 'Promo_Program_Policy') {
                        if(pp.Warranty_End__c != null && System.today() < pp.Warranty_End__c) { 
                            promoPP = pp;
                        }
                    }

                    
            }
        }
        IF(basePP != null){
            selectedProgramId = basePP.Program__c;
            if(basePP.Extend_Service_Limit__c != null && basePP.Extend_Service_Limit__c != 0){
                extservLimit = basePP.Extend_Service_Limit__c;
            }
        }else IF(extPP != null){
            selectedProgramId = extPP.Program__c;
            if(extPP.Extend_Service_Limit__c != null && extPP.Extend_Service_Limit__c != 0){
                extservLimit = extPP.Extend_Service_Limit__c;
            }
        }else IF(promoPP != null){
            selectedProgramId = promoPP.Program__c;
            if(promoPP.Extend_Service_Limit__c != null && promoPP.Extend_Service_Limit__c != 0){
                extservLimit = promoPP.Extend_Service_Limit__c;
            }
        }
        if (selectedProgramId != null) {
            getProgramFields();
            String query = 'Select ' + programFields + ', Account__r.Name from Program__c where Id=\''+ selectedProgramId +'\' LIMIT 1';
            selectedProgram = database.query(query); 
        }

        getHistory();
    }        

    public void resetCounter() {
        if (caseHistory.size() != 0) {
            for (Case c: caseHistory) {
                c.Non_Service_Call__c = true;
            }
            update caseHistory;
            getHistory();
        }
    }

    public void getProgramFields() {
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);
    }

    public void getVINFields() {
        vinFields='';
        for(Schema.SObjectField f : Vehicle__c.SObjectType.getDescribe().fields.getMap().values()) {
            vinFields += (f.getDescribe().getName() + ',');
        }
        vinFields=vinFields.left(vinFields.length()-1);
    }  

    public void getHistory() {
        Boolean hasVIN = FALSE;
        Boolean hasPhone = FALSE;
        Boolean includePhoneSearch = selectedProgram==null||!selectedProgram.Exclude_Phone_From_History_Check__c;
        service_call_count = 0;
        casesServiceCall = new List<ServiceCall>();
        String queryHistory = 'Select Id, Is_your_vehicle_an_electric_vehicle__c, RecordType.DeveloperName, OwnerId, VIN_Member_ID__c, Current_Odometer__c, CaseNumber, First_Name__c, Last_Name__c, Phone__c, Phone_Type__c, Email__c, License__c, Vehicle_Year__c, Vehicle_Make__c, Vehicle_Model__c, Vehicle_Colour__c, Vehicle_Type__c, Vehicle_4WD__c, Vehicle_FB_Reg__c, RE_Status_Date_Time__c, Status, Trouble_Code__c, CLUB_CALL_NUMBER__C, DI_Status_Date_Time__c, Non_Service_Call__c from Case where Id<>null ';//, Service_call__c
      
        if(selectedVIN!=null&&selectedVIN.Id!=null) { 
            hasVIN=TRUE; 
            if (selectedProgram!=null && (selectedProgram.Base_Warranty_Service_Limit_Term__c>0))
            {
                integer term = integer.valueof(selectedProgram.Base_Warranty_Service_Limit_Term__c);
                Datetime d = Datetime.newInstance(System.now().addmonths(-term).year(), System.now().addmonths(-term).month(),System.now().addmonths(-term).day(), 0, 0, 0);
                queryHistory += ' AND RE_Status_Date_Time__c>=:d '; 
                system.debug('d:' +d);
            }
            else
            {
                if(basePP != null){
                    if(basePP.Warranty_Start__c!=null) { 
                        Datetime d = Datetime.newInstance(basePP.Warranty_Start__c.year(), basePP.Warranty_Start__c.month(), basePP.Warranty_Start__c.day(), 0, 0, 0);
                        queryHistory += ' AND RE_Status_Date_Time__c>=:d ';
                    }
                }
            }
        }
        if(selectedProgram!=null && selectedProgram.Account__c != null&&String.valueOf(selectedProgram.Account__c).length()>0) { hasPhone=TRUE; }
        
        if(selectedProgram!=null && selectedProgram.Account__c!=null) {
            if(hasVIN && !hasPhone) {
                queryHistory += ' AND ((Vehicle__c=\''+selectedVIN.Id+'\' OR VIN_Member_ID__c=\''+selectedVIN.Name+'\') AND Program__r.Account__c=\''+selectedProgram.Account__c+'\')';
            } else if(!hasVIN && hasPhone) {
                queryHistory += ' AND Program__r.Account__c=\''+selectedProgram.Account__c+'\'';
            } else if(hasVIN && hasPhone) {
                queryHistory += ' AND (((Vehicle__c=\''+selectedVIN.Id+'\' OR VIN_Member_ID__c=\''+selectedVIN.Name+'\') AND Program__r.Account__c=\''+selectedProgram.Account__c+'\') OR (Program__r.Account__c=\''+(selectedProgram.Account__c!=null?selectedProgram.Account__c:'')+'\'))';
            }
        }
        
        queryHistory += ' And Kill_code__c != \'Test Call / Training Call\' AND Non_Service_Call__c = false';// AND Service_call__c = true

        if(selectedVIN != null && selectedProgram!=null) {
            service_call_limit = (Integer.valueOf(selectedProgram.Base_Warranty_Service_Limit__c) != null ? Integer.valueOf(selectedProgram.Base_Warranty_Service_Limit__c) : 0) + 
                                 (Integer.valueOf(extservLimit) != null ? Integer.valueOf(extservLimit) : 0);
        }                         

        Integer qlmt = 10;
        if (selectedProgram!=null&&selectedProgram.Base_Warranty_Service_Limit__c!=null)
            qlmt = Integer.valueof(selectedProgram.Base_Warranty_Service_Limit__c) + 10;
   
        queryHistory += ' ORDER BY RE_Status_Date_Time__c DESC LIMIT '+ qlmt;
        
        if(!hasVIN && !hasPhone) {
            caseHistory = new List<Case>();
        } else {
            caseHistory = Database.query(queryHistory);
            if(caseHistory!=null&&caseHistory.size()>0) {

                for(Case c_temp : caseHistory) {

                    if(c_temp.RecordType.DeveloperName=='Roadside_Assistance'&&selectedVIN!=null&&c_temp.VIN_Member_ID__c==selectedVIN.Name) 
                    {
                        if ((c_temp.Trouble_Code__c!=settings.Non_Dispatch_Call_Types__c) && (c_temp.CLUB_CALL_NUMBER__C != '')&& (c_temp.CLUB_CALL_NUMBER__C != null))
                        {
                            if (selectedProgram!=null && (selectedProgram.Base_Warranty_Service_Limit_Term__c>0))
                            {
                                integer term = integer.valueof(selectedProgram.Base_Warranty_Service_Limit_Term__c);
                                Datetime d = Datetime.newInstance(System.now().addmonths(-term).year(), System.now().addmonths(-term).month(),System.now().addmonths(-term).day(), 0, 0, 0);
                                if (selectedProgram.RollingSvcCounter__c)
                                {
                                    if (DateTime.valueof(c_temp.RE_Status_Date_Time__c) > d && DateTime.valueof(c_temp.RE_Status_Date_Time__c) > basePP.Warranty_Start__c) 
                                        service_call_count++;
                                        casesServiceCall.add(new ServiceCall(c_temp));
                                }
                                else
                                {
                                    if (DateTime.valueof(c_temp.RE_Status_Date_Time__c) > d)
                                        service_call_count++;
                                        casesServiceCall.add(new ServiceCall(c_temp));
                                }
                            }
                            else
                            {
                                service_call_count++;
                                casesServiceCall.add(new ServiceCall(c_temp));
                            }
                        } 
                    }
                }
            }      
        }
    }

    public void updateSelected() {
        List<Case> casesToUp = new List<Case>();

        if(casesServiceCall.size() != 0) {
            for (ServiceCall sc: casesServiceCall) {
                if (sc.serviceChecker == true) {
                    sc.histCase.Non_Service_Call__c = true;
                    casesToUp.add(sc.histCase);
                }
            }
            if (casesToUp.size() != 0) {
                update casesToUp;
            }    
        }    
            
        getHistory();
    }



    public class ServiceCall {
        public Boolean serviceChecker {get; set;}
        public Case histCase {get; set;}

        public ServiceCall(Case c) {
            serviceChecker = false;
            histCase = c;
        }
    }
}