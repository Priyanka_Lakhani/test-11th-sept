global class Schedule_EmailForJLRMTReport implements Schedulable{
	public List<Account> accountList;
	public String startDate{get; set;}
	public String enddate{get; set;}
	
	private void SetLastMonthDates()
	{
	 Date now = System.Date.today();
        Date firstDate = now.addMonths(-1).toStartOfMonth();
        Date lastDate = firstDate.addDays(date.daysInMonth(firstDate.year() , firstDate.month())); 
        DateTime firstDateTime = firstDate;
        DateTime lastDateTime = lastDate;
        startDate = firstDateTime.addDays(1).format('dd/MM/yyyy');
        enddate = lastDateTime.format('dd/MM/yyyy');
        System.debug('startDate:'+startDate);
		System.debug('enddate:'+enddate);
	}
	
	public Schedule_EmailForJLRMTReport(){
		accountList = new List<Account>(); 
        String accTestString = 'test-%';
        String accType = 'Mobile Tech';
        accountList = [SELECT Id,Name,createdDate,Account_Number__c,SecondAccountNumber__c,Status__c,dispatch_email__c FROM Account WHERE recordtype.name=:accType  and Name != null and (NOT Name Like :accTestString) and isTestAccount__c !=true Order by Name ASC];
       SetLastMonthDates();
	}
	public Schedule_EmailForJLRMTReport(List<Account> accList,String fromDate,String toDate){
		accountList = accList;
		//startDate = fromDate;
		//enddate = toDate;
		SetLastMonthDates();
		System.debug('startDate:'+startDate);
		System.debug('enddate:'+enddate);
	}
    global void execute(SchedulableContext sc) {
        
        
        if (Schedule_MT_Report__c.getInstance('JLRMTRetailerPerformanceReport')!= null && Schedule_MT_Report__c.getInstance('JLRMTRetailerPerformanceReport').Is_Schedular_On__c == true){ 
        	scheduleRetailerPerformanceReport(accountList,startDate,enddate);
        }
        if (Schedule_MT_Report__c.getInstance('JLRMTRetailerBillingReport')!= null && Schedule_MT_Report__c.getInstance('JLRMTRetailerBillingReport').Is_Schedular_On__c == true){ 
            scheduleRetailerBillingReport(accountList,startDate,enddate);
        }
        if (Schedule_MT_Report__c.getInstance('JLRMTCorporateReport')!= null && Schedule_MT_Report__c.getInstance('JLRMTCorporateReport').Is_Schedular_On__c == true)                             
            scheduleJLRMTCorporateReport(startDate,enddate);
    }
    
    public void scheduleRetailerPerformanceReport(List<Account> AcctList,String startDate1,String enddate1){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@xperigo.com'];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();           
        EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where DeveloperName = 'JLR_MT_Retailer_Performance_Report'];
		SetLastMonthDates();
	    Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
        DateTime sdate= date.parse(startDate);
        for(Account a: AcctList){
        	if(a.Retailer_Mail_Id__c != null){
                Messaging.EmailFileAttachment attach1 = new Messaging.EmailFileAttachment();
                PageReference pref1;
                Blob b1;// = pref1.getContent();
                if (Test.IsRunningTest()){
                  b1 = Blob.valueOf('UNIT-TEST');
                }
                else{
                  pref1 = page.JLRMTRetailerPerformanceReport;
                  pref1.getParameters().put('Id',(String)a.Account_number__c);    
                  pref1.getParameters().put('startDate',startDate);
                  pref1.getParameters().put('endDate',enddate);
                  pref1.getParameters().put('reportName','MTRetailerPerformance');
                  pref1.setRedirect(true);
                  b1 = pref1.getContent();
                  System.debug('blob content:'+b1);
                }
                attach1.setFileName('Mobile Technician Retailer Performance Report.xls');
                attach1.setBody(b1);

                string[] emailsToSend  = new String[]{a.Retailer_Mail_Id__c};
                Messaging.EmailFileAttachment[] emailAttachments = new Messaging.EmailFileAttachment[]{attach1};
                string[] emailccAddress = new String[]{}; 
                if(Schedule_MT_Report__c.getInstance('JLRMTRetailerPerformanceReport')!= null){	                
	                String strCCAddress = Schedule_MT_Report__c.getInstance('JLRMTRetailerPerformanceReport').EmailccAddress__c;                    
	            	if(strCCAddress != null){
	            	    for(String em : strCCAddress.split(',')){
	             	       emailccAddress.add(em);
	             	   }
	           		}
                }
                			
			    // process the merge fields
			    String emailSubject = emailTemplate.Subject;
			    			
			    String emailHtmlBody = emailTemplate.HtmlValue;
                emailHtmlBody = emailHtmlBody.replace('{!monthOfReport}', getMonthName(sdate) + ' ' + sdate.year());
			
			    String emailPlainBody = emailTemplate.Body;
                emailPlainBody = emailPlainBody.replace('{!monthOfReport}', getMonthName(sdate) + ' ' + sdate.year());
                if(emailsToSend.size()>0){
               		mails.add(buildMailToSend(ctc.id,emailsToSend,emailAttachments,emailSubject,emailHtmlBody,emailPlainBody,emailccAddress,owea));     
                }
        	}
        }   
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(mails);
        }     
    }
    public void scheduleRetailerBillingReport(List<Account> AcctList,String startDate1,String enddate1){
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@xperigo.com'];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();    
        EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where DeveloperName = 'JLR_MT_Retailer_Billing_Report'];
		SetLastMonthDates();
	    Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];		
        DateTime sdate= date.parse(startDate);
        for(Account a: AcctList){
        	if(a.Retailer_Mail_Id__c != null){
                Messaging.EmailFileAttachment attach2 = new Messaging.EmailFileAttachment();                
                PageReference pref2;
                Blob b2;// = pref2.getContent();
                if (Test.IsRunningTest()){
                   b2 = Blob.valueOf('UNIT-TEST');
                }
                else{
                  pref2 = page.JLRMTRetailerBillingReport;
                  pref2.getParameters().put('Id',(String)a.Account_number__c);
                  pref2.getParameters().put('startDate',startDate);
                  pref2.getParameters().put('endDate',enddate);
                  pref2.getParameters().put('reportName','MTRetailerBilling');
                  pref2.setRedirect(true);
                  b2 = pref2.getContent();
                }

                attach2.setFileName('Mobile Technician Retailer Billing Report.xls');
                attach2.setBody(b2);
                
                string[] emailsToSend = new String[]{a.Retailer_Mail_Id__c};
                Messaging.EmailFileAttachment[] emailAttachments = new Messaging.EmailFileAttachment[]{attach2};
                string[] emailccAddress = new String[]{}; 
                if(Schedule_MT_Report__c.getInstance('JLRMTRetailerBillingReport')!= null){
	                String strCCAddress = Schedule_MT_Report__c.getInstance('JLRMTRetailerBillingReport').EmailccAddress__c;                    
	            	if(strCCAddress != null){
	            	    for(String em : strCCAddress.split(',')){
	             	       emailccAddress.add(em);
	             	   }
	           		}
                }
                	
			    // process the merge fields
			    String emailSubject = emailTemplate.Subject;
			    			
			    String emailHtmlBody = emailTemplate.HtmlValue;
                emailHtmlBody = emailHtmlBody.replace('{!monthOfReport}', getMonthName(sdate) + ' ' + sdate.year());
			
			    String emailPlainBody = emailTemplate.Body;
                emailPlainBody = emailPlainBody.replace('{!monthOfReport}', getMonthName(sdate) + ' ' + sdate.year());
                if(emailsToSend.size()>0){
               		mails.add(buildMailToSend(ctc.id,emailsToSend,emailAttachments,emailSubject,emailHtmlBody,emailPlainBody,emailccAddress,owea));     
                }
        	}
        }   
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(mails);
        }     
    }
    
    public void scheduleJLRMTCorporateReport(String startDate1,String enddate1){
        DateTime sdate= date.parse(startDate);
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'noreply@xperigo.com'];
        List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();    
        Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
        SetLastMonthDates();
        PageReference pref;
        Blob b;
        if (Test.IsRunningTest()){
          b = Blob.valueOf('UNIT-TEST');
        }
        else{
          pref = page.ExportJLRReport;
          pref.getParameters().put('startDate',startDate);
          pref.getParameters().put('endDate',enddate);
          pref.setRedirect(true);
          b = pref.getContent();
        }
        attach.setFileName('JLR MT Corporate report.xls');
        attach.setBody(b);
        String toAddress = '';
         
        Messaging.EmailFileAttachment[] emailAttachments = new Messaging.EmailFileAttachment[]{attach};
        string[] emailccAddress = new String[]{}; 
        if(Schedule_MT_Report__c.getInstance('JLRMTCorporateReport')!= null){
        	toAddress = Schedule_MT_Report__c.getInstance('JLRMTCorporateReport').EmailToAddress__c;
	        String strCCAddress = Schedule_MT_Report__c.getInstance('JLRMTCorporateReport').EmailccAddress__c;                    
	    	if(strCCAddress != null){
	    	    for(String em : strCCAddress.split(',')){
	     	       emailccAddress.add(em);
	     	   }
	   		}    
        }  
        string[] emailsToSend = new String[]{toAddress};
        EmailTemplate emailTemplate = [select Id, Subject, HtmlValue, Body from EmailTemplate where DeveloperName = 'JLR_MT_Corporate_Report'];

	    Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];			
	    // process the merge fields
	    String emailSubject = emailTemplate.Subject;
	    			
	    String emailHtmlBody = emailTemplate.HtmlValue;
        emailHtmlBody = emailHtmlBody.replace('{!monthOfReport}', getMonthName(sdate) + ' ' + sdate.year());
	
	    String emailPlainBody = emailTemplate.Body;
        emailPlainBody = emailPlainBody.replace('{!monthOfReport}', getMonthName(sdate) + ' ' + sdate.year());  
        if(emailsToSend.size()>0){
        	mails.add(buildMailToSend(ctc.id,emailsToSend,emailAttachments,emailSubject,emailHtmlBody,emailPlainBody,emailccAddress,owea));      
        }
        if (!Test.isRunningTest()) {
            Messaging.sendEmail(mails);
        }     
    }
    
    global Messaging.SingleEmailMessage buildMailToSend(id objId,string[] emailsToSend,Messaging.EmailFileAttachment[] emailAttachments,String emailSubject,String emailHtmlBody,String emailPlainBody,string[] emailccAddress,OrgWideEmailAddress[] owea){
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject(emailSubject);
        if(emailsToSend.size()>0){
        	System.debug('emailsToSend'+emailsToSend);
       		mail.setToAddresses(emailsToSend);
        }
   		if(emailccAddress.size()>0)
        	mail.setCcAddresses(emailccAddress);
        mail.setHtmlBody(emailHtmlBody);
        mail.setPlainTextBody(emailPlainBody);
        mail.setFileAttachments(emailAttachments);
        mail.setSaveAsActivity(true); 
        mail.setTargetObjectId(objId); 
        mail.setTreatTargetObjectAsRecipient(false);       
        mail.setUseSignature(false);
        if(owea.size() > 0) 
        {
            mail.setOrgWideEmailAddressId(owea.get(0).Id);
        }  
        return mail;
    }
    
    private string getMonthName(datetime dt)
    {
    string[] monthnames = new List<String>();
    monthnames.add('January');
    monthnames.add('February');
    monthnames.add('March');
    monthnames.add('April');
    monthnames.add('May');
    monthnames.add('June');
    monthnames.add('July');
    monthnames.add('August');
    monthnames.add('September');
    monthnames.add('October');
    monthnames.add('November');
    monthnames.add('December');
    Integer MonthNo = (dt.month());
    if (MonthNo > 11) { MonthNo = 0;}
    system.debug('MonthNo:' + MonthNo);
    return monthnames[MonthNo];

    }
}