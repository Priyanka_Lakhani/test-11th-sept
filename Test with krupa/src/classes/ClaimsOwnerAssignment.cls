/*
Created Date: Feb 4 2019
Last Modified Date: Feb 4 2019
Last Modified By: I2C Thamizh
Description: Assign ownership to claims
*/
public without sharing class ClaimsOwnerAssignment{

    @AuraEnabled
    public Expense_Claim__c exp{get;set;}
    
    @AuraEnabled
    public Boolean Err{get;set;}
    
    @AuraEnabled
    public String ErrMsg{get;set;}
    
    @AuraEnabled
    public static ClaimsOwnerAssignment assignSelf(String claimId){
        ClaimsOwnerAssignment cd1 = new ClaimsOwnerAssignment();
        cd1.Err = false;
        cd1.ErrMsg = '';
        cd1.exp = new Expense_Claim__c();
        String clQuery = '';
        system.debug('***claimId***'+claimId);
        try{
            if(claimId != null && claimId !=''){
                clQuery = 'Select id , OwnerId, Claim_Status__c from Expense_Claim__c where Id =:claimId';
                cd1.exp = Database.query(clQuery);
                cd1.exp.Claim_Status__c = 'Processing';
                cd1.exp.OwnerId = Userinfo.getuserId();
                system.debug('***cd1.exp***'+cd1.exp);
                update cd1.exp;
                
                Set<String> cdids = new Set<String>();
                Map<Id,Id> claimownerMap = new Map<Id,Id>();
                Map<Id,Id> docClaimMap = new Map<Id,Id>();
                for(Expense_Claim__c claim:[Select id, ownerId from Expense_Claim__c where Id =:claimId]){
                    claimownerMap.put(claim.Id, claim.ownerId);
                }
                List<ContentVersion> cvlist = new List<ContentVersion>();
                for(ContentDocumentLink CD:[Select id, ContentDocumentId, LinkedEntityId from ContentDocumentLink WHERE LinkedEntityId =:claimId]){
                    cdids.add(CD.ContentDocumentId);
                    docClaimMap.put(CD.ContentDocumentId,CD.LinkedEntityId);
                }
                if(cdids.size() > 0){
                    for(ContentVersion cv:[SELECT ContentDocumentId,OwnerId,SharingOption,SharingPrivacy,Status__c,Title,VersionNumber FROM ContentVersion where ContentDocumentId in:cdids]){
                        String claimId1 = '';
                        if(docClaimMap.keyset().contains(cv.ContentDocumentId)){
                            claimId1 = docClaimMap.get(cv.ContentDocumentId);
                        }
                        if(claimId1 != ''){
                            if(claimownerMap.keyset().contains(claimId1)){
                                cv.ownerid = claimownerMap.get(claimId1);
                                cvlist.add(cv);
                            }
                        }
                    }
                }
                if(cvlist.size() > 0){
                    update cvlist;
                }
                system.debug('***cvlist***'+cvlist);
            }
        }catch(Exception ex){
            system.debug('***ex***'+ex.getMessage());
            cd1.Err = true;
            cd1.ErrMsg = ex.getMessage();
        }
        return cd1;
    }
    
}