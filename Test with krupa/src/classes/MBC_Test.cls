@isTest(SeeAllData=true) private class MBC_Test {
    static Account club {get;set;}
    static Account client {get;set;}
    static Account tow {get;set;}
    static Contact cont {get;set;}
    static Territory_Coverage__c terr {get;set;}
    static Program__c prog {get;set;}
    static VIN__c vin {get;set;}
    static CA__c settings {get;set;}
    
    static void Utility()
    {
        club = new Account(Name='club ABC', Account_Number__c='999000',ETA__c= 60, RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        
        client = new Account(
            Name='Mercedes-Benz', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;
        cont = new Contact(LastName='Test',Email='abc@mail.com',Contact_External__c='DevelopmentContact_DoNotDelete004');
        insert cont;
        terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Florida',
            Province_Code__c='FL'
        );
        insert terr;
        
        prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            call_flow_JLR__c = true,
            Call_Flow_EHI__c = true,
            sponsorid__c= '000001',
            programid__c = '000012',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            NotifyExcessiveUsg__c = true
        );
        insert prog;
        
        vin = new VIN__c(
            Name='VINxxxxxxxxxxxxxx1',
            UID__c='VINxxxxxxxxxxxxxx1',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        settings = CA__c.getOrgDefaults();

    }
     static testMethod void testServiceCAA() {
        Utility();
        
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Miscellaneous_Call_Reason__c=settings.Call_Flow_Personal_Update_Request__c,
            Phone__c='123',
            Call_ID__c='ABC2',
            DsptCenter__c = '000034',
            club__c = club.id
        );
        insert c;
        
        ApexPages.currentPage().getParameters().put('Id', c.Id);
        
        CaseExtension ce = new CaseExtension(new ApexPages.StandardController(c));
        if(ce.current_call==null) { ce.current_call = new Case(); }
        ce.current_call.Program__c=prog.Id;
        //ce.selectedProgram=prog;
        //ce.init();
        //ce.saveAndExit();
        IP__c ip = new IP__c(
            Name=club.Account_Number__c,
            IP__c='IP',
            Port__c='Port',
            D2000__c=false
        );
        insert ip;
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'error',' { "id" : "67ba4fa16118b5c7:12ee0c53:15f7f145aac:1156" , "timestamp" : "2017-11-03T07:50:53Z" , "errors" : ["missing required details"]}',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        Test.startTest();
        DispatchCallController dc = new DispatchCallController(c.id,'Dispatched','20');
        dc.dispatchMBCCall();  

        dc.jlrNHSTAsaveSent('airbag - exploded with metal fragments');
        dc.jlrNHSTAsaveSent('airbag - did not inflate');
              
        dc = new DispatchCallController(c.id,'Dispatched','20');
        dc.dispatchMBCCall();
        
        MBC_Integration mbc = new MBC_Integration(c.id,'20');
        mbc.mbcAuthorize();
        
        settings.API_User_IDs__c='asd';
        //update settings;
        
        mbc = new MBC_Integration(c.id,'20');
        mbc.mbcAuthorize();
        
        ApexPages.currentPage().getParameters().put('Id', c.Id);
        mbc = new MBC_Integration();
        mbc.cancelCall();
        
        mbc = new MBC_Integration(c.id,'23');
        mbc.mbcAuthorize();
    
        RestCallouts rs = new RestCallouts();
        RestCallouts.callout('http://testurl.com','test','test123','jsonString',c.Id,'POST');
        rs.mbccallout('http://testurl.com','test','test123','jsonString',c.Id,'PUT');
        rs.mbccallout('http://testurl.com','test','test123','jsonString',c.Id,'PUTT');
        RestCallouts.getMercedesBenzBreakDownData(c.Id);
        RestCallouts.getMercedesBenzBreakDownData(ip.id);
        RestCallouts.sendNotDispatchEmailToMBC(c);
        test.stopTest();
    }
    static testMethod void testServiceAAA() {
        Utility();
        
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Miscellaneous_Call_Reason__c=settings.Call_Flow_Personal_Update_Request__c,
            Call_ID__c='ABC1',
            Trouble_Code__c = 'Boost',
            Street__c='1000 AAA dr',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Phone__c = '1234567891',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            Club_Call_Number__c = '111111',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            first_name__c = 'daviddaviddaviddaviddaviddavid',
            last_name__c = 'daviddaviddaviddaviddaviddavid',
            club__c = club.id
        );
        insert c;
        
        ApexPages.currentPage().getParameters().put('Id', c.Id);
        
        CaseExtension ce = new CaseExtension(new ApexPages.StandardController(c));
        if(ce.current_call==null) { ce.current_call = new Case(); }
        ce.current_call.Program__c=prog.Id;
        //ce.selectedProgram=prog;
        //ce.init();
        //ce.saveAndExit();
        //ce.ChangeBreakdownCountry();
        
                    
        system.debug('c.club_code__c:' + c.Club_Code__c);
        System.debug('Client_Account_ID__c :'+c.Client_Account_ID__c );
        
        settings.API_User_IDs__c='asd';
        update settings;
        
        IP__c ip = new IP__c(
            Name=club.Account_Number__c,
            IP__c='IP',
            Port__c='Port',
            D2000__c=false,
            AAA__c = true
        );
        insert ip;
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'success','',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        Test.startTest();
     
        DispatchCallController dc = new DispatchCallController(c.id,'Dispatched','20');
        dc.dispatchMBCCall();        
        
        MBC_Integration mbc = new MBC_Integration(c.id,'20');
        mbc.isAAAMsg = true;
        mbc.mbcAuthorize();
        
        ApexPages.currentPage().getParameters().put('Id', c.Id);
        mbc = new MBC_Integration();
        mbc.cancelCall();
        
        mbc = new MBC_Integration(c.id,'23');
        mbc.mbcAuthorize();
        
        c.Status =settings.ER_Status__c;
        update c;
        c.First_Name__c= 'abc';
        update c;
        c.Status =settings.CL_Status__c;
        update c;
        CloneCase cloneC = new CloneCase(new ApexPages.StandardController(c));
        cloneC.init();
        
        RestCallouts.updateServiceCall(c.Id);
        RestCallouts.dispatchCase(c.Id);
        RestCallouts.callout('http://testurl.com','test','test123','jsonString',c.Id,'POSTT');
        test.stopTest();
    }
    static testMethod void testServiceDispatchCalls() {
        Utility();
        Case c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Country__c = 'US',
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Call_ID__c='ABC3',
            Trouble_Code__c = 'Tow',
            Tow_Reason__c ='Lost Keys',
            Miscellaneous_Call_Reason__c ='Lost Keys',
            Tow_Exchange__c ='Yes',
            Street__c='1000 AAA dr',
            Vehicle_Colour__c = 'white',
            Vehicle_Make__c='Jaguar',
            Vehicle_Model__c ='XF',
            Phone__c = '1234567891',
            Location_Code__c = 'Home',
            Phone_Type__c = 'Cellular',
            Club_Call_Number__c = '111111',
            DsptCenter__c = '000034',
            servicingclub__c = '014',
            first_name__c = 'daviddaviddaviddaviddaviddavid',
            last_name__c = 'daviddaviddaviddaviddaviddavid',
            club__c = club.id,
            Exchange_Pickup_Location_Email__c ='abc@gmail.com;test1@mail.com'
        );
        insert c;
        
        ApexPages.currentPage().getParameters().put('Id', c.Id);
        
        CaseExtension ce = new CaseExtension(new ApexPages.StandardController(c));
        if(ce.current_call==null) { ce.current_call = new Case(); }
        ce.current_call.Program__c=prog.Id;
        //ce.selectedProgram=prog;
        //ce.init();
        //ce.saveAndExit();
        //ce.ChangeBreakdownCountry();
        
                    
        system.debug('c.club_code__c:' + c.Club_Code__c);
        System.debug('Client_Account_ID__c :'+c.Client_Account_ID__c );
        
        settings.API_User_IDs__c='asd';
        update settings;
        
        IP__c ip = new IP__c(
            Name=club.Account_Number__c,
            IP__c='IP',
            Port__c='Port',
            D2000__c=false,
            AAA__c = true
        );
        insert ip;
        GlobalClass.NewGuid();
        GlobalClass.UnscrambleText(GlobalClass.ScrambleText('xxxxxxx'));
        GlobalClass.getCharAtIndex('xxxxxxxx', 5);
        GlobalClass.getDatayyyyMMdd(system.today());
        ApexPages.currentPage().getParameters().put('s',settings.DI_Status__c);
        ApexPages.currentPage().getParameters().put('m', '20');
        DispatchCallController dc = new DispatchCallController(new ApexPages.StandardController(c));
        
        CA_Test_HttpCalloutMock fakeresp = new CA_Test_HttpCalloutMock(200,'success','',null);
        Test.setMock(HttpCalloutMock.class,fakeresp);
        Test.startTest();
        
        dc.dispatchCall();
        test.stopTest();       
    }
}