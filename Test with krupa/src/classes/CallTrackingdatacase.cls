public without sharing class CallTrackingdatacase {
	Public case conRec{get;set;}
	Public String callRecId;
	public string csId{get;set;}
	public XPR_NX_CallTrackingData_A__b callRec{get;set;}
	Public List<XPR_NX_CallTrackingData_A__b> callReclst{get;set;}
	Public List<PermissionSetAssignment> Permissioncheck{get;set;}
	public CallTrackingdatacase(ApexPages.StandardController controller) {
		csId = '';
		callRecId = '';
				callRec = new XPR_NX_CallTrackingData_A__b();
		callReclst = new List<XPR_NX_CallTrackingData_A__b>();
		csId = ApexPages.currentPage().getParameters().get('Id');
		system.debug('***csId****'+csId);
		callRecId = [select Id,Interaction_ID__c from case where Id = :ApexPages.currentPage().getParameters().get('Id')].Interaction_ID__c;
		system.debug('***callRecId****'+callRecId);
		Permissioncheck =  [SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId= :UserInfo.getUserId() AND PermissionSet.Name = 'Call_Tracking_Data'];
			 
		if (Permissioncheck.size() > 0){
			if(callRecId != ''){
				callReclst = [select XPR_NX_Call_ID__c,XPR_NX_RecordingFile__c,Created_Date__c,XPR_NX_PKID__c,XPR_NX_AgentReleased__c,XPR_NX_AgentUsername__c,XPR_NX_ANI__c,XPR_NX_Assisted__c,XPR_NX_CallDuration__c,XPR_NX_CallType__c,XPR_NX_Conference__c,XPR_NX_Direction__c,XPR_NX_DNIS__c,XPR_NX_EXT__c,XPR_NX_HeldCount__c,XPR_NX_HoldAbandon__c,XPR_NX_HoldTime__c,XPR_NX_CreatedTS__c,XPR_NX_InteractionType__c,XPR_NX_IvrTime__c,XPR_NX_PostCall__c,XPR_NX_Skill__c,XPR_NX_State__c,XPR_NX_TransferNumber__c,XPR_NX_Transferred__c,XPR_NX_WaitTime__c,XPR_NX_Workgroup__c from  XPR_NX_CallTrackingData_A__b where XPR_NX_Call_ID__c =:callRecId];
				system.debug('*******'+callReclst);
				if(callReclst.size() > 0){
					callRec = callReclst[0];
					system.debug('*******'+callReclst);
				}
				else{
					ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO,'No Call Record Found');
					ApexPages.addMessage(myMsg);
				}
			}
		}
		else{
			  ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.INFO,'Insufficent Access Privileges');
			 ApexPages.addMessage(myMsg1);
		}
			  

	}
  
}