public with sharing class CloseCase {
    
    public Case c { get; set; }
    public String mode { get; set; }
    public List<SelectOption> status_options { get; set; }
    public boolean dispAccuraNSX;
    
    public CloseCase(ApexPages.StandardController controller) {
        c = (Case)controller.getRecord();
        system.debug('****'+c.Id);
        String caseFields = '';
        for(Schema.SObjectField f : CASE.SObjectType.getDescribe().fields.getMap().values()) {
            caseFields+= (f.getDescribe().getName() + ',');
        }
        caseFields=caseFields.left(caseFields.length()-1);
        
        String query = 'Select ' + caseFields + ',RecordType.DeveloperName,Program__r.Program_Code__c,Dealership__r.Name from Case where Id=\''+c.id+'\' LIMIT 1';
            
            System.debug('REFRESH QUERY: ' + query);
            System.debug('QUERY length: ' + query.length());
            c = database.query(query);

        mode = 'Close';
        getCloseStatus();
        dispAccuraNSX = true;
        
        String status = ApexPages.currentPage().getParameters().get('s');
        if(status==NULL||status=='') { 
            status = CA__c.getOrgDefaults().CL_Status__c;
            c.Status=status; 
        }
        
        if(c.DI_Status_Date_Time__c!=NULL) { 
            mode = 'Cancel'; 
            c.Status=CA__c.getOrgDefaults().XX_Status__c;
        }
    }
    public void getCloseStatus() {
        status_options = new List<SelectOption>{
            new SelectOption(CA__c.getOrgDefaults().CL_Status__c, CA__c.getOrgDefaults().CL_Status__c),
            new SelectOption(CA__c.getOrgDefaults().XX_Status__c, CA__c.getOrgDefaults().XX_Status__c)
        };
    }
    public PageReference save() {
        if (mode=='Close'&& c.RecordType.DeveloperName=='Roadside_Assistance' && c.Kill_Code__c == null)
        {
            c.Kill_Code__c.addError('Please select a Close Reason');
            return null;
        }
            
        
        if (c.RecordType.DeveloperName=='Roadside_Assistance' && mode=='Cancel' && c.Close_Reason_Non_Roadside__c==null)
        {
            c.Close_Reason_Non_Roadside__c.addError('Please select a Cancel Reason');
            return null;
        }

        if(c.Trouble_Code__c == 'Miscellaneous (Non Dispatch)' && c.Miscellaneous_Call_Reason__c == 'NSX campaign' && (c.Client_Code__c == '566' || c.Client_Code__c == '567' || c.Client_Code__c == '568' || c.Client_Code__c == '805')){
            dispAccuraNSX = false;
        }
         //TB SiriusXM start
        //if(mode == 'Close'){
                String msgerror = '';
                String caseSiriusId = c.Sirius_XM_ID__c;
                if(c.Sirius_XM_ID__c != null && c.Sirius_XM_ID__c != ''){
                
                    String siriusXMclienCode = '';
                    if(c.Program__r.Program_Code__c == '560' || c.Program__r.Program_Code__c == '801'){
                    
                        siriusXMclienCode = 'honda';
                        
                    }
                    else if(c.Program__r.Program_Code__c == '563' || c.Program__r.Program_Code__c == '804'){
                    
                        siriusXMclienCode = 'acura';
                        
                    }
                    try{
                    
                        SiriusXM_Service.terminate(caseSiriusId,'ReasonCode',siriusXMclienCode,c.Program__r.Program_Code__c);
                        //c.Description = 'TEST SIRIUS FROM TAKECALL';
                        
                    }catch(exception ex){
                    
                        msgerror = 'Sirius XM Exceptiom :' + ex.getMessage();
                        //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,msgerror);
                        //ApexPages.addMessage(myMsg);
                        
                    }
                }
                //TB SiriusXM end
                //Send Email Acura NSX
                if(dispAccuraNSX == false){
                    sendEmailForAcuraNSXCampaign();
                }
        //}
        update c;
        if(mode=='Close') { return back(); }
        if(c.Status==CA__c.getOrgDefaults().XX_Status__c) { return new PageReference('/apex/DispatchCallUI?d=o&m=21&title=Cancel Service Call&Id='+c.Id).setRedirect(true); }
        return back();
    }
    public PageReference saveAndRoadside() {
        
        update c;
   
        return new PageReference('/apex/TakeCall?pid='+c.Id).setRedirect(true);
    }
    
    public void sendEmailForAcuraNSXCampaign(){
        List<OrgWideEmailAddress> orgwide = [select id, address, displayname from OrgWideEmailAddress where displayname = 'noreply@xperigo.com'];
        Id templateId;    
        try {
            templateId = [select id, name from EmailTemplate where developername = 'Acura_NSX_Campaign'].id;
        }catch (Exception e) {}
        
        Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
        
        Automatic_email_recipients__c emails_addr = Automatic_email_recipients__c.getOrgDefaults();
    
        Messaging.SingleEmailMessage mailRon = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {};
        emails_addr.Acura_NSX_Campaign_Emails__c = emails_addr.Acura_NSX_Campaign_Emails__c == null ? '' : emails_addr.Acura_NSX_Campaign_Emails__c;
        for (String em: emails_addr.Acura_NSX_Campaign_Emails__c.split(',')) {
            toAddresses.add(em);
        }
        mailRon.setToAddresses(toAddresses);
        if(orgwide.size() > 0){
            mailRon.setOrgWideEmailAddressId(orgwide[0].Id);
        }
        String[] ccAddresses = new String[] {};
        emails_addr.Acura_NSX_Campaign_CC_Emails__c = emails_addr.Acura_NSX_Campaign_CC_Emails__c == null ? '' : emails_addr.Acura_NSX_Campaign_CC_Emails__c;
        for (String em: emails_addr.Acura_NSX_Campaign_CC_Emails__c.split(',')) {
            ccAddresses.add(em);
        }
        mailRon.setCCAddresses(ccAddresses);
        mailRon.setTemplateID(templateId);
        mailRon.setWhatId(c.id);
        mailRon.setTargetObjectId(ctc.id);
        mailRon.setTreatTargetObjectAsRecipient(false);

        if (!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                mailRon
            });
        }

    }
    
    public PageReference back() {
        return new PageReference('/'+c.Id).setRedirect(true);
    }
}