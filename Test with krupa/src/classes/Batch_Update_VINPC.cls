global class Batch_Update_VINPC implements Database.Batchable<sObject>, Database.Stateful{

    global INTEGER totRec2upd = 0;//Total records with null VINPC
    global INTEGER totRecPassed = 0;//Total records passed
    global INTEGER totRecFailed = 0;//Total records failed

   global Database.QueryLocator start(Database.BatchableContext BC){
      //Query to retrieve all VIN records with blank VINPC
      List<String> progc = new List<String>{'930'};
      String queryVIN = 'SELECT ID, VINPC__c, PROGRAM_CODE__C, NAME FROM VIN__c WHERE VINPC__c = NULL AND PROGRAM_CODE__C in:progc';
      return Database.getQueryLocator(queryVIN);
   }

  global void execute(Database.BatchableContext BC, List<sObject> scope){
      List<VIN__c> vinlist = new List<VIN__c>();
      for(sobject s : scope){
        VIN__c vin = (VIN__c)s;
        if(vin.NAME != null && vin.PROGRAM_CODE__C != null){
          totRec2upd++;
          vin.VINPC__c = vin.NAME + vin.PROGRAM_CODE__C;//Logic to concatenate the VIN NO & PROGRAM CODE
          system.debug('****vin.VINPC__c****'+vin.VINPC__c);
        }
        vinlist.add(vin); 
      }
      if(vinlist.size() > 0){
        try{
          List<Database.SaveResult> updateresults = Database.Update(vinlist,false);//DML update that skips the failed records
          for(Integer i=0;i<updateresults.size();i++){
            if(updateresults.get(i).isSuccess()){
              totRecPassed++;//Increment the total passed record count
            }else{
              totRecFailed++;//Increment the toal failed record count
            }
          }
        }catch(Exception ex){
          system.debug('****EXCEPTION THROWN****'+ex.getmessage());//Mostly error wouldnt be captured since we are using database dml
        }
      }
  }

  global void finish(Database.BatchableContext BC){//Logic to send out email to scheduler on completion
    AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email from AsyncApexJob where Id =:BC.getJobId()];
    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
    String[] toAddresses = new String[] {a.CreatedBy.Email};
    mail.setToAddresses(toAddresses);
    mail.setSubject('VINPC Update Batch: ' + a.Status);
    mail.setPlainTextBody('Total Records With Null VINPC:' + totRec2upd +', Total Records Updated:' + totRecPassed +
    ', Total Records Failed:'+ totRecFailed);
    if(!test.IsRunningTest()){
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
  }
}