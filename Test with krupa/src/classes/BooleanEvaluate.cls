/*************************************************************************************************
* @author Alex Parsin   <alex.parsin@integrate2cloud.com>
*
* @description Processes the complex logic provided for the "OR" Dependencies
*/
public with sharing class BooleanEvaluate {
    private static final String ANDv = 'AND';
    private static final String ORv = 'OR';
    private static final String OPEN = '(';
    private static final String CLOSE = ')';
    private String expression;
    private Boolean[] values;
    private String finalExpression;

    /*************************************************************************************************
     * @description Constructor.
     * 
     * @param expression String, the Complex Logic expression to be processed
     * 
     * @param values List of Booleans, the list of all parent Dependencies results
    */
    public BooleanEvaluate(String expression, Boolean[] values){
		this.values = values;
        this.expression = expression.replaceAll('\\|\\|', 'OR').replaceAll('&&', 'AND');
        System.debug('[AP] [BooleanEvaluate] expression = ['+ this.expression +']');
        this.formatExpression();
    }

    /*************************************************************************************************
     * @description Constructor.
     * 
     * @param expression String, the Complex Logic expression to be processed
     * 
     * @param arguments List of Strings, the list of all parent Dependencies results
    */
    public BooleanEvaluate(String expression, String[] arguments){
        expression = expression.replaceAll('\\|\\|', 'OR').replaceAll('&&', 'AND');
        this.expression = String.format( expression , arguments );
    }

    /*************************************************************************************************
     * @description Processes the Complex Logic
     *
     * @return Boolean, the result of the Complex Logic evaluation
    */
    public Boolean evaluate(){
        evaluate(this.expression);
        System.debug(this.expression);
        System.debug(this.finalExpression);
        Boolean result = evaluateExpression(this.finalExpression);
        System.debug(result);
        return result;
    }

    /*************************************************************************************************
     * @description Evaluates the string containing the Complex Logic
     * 
     * @param expression String, contains the Complex Logic (i.e. 1 OR 2)
    */
    private void evaluate(String expression){
        if(!expression.contains(OPEN)){
            this.finalExpression = expression;
            return;
        }
        Integer indexOfOpen = -1;
        Integer indexOfClose = -1;
        String[] chars = expression.split('');
        for(Integer i = 0; i < chars.size(); i++){
            String singleChar = chars[i];
            if(singleChar == OPEN) {
                indexOfOpen = i;
                continue;
            }
            if(singleChar == CLOSE) {
                indexOfClose = i;
                break;
            }
        }
        String replace = expression.substring(indexOfOpen + 1 , indexOfClose);
        expression = expression.replace( OPEN + replace + CLOSE, String.valueOf( evaluateExpression(replace) ) );
        System.debug(replace);
        System.debug(expression);
        System.debug('########');
        evaluate(expression);
    }

    /*************************************************************************************************
     * @description Evaluates the string containing the Complex Logic
     * 
     * @param expression String, contains the Complex Logic (i.e. true OR false)
     *
     * @return Boolean, the result of the Complex Logic evaluation
    */
    private Boolean evaluateExpression(String expression){
        Boolean result = false;
        System.debug('[AP] [evaluateExpression] expression = [' + expression + ']');
        for( String conj : expression.split(ORv) ){
            System.debug('[AP] [evaluateExpression] conj = [' + conj + ']');
            Boolean b = true;
            for( String single : conj.split(ANDv) ){
            	System.debug('[AP] [evaluateExpression] single = [' + single + ']');
                b &= Boolean.valueOf(single.trim());
            }
            System.debug('[AP] [evaluateExpression] b = [' + b + ']');
            result |= b;
        }
        System.debug(result);
        return result;
    }

    /*************************************************************************************************
     * @description Formats the expression and replaces arguments with the respective values
    */
    private void formatExpression(){
		if(this.expression.countMatches(OPEN) != this.expression.countMatches(CLOSE))
            this.expression = '';
		String regex = '[^0-9]{1,}|\\-';
        List<String> arguments = this.expression.replaceAll(regex, ' ').trim().split(' ');
        System.debug('[AP] [formatExpression] arguments = [' + JSON.serialize(arguments) + ']');
        System.debug('[AP] [formatExpression] this.values = [' + JSON.serialize(this.values) + ']');
        for(Integer i = arguments.size();i>=0;i--){
            try{
                this.expression = this.expression.replaceFirst(arguments[i], String.valueOf(this.values[i]));
            }catch(Exception e){
                continue;
            }  
        }
        System.debug(this.expression);
    }
}