public with sharing class IntergationCaseReportCtrl {
    public List<CaseWrapper> caseWrapListInt {get; set;}
    public List<CaseWrapper> caseWrapListSucc {get; set;}
    public List<AccountWrapper> dispClubsInt {get; set;}
    public List<CaseWrapper> caseWrapListServTrack {get; set;}
    public List<AccountWrapper> dispClubsServTrack {get; set;}
    public Program__c program {get;set;}
    public Set<String> programCodeToSet;
    public DateTime startDate {get;set;}
    public DateTime endDate {get;set;}
    public String stDate {get;set;}
    public String enDate {get;set;}
    
    public IntergationCaseReportCtrl() {
        String programCode = ApexPages.currentPage().getParameters().get('code');
        stDate = ApexPages.currentPage().getParameters().get('start');
        enDate = ApexPages.currentPage().getParameters().get('end');
        try{
            program = [SELECT Id, Name FROM Program__c WHERE Program_code__c =:programCode LIMIT 1];
        }
        catch(Exception ex){
            program = new Program__c();
        }
        programCodeToSet = new Set<String>();
        if(programCode.contains(',')) {
            List<String> codes = programCode.split(',');
            for(String cd : codes) {
                programCodeToSet.add(cd);
            }             
        } else {
            programCodeToSet.add(programCode);
        }
        if(stDate != '' && stDate != null){
            string[] stDt = stDate.split('/');
            startDate = DateTime.newInstance(Integer.valueof(stDt[2]),Integer.valueof(stDt[1]),Integer.valueof(stDt[0]),00,00,01);
        }
        else {
            startDate = DateTime.newInstance(2017,12,07,00,00,01);
            stDate = '07/12/2017';
        }
        if(enDate != '' && enDate != null){
            string[] enDt = enDate.split('/');
            endDate = DateTime.newInstance(Integer.valueof(enDt[2]),Integer.valueof(enDt[1]),Integer.valueof(enDt[0]),23,59,59);
        }
        else {
            endDate = system.now();
            enDate = system.now().format('dd/MM/yyyy');
        }
        system.debug('TB DEBUG START DATE: ' + startDate);
        system.debug('TB DEBUG END DATE: ' + endDate);
        intergationCaseReport();
        serviceTrackerCaseReport();
    }
    
    public void intergationCaseReport() {
        List<Case> caseList = new List<Case>();
        caseList = [SELECT id, Club__c, Club__r.Name, CaseNumber, Program__r.Name, Program__c,
                           (SELECT id, Message__c, CreatedDate, Name 
                            FROM Integration__r 
                            ORDER BY CreatedDate DESC) 
                    FROM Case 
                    WHERE 
                        Program__r.Program_Code__c IN :programCodeToSet AND Club__c != null AND CreatedDate >=: startDate AND CreatedDate <=: endDate];        
        dispClubsInt = new List<AccountWrapper>();             
        caseWrapListInt = new List<CaseWrapper>(); 
        map<id,String> mapClub = new map<id,String>();
        //map<id,Integer> mapClubSuccess = new map<id,Integer>();
        map<id,List<Case>> mapClubSuccessCases = new map<id,List<Case>>();
        caseWrapListSucc = new List<CaseWrapper>();
        for(Case cs : caseList) {
            if(cs.Integration__r.size() > 0) {
                Set<String> setIntCode = new Set<String>();
                for(Integration__c integ : cs.Integration__r) {
                    setIntCode.add(integ.Message__c);
                }
                if(setIntCode.contains('31') || 
                   setIntCode.contains('32') || 
                   setIntCode.contains('33') || 
                   setIntCode.contains('34')|| 
                   setIntCode.contains('35')) {
                    
                    /*if(mapClubSuccess.containsKey(cs.Club__c)){
                        mapClubSuccess.put(cs.Club__c, mapClubSuccess.get(cs.Club__c) + 1);
                    }else{
                        mapClubSuccess.put(cs.Club__c,1);
                    }*/
                    
                    if(mapClubSuccessCases.containsKey(cs.Club__c)){
                        mapClubSuccessCases.get(cs.Club__c).add(cs);
                        CaseWrapper cw = new CaseWrapper();
                        cw.caseId = cs.id;
                        cw.caseNumber = cs.CaseNumber;
                        cw.integrationList = cs.Integration__r;
                        cw.recordsSize = cs.Integration__r.size();
                        cw.clubId = cs.Club__c;
                        cw.programName = cs.Program__r.Name;
                        cw.programId = cs.Program__c;
                        caseWrapListSucc.add(cw);
                    }else{
                        mapClubSuccessCases.put(cs.Club__c,new List<Case>{cs});
                        CaseWrapper cw = new CaseWrapper();
                        cw.caseId = cs.id;
                        cw.caseNumber = cs.CaseNumber;
                        cw.integrationList = cs.Integration__r;
                        cw.recordsSize = cs.Integration__r.size();
                        cw.clubId = cs.Club__c;
                        cw.programName = cs.Program__r.Name;
                        cw.programId = cs.Program__c;
                        caseWrapListSucc.add(cw);
                    }
                      
                } else {
                    //AccountWrapper aw = new ()
                    mapClub.put(cs.Club__c, cs.Club__r.Name);
                    CaseWrapper cw = new CaseWrapper();
                    cw.caseId = cs.id;
                    cw.caseNumber = cs.CaseNumber;
                    cw.integrationList = cs.Integration__r;
                    cw.recordsSize = cs.Integration__r.size();
                    cw.clubId = cs.Club__c;
                    cw.programName = cs.Program__r.Name;
                    cw.programId = cs.Program__c;
                    caseWrapListInt.add(cw);
                }
            }
        }   
        
        for (id clubId : mapClubSuccessCases.keySet()) {
            if (!mapClub.keySet().contains(clubId)) {
                AccountWrapper aw = new AccountWrapper();
                aw.accId = clubId;
                aw.accName = mapClubSuccessCases.get(clubId)[0].Club__r.Name;
                aw.numCaseFailed = 0;
                List<CaseWrapper> innerList = new List<CaseWrapper>();
                for(CaseWrapper cw : caseWrapListSucc) {
                    if(clubId == cw.clubId) {
                        innerList.add(cw);
                    }
                }
                aw.cwList = innerList;
                if(mapClubSuccessCases.containsKey(clubId)){
                    if(mapClubSuccessCases.get(clubId).size() != 0){
                        aw.caseSuccess = 100;
                        aw.numCaseSuccess = mapClubSuccessCases.get(clubId).size();
                    }
                }
                else {
                    aw.caseSuccess = 0.00;
                    aw.numCaseSuccess = 0;
                }
                dispClubsInt.add(aw);
            }
        }
            
        for(id clubId : mapClub.keySet()) {
            AccountWrapper aw = new AccountWrapper();
            aw.accId = clubId;
            aw.accName = mapClub.get(clubId);
            List<CaseWrapper> innerList = new List<CaseWrapper>();
            for(CaseWrapper cw : caseWrapListINt) {
                if(clubId == cw.clubId) {
                    innerList.add(cw);
                }
            }
            aw.cwList = innerList;
            aw.numCaseFailed = innerList.size();
            if(mapClubSuccessCases.containsKey(clubId)){
                if(mapClubSuccessCases.get(clubId).size() != 0 || innerList.size() != 0){
                    aw.caseSuccess = Decimal.valueOf(mapClubSuccessCases.get(clubId).size()).divide(mapClubSuccessCases.get(clubId).size() + innerList.size(), 3) * 100;
                    aw.numCaseSuccess = mapClubSuccessCases.get(clubId).size();
                }
            }
            else {
                aw.caseSuccess = 0.00;
                aw.numCaseSuccess = 0;
            }
            dispClubsInt.add(aw);
        } 
    }
    
    public void serviceTrackerCaseReport() {
        List<Case> caseList = new List<Case>();
        caseList = [SELECT id, Club__c, Club__r.Name, CaseNumber, Program__r.Name, Program__c,
                           (SELECT id, Name, CreatedDate,Driver_Geolocation__Latitude__s
                            FROM Service_Tracker_Details__r
                            ORDER BY CreatedDate DESC) 
                    FROM Case 
                    WHERE 
                        Program__r.Program_Code__c IN :programCodeToSet AND Club__c != null AND Club__r.Service_Tracker_Enabled__c = TRUE 
                        AND Program__r.Service_Tracker_Enabled__c = TRUE /*AND isClosed = FALSE*/ AND Club_Call_Number__c != null
                        AND CreatedDate >=: startDate AND CreatedDate <=: endDate ORDER BY CreatedDate DESC];   
                             
        dispClubsServTrack = new List<AccountWrapper>();             
        caseWrapListServTrack = new List<CaseWrapper>(); 
        map<id,String> mapClub = new map<id,String>();
        map<id,Integer> mapClubSuccess = new map<id,Integer>();
        //map<id,List<Service_Tracker_Detail__History>> serviceTrackerHistories = new map<id,List<Service_Tracker_Detail__History>>();
        for(Case cs : caseList) {
            
                if (true) {
                    //AccountWrapper aw = new ()
                    mapClub.put(cs.Club__c, cs.Club__r.Name);
                    CaseWrapper cw = new CaseWrapper();
                    cw.caseId = cs.id;
                    cw.caseNumber = cs.CaseNumber;
                    //cw.serviceTrackerList = cs.Service_Tracker_Details__r;
                    //cw.recordsSize = cs.Service_Tracker_Details__r.size();
                    cw.clubId = cs.Club__c;
                    cw.programName = cs.Program__r.Name;
                    cw.programId = cs.Program__c;
                    caseWrapListServTrack.add(cw);
                }
            if(cs.Service_Tracker_Details__r.size() > 0) {
                if(cs.Service_Tracker_Details__r[0].Driver_Geolocation__Latitude__s != null && cs.Service_Tracker_Details__r[0].Driver_Geolocation__Latitude__s != 0){
                    if( mapClubSuccess.containsKey(cs.Club__c)){
                        mapClubSuccess.put(cs.Club__c, mapClubSuccess.get(cs.Club__c) + 1);
                    }else{
                         mapClubSuccess.put(cs.Club__c,1);
                    }
                }
                /*for(Service_Tracker_Detail__c std: cs.Service_Tracker_Details__r){
                    serviceTrackerHistories.put(std.id, null);
                }*/
            }
        }   
        /*for(Service_Tracker_Detail__c std: [SELECT id,(SELECT OldValue, NewValue, CreatedDate, Field FROM Histories ORDER BY CreatedDate DESC) FROM Service_Tracker_Detail__c WHERE id IN: serviceTrackerHistories.keySet()]){
        
        }   */
        for(id clubId : mapClub.keySet()) {
            AccountWrapper aw = new AccountWrapper();
            aw.accId = clubId;
            aw.accName = mapClub.get(clubId);
            List<CaseWrapper> innerList = new List<CaseWrapper>();
            for(CaseWrapper cw : caseWrapListServTrack) {
                if(clubId == cw.clubId) {
                    innerList.add(cw);
                }
            }
            aw.cwList = innerList;
            
            if(mapClubSuccess.containsKey(clubId)){
            
                aw.numCaseFailed = innerList.size() - mapClubSuccess.get(clubId);
                aw.numCaseSuccess = mapClubSuccess.get(clubId);
                aw.caseSuccess = Decimal.valueOf(mapClubSuccess.get(clubId)).divide(innerList.size(), 3) * 100;
                
            }
            else {
                aw.caseSuccess = 0.00;
                aw.numCaseSuccess = 0;
                aw.numCaseFailed = innerList.size();
            }
            dispClubsServTrack.add(aw);
        } 
    }
    
    public class CaseWrapper {
        public Id clubId {get; set;}
        public Integer recordsSize {get; set;}
        public Id caseId {get; set;}
        public String caseNumber {get; set;}
        public String programName {get; set;}
        public Id programId {get; set;}
        public List<Integration__c> integrationList {get; set;}
        public List<Service_Tracker_Detail__c> serviceTrackerList {get; set;}
    }
    public class AccountWrapper {
        public Id accId {get; set;}
        public String accName {get; set;}
        public List<CaseWrapper> cwList {get; set;} 
        public Decimal caseSuccess {get;set;}
        public Integer numCaseSuccess {get;set;}
        public Integer numCaseFailed {get;set;}
    }    
}