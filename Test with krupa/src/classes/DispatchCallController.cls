public with sharing class DispatchCallController {
    public String page_message { get; set; }
    public String altMessage { get; private set; }
    public static CA_Integration ca { get; set; }
    public String m { get; set; }
    public String title {get;set;}
    String new_status;
    Public Case c;

    public DispatchCallController(ApexPages.StandardController controller) {
        //c = new Case(Id=controller.getRecord().Id);
        //c = (Case)controller.getRecord();
        c = [select id,NHSTA_prompt__c,ETA__c,Close_the_loop_option__c,casenumber,Phone__c,Tow_Reason__c,Program__r.Program_Code__c,Status,DsptCenter__c,Sirius_XM_ID__c,Club_Call_Number__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c from case where id = :controller.getRecord().id limit 1];
        new_status = ApexPages.currentPage().getParameters().get('s');
        m = ApexPages.currentPage().getParameters().get('m');
        title = ApexPages.currentPage().getParameters().get('title');
    } 

    public void dispatchCall() {
        System.debug('123,'+c.Club_Call_Number__c+','+c.DsptCenter__c+','+m);
        try {
            if((c.Club_Call_Number__c==null && m=='20') || m!='20') {
                ApexPages.currentPage().getParameters().put('d','o');
                system.debug('Success4');
                
                //TB SiriusXM start
                String msgerror = '';
                String caseSiriusId = c.Sirius_XM_ID__c;
                
                if(c.Sirius_XM_ID__c != null && c.Sirius_XM_ID__c != '' && (title == null ? false : title.containsIgnoreCase('dispatch'))){
                
                    String siriusXMclienCode = '';
                    
                    if(c.Program__r.Program_Code__c == '560' || c.Program__r.Program_Code__c == '801'){
                    
                        siriusXMclienCode = 'honda';
                        
                    }
                    else if(c.Program__r.Program_Code__c == '563' || c.Program__r.Program_Code__c == '804'){
                    
                        siriusXMclienCode = 'acura';
                        
                    }
                    
                    try{
                    
                        SiriusXM_Service.terminate(caseSiriusId,'ReasonCode',siriusXMclienCode,c.Program__r.Program_Code__c);
                        
                    }catch(exception ex){
                    
                        msgerror = 'Sirius XM Exceptiom :' + ex.getMessage();
                        Bug__c bug = new Bug__c(
                            Case__c = c.Id,
                            Description__c = ex.getMessage(),
                            Resolved__c = false,
                            Type__c = 'Dispatch Sirius XM Exceptiom'
                        );
                        insert bug;
                        
                    }
                }
                //TB SiriusXM end
                
                ca = new CA_Integration();
                if(Test.isRunningTest()){
                    ca.final_url = 'Success';
                    new_status = 'Dispatched';
                }
                else{
                    ca.authorize();
                }
                
                Boolean updateCase = (ca.final_url!=NULL&&!ca.final_url.startsWithIgnoreCase('error'));
                System.debug('123.');
                if((updateCase && new_status!=NULL&&new_status!='')|| Test.isRunningTest()) {
                    
                    //TB TR 656 start
                    c.Status=new_status;
                    update c;
                    //TB TR 656 END
                    
                    //TB TR 416 start
                    if((title == null ? false : title.containsIgnoreCase('dispatch'))){
                        if (c.NHSTA_prompt__c != null) {
                            if(c.NHSTA_prompt__c.startsWithIgnoreCase('Yes')) {
                                jlrNHSTAsaveSent((c.NHSTA_prompt__c.substringAfter('Yes')).trim());
                            }
                        }
                        //TB TR 416 end
                        
                        //TB close the loop start
                        if(c.Close_the_loop_option__c == 'Yes'){
                            Close_the_loop__c newCl_lp = new Close_the_loop__c();
                            newCl_lp.Case__c = c.id;
                            newCl_lp.ETA__c = c.ETA__c == null ? 45 : c.ETA__c;
                            newCl_lp.ETA_total__c = c.ETA__c == null ? 45 : c.ETA__c;
                            newCl_lp.Note__c = 'Initial request';
                            insert newCl_lp;
                        }
                        
                    }
                    
                    //TB close the loop end
                    // c = [select id,casenumber,Phone__c,Tow_Reason__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c from case where id = :c.id limit 1];
                    
                    
                    //Excessiv Usage: 72 hours 3+ dispatches
                    if (c.Program__r.NotifyExcessiveUsg__c && c.VIN_Member_ID__c !='' & c.VIN_Member_ID__c != null)
                        CheckExcessiveUsage();  
                    if (c.Program__r.Call_Flow_EHI__c)
                    {
                        System.debug('123....');
                        try
                        {
                            if (c.Trouble_Code__c.Contains('Tow') && c.Miscellaneous_Call_Reason__c !=null && c.Miscellaneous_Call_Reason__c =='Lost Keys')
                                EmailEHILostKey();
                        }
                        catch(System.exception ex){

                            Bug__c bug = new Bug__c(
                                Case__c = c.Id,
                                Description__c = ex.getMessage(),
                                Resolved__c = false,
                                Type__c = 'Dispatch EmailEHILostKey Exception'
                            );
                            insert bug;

                        }
                        
                        try
                        {
                            if(c.Tow_Exchange__c !=null && c.Tow_Exchange__c =='Yes')
                                EmailEHITowExchange();
                        }
                        catch(System.exception ex){

                            Bug__c bug = new Bug__c(
                                Case__c = c.Id,
                                Description__c = ex.getMessage(),
                                Resolved__c = false,
                                Type__c = 'Dispatch EmailEHITowExchange Exception'
                            );
                            insert bug;

                        }
                    }
                    
                }
                system.debug('Success8');
                page_message = updateCase?'Confirm':'Error';
            } else {
                page_message = 'Warning';
                altMessage = 'This call has already been dispatched. If you would like to dispatch another service call, please CLONE this call and click on the Dispatch Call button.';
            }
        } catch(Exception ex) {
            page_message = 'Error';
            Bug__c bug = new Bug__c(
                Case__c = c.Id,
                Description__c = ex.getMessage(),
                Resolved__c = false,
                Type__c = 'Dispatch Page Exception'
            );
            insert bug;
        }
    }
    public pageReference back() {
        return new PageReference('/'+c.Id).setRedirect(true);
    }
    
    public void EmailEHILostKey()
    {
        list<string> emails = new list<string>();
        boolean branchemail = false;
        try
        {
            string bemail =  [select Tow_Exchange_Email_Address__c from account where gpbr__c = :c.gpbr__c and status__c != 'Inactive' limit 1].Tow_Exchange_Email_Address__c;
            string[] bemails = bemail.split(';');
            for (string st : bemails)
            {
                if (st.contains('@'))
                {
                    branchemail = true;
                    emails.add(st);
                }
            }
        }
        catch(System.exception ex){}
        
        string[] demails = CA__c.getOrgDefaults().EHILostKeyEmail__c.split(';');
        for (string st : demails)
            emails.add(st);

        Id templateId;  
        try {
            if(branchemail)
                templateId = [select id, name from EmailTemplate where developername = 'EHI_Lost_Key'].id;
            else
                templateId = [select id, name from EmailTemplate where developername = 'EHI_Lost_Key_No_Tow_Exchange_Email_Setup'].id;
        }catch (Exception e) {}
        
        Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.toAddresses = emails;
        mail.setReplyTo('noreply@salesforce.com');
        mail.SetSenderDisplayName ('noreply@salesforce.com');
        mail.setTemplateId(templateId);
        mail.setTargetObjectId(ctc.id);
        mail.setTreatTargetObjectAsRecipient(false);       
        mail.setWhatId(c.id);
        
        Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage> {mail};
        if(!Test.isRunningTest()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
        }
    }
    
    public void EmailEHITowExchange()
    {
        string bemail1 = c.Exchange_Pickup_Location_Email__c;
        string empemail = c.Employee_s_Email__c;
        list<string> emails1 = new list<string>();
        list<string> emails2 = new list<string>();

        System.debug('1231111');
        boolean gpbremail = false;
        
        if (bemail1 !=null && bemail1 != '')
        {
            string[] demails1 =bemail1.split(';');
            for (string st : demails1)
            {   
                gpbremail = true;
                emails1.add(st);
            }
        }   
            
        if (!gpbremail)
            return;
            
        emails2.add(UserInfo.getUserEmail());
        string[] demails = CA__c.getOrgDefaults().EHITowExchangeEmail__c.split(';');
        for (string st : demails)
            emails2.add(st);

        if (empemail != null && empemail != '')
            emails2.add(empemail);

        Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(emails1);
        mail.setCcAddresses(emails2);
            
        Id templateId;    
        try {
            templateId = [select id, name from EmailTemplate where developername = 'Miscellaneous_Calls_Tow_Exchange_Vehicle_Pickup'].id;
        }catch (Exception e) {}
        
        mail.setReplyTo('noreply@salesforce.com');
        mail.SetSenderDisplayName ('Xperigo');
        mail.setTemplateID(templateId);
        mail.setWhatId(c.id);
        mail.setSaveAsActivity(true);
        mail.setTargetObjectId(ctc.id);
        mail.setTreatTargetObjectAsRecipient(false);  
                        
        Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage> {mail};
        if(!Test.isRunningTest()) {
            Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
        }
        
        c.Tow_Exchange_Date__c = System.date.today();
        system.debug('c.Tow_Exchange_Date__c:' + c.Tow_Exchange_Date__c);
        update c;       
    }   
    
    public void CheckExcessiveUsage()
    {
        //DI_Status_Date_Time__c
        DateTime dt = System.now().addhours(-72);
        try
        {
            System.debug('123222');            
            list<case> cs = [select id from case where DI_Status_Date_Time__c > :dt and SP_Status_Date_Time__c > :dt  and Club_Call_Number__c !='' and VIN_Member_ID__c = :c.VIN_Member_ID__c limit 100];
            if (cs.size() >=3)
            {
                list<ExcessiveUsage__c> eus = new list<ExcessiveUsage__c>();
                System.debug('123eus');
                for (Case ce : cs)
                {
                    ExcessiveUsage__c eu = new ExcessiveUsage__c();
                    eu.case__c = ce.id;
                    eus.add(eu);
                }
                
                insert eus; 
            }
        }
        catch(System.Exception ex)
        {
            Bug__c bug = new Bug__c(
                Case__c = c.Id,
                Description__c = ex.getMessage(),
                Resolved__c = false,
                Type__c = 'Dispatch CheckExcessiveUsage Exception'
            );
            insert bug;
        }
        system.debug('CheckExcessiveUsage....');
        
    } 
     // MBC Changes Start
    public DispatchCallController(Id caseId,String status,String message_type) {
        //c = new Case(Id=controller.getRecord().Id);
        //c = (Case)controller.getRecord();
        c = [select id,casenumber,Phone__c,Tow_Reason__c,Status,DsptCenter__c,Club_Call_Number__c,Dealership__r.Name,Program__r.NotifyExcessiveUsg__c,gpbr__c,Breakdown_Location__latitude__s,Breakdown_Location__longitude__s,PickupCountry__c,PickupProvince__c,PickupStreet__c,PickupCity__c,LastModifiedDate,Trouble_Code__c,Tow_Exchange__c,Tow_Exchange_Date__c,Exchange_Pickup_Location_Email__c,Employee_s_Email__c,Email_Recipients__c,Miscellaneous_Call_Reason__c,program__r.Call_Flow_EHI__c,VIN_Member_ID__c,first_name__c,last_name__c,Reservation__c,street__c,City__c,Province__c,Country__c,Destination_Name__c,Destination_Street__c,Destination_City__c,Destination_Province__c,Destination_Country__c,DI_Status_Date_Time__c from case where id = :caseId limit 1];
        new_status=status;
        m=message_type;
    }

    public void dispatchMBCCall() {
        System.debug('dispatchMBCCall ========= '+ c.id+' - ' + new_status+c.DsptCenter__c);
        try {
            if((c.Club_Call_Number__c==null || c.DsptCenter__c !=null || m!='20')) {
                system.debug('Success4');
                MBC_Integration mbc = new MBC_Integration(c.id,'20');
                mbc.mbcAuthorize();
                Boolean updateCase = (mbc.final_url!=NULL&&!mbc.final_url.startsWithIgnoreCase('error'));
                if((updateCase && new_status!=NULL&&new_status!='')|| Test.isRunningTest()) {
                    System.debug('update case with new status:'+new_status);
                    c.Status=new_status;                                      
                    update c;

                    //Excessiv Usage: 72 hours 3+ dispatches
                    if (c.Program__r.NotifyExcessiveUsg__c && c.VIN_Member_ID__c !='' & c.VIN_Member_ID__c != null)
                    CheckExcessiveUsage();                    
                }
                system.debug('Success8');
                page_message = updateCase?'Confirm':'Error';
            } else {
                page_message = 'Warning';
                altMessage = 'This call has already been dispatched. If you would like to dispatch another service call, please CLONE this call and click on the Dispatch Call button.';
            }
        } catch(Exception ex) {
            page_message = 'Error';
            Bug__c bug = new Bug__c(
                Case__c = c.Id,
                Description__c = ex.getMessage(),
                Resolved__c = false,
                Type__c = 'Dispatch dispatchMBCCall Exception'
            );
            insert bug;
        }
    }
     
     // MBC Changes END   
     
    //TB TR 416 start
    public void jlrNHSTAsaveSent(String nHSTAreason) {
        try{
            NHSTA__c nhsta = NHSTA__c.getOrgDefaults();
            
            Id templateId;  
            try {
                templateId = [select id, name from EmailTemplate where developername = 'JLR_NHSTA'].id;
            }catch (Exception e) {}
             
            Messaging.SingleEmailMessage jlrNHSTAmail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[]{};
                
            if (nHSTAreason == 'airbag - exploded with metal fragments' || nHSTAreason == 'airbag - ruptured, hole in airbag, airbag in pieces') {
                for(String em : nhsta.Critical_Concerns_team__c.split(',')){
                    toAddresses.add(em);
                }
                for(String em : nhsta.NA_Product_Investigation_team__c.split(',')){
                    toAddresses.add(em);
                }
            }
            else if (nHSTAreason == 'airbag - did not inflate' || (nHSTAreason == null? false : nHSTAreason.containsIgnoreCase('other'))) {
                for(String em : nhsta.NA_Product_Investigation_team__c.split(',')){
                    toAddresses.add(em);
                }
            }

            String[] ccAddresses = new String[]{};
            for(String em : nhsta.JLR_RSA_general_email__c.split(',')){
                ccAddresses.add(em);
            } 
            Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
            
            jlrNHSTAmail.setToAddresses(toAddresses);
            jlrNHSTAmail.setCCAddresses(ccAddresses);
            jlrNHSTAmail.setTemplateId(templateId);
            jlrNHSTAmail.setTargetObjectId(ctc.id);
            jlrNHSTAmail.setTreatTargetObjectAsRecipient(false);       
            jlrNHSTAmail.setWhatId(c.id);
            
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'noreply@xperigo.com'];
            if(owea.size() > 0) 
            {
                jlrNHSTAmail.setOrgWideEmailAddressId(owea[0].Id);
            } 
            if(!Test.isRunningTest()) {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] { jlrNHSTAmail });
            }
        }catch(Exception ex){
            Bug__c bug = new Bug__c(
                Case__c = c.Id,
                Description__c = ex.getMessage(),
                Resolved__c = false,
                Type__c = 'NHSTA Email Alert Exception'
            );
            insert bug;
        }
    }
    //TB TR 416 end
}