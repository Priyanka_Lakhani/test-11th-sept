/*
Created Date: Jan 10 2019
Last Modified Date: Jan 10 2019
Last Modified By: I2C Thamizh
Description: Contains invocable method which is called from flow to clone flow entity parent record along with its dependents
*/
public without sharing class FlowEntity_Clone{

    @AuraEnabled
    public Flow_Entity__c FE{get;set;}
    
    @AuraEnabled
    public String retId{get;set;}
    
    @AuraEnabled
    public Boolean Err{get;set;}
    
    @AuraEnabled
    public String ErrMsg{get;set;}

    //@InvocableMethod(label='Clone Function' description='Clone Parent & Child Records of Entity')
    @AuraEnabled
    public static FlowEntity_Clone cloneFlowEntity(List<String> feIds){
        FlowEntity_Clone FEClone = new FlowEntity_Clone();
        FEClone.Err = false;
        FEClone.ErrMsg = '';
        FEClone.retId = '';
        FEClone.FE = new Flow_Entity__c();
        List<Id> clonedIds = new List<Id>(); //Returns the Id of the parent flow record
        List<String> feIdLst = new List<String>(); //Assigns the Id of the parent flow record to be cloned
        List<Id> flowIds = new List<Id>(); //Assigns the Ids of all the flow entity parent & child records to be cloned
        Map<Id, Id> oldNewMap = new Map<Id, Id>(); //Assigns the ids of the record to be cloned as key & ids of the record cloned as values
        Map<Id, Flow_Entity__c> feMap = new Map<Id, Flow_Entity__c>(); //Assigns ids of the Flow Entity record to be cloned as key & flow entity clone record as values
        Map<Id, Flow_Entity__c> feLogicMap = new Map<Id, Flow_Entity__c>(); //Assigns ids of the Flow Entity record that contain dependency logic and needs to be updated after creating dependencies
        List<Flow_Entity__c> feToUpdate = new List<Flow_Entity__c>(); //Assigns list of flow entities records that needs to set dependency logic
        List<Flow_Entity_Dependency__c> fedList = new List<Flow_Entity_Dependency__c>(); //Assigns list of cloned flow entity records 
        string feQuery = ''; //Dynamic query to retrieve flow entity records
        string fedQuery = ''; //Dynamic query to retrieve flow entity dependency records
        Boolean isFlow = true; //Keeps track whether the cloned entity is a Flow
        List<Flow_Entity__c> flowList = new List<Flow_Entity__c>();
        flowList = [SELECT RecordType.Name FROM Flow_Entity__c WHERE Id IN :feIds];
        if(flowList.size() > 0){
            if(flowList[0].RecordType.Name == 'Field'){
                isFlow = false;
            }
        }
        system.debug('***feIds***'+feIds);
        try{
            if(feIds != null && feIds.size() > 0){
                feIdLst.addAll(feIds);
            }
            if(feIdLst.size() >0){ // Retrieves Parent flow Entity record along with its child flow entity records.
                feQuery = 'Select ' + getAllFields('Flow_Entity__c') + ' from Flow_Entity__c where Id in:feIdLst';
                if(isFlow){
                    feQuery += ' OR Parent_Flow__c in:feIdLst';
                }
                feQuery += ' ORDER BY Record_Type_Hierarchy__c ASC, Display_Order__c ASC, CreatedDate ASC';
                for(Flow_Entity__c FE: Database.query(feQuery)){
                    if(FE.Parent_Flow__c == null && FE.Parent_Entity__c == null){
                        FE.Name = FE.Name + ' Cloned';
                    }
                    FE.Upload_ID__c = ''; 
                    if(FE.Dependency_Logic__c != null && FE.Dependency_Logic__c != '')
                        feLogicMap.put(FE.Id,FE.clone(false,true,false,false));
                    FE.Dependency_Logic__c= '';                              
                    feMap.put(FE.Id,FE.clone(false,true,false,false));
                    
                }
            }
            system.debug('***feMap***'+feMap);
            system.debug('***feMap size***'+feMap.size());
            if(feMap.size() > 0){ //Retrieve All Flow Entity Dependencies records associated to the flow that is cloned
                flowIds.addAll(feMap.keyset());
                system.debug('***flowIds size***'+flowIds.size());
                fedQuery = 'Select ' + getAllFields('Flow_Entity_Dependency__c') + ' from Flow_Entity_Dependency__c where Dependency_Parent__c in:flowIds OR Dependency_Child__c in:flowIds OR Marker_Dependency_Parent__c in:flowIds OR Marker_Dependency_Child__c in:flowIds';
                for(Flow_Entity_Dependency__c FED: Database.query(fedQuery)){
                    FED.Upload_ID__c = '';                    
                    fedList.add(FED.clone(false,true,false,false));
                }
            }
            system.debug('***fedList***'+fedList);
            system.debug('***fedList size***'+fedList.size());
            if(feMap.size() > 0){
                insert feMap.values(); //Insert all cloned flow entity records
                for(Id oldId:feMap.keyset()){ //Reassign relationship fields to refer to the new record id instead of old
                    oldNewMap.put(oldId,feMap.get(oldId).Id);
                    Flow_Entity__c FE = feMap.get(oldId);
                    system.debug('***FE***'+FE.ID);
                    if(((FE.Parent_Flow__c == null) && (FE.Parent_Entity__c == null)) || !isFlow){
                        system.debug('***FE1***'+FE.ID);
                        FEClone.FE = FE;
                        FEClone.retId = FE.Id;
                        clonedIds.add(FE.Id);
                    }
                    if(FE.Parent_Flow__c != null){
                        if(feMap.keyset().contains(FE.Parent_Flow__c)){
                            FE.Parent_Flow__c = feMap.get(FE.Parent_Flow__c).Id;
                        }
                    }
                    if(FE.Parent_Entity__c != null){
                        if(feMap.keyset().contains(FE.Parent_Entity__c)){
                            FE.Parent_Entity__c = feMap.get(FE.Parent_Entity__c).Id;
                        }
                    }
                    if(FE.Map_Value_From__c != null){
                        if(feMap.keyset().contains(FE.Map_Value_From__c)){
                            FE.Map_Value_From__c = feMap.get(FE.Map_Value_From__c).Id;
                        }
                    }
                }
                system.debug('***oldNewMap***'+oldNewMap);
                upsert feMap.values(); //update the cloned records with new record ids for the relationship fields
                if(fedList.size() > 0){
                    for(Flow_Entity_Dependency__c FED:fedList){ //Reassign relationship fields to refer to the new record id instead of old
                        if(FED.Dependency_Parent__c != null){
                            if(oldNewMap.keyset().contains(FED.Dependency_Parent__c)){
                                FED.Dependency_Parent__c = oldNewMap.get(FED.Dependency_Parent__c);
                            }
                        }
                        if(FED.Dependency_Child__c != null){
                            if(oldNewMap.keyset().contains(FED.Dependency_Child__c)){
                                FED.Dependency_Child__c = oldNewMap.get(FED.Dependency_Child__c);
                            }
                        }
                        if(FED.Marker_Dependency_Parent__c != null){
                            if(oldNewMap.keyset().contains(FED.Marker_Dependency_Parent__c)){
                                FED.Marker_Dependency_Parent__c = oldNewMap.get(FED.Marker_Dependency_Parent__c);
                            }
                        }
                        if(FED.Marker_Dependency_Child__c != null){
                            if(oldNewMap.keyset().contains(FED.Marker_Dependency_Child__c)){
                                FED.Marker_Dependency_Child__c = oldNewMap.get(FED.Marker_Dependency_Child__c);
                            }
                        }
                    }
                    insert fedList;
                }
                if(feLogicMap.size() > 0){
                   for(Id feId:feLogicMap.keyset()){
                        Flow_Entity__c FE = feLogicMap.get(feId);
                        if(oldNewMap.keyset().contains(feId)){
                            feToUpdate.add(new Flow_Entity__c(Id=oldNewMap.get(feId), Dependency_Logic__c=FE.Dependency_Logic__c));
                        }
                   } 
                   System.debug('***feToUpdate***'+feToUpdate);
                   upsert feToUpdate;
                }
            }
            FEClone.Err = false;
            system.debug('***FEClone***'+FEClone);
        }catch(Exception ex){
            system.debug('***ex***'+ex.getMessage());
            FEClone.Err = true;
            FEClone.ErrMsg = ex.getMessage();
        }
        return FEClone;
    }
    
    private static string getAllFields(String objectAPI){ //Retrieves all field api names for the object
        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objectAPI).getDescribe().fields.getMap();
        String strFields = '';
        for(String fieldName : fieldMap.keyset()){
          if(strFields == null || strFields == ''){
            strFields = fieldName;
          }else{
            strFields = strFields + ' , ' + fieldName;
          }
        }
        return strFields;
    }
    
    @AuraEnabled
    public static FlowEntity_Clone saveClonedRec(String caFeClone, string retId){
        FlowEntity_Clone FEClone = new FlowEntity_Clone();
        try{
            FEClone.FE = new Flow_Entity__c();
            system.debug('******caFeClone*****'+caFeClone);
            FEClone.FE = (Flow_Entity__c)JSON.deserialize(caFeClone, Flow_Entity__c.class);
            FEClone.retId = retId;
            update FEClone.FE;
            FEClone.Err = false;
        }catch(Exception ex){
            system.debug('***ex***'+ex.getMessage());
            FEClone.Err = true;
            FEClone.ErrMsg = ex.getMessage();
        }
        return FEClone;
    }

}