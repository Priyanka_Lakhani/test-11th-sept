public class JSON2Apex {

    public class Address_components {
        public String long_name;
        public String short_name;
        public List<String> types;
    }

    public List<Results> results;
    public String status;

    public class Location {
        public Double lat;
        public Double lng;
    }

    public class Geometry {
        public Location location;
        public String location_type;
        public Viewport viewport;
    }

    public class Results {
        public List<Address_components> address_components;
        public String formatted_address;
        public Geometry geometry;
        public String place_id;
        public List<String> types;
    }

    public class Viewport {
        public Location northeast;
        public Location southwest;
    }

    
    public List<Calls> Calls;
    public String UserIdentifier;
    public Error Error;
    
    public class Error {
        public String ErrorCode;
        public String Description;
    }
    public class Calls {
        public String CallNumber;
        public DateTime CallDate;
        public String CallState;
        public String ProblemDescription;
        public String GarageName;
        public String CarMake;
        public String CarModel;
        public String CarYear;
        public String CarColor;
        public String BreakDownLocation;
        public String BreakDownCity;
        public String CrossStreet;
        public String LocationDescription;
        public String Direction;
        public String ETA;
        public DateTime ETA_DateTime;
        public String ReceiveTime;
        public String TowDestination;
        public Double BreakDownLatitude;
        public Double BreakDownLongitude;
        public Integer BreakDownAccuracy;
        public Double DriverLatitude;
        public Double DriverLongitude;
        public Integer DriverGPSAccuracy;
        public String DriverGPSTime;
        public Integer NextUpdate;
        public String AlertMessage;
        public String ServicingClubCode;
        public String DeviceID;
        public String DeviceType;
        public String DriverCode;
        public String DriverName;
        public String TrackingToken;
    }
    
    public class SecurityToken {
        public String ApplicationID;
        public String ApplicationPassword;
        public String ApplicationMethodName;
    }

    public String RequestID;
    public String ServicingClubCode;
    public String CallNumber;
    public SecurityToken SecurityToken;
    public String TrackingToken;


    public static JSON2Apex parse(String json) {
        return (JSON2Apex) System.JSON.deserialize(json, JSON2Apex.class);
    }
    
    /*static testMethod void testParse() {
        String json = '{'+
        '   \"results\" : ['+
        '      {'+
        '         \"address_components\" : ['+
        '            {'+
        '               \"long_name\" : \"9140\",'+
        '               \"short_name\" : \"9140\",'+
        '               \"types\" : [ \"street_number\" ]'+
        '            },'+
        '            {'+
        '               \"long_name\" : \"Leslie Street\",'+
        '               \"short_name\" : \"Leslie St\",'+
        '               \"types\" : [ \"route\" ]'+
        '            },'+
        '            {'+
        '               \"long_name\" : \"Richmond Hill\",'+
        '               \"short_name\" : \"Richmond Hill\",'+
        '               \"types\" : [ \"locality\", \"political\" ]'+
        '            },'+
        '            {'+
        '               \"long_name\" : \"York Regional Municipality\",'+
        '               \"short_name\" : \"York Regional Municipality\",'+
        '               \"types\" : [ \"administrative_area_level_2\", \"political\" ]'+
        '            },'+
        '            {'+
        '               \"long_name\" : \"Ontario\",'+
        '               \"short_name\" : \"ON\",'+
        '               \"types\" : [ \"administrative_area_level_1\", \"political\" ]'+
        '            },'+
        '            {'+
        '               \"long_name\" : \"Canada\",'+
        '               \"short_name\" : \"CA\",'+
        '               \"types\" : [ \"country\", \"political\" ]'+
        '            },'+
        '            {'+
        '               \"long_name\" : \"L4B 3L6\",'+
        '               \"short_name\" : \"L4B 3L6\",'+
        '               \"types\" : [ \"postal_code\" ]'+
        '            }'+
        '         ],'+
        '         \"formatted_address\" : \"9140 Leslie Street, Richmond Hill, ON L4B 3L6, Canada\",'+
        '         \"geometry\" : {'+
        '            \"location\" : {'+
        '               \"lat\" : 43.8571541,'+
        '               \"lng\" : -79.38652999999999'+
        '            },'+
        '            \"location_type\" : \"ROOFTOP\",'+
        '            \"viewport\" : {'+
        '               \"northeast\" : {'+
        '                  \"lat\" : 43.8585030802915,'+
        '                  \"lng\" : -79.38518101970848'+
        '               },'+
        '               \"southwest\" : {'+
        '                  \"lat\" : 43.8558051197085,'+
        '                  \"lng\" : -79.3878789802915'+
        '               }'+
        '            }'+
        '         },'+
        '         \"place_id\" : \"ChIJH4qU8TErK4gR_fS7gHszVMQ\",'+
        '         \"types\" : [ \"street_address\" ]'+
        '      }'+
        '   ],'+
        '   \"status\" : \"OK\"'+
        '}';
        JSON2Apex obj = parse(json);
        System.assert(obj != null);
    }
    
    json = '{'+
        '   \"Calls\":'+
        '   ['+
        '       {'+
        '           \"CallNumber\":\"2251\",'+
        '           \"CallDate\":\"2015-10-05T00:00:00\",'+
        '           \"CallState\":\"OS\",'+
        '           \"ProblemDescription\":\"TOW\",'+
        '           \"GarageName\":\"STELLAR\",'+
        '           \"CarMake\":\"HYUNDAI\",'+
        '           \"CarModel\":\"SANTA FE\",'+
        '           \"CarYear\":\"2003\",'+
        '           \"CarColor\":\"SILVER\",'+
        '           \"BreakDownLocation\":\"18 ROSEGLOR CRES\",'+
        '           \"BreakDownCity\":\"TORONTO, SCARBOROUGH\",'+
        '           \"CrossStreet\":\"BRITWELL AVE (LAWRENCE AVE E & BRIMLEY RD)\",'+
        '           \"LocationDescription\":\"DRIVEWAY\",'+
        '           \"Direction\":\"\",'+
        '           \"ETA\":\"02:25pm\",'+
        '           \"ReceiveTime\":\"01:22pm\",'+
        '           \"TowDestination\":\"100 COMMERCE VALLEY DRIVE WEST\",'+
        '           \"BreakDownLatitude\":43.7551231,'+
        '           \"BreakDownLongitude\":-79.2578354,'+
        '           \"BreakDownAccuracy\":0,'+
        '           \"DriverLatitude\":43.7544136,'+
        '           \"DriverLongitude\":-79.25761,'+
        '           \"DriverGPSAccuracy\":0,'+
        '           \"DriverGPSTime\":\"02:25pm\",'+
        '           \"NextUpdate\":0,'+
        '           \"AlertMessage\":\"\",'+
        '           \"ServicingClubCode\":\"282\",'+
        '           \"DeviceID\":null,'+
        '           \"DeviceType\":null,'+
        '           \"DriverCode\":null,'+
        '           \"DriverName\":null,'+
        '           \"TrackingToken\":null'+
        '       }'+
        '   ],'+
        '   \"UserIdentifier\":\"6202821234567890\",'+
        '   \"Error\":null'+
        '}';
        JSON2Apex obj = JSON2Apex.parse(json);
        System.assert(obj != null);
        
        json = '{'+
        '   \"RequestID\":\"KV-TEST-1\",'+
        '   \"ServicingClubCode\":\"\",'+
        '   \"UserIdentifier\":\"6202821234567890\",'+
        '   \"CallNumber\":\"\",'+
        '   \"SecurityToken\":'+
        '   {'+
        '       \"ApplicationID\":\"EnterpriseServiceTrackerWS\",'+
        '       \"ApplicationPassword\":\"Caasco2011\",'+
        '       \"ApplicationMethodName\":\"\"'+
        '   },'+
        '   \"TrackingToken\":\"\" '+
        '}';
        JSON2Apex obj = JSON2Apex.parse(json);
        System.assert(obj != null);

        */
}