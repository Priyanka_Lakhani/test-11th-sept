public class EscalationProcessCtrl{

    public Case cs {get;set;}    
    public Id cId {get;set;}    
    public Integer results {get;set;}
    public Map<Id,Account> nearbyDestinations {get; set;}
    public Map<String, String> parameterMap;
    public List<Preferred_Service_Provider__c> porvidersList {get; set;}
    public Boolean saveCase {get;set;}
    public String selectDealer {get;set;}
    public String ETA_time {get;set;}
    public String ETA_note {get;set;}
    public Boolean isCTL_exist {get;set;}
    public List<Close_the_loop__c> closeTheloopList {get;set;}
    public String selectedDealer {get;set;}
    public Boolean sentEmail {get;set;}
    public List<Close_the_loop__c> openCTL;
    public String dealerProvSeverity {get;set;}
    public String dealerProvText {get;set;}
    
    public EscalationProcessCtrl(){
        //saveCase = false;
        parameterMap = ApexPages.currentPage().getParameters();
        cId = parameterMap.get('Id');
        results = 5;
        openCTL = new List<Close_the_loop__c>();
        dealerProvSeverity = '';
        dealerProvText = '';
        //porvidersList = new List<Preferred_Service_Provider__c>();
        isCTL_exist = false;
        getCaseDetails(cId);
        selectDealer = cs.Dealership__c;
        selectedDealer = cs.Dealership__r.Name;
        sentEmail = !cs.Escalation_Email_sent__c;
        getNearbyAccounts();
    }
     
    public void getNearbyAccounts(){
        
        if(cs.Breakdown_Location__Latitude__s != null && cs.Breakdown_Location__Longitude__s != null){
            dealerProvSeverity = 'INFO';
            dealerProvText = 'Dealerships Details';
            nearbyDestinations = new Map<Id,Account>();
            Decimal lat = cs.Breakdown_Location__Latitude__s;
            Decimal lng = cs.Breakdown_Location__Longitude__s;
            
            Integer distance_limiter = 500;
            String query = 'SELECT ' + getAllFields(new Account()) + ' FROM Account WHERE Account.Location__Latitude__s <> null AND Status__c != \'Inactive\' ';
            
            query += ' AND DISTANCE(Account.Location__c, GEOLOCATION(' + lat + ',' + lng + '), \'km\') < ' + distance_limiter + ' AND Client__c = \'' + cs.Program__r.Account__c + '\' AND (Type=\'Dealership / Tow Destination\' OR Type=\'GPBR\') ';
            query += ' ORDER BY DISTANCE(Account.Location__c, GEOLOCATION(' + lat + ',' + lng + '), \'km\') ASC LIMIT ' + results + '';
            
            system.debug('get nearby accounts query: ' + query);
            
            for(Account acc : Database.query(query)) {
                nearbyDestinations.put(acc.id,acc);
                System.debug('Account: ' + acc.Name);
            }
        }
        else{
            dealerProvSeverity = 'ERROR';
            dealerProvText = 'We can not identify service providers for this Breakdown location. Please double check Breakdown location address.';
        }
    }
    
    public void getCaseDetails(Id cId){
        String query = 'SELECT ' + getAllFields(new Case()) + ', Program__r.Account__c, Dealership__r.Name,Club__r.Name, Program__r.Credit_Card_Details__c FROM Case WHERE Id=\'' + cId + '\' LIMIT 1';
        cs = database.query(query);
        //[SELECT Id, Name, Phone, BillingStreet, BillingCity, BillingState, BillingPostalCode, BillingStreet FROM Account WHERE];
        getProviders(cs.Dealership__c);
        closeTheloopList = [SELECT id, Name, ETA__c, Note__c, Request_Has_Been_Sent__c, Response__c, Response_Date__c,Est_time_of_service__c,ETA_total__c,Close_the_loop_Parent__c,Close_the_loop_Parent__r.Name FROM Close_the_loop__c WHERE Case__c =: cs.id ORDER BY NAME ASC];
        for (Close_the_loop__c ctl : closeTheloopList) {
            if(ctl.Request_Has_Been_Sent__c == false && ctl.Close_the_loop_Parent__c == null){
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Current ETA for Close The Loop ' + ctl.Name + ' is ' + ctl.ETA__c + ' minutes with an estimated time of service at ' + ctl.Est_time_of_service__c.format('dd/MM/yyyy HH:mm a') + '<br/>To increase ETA please enter increment below.'));
                openCTL.add(ctl);
            }
        }
    }
    
    public void getProviders(Id dealerId) {

        porvidersList = new List<Preferred_Service_Provider__c>();
        porvidersList = [SELECT Id, Dealer__c, Provider__c, Provider__r.Name, Provider__r.Phone, Provider__r.BillingStreet, Provider__r.BillingCity, Provider__r.BillingState, Provider__r.BillingPostalCode, Order_Number__c FROM Preferred_Service_Provider__c WHERE Dealer__c =: dealerId ORDER BY Order_Number__c ASC LIMIT 20];
    }
    
    public String getCaseFields() {
        String caseFields = '';
        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.Call_Flow_Field_Inclusions.getFields()) {
            caseFields += (f.getFieldPath() + ',');
        }
        return caseFields.removeEnd(',');
    }
    
    public String getAllFields(SObject sType) {
        DescribeSObjectResult describeResult = sType.getSObjectType().getDescribe();
        List<String> fieldNames = new List<String>( describeResult.getSObjectType().getDescribe().fields.getMap().keySet() );  
        return String.join( fieldNames, ',' );
    }
    
    public void requestCCInfo() {
        cs.Club_Auto_Paid_ETE__c = true;
        
        //cs.ETE_Credit_Card_Retrieved__c = Schema.SObjectType.Program__c.fields.getMap().get('Credit_Card_Details__c').getDescribe().isAccessible();
        JLR_Escalation_settings__c jlrEscSettings = JLR_Escalation_settings__c.getOrgDefaults();
        cs.ETE_Credit_Card_Retrieved__c = jlrEscSettings.Authorized_to_retrieve_the_CC_info_users__c.contains(UserInfo.getName()) || jlrEscSettings.Authorized_to_retrieve_the_CC_info_ext__c.contains(UserInfo.getName());
        if(cs.ETE_Credit_Card_Retrieved__c) {
            cs.ETE_Credit_Card_Retrieved_On__c = DateTime.now();
            cs.ETE_Credit_Card_Retrieved_By__c = userInfo.getUserId();
        } else {
            cs.ETE_Credit_Card_Retrieved_On__c = null;
            cs.ETE_Credit_Card_Retrieved_By__c = null;
        }
    }
    
    public void selectNewDealer () {

        //Account newDealer = [SELECT Id, Name, BillingStreet, BillingCity, BillingStateCode, BillingCountryCode, BillingPostalCode, Phone FROM Account WHERE Id = :selectDealer];
        selectedDealer = nearbyDestinations.get(selectDealer).Name;
        getProviders(selectDealer);
        
    }
    
    public void showMoreDealers() {
        results += 5;
        getNearbyAccounts();
    }
    
    public PageReference closeEscalation(){ 
        PageReference pageRef = new PageReference('/'+ cs.id);
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference saveAndRedirect(){
    
        if (saveCase) {
            
            
            if (ETA_time != null && ETA_time != '') {
                if (ETA_time.isNumeric()) {
                    if(Integer.valueOf(ETA_time) > 0){
                        if (ETA_note == '') {
                             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter value for Note'));
                             return null;
                        }
                        else {
                            
                                Close_the_Loop__c newCTL = new Close_the_loop__c();
                                newCTL.Case__c = cs.id;
                                newCTL.ETA__c = Integer.valueOf(ETA_time);
                                newCTL.Note__c = ETA_note;
                                newCTL.ETA_total__c = newCTL.ETA__c;
                                if(cs.Close_the_loop_option__c != 'Yes'){
                                    newCTL.Request_Has_Been_Sent__c = true;
                                }
                                if(openCTL.size() != 0){
                                    newCTL.Close_the_loop_Parent__c = openCTL[0].id;
                                    newCTL.Request_Has_Been_Sent__c = true;
                                    openCTL[0].ETA_total__c = (openCTL[0].ETA_total__c == null ? openCTL[0].ETA__c + newCTL.ETA__c : openCTL[0].ETA_total__c + newCTL.ETA__c);
                                    update openCTL[0];
                                }
                                
                                insert newCTL;
                                
                        }
                    }
                    else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid value for ETA'));
                        return null;
                    }
                }
                else{
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid value for ETA'));
                    return null;
                }
                
            }
			try{
                //get new ETA set through trigger, and set in available record details cs
                cs.ETA__c = [select id,ETA__c from case where id=: cs.id limit 1].ETA__c; 
            }catch(Exception ex){
                System.debug('Exception:'+ex.getMessage());
            }
            
            cs.Escalation_Modified_By__c = UserInfo.getUserId();
            cs.Escalation_Modified_Date__c = system.now();
            
            Boolean isSent = false;
            if (cs.Customer_accepted_Club_initial_option__c == 'No' && sentEmail) {
                //send email
                isSent = true;
                cs.Escalation_Email_sent__c = isSent;
            }
            
            update cs;
            
            if(isSent){
                sendEmails();
            }
            
        }
        PageReference pageRef = new PageReference('/'+ cs.id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    public void sendEmails() {

        Id templateId;    
        try {
            templateId = [select id, name from EmailTemplate where developername = 'JLR_Escalation_Case'].id;
        }catch (Exception e) {}   
        
		Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
		
        Automatic_email_recipients__c aer = Automatic_email_recipients__c.getOrgDefaults();

        Messaging.SingleEmailMessage escMail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[]{};

        for(String em : aer.Escalation_email_addresses__c.split(',')){
            toAddresses.add(em);
        } 
        
        escMail.setToAddresses(toAddresses);
        escMail.setTemplateID(templateId);
        escMail.setWhatId(cs.id);
        escMail.setTargetObjectId(ctc.id);
        escMail.setTreatTargetObjectAsRecipient(false);
		OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = 'noreply@xperigo.com'];
        if(owea.size() > 0) {
            escMail.setOrgWideEmailAddressId(owea[0].Id);
        }

        if(!Test.isRunningTest()) {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { escMail });
        }
    }
}