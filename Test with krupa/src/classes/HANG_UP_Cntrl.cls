public class HANG_UP_Cntrl{
    

    public PageReference terminateSiriusXM_call(){
        
        Id caseId = ApexPages.currentPage().getParameters().get('id');
        
        Case c = [SELECT id, Sirius_XM_ID__c, Client_Code_Formula__c, Terminate_info__c FROM Case WHERE id=:caseId];
        
        String msgerror = '';
        String caseSiriusId = c.Sirius_XM_ID__c;
                
        if(c.Sirius_XM_ID__c != null && c.Sirius_XM_ID__c != ''){
                
            String siriusXMclienCode = '';
                    
            if(c.Client_Code_Formula__c == '560' || c.Client_Code_Formula__c == '801'){
                    
                siriusXMclienCode = 'honda';
                        
            }
            else if(c.Client_Code_Formula__c == '563' || c.Client_Code_Formula__c == '804'){
                    
                siriusXMclienCode = 'acura';
                        
            }
            
            if(c.Terminate_info__c == null){
                c.Terminate_info__c = '';
            }
            
            try{
  
                SiriusXM_Service.terminate(caseSiriusId,'ReasonCode',siriusXMclienCode,c.Client_Code_Formula__c);
                c.Terminate_info__c += ' Terminate success';        
            }catch(exception ex){
                c.Terminate_info__c += ' Terminate exception: ' + ex.getMessage();
                msgerror = 'Sirius XM Exceptiom :' + ex.getMessage();
                        
            }
            update c;
        }
        PageReference pageRef = new PageReference('/' + caseId);
        pageRef.setRedirect(true);
        return pageRef;
    }
}