@isTest
(SeeAllData=true)
public class IntergationCaseReportCtrlTest {
    static testMethod void myUnitTest() {
        //ApexPages.currentPage().getParameters().put('id', testQuote.id);
        
        Account acc1 = new Account();
        acc1.Name = 'Test';
        insert acc1;
        
        Program__c pr = new Program__c();
        pr.Program_Code__c = '10000';
        pr.Account__c = acc1.id;
        pr.Service_Tracker_Enabled__c = true;
        insert pr;

        Account acc2 = new Account();
        acc2.Name = 'Test2';
        acc2.Service_Tracker_Enabled__c = true;
        acc2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId();
        insert acc2;   
        
        Case cs = new Case();
        cs.Program__c = pr.id;
        cs.Club__c = acc2.id;
        cs.Club_Call_Number__c = '1234';
        insert cs;
        
        Integration__c integ = new Integration__c();
        integ.Case__c = cs.id;
        integ.Message__c = '30';
        insert integ;
                
        Test.startTest();
            ApexPages.currentPage().getParameters().put('code', '10000');
            IntergationCaseReportCtrl cls = new IntergationCaseReportCtrl();
        Test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() {
        //ApexPages.currentPage().getParameters().put('id', testQuote.id);
        
        Account acc1 = new Account();
        acc1.Name = 'Test';
        insert acc1;
        
        Program__c pr = new Program__c();
        pr.Program_Code__c = '10000';
        pr.Account__c = acc1.id;
        pr.Service_Tracker_Enabled__c = true;
        insert pr;

        Account acc2 = new Account();
        acc2.Name = 'Test2';
        acc2.Service_Tracker_Enabled__c = true;
        acc2.RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId();
        insert acc2;   
        
        Case cs = new Case();
        cs.Program__c = pr.id;
        cs.Club__c = acc2.id;
        cs.Club_Call_Number__c = '1234';
        insert cs;
        
        Integration__c integ = new Integration__c();
        integ.Case__c = cs.id;
        integ.Message__c = '35';
        insert integ;
                
        Test.startTest();
            ApexPages.currentPage().getParameters().put('code', '10000');
            IntergationCaseReportCtrl cls = new IntergationCaseReportCtrl();
        Test.stopTest();
        
    }
}