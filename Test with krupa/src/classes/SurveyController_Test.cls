@isTest
private class SurveyController_Test {
    static Account client{get;set;}
    static Account dealer{get;set;}

    static VIN__c vin{get;set;}
    static Program__c prog{get;set;}
    static Case c {get;set;}

    static VIN__c vin2{get;set;}
    static Program__c prog2{get;set;}

    static VIN__c vin3{get;set;}
    static Program__c prog3{get;set;}

    static VIN__c vin4{get;set;}
    static Program__c prog4{get;set;}

    static VIN__c vin5{get;set;}
    static Program__c prog5{get;set;}

    static Program__c prog6{get;set;}

    static Survey__c survey {get;set;}
    static Survey__c survey2 {get;set;}
    static CA__c settings {get;set;}
    static Survey_Question__c sq1 {get;set;}
    static Survey_Question__c sq2 {get;set;}
    static Survey_Question__c sq21 {get;set;}
    static Survey_Question__c sq22 {get;set;}
    static SurveyResponse__c sr1 {get;set;}
    static SurveyResponse__c sr2 {get;set;}
    static SurveyResponse__c sr21 {get;set;}
    static SurveyResponse__c sr22 {get;set;}

    //static Sites__c sites {get;set;}
    
    static void Utility()
    {
    	List<Sites__c> siteList = new List<Sites__c>();
        Sites__c sites = new Sites__c();
        sites.name = 'Survey';
        sites.value__c = 'http://www.salesforce.com';
        siteList.add(sites);
        
        Sites__c sites1 = new Sites__c();
        sites1.name = 'JLRLogo';
        sites1.value__c = '015m0000001BLhX';
        siteList.add(sites1);
        
        Sites__c sites2 = new Sites__c();
        sites2.name = 'Instance';
        sites2.value__c = 'cs20';
        siteList.add(sites2);
        
        Sites__c sites3 = new Sites__c();
        sites3.name = 'KIALogo';
        sites3.value__c = '015m0000001BLhX';
        siteList.add(sites3);
        
        upsert siteList;
        
        settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';
        upsert settings;
        
         client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        survey = new Survey__c(
            name ='JLR Survey'
        );
        insert survey;
        
        survey2 = new Survey__c(
            name ='Kia Survey'
        );
        insert survey2;
        
        List<Survey_Question__c> sqList = new List<Survey_Question__c>();
        
        sq1 = new Survey_Question__c();
        sq1.question__c = 'how are your?';
        sq1.order__c = 1;
        sq1.survey__c = survey.id;
        sq1.type__c = 'Single Select';
        sq1.Language__c = 'English';
        sqList.add(sq1);

        sq2 = new Survey_Question__c();
        sq2.question__c = 'how old are you?';
        sq2.order__c = 2;
        sq2.survey__c = survey.id;
        sq2.type__c = 'Single Select';
        sq2.Language__c = 'French';
        sqList.add(sq2);
        
        sq21 = new Survey_Question__c();
        sq21.question__c = 'how are your?';
        sq21.order__c = 1;
        sq21.survey__c = survey2.id;
        sq21.type__c = 'Single Select';
        sq21.Language__c = 'English';
        sqList.add(sq21);

        sq22 = new Survey_Question__c();
        sq22.question__c = 'how old are you?';
        sq22.order__c = 2;
        sq22.survey__c = survey2.id;
        sq22.type__c = 'Single Select';
        sq22.Language__c = 'French';
        sqList.add(sq22);
        
        upsert sqList;
        
        List<Program__c> prodList = new List<Program__c>();
        
        prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='858',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        prodList.add(prog);

        prog2 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='560',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        prodList.add(prog2);

        prog3 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='563',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        prodList.add(prog3);

        prog4 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='566',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        prodList.add(prog4);

        prog5 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='569',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey.id
        );
        prodList.add(prog5);
        
        prog6 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='503',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            survey__c = survey2.id
        );
        prodList.add(prog6);
        
        insert prodList;
        
        List<VIN__c> vinList = new List<VIN__c>();
                
         vin = new VIN__c(
            Name='VIN1234567890456',
            UID__c='VIN1234567890456',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        vinList.add(vin);

        vin2 = new VIN__c(
            Name='VIN123456789045',
            UID__c='VIN123456789045',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog2.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        vinList.add(vin2);

        vin3 = new VIN__c(
            Name='VIN12345678904560',
            UID__c='VIN12345678904560',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog3.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        vinList.add(vin3);

        vin4 = new VIN__c(
            Name='VIN12345678904563',
            UID__c='VIN12345678904537',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog4.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        vinList.add(vin4);

        vin5 = new VIN__c(
            Name='VIN12345678104567',
            UID__c='VIN12345678901567',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Base_Warranty_Max_Odometer__c=999999,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog5.Id,
            first_name__c = 'test',
            last_name__c = 'test',
            phone__c = '4169999999',
            email__c = 'A@abc.com'
        );
        vinList.add(vin5);

		insert vinList;
    }


    static testMethod void myUnitTest1() {
       
       Utility();
       c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;


        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c).replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss;
        update c;
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        SurveyController sc = new SurveyController();
        sc.init();    
        
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-550'); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        sc.init();    
        
        sr1 = new SurveyResponse__c();
        sr1.Case__c = c.id;
        sr1.Phone__c = '123456';
        sr1.Response__c = '1';
        sr1.ResponseDate__c = DateTime.now();
        sr1.SubmissionDate__c = DateTime.now();
        sr1.SurveyQuestion__c = sq1.id;
        sr1.Callback__c = 'Yes';
        insert sr1;

        sr2 = new SurveyResponse__c();
        sr2.Case__c = c.id;
        sr2.Phone__c = '123456';
        sr2.Response__c = '1';
        sr2.ResponseDate__c = DateTime.now();
        sr2.SubmissionDate__c = DateTime.now();
        sr2.SurveyQuestion__c = sq2.id;
        sr2.Callback__c = 'Yes';
        insert sr2;

        
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        sc.init();    
        sc.submitResults();

        
        sc = new SurveyController();
        sc.init(); 
    
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-555'); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        
        ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c+'-xx'); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        sc = new SurveyController();
        sc.init();           
        
        /*
        ApexPages.currentPage().getParameters().put('id','xxxxx');
        sc = new SurveyController();
        sc.init();*/           

    }
    
    static testMethod void myUnitTest2() 
    {
           
       Utility();
       c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin2.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog2.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;
        
        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();    
    }
    
    static testMethod void myUnitTest3() 
    {
        Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin3.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog3.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;
        
        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,aa,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();    
        
        ss1 = GlobalClass.ScrambleText('aabc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();   
        
        ss1 = GlobalClass.ScrambleText('a@abc.com,007'); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();    
        
        ApexPages.currentPage().getParameters().put('unsubscribe','xxxx');
        sc = new SurveyController();
        sc.init();    

    }

    static testMethod void myUnitTest4() 
    {
        Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin4.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog4.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;
        
        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,aa,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();    
        
        ss1 = GlobalClass.ScrambleText('aabc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();   
        
        ss1 = GlobalClass.ScrambleText('a@abc.com,007'); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();    
        
        ApexPages.currentPage().getParameters().put('unsubscribe','xxxx');
        sc = new SurveyController();
        sc.init();    
    }

    static testMethod void myUnitTest5() 
    {
        Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin5.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog5.id,
            Vehicle_Make__c ='Jaguar',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;
        
        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss.replace('+','%2B').replace('/','%2F');
        update c;
        string ss1 = GlobalClass.ScrambleText('a@abc.com,aa,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        SurveyController sc = new SurveyController();
        sc.init();  
        sc.getAdditValue();  
        
        ss1 = GlobalClass.ScrambleText('aabc.com,'+ c.program__r.program_code__c); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();   
        
        ss1 = GlobalClass.ScrambleText('a@abc.com,007'); //.replace('+','%2B').replace('/','%2F');
    
        ApexPages.currentPage().getParameters().put('unsubscribe',ss1);
        sc = new SurveyController();
        sc.init();    
        
        ApexPages.currentPage().getParameters().put('unsubscribe','xxxx');
        sc = new SurveyController();
        sc.init();
        //sc.additionalQ();
        sc.clearAddInfoPhone();  
        //sc.submitResults();  
    }
    
    static testMethod void myUnitTest6() 
    {
   		Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog6.id,
            Vehicle_Make__c ='Kia',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'English'
        );
        insert c;


        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c).replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss;
        update c;
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        SurveyController sc = new SurveyController();
        sc.init();       
        sc.submitResults(); 
        
        sr21 = new SurveyResponse__c();
        sr21.Case__c = c.id;
        sr21.Phone__c = '123456';
        sr21.Response__c = '1';
        sr21.ResponseDate__c = DateTime.now();
        sr21.SubmissionDate__c = DateTime.now();
        sr21.SurveyQuestion__c = sq21.id;
        sr21.Callback__c = 'Yes';
        insert sr21;
        
    	sc.questions[0].selectedOption = sr21.Response__c;
    	sc.additionalQ();
        sc.submitResults(); 
        sc.additInfo = 'Yes';
        sc.submitResults(); 
        
    }
    static testMethod void myUnitTest7() 
    {
   		Utility();
        c = new Case(
            Status='Dispatched',
            City__c='Toronto',
            Province__c='Ontario',
            
            VIN_Member_ID__c=vin.Name,
            Interaction_ID__c='INTERACTION',
            Trouble_Code__c='Gas',
            Phone__c='123',
            Call_ID__c='ABC',
            RE_Status_Date_Time__c = date.today()-1,
            Club_Call_Number__c = '2251',
            program__c = prog6.id,
            Vehicle_Make__c ='Kia',
            first_name__c='test',
            last_name__c='test',
            email__c = 'abc@clubautoltd.com',
            Country__c = 'US',
            Language__c = 'French'
        );
        insert c;


        c = [select id,casenumber,createddate,program__r.program_code__c,surveysent__c,Language__c from case where id =:c.id];
        
        
        string ss = GlobalClass.ScrambleText( GlobalClass.getDatayyyyMMdd(c.CreatedDate) +'-'+ c.casenumber+'-'+ c.program__r.program_code__c).replace('+','%2B').replace('/','%2F');
        c.surveysent__c = ss;
        update c;
    
        system.debug('c.surveysent__c:' + c.surveysent__c);
        ApexPages.currentPage().getParameters().put('id',ss);
        SurveyController sc = new SurveyController();
        sc.init();       
        sc.submitResults(); 
        
        sr22 = new SurveyResponse__c();
        sr22.Case__c = c.id;
        sr22.Phone__c = '123456';
        sr22.Response__c = '1';
        sr22.ResponseDate__c = DateTime.now();
        sr22.SubmissionDate__c = DateTime.now();
        sr22.SurveyQuestion__c = sq22.id;
        sr22.Callback__c = 'Yes';
        insert sr22;
        
    	sc.questions[0].selectedOption = sr22.Response__c;
    	sc.additionalQ();
        sc.submitResults(); 
        sc.additInfo = 'Yes';
        sc.submitResults(); 
        
    }
}