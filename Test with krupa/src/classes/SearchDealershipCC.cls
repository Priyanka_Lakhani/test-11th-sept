public class SearchDealershipCC {
    public String caseId {get;set;}
    public String programId {get;set;}
    public Case selectedCase;
    transient String caseFields;
    transient List<Account> accList {get; set;}
    public Map<Id, Account> accMap;
    public String accName {get; set;}
    public String accCity {get; set;}
    public String accStreet {get; set;}
    public Case tempCase {get; set;}
    public Integer accontsCount {get; set;}
    public Boolean programIn {get; set;}
    public Double lat;
    public Double lng;
    public List<AccountWithDistance> accWithDistList {get; set;}
    //public Id accIdChosen {get; set;}
    transient Location breakLocation;

    public SearchDealershipCC() {
        caseId = ApexPages.currentPage().getParameters().get('Id');
        programId = ApexPages.currentPage().getParameters().get('programId');
        String latPar = ApexPages.currentPage().getParameters().get('lat');
        String lngPar = ApexPages.currentPage().getParameters().get('lng');
        tempCase = new Case();

        if(caseId != null && programId == null){
            getCaseFields();

            String caseQuery = 'SELECT ' + caseFields + 'Program__r.Account__c, Breakdown_Location__c FROM Case WHERE Id = \''+ caseId +'\' LIMIT 1';
            selectedCase = database.query(caseQuery);

            lat = selectedCase.Breakdown_Location__Latitude__s;
            lng = selectedCase.Breakdown_Location__Longitude__s;

            tempCase.Program__c = selectedCase.Program__c;
        }

        if(caseId == null && programId != null){
            tempCase.Program__c = programId;
        }

        if(latPar!=''&&latPar!=null&&lngPar!=''&&lngPar!=null){
            lat = Double.valueOf(latPar);
            lng = Double.valueOf(lngPar);
        }

        accName = '';
        accCity = '';
        accStreet = '';

        
        
        system.debug('1111111111111111 tempCase.Program__c ' + tempCase.Program__c);
        system.debug('1111111111111111 tempCase.Program__r.Account__c ' + tempCase.Program__r.Account__c);

        getAccounts();
    }

    public void getCaseFields() {
        caseFields = '';
        Set<String> excludeSet = new Set<String>();
        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.Call_Flow_Field_Inclusions.getFields()) {
            caseFields += (f.getFieldPath() + ',');
        }
    }

    public void getAccounts() {
        accList = new List<Account>();
        accMap = new Map<Id, Account>();
        String accListQuery = 'SELECT Id, Name, Account_Number__c, Phone, BillingStreet, BillingCity, BillingState, BillingStateCode, BillingCountry, BillingCountryCode, BillingPostalCode, Location__c, Location__Latitude__s, Location__Longitude__s, Client__c, Client__r.Name, RecordType.Name FROM Account WHERE RecordType.Name = \'Dealership / Tow Destination\' AND Location__Latitude__s<>null AND Status__c != \'Inactive\' ';
        String accListQueryWithoutProgram = '';
        Program__c clientProgram = new Program__c();

        if (accName != '') {
            accListQuery += 'AND Name LIKE \'%' + accName + '%\' ';
        }

        if (accCity != '') {
            accListQuery += 'AND BillingCity LIKE \'%' + accCity + '%\' ';
        }

        if (accStreet != '') {
            accListQuery += 'AND BillingStreet LIKE \'%' + accStreet + '%\' ';
        }
        accListQueryWithoutProgram = accListQuery;
        if (tempCase.Program__c != NULL) {
            programIn = true;
            String clientQuery = 'SELECT Id, Name, Account__c, Program_Code__c,Call_Flow_Client_Code_for_Dealer_List__c  FROM Program__c WHERE Id =\'' + tempCase.Program__c + '\'';
            clientProgram = database.query(clientQuery);

            if(clientProgram.Call_Flow_Client_Code_for_Dealer_List__c != null && clientProgram.Call_Flow_Client_Code_for_Dealer_List__c!= ''){
                accListQuery += ' AND Client__r.account_number__c in (';
                for(String s : ((clientProgram.Call_Flow_Client_Code_for_Dealer_List__c).split(','))) {
                    accListQuery += '\''+s+'\',';
                }
                accListQuery = accListQuery.removeEnd(',') +')';   
            }else if (clientProgram.Account__c != NULL) {
                accListQuery += 'AND Client__c = \'' + clientProgram.Account__c + '\' ';
            }
        }

        else {
            programIn = false;
        }

        if (lat != NULL && lng != NULL) {
            accListQuery += 'ORDER BY DISTANCE(Location__c, GEOLOCATION('+lat+','+lng+'), \'km\') ASC LIMIT 500';
        }
        else {
            accListQuery += 'ORDER BY BillingCity ASC LIMIT 500';
        }

        if (programIn) {
            accList = database.query(accListQuery);
            accontsCount = accList.size();
            breakLocation = Location.newInstance(lat, lng);
        } 
         
        if(clientProgram.Program_Code__c == '546' || clientProgram.Program_Code__c == '580' || clientProgram.Program_Code__c == '579' || clientProgram.Program_Code__c == '596'){
            Integer qQuantity = 500-accontsCount;
            accListQueryWithoutProgram += 'ORDER BY BillingCity ASC LIMIT '+ qQuantity;
            List<Account> tempAcc = database.query(accListQueryWithoutProgram);
            accList.addAll(tempAcc);
            accontsCount += accList.size();
            breakLocation = Location.newInstance(lat, lng);
        }  

        if (accontsCount != 0) {
            accWithDistList = new List<AccountWithDistance>();
            for (Account a: accList) {
                accMap.put(a.Id, a);
                Location accLoc = Location.newInstance(a.Location__Latitude__s, a.Location__Longitude__s);
                Double dist = Location.getDistance(breakLocation, accLoc, 'km');
                accWithDistList.add(new AccountWithDistance(a, dist));
            }
        }
    }

    /*public void saveCase() {
        system.debug('222222222222 accIdChosen: ' + accIdChosen);
        if (accIdChosen != NULL) {
            selectedCase.Dealership__c = accIdChosen;
            selectedCase.Destination_Name__c = accMap.get(accIdChosen).Name;
            selectedCase.Destination_Street__c = accMap.get(accIdChosen).BillingStreet;
            selectedCase.Destination_City__c = accMap.get(accIdChosen).BillingCity;
            selectedCase.Destination_Postal_Code__c = accMap.get(accIdChosen).BillingPostalCode;
            selectedCase.Destination_Phone__c = accMap.get(accIdChosen).Phone;
            selectedCase.Destination_Province__c = accMap.get(accIdChosen).BillingStateCode;
            selectedCase.Destination_Country__c = accMap.get(accIdChosen).BillingCountryCode;            

            update selectedCase;
        }
    }*/

    public class AccountWithDistance {
        public Account acc {get; set;}
        public Double dist {get; set;}

        public AccountWithDistance(Account accountRec, Double distance) {
            this.acc = accountRec;
            this.dist = distance;
        }
    }
}