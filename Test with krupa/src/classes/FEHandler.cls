/**
    Created Date: Jan 17 2019
    Last Modified Date: Jan 17 2019
    Last Modified By: Thamizh
    Description: Handler for Flow Entity object
 */
public without sharing class FEHandler implements ITrigger
{   
    // Member variable to hold the Id's of Flow Entity 'in use'
    public Set<Id> fes_PF = new Set<Id>();
    public Set<Id> fes_PE = new Set<Id>();
    public Recordtype feFlowRT{get;set;}

    String errorMsgNotMatch = 'Incorrect format. Only the following characters are allowed: "(", ")", 0-9, [OR], [AND]';
    String errorMsgParenthesisNotMatching = 'The number of open and closed parentheses should match';
    String errorMsgWithoutDependecy = 'No complex logic can be inserted before having a minimum of 2 linked Layout Flow Entity Dependencies';
    String errorMsgOrderNotFound = 'The expression should contain all arguments starting from 1 through the number of linked Layout Flow Entity Dependencies';
    String errorMsgNumberOfArguments = 'The number of specified arguments does not match with the number of linked Layout Flow Entity Dependencies';
    
    // Constructor
    public FEHandler()
    {
        feFlowRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Flow' AND SobjectType = 'Flow_Entity__c'];
    }

    public void bulkBefore()
    {
        if(Trigger.isInsert){
            Decimal runner = 0;
            List<Flow_Entity__c> flowEntityList = new List<Flow_Entity__c>();
            flowEntityList = [SELECT Count__c FROM Flow_Entity__c ORDER BY Count__c DESC NULLS LAST LIMIT 1];
            if(flowEntityList.size() > 0 && flowEntityList[0].Count__c != null){
                runner = flowEntityList[0].Count__c + 1;
            }
            System.debug('[AP] [FEHandler] [bulkBefore] runner = ' + runner);
            for(sObject obj : Trigger.new){
                Flow_Entity__c entity = (Flow_Entity__c)obj;
                if(entity.Count__c == null){
                    entity.Count__c = runner;
                    runner++;
                }
            }
            /*for(sObject sobj : Trigger.new){
                Flow_Entity__c FE = (Flow_Entity__c)sobj;
                FE.Parent_Flow__c = FE.Parent_Flow_Id__c;
            }*/
        }
        if(Trigger.isUpdate){
            /*for(sObject sobj : Trigger.new){
                Flow_Entity__c FE = (Flow_Entity__c)sobj;
                FE.Parent_Flow__c = FE.Parent_Flow_Id__c;
            }*/
            String regex = '[^0-9]{1,}|\\-';
            Set<String> entitySet = new Set<String>();
            for(sObject entity : Trigger.new){
                entitySet.add(entity.Id);
            }
            List<String> listAcceptableRecordType = new List<String>();
            listAcceptableRecordType.add('layout');
            List<Flow_Entity_Dependency__c> dependencyList = [SELECT Id, Dependency_Parent__c,Dependency_Child__c FROM Flow_Entity_Dependency__c WHERE Dependency_Child__c IN :entitySet AND RecordType.Name IN :listAcceptableRecordType];
            
            Map<String, Integer> dependencyCountMap = new Map<String, Integer>();
            
            for(Flow_Entity_Dependency__c fed : dependencyList){
                if(!dependencyCountMap.containsKey(fed.Dependency_Child__c)){
                    dependencyCountMap.put(fed.Dependency_Child__c, 0);
                }
                Integer temp = dependencyCountMap.get(fed.Dependency_Child__c);
                temp++;
                dependencyCountMap.put(fed.Dependency_Child__c, temp);
            }
            for(sObject obj : Trigger.new){
                Flow_Entity__c entity = (Flow_Entity__c)obj;
                if(!String.isBlank(entity.Dependency_Logic__c)){
                    if(dependencyCountMap.get(entity.id) > 1){
                        if(entity.Dependency_Logic__c.countMatches('(') == entity.Dependency_Logic__c.countMatches(')')){
                            List<String> dependencyNumberList = entity.Dependency_Logic__c.replaceAll(regex, ' ').split(' ');
                            if(dependencyNumberList.size() == dependencyCountMap.get(entity.id)){
                                for(Integer i = 1; i <= dependencyCountMap.get(entity.id); i++){
                                    if(!dependencyNumberList.contains(String.valueOf(i))){
                                        entity.Dependency_Logic__c.addError(errorMsgOrderNotFound);
                                        break;
                                    }
                                }
                            } else {
                                entity.Dependency_Logic__c.addError(errorMsgNumberOfArguments);
                            }
                        } else{
                            entity.Dependency_Logic__c.addError(errorMsgParenthesisNotMatching);
                        }
                    } else{
                        entity.Dependency_Logic__c.addError(errorMsgWithoutDependecy);
                    }
                }
            }
        }
        if(Trigger.isDelete){
            if(FE_TriggerHelper.isFirstTime){
                FE_TriggerHelper.isFirstTime = false;
                for(sObject sobj : Trigger.old){
                    Flow_Entity__c FE = (Flow_Entity__c)sobj;
                    if(FE.recordtypeId == feFlowRT.Id){
                        fes_PF.add(FE.Id);
                    }else{
                        fes_PE.add(FE.Id);
                    }
                }
            }
        }
        if(fes_PF.size() > 0){
            FE_TriggerHelper.FE_DeleteFlowChildRecords(fes_PF);
        }
        if(fes_PE.size() > 0){
            FE_TriggerHelper.FE_DeleteEntityChildRecords(fes_PE);
        }
    }
    
    public void bulkAfter()
    {
        
    }
        
    public void beforeInsert(SObject so)
    {
        //Dependency Logic cannot be provided on insert, as no Dependencies are linked yet, so no complex logic can be created
        for(sObject obj : Trigger.new){
            Flow_Entity__c entity = (Flow_Entity__c)obj;
            if(!String.isBlank(entity.Dependency_Logic__c)){
                entity.Dependency_Logic__c.addError(errorMsgWithoutDependecy);
            }
        }
    }
    
    public void beforeUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void beforeDelete(SObject so)
    {   
        // Cast the SObject to an Claim
    }
    
    public void afterInsert(SObject so)
    {
    }
    
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    public void afterDelete(SObject so)
    {
    }
    
    public void andFinally()
    {
        // insert any audit records
    }
}