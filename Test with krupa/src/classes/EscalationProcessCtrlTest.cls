@isTest
private class EscalationProcessCtrlTest {

    @testSetup public static void setup(){

        CA__c settings = new CA__c(
            Last_Case_Number__c = '02785882'

            );
        insert settings;

        Automatic_email_recipients__c emails = new Automatic_email_recipients__c(
            Acura_NSX_Emails__c = 'test1@test.ca',
            Escalation_email_addresses__c = 'test2@test.ca'
            );
        insert emails;

         Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
		
		Contact con = new Contact(
			lastname = 'JLR',
			firstname = 'client',
			email = 'test@testxpg.com',
			accountid = client.Id,
			Contact_External__c='DevelopmentContact_DoNotDelete002'
		);
		insert con;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test'
        );
        insert prog;

        VIN__c vin = new VIN__c(
            Name='TEST1111',
            UID__c='TEST2222',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;

        Account tow = new Account(
            Name='TEST Dealership', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;

        Account club = new Account(
            Name='test',
             Account_Number__c='test',
              RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;

         Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;

       Case c = new Case(
            Status = settings.DI_Status__c,
            City__c = terr.City__c,
            Province__c = terr.Province__c,
            Breakdown_Location__Longitude__s = tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s = tow.Location__Latitude__s,
            VIN_Member_ID__c = vin.Name,
            Interaction_ID__c = 'INTERACTION111',
            vin__c = vin.id,
            Phone__c = '416-999-9999',
            Call_ID__c = 'ABCDD',
            trouble_code__c = 'Tow',
            tow_reason__c = 'brake problem',
            program__c = prog.id,
            Service_secured_in_90mns__c = 'No',
            CAR_secured_service_with_P_S_P__c = 'No'
        );

       insert c;
    }
    
    @isTest static void test_method_one() {
        Case c = [SELECT Id FROM Case LIMIT 1];

        ApexPages.currentPage().getParameters().put('id', c.Id);
        
        EscalationProcessCtrl ctrl = new  EscalationProcessCtrl();

        ctrl.getCaseFields();

        ctrl.sendEmails();

        ctrl.showMoreDealers();

        ctrl.closeEscalation();

        ctrl.saveCase = true;
        
        ctrl.ETA_time = '10';
        ctrl.saveAndRedirect();
    }
    
}