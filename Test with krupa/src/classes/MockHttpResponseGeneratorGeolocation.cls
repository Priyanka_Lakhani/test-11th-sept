@isTest
global class MockHttpResponseGeneratorGeolocation implements HttpCalloutMock {
    // Implement this interface method
    global HTTPResponse respond(HTTPRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"location":{"lat" : 49.1107938,"lng" : -122.6762257}}');
        res.setStatusCode(200);
        return res;
    }
}