/*
Created Date: Mar 18 2019
Last Modified Date: Mar 18 2019
Last Modified By: I2C Thamizh
Description: Test Class for CaseDetailsLtng
*/
@isTest(SeeAllData=true)
public class Test_CaseDetailsLtng {

	static Account club {get;set;}
    static Account client{get;set;}
    static Account tow{get;set;}
    static Territory_Coverage__c terr{get;set;}
    static Vehicle__c veh{get;set;}
    static CA__c settings{get;set;}
    static Case c{get;set;}
    static Program__c prog{get;set;}
	
	static void Utility()
    {
        club = new Account(Name='Club', Account_Number__c='Club', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        update club;
        
       client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='12333311'
        );
        insert client;
        
        tow = new Account(
            Name='Tow', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;

        terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;
        
        prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='12312345',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Base_Warranty_Service_Limit_Term__c = 3,
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        insert prog;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        //Modified to create vehicle / Policy & Vehicle Policy Record
        veh = new Vehicle__c(
            Name='VIN',
            Vehicle_External__c='VIN',
            Make__c='Audi',
            Model__c='R8'
        );
        insert veh;
        
        Policy__c pol = new Policy__c(
            Name='POL',
            Policy_External__c='POL123',
            Warranty_Start__c=Date.today().addYears(-5),
            Warranty_End__c=Date.today().addYears(1),
            Warranty_Max_Odometer__c = 10000,
            Program__c=prog.Id,
            RecordTypeId=Schema.SObjectType.Policy__c.getRecordTypeInfosByName().get('Base Program Policy').getRecordTypeId()
        );
        insert pol;
        
        Vehicle_Policy__c vehPol = new Vehicle_Policy__c    (
            Policy__c=pol.Id,
            Vehicle__c=veh.Id,
            Vehicle_Policy_External__c='VIN-POL123'
        );
        insert vehPol;
        
        settings = CA__c.getOrgDefaults();
        
        c = new Case(
            Status=settings.RE_Status__c,
            City__c=terr.City__c,
            Province__c=terr.Province__c,
            Breakdown_Location__Longitude__s=tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s=tow.Location__Latitude__s,
            VIN_Member_ID__c=veh.Name,
            Vehicle__c = veh.Id,
            Club_Call_Number__c = '123',
            Interaction_ID__c='INTERACTION',
            Miscellaneous_Call_Reason__c=settings.Call_Flow_Personal_Update_Request__c,
            Phone__c='1233213',
            Call_ID__c='ABC123',
            Program__c=prog.Id,
            Non_Service_Call__c = false
        );
        insert c;
    }
	static testMethod void CaseDetailsLtng_Test() {
        Test.startTest();
			Utility();
            CaseDetailsLtng.getCaseDetails(c.Id);
			CaseDetailsLtng cd = new CaseDetailsLtng();
			cd.cs = c;
			cd.CA = settings;
			cd.Err = false;
			cd.ErrMsg = ' Test Message';
			CaseDetailsLtng.saveKF(JSON.serialize(cd));
			CaseDetailsLtng.getCaseDetails(club.Id);
        Test.stopTest();
    }
}