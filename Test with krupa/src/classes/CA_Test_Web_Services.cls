@IsTest
private class CA_Test_Web_Services {
    @testSetup static void setup(){
        
        CA__c settings = new CA__c();
        settings.Last_Case_Number__c = '12345';
        insert settings;
        
        Account club = new Account(Name='Club', Account_Number__c='Club', RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        Account mobiletech = new Account(
            Name = 'Mobile Tech', 
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(), 
            Client__c = client.Id,
            Location__Longitude__s = 0.1, 
            Location__Latitude__s = 0.1,
            account_number__c = 'test',
            firstname__c = 'test',
            lastname__c = 'test',
            dispatch_phone__c = '123445678',
            dispatch_email__c = 'test@gamil.com',
            billingstreet = 'test',
            billingcity = 'Toronto',
            billingstate = 'Ontario',
            billingcountry = 'Canada',
            billingpostalcode = '1X2 X1X',
            Monday__c = '00:01-23:59',
            Tuesday__c = '00:01-23:59',
            Wednesday__c = '00:01-23:59',
            Thursday__c = '00:01-23:59',
            Friday__c = '00:01-23:59',
            Saturday__c = '00:01-23:59',
            Sunday__c = '00:01-23:59',
            Type = 'Mobile Tech'
        );
        insert mobiletech;
        
        Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            Power_Train_EV_Warranty_Year_Limit__c = 5,
            Power_Train_EV_Warranty_Mileage_Limit__c = 100000,
            Power_Train_Warranty_Year_Limit__c = 10,
            Power_Train_Warranty_Mileage_Limit__c = 160000,
            T4_Limit__c = 3
        );
        insert prog;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-6),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        Case c = new Case(
            Status = 'Received',
            City__c = terr.City__c,
            Province__c = terr.Province__c,
            Breakdown_Location__Longitude__s = mobiletech.Location__Longitude__s, 
            Breakdown_Location__Latitude__s = mobiletech.Location__Latitude__s,
            VIN_Member_ID__c = vin.Name,
            Interaction_ID__c = 'INTERACTION',
            Phone__c = '123',
            Call_ID__c = 'ABC',
            Club_Call_Number__c = '123'
        );
        insert c;
    
    }
    
    @isTest static void test_MTAck_RESTInboundMTMessageService() {
        
        String caseNum = [SELECT id, CaseNumber FROM Case LIMIT 1].CaseNumber;
        
        RestResponse res = new RestResponse();
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/RESTInboundMTMessageService/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Type" : "MTAck","CallDetails" : {"CaseNumber" : "' + caseNum + '","ETA" : 80}}');
        RestContext.request = request;
        RestContext.response = res;    
        
        Test.startTest();
        RESTInboundMTMessageService.startService();
        Test.stopTest();
        
    }  

    @isTest static void test_CloseCall_RESTInboundMTMessageService() {
    
        String caseNum = [SELECT id, CaseNumber FROM Case LIMIT 1].CaseNumber;
        
        RestResponse res = new RestResponse();
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/RESTInboundMTMessageService/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Type" : "CloseCall","CallDetails" : {"CaseNumber" : "' + caseNum + '","Reason" : "MT Rejected","Notes" : "Free text field"}}');
        RestContext.request = request;
        RestContext.response = res;    
        
        Test.startTest();
        RESTInboundMTMessageService.startService();
        Test.stopTest();
        
    }
    
    @isTest static void test_CloseCall_V2_RESTInboundMTMessageService() {
    
        String caseNum = [SELECT id, CaseNumber FROM Case LIMIT 1].CaseNumber;
        
        RestResponse res = new RestResponse();
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/RESTInboundMTMessageService/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Type" : "CloseCall","CallDetails" : {"CaseNumber" : "' + caseNum + '","Reason" : "GOA","Notes" : "Free text field"}}');
        RestContext.request = request;
        RestContext.response = res;    
        
        Test.startTest();
        RESTInboundMTMessageService.startService();
        Test.stopTest();
        
    }
    
    @isTest static void test_MTCallInfo_RESTInboundMTMessageService() {
    
        Case cs = [SELECT id,CaseNumber,Club_Call_Number__c FROM Case LIMIT 1];
        
        cs.Club_Call_Number__c = cs.CaseNumber;
        update cs;
        
        RestResponse res = new RestResponse();
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/RESTInboundMTMessageService/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Type" : "MTCallInfo","LastUpdatedDate" : "2017-10-10","CallDetails" : {"CaseNumber" : "' + cs.CaseNumber + '","lat" : 43.54021010,"lon" : -109.65362640,"Status" : "RE"}}');
        RestContext.request = request;
        RestContext.response = res;    
        
        Test.startTest();
        RESTInboundMTMessageService.startService();
        Test.stopTest();
        
    }   
    
    @isTest static void test_GETMTInfo_RESTInboundMTMessageService() {
        
        String caseNum = [SELECT id, CaseNumber FROM Case LIMIT 1].CaseNumber;
        
        
        RestResponse res = new RestResponse();
        RestRequest request = new RestRequest();
        request.requestUri = '/services/apexrest/RESTInboundMTMessageService/';
        request.httpMethod = 'POST';
        request.addHeader('Content-Type', 'application/json');
        request.requestBody = Blob.valueOf('{"Type" : "GETMTInfo","LastUpdatedDate" : "2017-10-10"}');
        RestContext.request = request;
        RestContext.response = res;    
        
        Test.startTest();
        RESTInboundMTMessageService.startService();
        Test.stopTest();
        
    }  

}