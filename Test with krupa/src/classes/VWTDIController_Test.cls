@isTest
private class VWTDIController_Test {
    static String formUserFields='';
    static String programFields='';
    static String vinFields='';
    @testSetup 
    static void setup() {
        FormUser__c user = new FormUser__c();
        user.firstname__c='Test';
        user.lastname__c='Test';
        user.loginname__c = 'test';
        user.password__c ='49';
        user.reset__c = false;
        insert user;
        
        FormUser__c user2 = new FormUser__c();
        user2.firstname__c='Test';
        user2.lastname__c='Test';
        user2.loginname__c = 'test1';
        user2.password__c ='49';
        user2.reset__c = false;
        user2.whitelist__c = '10.10.1.1';
        insert user2;
        
        Account client = new Account(
            Name='Audi', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='511'
        );
        insert client;
        Account client2 = new Account(
            Name='VW', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='512'
        );
        insert client2;
        Program__c progAudi = new Program__c(
            Account__c=client.Id,
            Program_Code__c='511',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='Audi',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            OEM__c=true
        );
        insert progAudi;
        
        Program__c prog888 = new Program__c(
            Account__c=client2.Id,
            Program_Code__c='888',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='Audi',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3',
            OEM__c=true
        );
        insert prog888;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='Audi',
            Model__c='R8',
            Program__c=progAudi.Id
        );
        insert vin;
        
        List<Sites__c> siteList = new List<Sites__c>();
        
        Sites__c sites2 = new Sites__c();
        sites2.name = 'Instance';
        sites2.value__c = 'cs20';
        siteList.add(sites2);
        
        Sites__c sites3 = new Sites__c();
        sites3.name = 'VWLogo';
        sites3.value__c = '015m0000001CAb6';
        siteList.add(sites3);
        
        upsert siteList;
    }
    static testMethod void myUnitTest() 
    {
        formUserFields='';
        for(Schema.SObjectField f : FormUser__c.SObjectType.getDescribe().fields.getMap().values()) {
            formUserFields+= (f.getDescribe().getName() + ',');
        }
        formUserFields=formUserFields.left(formUserFields.length()-1);     
        
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);      
        
        vinFields='';
        for(Schema.SObjectField f : VIN__c.SObjectType.getDescribe().fields.getMap().values()) {
            vinFields += (f.getDescribe().getName() + ',');
        }
        vinFields=vinFields.left(vinFields.length()-1);
        
        String query1 = 'Select ' + formUserFields+ '  FROM FormUser__c WHERE loginname__c = \'test\' LIMIT 1';
        FormUser__c user = database.query(query1);
        
        
        String query2 = 'Select ' + programFields+ ' FROM Program__c WHERE Program_Code__c = \'511\' LIMIT 1';
        Program__c  prog = database.query(query2);    
        
        String query3 = 'Select ' + vinFields+ ' FROM VIN__c WHERE Name = \'VIN\' LIMIT 1';
        VIN__c vin = database.query(query3);
        
        VWTDIController tdi = new VWTDIController();
        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.login();
        
        tdi.ResetPwd();
        tdi.newpwd1 = '2';
        tdi.newpwd2 = '1';
        tdi.ChangePwd();

        tdi.pwd = '1';
        tdi.newpwd1 = '1';
        tdi.newpwd2 = '1';
        tdi.loginname = 'teset1';
        tdi.ChangePwd();

        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.newpwd1 = '1';
        tdi.newpwd2 = '1';
        tdi.ChangePwd();



        tdi.vin = 'VIN1';
        tdi.checkVIN();
        
        tdi.vin = 'VIN';
        tdi.checkVIN();
        
        tdi.firstname = 'abc';
        tdi.lastname = 'abc';
        tdi.vin = 'vin';
        tdi.phone='4169999999';
        tdi.email='a@a.com';
        tdi.comments = 'aaa';
        tdi.callReason='Other';
        tdi.Submit();
        
        tdi.ChangeLanguage();
        
        //tdi.Logout();
        
        tdi.internalUser = false;
        tdi.CallReason = 'None';
        tdi.Submit();       
        tdi.firstname = 'abc';
        tdi.lastname = 'abc';
        tdi.vin = 'vin';
        tdi.phone='4169999999';
        tdi.email='a@a.com';
        tdi.comments = 'aaa';
        tdi.callReason='Other';
        tdi.Submit();
        
        tdi.Next();
        
        tdi.Logout();
        
    }
    
    
    static testMethod void myUnitTest1() 
    {
        formUserFields='';
        for(Schema.SObjectField f : FormUser__c.SObjectType.getDescribe().fields.getMap().values()) {
            formUserFields+= (f.getDescribe().getName() + ',');
        }
        formUserFields=formUserFields.left(formUserFields.length()-1); 
        
        programFields='';
        for(Schema.SObjectField f : Program__c.SObjectType.getDescribe().fields.getMap().values()) {
            programFields += (f.getDescribe().getName() + ',');
        }
        programFields=programFields.left(programFields.length()-1);      
        
        vinFields='';
        for(Schema.SObjectField f : VIN__c.SObjectType.getDescribe().fields.getMap().values()) {
            vinFields += (f.getDescribe().getName() + ',');
        }
        vinFields=vinFields.left(vinFields.length()-1);          
        
        String query1 = 'Select ' + formUserFields+ '  FROM FormUser__c WHERE loginname__c = \'test1\' LIMIT 1';
        FormUser__c user = database.query(query1);
        
        String query2 = 'Select ' + programFields+ ' FROM Program__c WHERE Program_Code__c = \'511\' LIMIT 1';
        Program__c  prog = database.query(query2);    
        
        String query3 = 'Select ' + vinFields+ ' FROM VIN__c WHERE Name = \'VIN\' LIMIT 1';
        VIN__c vin = database.query(query3);
               
        PageReference pageRef = Page.VWTDI; //replace with your VF page name
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.1');
        VWTDIController tdi = new VWTDIController();

        tdi.internalUser = false;
        tdi.loginname = 'test1';
        tdi.pwd = '1';
        tdi.login();

        tdi.internalUser = false;
        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.login();

        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.2');
        tdi.internalUser = false;
        tdi.pwd = '1';
        tdi.newpwd1 = '1';
        tdi.newpwd2 = '1';
        tdi.ChangePwd();
        
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.2');
        tdi.internalUser = false;
        tdi.vin = 'VIN';
        tdi.checkVIN();
        
        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.1.2');
        tdi.internalUser = false;
        tdi.firstname = 'abc';
        tdi.lastname = 'abc';
        tdi.vin = 'vin';
        tdi.phone='4169999999';
        tdi.email='a@a.com';
        tdi.comments = 'aaa';
        tdi.callReason='Other';
        tdi.Submit();
        


        ApexPages.currentPage().getHeaders().put('X-Salesforce-SIP', '10.10.10.10');
        tdi = new VWTDIController();
        tdi.internalUser = false;
        tdi.loginname = 'test';
        tdi.pwd = '1';
        tdi.login();

                
    }
    
}