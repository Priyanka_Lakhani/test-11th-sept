public class JLR_CloseTheLoop{

/*          
URL : http://dev-clientclosetheloop.azurewebsites.net/closetheloop/initialcall

AUTHENTICATION :
username : closetheloop
password : CLOSETHELOOP123$
Method : Post

Payload : 
{
"CaseNumber":"027874",
"PhoneNumber":"9057654321",
"ClientAccount":"858", --- 858 is JLR
"VehicleMaker":"JAGUAR" or "LANDROVER",
"LanguageCode" :"EN"
}
*/
    
    public static String sendCaseToNuvaxx(Close_the_loop__c closeLoop) {     
        
        String requesrInfo = '';
        JLR_Close_The_Loop__c jlrSet = JLR_Close_The_Loop__c.getOrgDefaults();
        
        //Request to Nuvaxx
        Request_data reqD = new Request_data();
        reqD.CaseNumber = closeLoop.Case__r.CaseNumber;
        reqD.CloseTheLoopId = closeLoop.Name;
        reqD.PhoneNumber = closeLoop.Case__r.Phone__c;
        reqD.ClientAccount = closeLoop.Case__r.Client_Code__c;
        reqD.VehicleMaker = closeLoop.Case__r.Vehicle_Make__c;
        reqD.LanguageCode = closeLoop.Case__r.Language__c == 'English' ? 'EN' : (closeLoop.Case__r.Language__c == 'French' ? 'FR' : closeLoop.Case__r.Language__c);
        
        String reqBody = JSON.serialize(reqD);
        
        try {
            
            String tokenAccess = jlrSet.User_Name__c + ':' + jlrSet.Password__c;
            String tokenAccessBase64 = EncodingUtil.base64Encode(Blob.valueof(tokenAccess));
            
            HttpRequest reqNuv = new HttpRequest();
            reqNuv.setMethod('POST');
            reqNuv.setTimeout(30000);
            reqNuv.setEndpoint(jlrSet.Endpoint__c);
            reqNuv.setHeader('Content-Type','application/json');
            reqNuv.setHeader('Authorization','Basic ' + tokenAccessBase64);
            reqNuv.setBody(reqBody); 
                
            Http httpNuv = new Http();
            
            HTTPResponse resNuv;
        
            resNuv = httpNuv.send(reqNuv);
            
            requesrInfo = resNuv.getStatusCode() + ' ' + resNuv.getStatus() + ' ' + resNuv.getBody();
            
            System.debug('THE RESPONSE: ' + resNuv.getBody());
            
        }
        catch(Exception ex){
            requesrInfo = ex.getMessage();
        }
        
        return requesrInfo;
        
    }
    
    public class Request_data {

        public String CaseNumber {get;set;}
        public String CloseTheLoopId {get;set;}
        public String PhoneNumber {get;set;}
        public String ClientAccount {get;set;}
        public String VehicleMaker {get;set;}
        public String LanguageCode {get;set;}

    }
    
    public class TokenResp {
        public String accessToken {get;set;}
    }
    
}