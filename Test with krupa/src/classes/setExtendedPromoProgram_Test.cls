@isTest
private class setExtendedPromoProgram_Test {
    
    @isTest static void test_method_one() {
    
        test.starttest();
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='12333311'
        );
        insert client;
        
        List<Program__c> progToIns = new List<Program__c>();
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='560',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog);

        Program__c prog1 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='561',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog1);

        Program__c prog2 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='562',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog2);
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Extended_Warranty_Start__c=Date.today().addYears(-5),
            Extended_Warranty_End__c=Date.today().addYears(1),
            Extended_Warranty_Max_Odometer__c=123,
            Promo_Start__c=Date.today().addYears(-5),
            Promo_End__c=Date.today().addYears(1),
            Promo_Max_Odometer__c=123,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;
        
        vin.Program__c = prog1.Id;
        update vin;
        vin.Program__c = prog2.Id;
        update vin;
        
        test.stoptest();
        
    }
    
    @isTest static void test_method_two() {
    
        test.starttest();
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='12333311'
        );
        insert client;
        
        List<Program__c> progToIns = new List<Program__c>();
        
        Program__c prog3 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='563',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog3);

        Program__c prog4 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='564',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog4);

        Program__c prog5 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='565',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog5);
        
        insert progToIns;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Extended_Warranty_Start__c=Date.today().addYears(-5),
            Extended_Warranty_End__c=Date.today().addYears(1),
            Extended_Warranty_Max_Odometer__c=123,
            Promo_Start__c=Date.today().addYears(-5),
            Promo_End__c=Date.today().addYears(1),
            Promo_Max_Odometer__c=123,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog3.Id
        );
        insert vin;
        
        vin.Program__c = prog4.Id;
        update vin;
        vin.Program__c = prog5.Id;
        update vin;
        
        test.stoptest();
    
    }
    
     @isTest static void test_method_three() {
     
        test.starttest();
        
        Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='12333311'
        );
        insert client;
        
        List<Program__c> progToIns = new List<Program__c>();
        
        Program__c prog6 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='566',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog6);

        Program__c prog7 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='567',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog7);

        Program__c prog8 = new Program__c(
            Account__c=client.Id,
            Program_Code__c='568',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test1,test2,test3'
        );
        progToIns.add(prog8);
        insert progToIns;
        
        VIN__c vin = new VIN__c(
            Name='VIN',
            UID__c='VIN',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Extended_Warranty_Start__c=Date.today().addYears(-5),
            Extended_Warranty_End__c=Date.today().addYears(1),
            Extended_Warranty_Max_Odometer__c=123,
            Promo_Start__c=Date.today().addYears(-5),
            Promo_End__c=Date.today().addYears(1),
            Promo_Max_Odometer__c=123,
            Make__c='Audi',
            Model__c='R8',
            Program__c=prog6.Id
        );
        insert vin;
        
        vin.Program__c = prog7.Id;
        update vin;
        vin.Program__c = prog8.Id;
        update vin;
        
        

        vin.Program__c = null;
        update vin;
        
        test.stoptest();
    
    }
    
}