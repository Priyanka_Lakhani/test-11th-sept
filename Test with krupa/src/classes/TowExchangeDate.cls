public class TowExchangeDate {

    public Id recordId { get; private set; }
    
    public TowExchangeDate(ApexPages.StandardController controller) {
        recordId = controller.getRecord().Id;
    }

    public PageReference UpdateCase() {
        try {
            Case c = new Case(Id=recordId, Tow_Exchange_Date__c=System.today());
            update c;
            
            return new PageReference('/' + recordId);
        } catch (Exception e) {
        
        }
        return null;
    }
}