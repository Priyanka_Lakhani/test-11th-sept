global class Schedule_ServiceTracker implements Schedulable{
    global void execute(SchedulableContext sc) {
        ServiceTrackerBatchable b = new ServiceTrackerBatchable(null); 
        database.executebatch(b,20);
    }
}