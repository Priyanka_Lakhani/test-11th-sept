public with sharing class EHIEmailVehicleRecovery {
    private static list<string> getGPBREmails(string gpbr)
    {
        list<string> emails = new list<string>();
        try
        {
            string bemail =  [select Tow_Exchange_Email_Address__c from account where gpbr__c = :gpbr and status__c != 'Inactive' limit 1].Tow_Exchange_Email_Address__c;
            string[] bemails = bemail.split(';');
            for (string st : bemails)
            {
                if (st.contains('@'))
                {
                    emails.add(st);
                }
            }
        }
        catch(System.exception ex){}
        return emails;
    }
    
    @InvocableMethod(label='EmailVehicleRecovery' description='EmailVehicleRecovery')
    public static void EmailVehicleRecovery(List<Case> cs)
    {
        Case c = cs[0];
        
        list<string> emails1 = getGPBREmails(c.gpbr__c);
        list<string> emails2 = new list<string>(); //getNearestGPBREmail(c.Breakdown_Location__Latitude__s,c.Breakdown_Location__Longitude__s,getGPBRClientId(c.gpbr__c)); //getGPBREmails(c.VehRecoveryGPBR__c);
        emails2.add(c.gpbr__c.substring(0,2)+ CA__c.getOrgDefaults().Email_Vehicle_Recovery_Address__c);
        //string[] demails = CA__c.getOrgDefaults().Email_Vehicle_Recovery_Address__c.split(';');
        //for (string st : demails)
        //  emails1.add(st);

        if (emails1.size() == 0 && emails2.size() == 0)
            return;
            
        emails2.add(UserInfo.getUserEmail());

        Contact ctc = [select id, Email from Contact where Contact_External__c like '%DevelopmentContact_DoNotDelete%' limit 1];
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        mail.setToAddresses(emails1);
        mail.setCcAddresses(emails2);
         
        Id templateId;    
        try {
            templateId = [select id, name from EmailTemplate where developername = 'Miscellaneous_Calls_Vehicle_Recovery'].id;
        }catch (Exception e) {}       
        
        mail.setReplyTo('noreply@salesforce.com');
        mail.SetSenderDisplayName ('Xperigo');
        mail.setTemplateID(templateId);
        mail.setWhatId(c.id);
        mail.setSaveAsActivity(true);
        mail.setTargetObjectId(ctc.id);
        mail.setTreatTargetObjectAsRecipient(false);  
        
        Messaging.SingleEmailMessage[] mails = new List<Messaging.SingleEmailMessage> {mail};
	    if(!Test.isRunningTest()) {
	        Messaging.SendEmailResult[] results = Messaging.sendEmail(mails);
	    }
    
    } 
}