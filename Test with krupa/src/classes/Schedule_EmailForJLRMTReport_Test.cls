@isTest
private class Schedule_EmailForJLRMTReport_Test {
   
    static Account a1 {get;set;}
    static Account a2 {get;set;}
    static Contact cont {get;set;}
    
    static Case c1 {get;set;}
    static Case c2 {get;set;}
    static Case c3 {get;set;}
    static Case caseTowRequired {get;set;}
    static Case caseCompleted {get;set;}
    
    static CA__c settings {get;set;}
    
    static Integration__c intgration1 {get;set;}
    static Integration__c intgration2 {get;set;}
    static Integration__c intgration3 {get;set;}
    static Integration__c intgration4 {get;set;}
    static Integration__c intgration5 {get;set;}
    
    static list<MTBilling__c> MTBillingSettings {get;set;}
    static list<Schedule_MT_Report__c> scheduleSettings {get;set;}
    
    static Integration__c intgrationAck1 {get;set;}
    static Integration__c intgrationAck2 {get;set;}
    static Integration__c intgrationAck3 {get;set;}

   static void Schedule_EmailForJLRMTReport_Test()
   {
   
  
   }
   
   static void utility()
   {
        a1 = new Account(
          name='MT1',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-123',
          SecondAccountNumber__c='R0123',
          dispatch_email__c ='abc@mail.com',
          Retailer_Mail_Id__c = 'abc@mail.com',
          Status__c ='Active');
        insert a1;
        a2 = new Account(
          name='MT3',
          Type='Mobile Tech',
          RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Mobile Tech').getRecordTypeId(),
          Account_Number__c='MT-333',
          SecondAccountNumber__c='R0333',
          dispatch_email__c ='abc@mail.com',
          Retailer_Mail_Id__c = 'abc@mail.com',
          Status__c ='Active');
        insert a2;  
        cont = new Contact(LastName='Test',Email='abc@mail.com',Contact_External__c='DevelopmentContact_DoNotDelete005');
        insert cont;
        
        MTBillingSettings = new list<MTBilling__c>();        
        MTBillingSettings.add(new MTBilling__c(name = 'GOA',Amount__c = 40.00));
        MTBillingSettings.add(new MTBilling__c(name = 'MRS',Amount__c = 85.00));
        MTBillingSettings.add(new MTBilling__c(name = 'PCAN',Amount__c = 40.00));
        MTBillingSettings.add(new MTBilling__c(name = 'SCP',Amount__c = 85.00));
        MTBillingSettings.add(new MTBilling__c(name = 'UNS',Amount__c = 85.00));
        upsert MTBillingSettings;
        
        scheduleSettings = new list<Schedule_MT_Report__c>();        
        scheduleSettings.add(new Schedule_MT_Report__c(name = 'JLRMTCorporateReport',EmailToAddress__c= 'abc@mail.com',EmailccAddress__c='abc1@mail.com',Is_Schedular_On__c = false));
        scheduleSettings.add(new Schedule_MT_Report__c(name = 'JLRMTRetailerBillingReport',EmailccAddress__c='abc1@mail.com',Is_Schedular_On__c = false));
        scheduleSettings.add(new Schedule_MT_Report__c(name = 'JLRMTRetailerPerformanceReport',EmailccAddress__c='abc1@mail.com',Is_Schedular_On__c = false));
        upsert scheduleSettings ;
        
        settings = CA__c.getOrgDefaults();
        settings.Last_Case_Number__c='1000';
        settings.Location_Codes_French__c='L,M';
        settings.Location_Codes_English__c='L,M';
        settings.Non_Dispatch_Call_Types__c='N';        
        upsert settings;
        
        Sites__c sites = new Sites__c();
        sites.name = 'Survey';
        sites.value__c = 'http://www.salesforce.com';
        upsert sites;
        
        Sites__c sites1 = new Sites__c();
        sites1.name = 'JLRLogo';
        sites1.value__c = '015m0000001BLhX';
        upsert sites1;
        
        Sites__c sites2 = new Sites__c();
        sites2.name = 'Instance';
        sites2.value__c = 'cs20';
        upsert sites2;


        c1 = new Case(       
          Trouble_Code__c='Gas',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',         
          MTCloseReason__c='GOA',
          kill_code__c = 'GOA',
          ETA__c = 60,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABC',
          postal_code__c='K178J8',
          surveysent__c ='test'
          );
        insert c1;
        c2 = new Case(       
          Trouble_Code__c='Install Spare',
          MobileTech__c= a1.id, 
          JLRTriageAgent__c = 'Yes',        
          ETA__c = 30,
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtyy',
          postal_code__c='K178J9',
          complaint__c=true
          );
        insert c2;
        c3 = new Case(       
          Trouble_Code__c='Gas',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',
          MobTechDenied__c = 'Yes',
          MTDenyReason__c ='Rejected by Customer',
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwt',
          postal_code__c='K178J8',
          damage__c=true,
          ETA__c=15);
        insert c3;

        c1.DI_Status_Date_Time__c = DateTime.now();
        c2.DI_Status_Date_Time__c = DateTime.now();
        c3.DI_Status_Date_Time__c = DateTime.now();
        c1.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        c2.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        c3.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        System.debug('c1.final_status__c:'+c1.final_status__c);
        update c1;
        update c2;
        update c3;
        System.debug('c1.final_status__c:'+c1.final_status__c); 

         intgration1 = new Integration__c();
        intgration1.Case__c = c1.id;
        intgration1.IP__c = '49.213.53.37';
        intgration1.Message__c = 'Dispatch';
        intgration1.Parameters__c = 'DispatchCall';
        intgration1.Reprocess_Date_Time__c = DateTime.now();
        insert intgration1;

        intgration2 = new Integration__c();
        intgration2.Case__c = c2.id;
        intgration2.IP__c = '49.213.53.37';
        intgration2.Message__c = 'Dispatch';
        intgration2.Parameters__c = 'DispatchCall';
        intgration2.Reprocess_Date_Time__c = DateTime.now();
        insert intgration2;

        intgration3 = new Integration__c();
        intgration3.Case__c = c3.id;
        intgration3.IP__c = '49.213.53.37';
        intgration3.Message__c = 'Dispatch';
        intgration3.Parameters__c = 'DispatchCall';
        intgration3.Reprocess_Date_Time__c = DateTime.now();
        insert intgration3;

        intgrationAck1 = new Integration__c();
       intgrationAck1.Case__c = c1.id;
       intgrationAck1.IP__c = '49.213.53.37';
       intgrationAck1.Message__c = 'MTAck';
       intgrationAck1.Parameters__c = 'Acknowledgment';
       intgrationAck1.Reprocess_Date_Time__c = DateTime.now();
       insert intgrationAck1;

       intgrationAck2 = new Integration__c();
       intgrationAck2.Case__c = c2.id;
       intgrationAck2.IP__c = '49.213.53.37';
       intgrationAck2.Message__c = 'MTAck';
       intgrationAck2.Parameters__c = 'Acknowledgment';
       intgrationAck2.Reprocess_Date_Time__c = DateTime.now();
       insert intgrationAck2;

       intgrationAck3 = new Integration__c();
       intgrationAck3.Case__c = c3.id;
       intgrationAck3.IP__c = '49.213.53.37';
       intgrationAck3.Message__c = 'MTAck';
       intgrationAck3.Parameters__c = 'Acknowledgment';
       intgrationAck3.Reprocess_Date_Time__c = DateTime.now();
       insert intgrationAck3;
       
       caseTowRequired = new Case(       
          Trouble_Code__c='Tow',
          //Final_Status__c = 'Dispatched',
          MobileTech__c= a1.id,
          JLRTriageAgent__c = 'Yes',
          MobTechDenied__c = 'Yes',
          MTDenyReason__c ='Tow Required',
          Status='Spotted',
          City__c='Toronto',
          Province__c='Ontario',
          VIN_Member_ID__c='11111',
          Interaction_ID__c='INTERACTION',
          Tow_Reason__c = 'Break Problem',
          Phone__c='123',
          Call_ID__c='ABCtsrfwu',
          postal_code__c='K178J9',
          damage__c=true,
          ETA__c=15);
        insert caseTowRequired;  
        caseTowRequired.DI_Status_Date_Time__c = DateTime.now();
        caseTowRequired.OL_Status_Date_Time__c = DateTime.now().addMinutes(30);
        update caseTowRequired;
        
        intgration4 = new Integration__c();
        intgration4.Case__c = caseTowRequired.id;
        intgration4.IP__c = '49.213.53.37';
        intgration4.Message__c = 'Dispatch';
        intgration4.Parameters__c = 'DispatchCall';
        intgration4.Reprocess_Date_Time__c = DateTime.now();
        insert intgration4;
   }
   
   static testMethod void SET_scheduleRetailerBillingReport() {
   utility();
      Schedule_EmailForJLRMTReport se = new Schedule_EmailForJLRMTReport();
      List<Account> accountList = new List<Account>(); 
      accountList.add(a1);
      accountList.add(a2);
      System.debug('accountlist:'+accountList.Size()); 
      System.debug('test:' + JSON.serializePretty(accountList));

      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      se.scheduleRetailerBillingReport(accountList,sdate,edate);     
   }
   static testMethod void SET_scheduleRetailerPerformanceReport() {
   utility();
      Schedule_EmailForJLRMTReport se = new Schedule_EmailForJLRMTReport();
      List<Account> accountList = new List<Account>(); 
      accountList.add(a1);
      accountList.add(a2);
      System.debug('accountlist:'+accountList.Size());
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate; 
      se.scheduleRetailerPerformanceReport(accountList,sdate,edate);     
   }

   static testMethod void SET_scheduleJLRMTCorporateReport() {
   utility();
      String sdate = Datetime.now().format('dd/MM/yyyy');
      String edate = sdate;
      Schedule_EmailForJLRMTReport se = new Schedule_EmailForJLRMTReport();
      se.scheduleJLRMTCorporateReport(sdate,edate);     
   }

   static testMethod void SET_execute() {
      utility();
      Schedule_EmailForJLRMTReport se = new Schedule_EmailForJLRMTReport();
      se.execute(null);     
   }
   

}