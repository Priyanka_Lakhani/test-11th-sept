@isTest
public class Test_FlowEntity {
    @testSetup 
    static void setup() {
        RecordType FlowRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Flow' AND SobjectType = 'Flow_Entity__c'];
        RecordType PageRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Page' AND SobjectType = 'Flow_Entity__c'];
        RecordType SecRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Section' AND SobjectType = 'Flow_Entity__c'];
        RecordType SubSecRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'SubSection' AND SobjectType = 'Flow_Entity__c'];
        RecordType FieldRT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Field' AND SobjectType = 'Flow_Entity__c'];
        RecordType InfoTxt_RT = [Select id, DeveloperName, SobjectType from RecordType where DeveloperName = 'Info_Text' AND SobjectType = 'Flow_Entity_Dependency__c'];
        Flow_Entity__c FE = new Flow_Entity__c();
        /*Flow*/
        FE.Name = 'Test American Honda';
        FE.Display_Name__c = 'Test American Honda';
        FE.IsVisible__c = true;
        FE.Populate_From_Controlled_Field__c = false;
        FE.Render_As_Text__c = false;
        FE.RecordTypeId = FlowRT.Id;
        insert FE;
        /*Page*/
        Flow_Entity__c FE1 = new Flow_Entity__c();
        FE1.Name = 'Test First Page';
        FE1.Display_Name__c = 'Test Customer Info';
        FE1.IsVisible__c = true;
        FE1.Populate_From_Controlled_Field__c = false;
        FE1.Render_As_Text__c = false;
        FE1.RecordTypeId = PageRT.Id;
        FE1.Parent_Entity__c = FE.Id;
        FE1.Parent_Flow__c = FE.Id;
        insert FE1;
        /*Section*/
        Flow_Entity__c FE2 = new Flow_Entity__c();
        FE2.Name = 'Test Needs Assestment';
        FE2.Display_Name__c = 'Test Needs Assestment';
        FE2.IsVisible__c = true;
        FE2.Populate_From_Controlled_Field__c = false;
        FE2.Render_As_Text__c = false;
        FE2.RecordTypeId = SecRT.Id;
        FE2.Parent_Entity__c = FE1.Id;
        FE2.Parent_Flow__c = FE.Id;
        insert FE2;
        /*SubSection*/
        Flow_Entity__c FE3 = new Flow_Entity__c();
        FE3.Name = 'Test Service Type Info';
        FE3.Display_Name__c = 'Test Service Type Info';
        FE3.Field_Type__c = 'Text';
        FE3.Subsection_Position__c = 'Right';
        FE3.IsVisible__c = true;
        FE3.Populate_From_Controlled_Field__c = false;
        FE3.Render_As_Text__c = false;
        FE3.RecordTypeId = SubSecRT.Id;
        FE3.Parent_Entity__c = FE2.Id;
        FE3.Parent_Flow__c = FE.Id;
        insert FE3;
        /*Field*/
        Flow_Entity__c FE4 = new Flow_Entity__c();
        FE4.Name = 'Test Roadside Info';
        FE4.Display_Name__c = 'Test Roadside Info';
        FE4.Field_Type__c = 'Info';
        FE4.Subsection_Position__c = 'Left';
        FE4.IsVisible__c = true;
        FE4.Populate_From_Controlled_Field__c = false;
        FE4.Render_As_Text__c = false;
        FE4.RecordTypeId = FieldRT.Id;
        FE4.Parent_Entity__c = FE3.Id;
        FE4.Parent_Flow__c = FE.Id;
        insert FE4;
        /*Field*/
        Flow_Entity__c FE5 = new Flow_Entity__c();
        FE5.Name = 'Test Roadside Service Picklist';
        FE5.Display_Name__c = 'What roadside service may I provide for you today?';
        FE5.Field_Type__c = 'Picklist';
        FE5.IsVisible__c = true;
        FE5.Populate_From_Controlled_Field__c = false;
        FE5.Render_As_Text__c = false;
        FE5.RecordTypeId = FieldRT.Id;
        FE5.Parent_Entity__c = FE3.Id;
        FE5.Parent_Flow__c = FE.Id;
        insert FE5;
        /*Field Dependency*/
        Flow_Entity_Dependency__c FED = new Flow_Entity_Dependency__c();
        FED.Dependency_Child__c = FE4.Id;
        FED.Dependency_Parent__c = FE5.Id;
        FED.Info_Value__c = 'A service person will boost the battery in an attempt to enable a disabled eligible vehicle to proceed under its own power. If not able to boost, please see tow notes.A service person will boost the battery in an attempt to enable a disabled eligible vehicle to proceed under its own power';
        FED.Multiple_Values__c = false;
        FED.Operator__c = 'equals';
        FED.Value__c = 'Boost';
        FED.RecordTypeId = InfoTxt_RT.Id;
        insert FED;
    }
    static testMethod void FlowEntity_Clone(){
        Test.startTest();
        Flow_Entity__c FE = [Select id, name from Flow_Entity__c limit 1];
        FlowEntity_Clone.cloneFlowEntity(new List<string>{FE.Id});
        Test.stopTest();
    }
    static testMethod void FlowEntity_Deletion1(){
        Test.startTest();
        Flow_Entity__c FE = [Select id, name from Flow_Entity__c limit 1];
        delete FE;
        Test.stopTest();
    }
    static testMethod void FlowEntity_Deletion2(){
        Test.startTest();
        for(Flow_Entity__c FE:[Select id, name from Flow_Entity__c]){
            if(FE.Name == 'Test Service Type Info'){
                delete FE;
            }
        }
        Test.stopTest();
    }
}