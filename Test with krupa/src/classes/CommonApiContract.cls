@RestResource(urlMapping='/api/v1/contract')
global class CommonApiContract {

    public static final String POLICY_EXTERNAL='policy_external';
    
    @HttpPost
    global static CommonApiResponse doPost(CommonApiRequest contract){
        CommonApiResponse response = new CommonApiResponse();

        Savepoint sp = Database.setSavepoint();
        try{
            upsert contract.policy Policy_External__c;
            contract.contact.Policy__c = contract.policy.Id;
        
            if(contract.contact.Contact_External__c != null){
             	upsert contract.contact Contact_External__c;   
            }
         
            if(contract.vehicle.Vehicle_External__c != null ){
                upsert contract.vehicle Vehicle_External__c;
                upsertVehiclePolicyRelation(contract.vehicle, contract.policy);
            }
        }catch(DMLException e){
            Database.rollback(sp);
            response.statusCode = '500';
            response.message = e.getMessage();
            return response;
        }catch(Exception e){
            Database.rollback(sp);
            response.statusCode = '500';
            response.message = e.getMessage();
            return response;
        }
        
        response.statusCode = '200';
        response.message = 'Success';
        
        return response;
    }
    
    
    @HttpPut
    global static CommonApiResponse doPut(CommonApiRequest contract){
        CommonApiResponse response = new CommonApiResponse();
        Savepoint sp = Database.setSavepoint();
        
        try{
            Policy__c[] policies = [select id from Policy__c where Policy_External__c = :contract.policy.Policy_External__c limit 1];
            if(policies.size() == 0){
                response.statusCode = '404';
        		response.message = 'Policy does not exists.';
                return response;
            }
            contract.policy.Id = policies[0].Id;
            update contract.policy;
            
            if(contract.contact != null){
             	upsert contract.contact Contact_External__c;   
            }
            
            if(contract.vehicle != null){
                Vehicle_Policy__c[] currentVps = [select Vehicle__r.Name, Vehicle__r.Vehicle_External__c, Vehicle_Policy_Status__c from Vehicle_Policy__c where Policy__r.Policy_External__c = :contract.policy.Policy_External__c and Vehicle_Policy_Status__c=true];
                if(currentVps.size() > 0 && currentVps[0].Vehicle__r.Vehicle_External__c != contract.vehicle.Vehicle_External__c){
                    currentVps[0].Vehicle_Policy_Status__c = false;
                    update currentVps[0];
                }
                upsert contract.vehicle Vehicle_External__c;
                upsertVehiclePolicyRelation(contract.vehicle, contract.policy);
            }
        }catch(Exception e){
            Database.rollback(sp);
            response.statusCode ='500';
            response.message = e.getMessage();
            return response;
        }

		response.statusCode = '200';
        response.message = 'Success';    
        return response;
    }
    
    @HttpDelete
    global static CommonApiResponse doDelete(){
        CommonApiResponse response = new CommonApiResponse();
        String policyExt = RestContext.request.params.get(POLICY_EXTERNAL);
        
        if(policyExt.trim()==''){
            response.statusCode = '400';
            response.message = 'Bad Request, Policy number is missing.';
            return response;
        }
        
        Savepoint sp = Database.setSavepoint();

        try{
            Policy__c[] policies = [select id, Policy_Status__c from Policy__c where Policy_External__c = :policyExt and Policy_Status__c = true];
            if(policies.size() == 0){
                response.statusCode = '404';
                response.message = 'Policy does not exists.';
                return response;
            }
            policies[0].Policy_Status__c = false;
            update policies[0];
            Vehicle_Policy__c[] vps = [select id, Vehicle_Policy_Status__c from Vehicle_Policy__c where Policy__r.Policy_External__c = :policyExt and Vehicle_Policy_Status__c = true ];
            for(Vehicle_Policy__c vp : vps){
                vp.Vehicle_Policy_Status__c = false;
            }
            update vps;
            
        }catch(Exception e){
            Database.rollback(sp);
            response.statusCode ='500';
            response.message = e.getMessage();
            return response;
        }
        response.statusCode = '200';
        response.message = 'Success';    
        return response;
    }
    
    @HttpGet
    global static CommonApiResponse doGet(){
        CommonApiResponse response = new CommonApiResponse();
        String policyExt = RestContext.request.params.get(POLICY_EXTERNAL);
        
        if(policyExt.trim()==''){
            response.statusCode = '400';
            response.message = 'Bad Request, Policy number is missing.';
            return response;
        }
     
        try{
            Policy__c[] policies = [select id, Policy_Status__c, Policy_External__c from Policy__c where Policy_External__c = :policyExt and Policy_Status__c = true];
            if(policies.size() == 0){
                response.statusCode = '200';
                response.message = 'There is no such policy.';
                return response;
            }
            response.policy = policies[0];
            
            Contact[] contacts = [select id, firstName, lastName, phone from Contact where Policy__r.Policy_External__c = :policies[0].Policy_External__c];
            if(contacts.size() > 0){
                response.contact = contacts[0];
            }
            
            Vehicle_Policy__c[] vps = [select id, Vehicle_Policy_Status__c, Vehicle__r.Vehicle_External__c from Vehicle_Policy__c where Policy__r.Policy_External__c = :policyExt and Vehicle_Policy_Status__c = true];
            if(vps.size() > 0){
                Vehicle__c vehicle = [select id, Name, make__c from Vehicle__c where Vehicle_External__c = :vps[0].Vehicle__r.Vehicle_External__c limit 1];
                response.vehicle = vehicle;
            }
            
            
        }catch(Exception e){
            response.statusCode ='500';
            response.message = e.getMessage();
            return response;
        }
        response.statusCode = '200';
        response.message = 'Success';    
        return response;
    }
    
    private static Vehicle_Policy__c upsertVehiclePolicyRelation(Vehicle__c vehicle, Policy__c policy){
        String vpExternal = vehicle.Vehicle_External__c+'-' + policy.Policy_External__c;
        Vehicle_Policy__c vehiclePolicy = new Vehicle_Policy__c();
        vehiclePolicy.Vehicle_Policy_External__c = vpExternal;
        vehiclePolicy.Vehicle__c = vehicle.Id;
        vehiclePolicy.Policy__c = policy.Id;
        upsert vehiclePolicy Vehicle_Policy_External__c;
        return vehiclePolicy;
    }
    
    global class CommonApiRequest{
        public Vehicle__c vehicle;
        public Policy__c Policy;
        public Contact contact;
    }
    
    global class CommonApiResponse{
        public String statusCode;
        public String message;
        public Vehicle__c vehicle;
        public Policy__c Policy;
        public Contact contact;
    }
    
    
    
}