public class CaseExtension{

    public Case current_call { get; set; }
    public Flow_Entity__c flowEnt { get;set; }
	public Boolean newCaseAutoSaveFlag{ get;set; }
	public Boolean existingCaseAutoSaveFlag{ get;set; }
    
    public CaseExtension(ApexPages.StandardController stdController){
        this.current_call = (Case)stdController.getRecord();
		this.newCaseAutoSaveFlag = false;
		this.existingCaseAutoSaveFlag = false;
		for(Case_Auto_Save_Flag__mdt autosaveFlg:[SELECT DeveloperName,Enable_Auto_Save__c,Id,Label,MasterLabel FROM Case_Auto_Save_Flag__mdt]){
			if(autosaveFlg.DeveloperName == 'Auto_Save_on_New_Case'){
				newCaseAutoSaveFlag = autosaveFlg.Enable_Auto_Save__c;
			}else if(autosaveFlg.DeveloperName == 'Auto_Save_on_Existing_Case'){
				existingCaseAutoSaveFlag = autosaveFlg.Enable_Auto_Save__c;
			}
		}
    }

    public PageReference defineAndRedirectToType(){

        Map<String, String> parameterMap = ApexPages.currentPage().getParameters();

        String cid = parameterMap.get('Id');
        String caseNumberParam = parameterMap.get('case_number');
        String caseType = parameterMap.get('Type');
        String vinMemberId = parameterMap.get('VIN_Member_ID__c');
        String casePhone = parameterMap.get('ANI__c');
        String clientCode = parameterMap.get('Client_Code__c');
        String urlParams = '';
        String redirectLink = '';
        Boolean isCallBack = false;
    
        CA__c settings = CA__c.getOrgDefaults();

        //is New call?
        if(!String.isBlank(cid)){

            current_call = [SELECT Id, Type, Program__c FROM Case WHERE id =: cid LIMIT 1];
            
            if(current_call.Program__c != null){
                List<Flow_Entity__c> flows = [SELECT Id, Display_Name__c, Program__c, RecordType.Name, Flow_Type__c FROM Flow_Entity__c WHERE Program__c = :current_call.Program__c AND RecordType.Name = 'Flow' AND Flow_Type__c = 'Program Flow'];
                if(flows.size() > 0){
                    flowEnt = flows[0];
                }
            }

            if(current_call.Type == 'Roadside Assistance'){
                redirectLink = '/apex/ControllingObjectTestPage?CsId=' + current_call.id + '&autoSave=' + existingCaseAutoSaveFlag;
                if(flowEnt != null){
                    redirectLink = redirectLink + '&FlowId=' + flowEnt.Id;
                }
            }
            else if(current_call.Type == 'Recall'){
                redirectLink = '/apex/TakeCall_Recall?id=' + current_call.id + '&autoSave=false';
            }
            else if(current_call.Type == 'Travel'){
                redirectLink = '/apex/TakeCall_Travel?id=' + current_call.id + '&autoSave=false';
            }

        }
        else{

            if(!String.isBlank(caseNumberParam)){

                try{

                    current_call = [SELECT Id, Type, Program__c, Program__r.Account__c FROM Case WHERE CaseNumber =: caseNumberParam LIMIT 1];
                    redirectLink = '/apex/ControllingObject_CallBack?CsId=' + current_call.id + '&autoSave=false';
                    if(current_call.Program__r.Account__c != null){
                        List<Flow_Entity__c> flows = [SELECT Id, Display_Name__c, Client__c, Program__c, RecordType.Name, Flow_Type__c FROM Flow_Entity__c WHERE Client__c = :current_call.Program__r.Account__c AND RecordType.Name = 'Flow' AND Flow_Type__c = 'Callback Flow'];
                        if(flows.size() > 0){
                            flowEnt = flows[0];
                            redirectLink = redirectLink + '&FlowId=' + flowEnt.Id;
                        }
                    }
					isCallBack=true;
                }
                catch(Exception ex){

                    system.debug('TB JLR CALLBACK DEBUG Exception ' + ex.getMessage());
                    redirectLink = '/apex/ControllingObjectTestPage?CsId=' + '&autoSave=' + newCaseAutoSaveFlag;

                }

            }
            else if(!String.isBlank(clientCode) && (!String.isBlank(vinMemberId) || !String.isBlank(casePhone))){

                String queryCallBack = 'SELECT Id, Program__c, Program__r.Account__c FROM Case WHERE Program__r.CTI_Search_Terms__c LIKE \'%' + clientCode + '%\'';

                if(!String.isBlank(vinMemberId)){
                    queryCallBack += ' AND VIN_Member_ID__c =\'' + vinMemberId +'\''; 
                }
                else if(!String.isBlank(casePhone)){
                    queryCallBack += ' AND Phone_Stripped__c=\'' + casePhone.replaceAll('\\D','') + '\'';
                }

                queryCallBack +=  ' AND Status IN (\'' + settings.DI_Status__c + '\',\'' + settings.ER_Status__c + '\',\'' + settings.OL_Status__c + '\') ORDER BY RE_Status_Date_Time__c DESC LIMIT 1';
                
                system.debug('TB CALL BACK DEBUG QUERY ' + queryCallBack);
                
                try{

                    current_call = Database.query(queryCallBack);
                    redirectLink = '/apex/ControllingObject_CallBack?CsId=' + current_call.id + '&autoSave=false';
                    if(current_call.Program__r.Account__c != null){
                        List<Flow_Entity__c> flows = [SELECT Id, Display_Name__c, Client__c, Program__c, RecordType.Name, Flow_Type__c FROM Flow_Entity__c WHERE Client__c = :current_call.Program__r.Account__c AND RecordType.Name = 'Flow' AND Flow_Type__c = 'Callback Flow'];
                        if(flows.size() > 0){
                            flowEnt = flows[0];
                            redirectLink = redirectLink + '&FlowId=' + flowEnt.Id;
                        }
                    }
                    isCallBack=true;
                }
                catch(Exception ex){

                    system.debug('TB CALLBACK DEBUG QUERY Exception ' + ex.getMessage());

                    if(!String.isBlank(caseType)){
                        if(caseType == 'RSA'){
                            redirectLink = '/apex/ControllingObjectTestPage' + '?autoSave=' + newCaseAutoSaveFlag;
                        }
                        else if(caseType == 'Recall'){
                            redirectLink = '/apex/TakeCall_Recall' + '?autoSave=false';
                        }
                        else if(caseType == 'Travel'){
                            redirectLink = '/apex/TakeCall_Travel' + '?autoSave=false';
                        }
                    }
                    else{
                        redirectLink = '/apex/ControllingObjectTestPage' + '?autoSave=' + newCaseAutoSaveFlag;
                    }
                    /*Modified on 27 SEPT TO populate MISCELLANEOUS FOR ACURA/HONDA FLOWS*/
                    if(!String.isBlank(clientCode)){
                        if(clientCode == 'Honda_US_RSA_Cancel' || clientCode == 'Honda_US_RSA_Update' 
                        || clientCode == 'Acura_US_RSA_Cancel' || clientCode == 'Acura_US_RSA_Update'
                        || clientCode == 'Acura_NSX_US_Cancel' || clientCode == 'Acura_NSX_US_Update'
                        || clientCode == 'Honda_Alternate_Fuel_US_RSA_Cancel' || clientCode == 'Honda_Alternate_Fuel_US_RSA_Update'){
                            String searchTXT = '%'+clientCode+'%';
                            List<Program__c> proglist = [Select id, Program_Code__c, CTI_Search_Terms__c from Program__c where CTI_Search_Terms__c like :searchTXT];
                            if(proglist.size() > 0){
                                String queryCallBack1 = 'SELECT Id, Program__c, Program__r.Account__c FROM Case WHERE Program__r.Program_Code__c =\'' + proglist[0].Program_Code__c +'\'';

                                if(!String.isBlank(vinMemberId)){
                                    queryCallBack1 += ' AND VIN_Member_ID__c =\'' + vinMemberId +'\''; 
                                }
                                else if(!String.isBlank(casePhone)){
                                    queryCallBack1 += ' AND Phone_Stripped__c=\'' + casePhone.replaceAll('\\D','') + '\'';
                                }

                                queryCallBack1 +=  ' AND Status IN (\'' + settings.DI_Status__c + '\',\'' + settings.ER_Status__c + '\',\'' + settings.OL_Status__c + '\') ORDER BY RE_Status_Date_Time__c DESC LIMIT 1';
                                
                                system.debug('TB CALL BACK DEBUG QUERY FOR HONDA ACURA' + queryCallBack1);
                                try{
                                    current_call = Database.query(queryCallBack1);
                                    redirectLink = '/apex/ControllingObject_CallBack?CsId=' + current_call.id + '&autoSave=false';
                                    if(current_call.Program__r.Account__c != null){
                                        List<Flow_Entity__c> flows = [SELECT Id, Display_Name__c, Client__c, Program__c, RecordType.Name FROM Flow_Entity__c WHERE Client__c = :current_call.Program__r.Account__c AND RecordType.Name = 'Flow'];
                                        if(flows.size() > 0){
                                            flowEnt = flows[0];
                                            redirectLink = redirectLink + '&FlowId=' + flowEnt.Id;
                                        }
                                    }
                                    isCallBack=true;
                                    
                                }catch(Exception exc){
                                    system.debug('TB CALL BACK DEBUG QUERY FOR HONDA ACURA' + exc.getMessage());
                                }
                            }
                        }
                    }
                    
                    
                }
                
            }
            else if(!String.isBlank(caseType)){

                if(caseType == 'RSA'){
                    redirectLink = '/apex/ControllingObjectTestPage' + '?autoSave=' + newCaseAutoSaveFlag;
                }
                else if(caseType == 'Recall'){
                    redirectLink = '/apex/TakeCall_Recall' + '?autoSave=false';
                }
                else if(caseType == 'Travel'){
                    redirectLink = '/apex/TakeCall_Travel' + '?autoSave=false';
                }

            }
            else{
                redirectLink = '/apex/ControllingObjectTestPage' + '?autoSave=' + newCaseAutoSaveFlag;
            }
			
			//-FLOW redirect based on Client_Code__c parameter--------------------------------------------------VC
            if(!String.isBlank(clientCode) && isCallBack==false){
                    string searchTXT = '%'+clientCode+'%';
                try{
                    Program__c a = [Select Id, Program_Code__c, Call_Flow_Name__c, Description__c, Account__r.Name, Client_Name_English__c, CTI_Search_Terms__c, Name from Program__c 
                                    where CTI_Search_Terms__c like :searchTXT and Status__c!='Inactive' AND Program_Code__c!=null 
                                    ORDER BY Program_Priority__c NULLS LAST, Call_Flow_Name__c ASC, Description__c ASC, Program_Code__c ASC ][0];           
                	System.debug('Program Selected: ' + a);
                
                    current_call.Program__c=a.Id;               
                    
                    Flow_Entity__c flowEnt = [SELECT Id, Display_Name__c, Program__c, RecordType.Name, Flow_Type__c FROM Flow_Entity__c WHERE Program__c = :current_call.Program__c AND RecordType.Name = 'Flow' AND Flow_Type__c = 'Program Flow'][0];
                    System.debug('Flow Selected: ' + flowEnt);
                    
                    redirectLink = '/apex/ControllingObjectTestPage?FlowId=' + flowEnt.Id + '&autoSave=' + newCaseAutoSaveFlag;         
                }
                catch(Exception ex){
                    
                    system.debug('ASSIGN FLOW DEBUG QUERY Exception ' + ex.getMessage());
                    
                    if(!String.isBlank(caseType)){
                        if(caseType == 'RSA'){
                            redirectLink = '/apex/ControllingObjectTestPage' + '?autoSave=' + newCaseAutoSaveFlag;
                        }
                        else if(caseType == 'Recall'){
                            redirectLink = '/apex/TakeCall_Recall' + '?autoSave=false';
                        }
                        else if(caseType == 'Travel'){
                            redirectLink = '/apex/TakeCall_Travel' + '?autoSave=false';
                        }
                    }
                    else{
                        redirectLink = '/apex/ControllingObjectTestPage' + '?autoSave=' + newCaseAutoSaveFlag;
                    }
                }
            }
            //--FLOW redirect based on Client_Code__c parameter-- End---------------------------------------------------------

            for(String key: parameterMap.keySet()){
                if(redirectLink.containsIgnoreCase('?') ){
                    redirectLink+= '&'+ key + '=' + parameterMap.get(key);
                }
                else{
                    redirectLink+= '?'+ key + '=' + parameterMap.get(key);
                }
            }
        }
        system.debug('************'+redirectLink);
        PageReference pageRef = new PageReference(redirectLink);

        return pageRef;
    }

}