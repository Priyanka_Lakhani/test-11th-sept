public class LocationCallouts {

     @future (callout=true)  // future method needed to run callouts from Triggers
      static public void getLocation(id accountId){
        // gather account info

        CA__c settings = CA__c.getOrgDefaults();

        Account a = [SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Location__Latitude__s,Location__Longitude__s,IncorrectLocation__c,isLocationUpdated__c FROM Account WHERE id =: accountId];
        
        system.debug('111111111111 ' + a.id);
        // create an address string
        String address = '';
        if (a.BillingStreet != null)
            address += a.BillingStreet +', ';
        if (a.BillingCity != null)
            address += a.BillingCity +', ';
        if (a.BillingState != null)
            address += a.BillingState +' ';
        if (a.BillingPostalCode != null)
            address += a.BillingPostalCode +', ';
        if (a.BillingCountry != null)
            address += a.BillingCountry;

        address = EncodingUtil.urlEncode(address, 'UTF-8');

        // build callout
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false&key=' + settings.Google_Geocoder_Key__c);
        req.setMethod('GET');
        req.setTimeout(60000);

        try{
            // callout
            HttpResponse res;
            
            res = h.send(req); 
            system.debug('333333333333 ' + RES.GETBODY());

           
            
            // parse coordinates from response
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                       parser.nextToken(); // object start
                       while (parser.nextToken() != JSONToken.END_OBJECT){
                           String txt = parser.getText();
                           parser.nextToken();
                           if (txt == 'lat')
                               lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                               lon = parser.getDoubleValue();
                       }

                }
            }
            system.debug('222222222222222 ' + lat + lon);
            // update coordinates if we get back
            if (lat != null){
                a.Location__Latitude__s = lat;
                a.Location__Longitude__s = lon;
                a.isLocationUpdated__c = true;
                a.IncorrectLocation__c = false;
                update a;
            }

        } catch (Exception e) {
            system.debug('GEOLOCATION EXCEPTION ' + e.getMessage());
        }
    }
}