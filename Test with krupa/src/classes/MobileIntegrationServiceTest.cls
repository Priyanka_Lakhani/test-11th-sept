@isTest
private class MobileIntegrationServiceTest {
    static testMethod void getFAQ() {
        FAQ__c faq = new FAQ__c();
        faq.Answer__c = 'Answer'; 
        faq.FAQType__c = 'Web & Mobile'; 
        faq.Language__c = 'English'; 
        faq.Order__c = 1; 
        insert faq; 
        FAQ__c faq2 = new FAQ__c();
        faq2.Answer__c = 'Answer'; 
        faq2.FAQType__c = 'Web & Mobile'; 
        faq2.Language__c = 'French'; 
        faq2.Order__c = 1; 
        insert faq2;
                
        Test.startTest();   
            MobileIntegrationService.go('getFAQ', 'en', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
            MobileIntegrationService.go('getFAQ', 'fr', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null); 
                        
        Test.stopTest();       
        //cls.go(method, LanguageCode, caseNumber, name, email, comment, VIN, mileageAmt, csiId, csID, clientCode, latitude, longitude, xMin, xMax, yMin, yMax, maxPOICount, sortBy, RequestRoadsideAssistanceClass RequestRoadsideAssistance);
    }
    static testMethod void getCaseStatus() {    
        CA__c ca = new CA__c();
        ca.Last_Case_Number__c = '1';
        insert ca;
        Case cs = new Case();       
        cs.Status = 'Driver On Location';
        insert cs; 
        cs = [select CaseNumber from Case where id =: cs.id limit 1];
        Test.startTest();   
            MobileIntegrationService.go('getCaseStatus', null, cs.CaseNumber, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null); 
            MobileIntegrationService.go('getCaseStatus', null, '00000', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);         
            /*RSARefreshController controller = new RSARefreshController();
            PageReference pageref = new PageReference('/apex/RSARefresh');
            pageref.getparameters().put('id', cs.Id);
            Test.setCurrentPage(pageref);
            controller.action();
            CreateRSALogin rsaLogin2 = new CreateRSALogin();
            rsaLogin2.action();*/
        Test.stopTest();            
    }
    static testMethod void setFeedback() { 
        Test.startTest();
            MobileIntegrationService.go('setFeedback', null, null, 'Name', 'email@email.com', 'comment', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
            MobileIntegrationService.go('setFeedback', null, null, 'Name', '#$#@./,', 'comment', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
        
        Test.stopTest(); 
    }
    static testMethod void getProvince() {
        Test.startTest();
            MobileIntegrationService.go('getProvince', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
        Test.stopTest();        
    } 
    static testMethod void getProblemType() {
        Problem_Type__c pt = new Problem_Type__c();
        pt.Active__c = true;
        pt.Language__c = 'English';
        pt.Order__c = 0;
        pt.Question__c = 'Question';
        insert pt;
        Problem_Type__c pt2 = new Problem_Type__c();
        pt2.Active__c = true;
        pt2.Language__c = 'French';
        pt2.Order__c = 0;
        pt2.Question__c = 'Question';
        insert pt2;     
        Test.startTest();
            MobileIntegrationService.go('getProblemType', 'en', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
            MobileIntegrationService.go('getProblemType', 'fr', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
        Test.stopTest();        
    }     
    static testMethod void getLocationType() {
        CA__c ca = new CA__c();
        ca.Location_Codes_English__c = '1,2,3,4,5';
        ca.Location_Codes_English_Ext__c = '1,2,3,4,5';
        ca.Location_Codes_French__c = '1,2,3,4,5';
        ca.Location_Codes_French_Ext__c = '1,2,3,4,5';
        insert ca;      
        Test.startTest();
            MobileIntegrationService.go('getLocationType', 'en', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
            MobileIntegrationService.go('getLocationType', 'fr', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);               
        Test.stopTest();        
    }
    static testMethod void ValidateVIN() {
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        //Client
        Account client = new Account();
        client.RecordTypeId = clientRecordTypeId;
        client.Name = '(PL_TEST_RECORD) Test Client';
        client.Status__c = 'Active';
        client.Account_Number__c = 'PL Test Client Account#';
        client.Type = 'Customer';

        insert client;
        Program__c program = ControllingObject_TestUtils.createTestProgram(client);
        
        List<Vehicle__c> vehicleList = ControllingObject_TestUtils.createVehicles();
        List<Policy__c> policyList = ControllingObject_TestUtils.createPolicies(program);
        ControllingObject_TestUtils.createVehiclePolicies(policyList, vehicleList);
        Test.startTest();
            MobileIntegrationService.go('ValidateVIN', null, null, null, null, null, vehicleList[0].Name, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
            MobileIntegrationService.go('ValidateVIN', null, null, null, null, null, '0d00024', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);          
            MobileIntegrationService.go('ValidateVIN', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);               
            MobileIntegrationService.go('ValidateVIN', null, null, null, null, null, '0d0002400000000000000000000000000000', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);                     
        Test.stopTest();        
    }
    static testMethod void VINWarrantyCheck() {
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        //Client
        Account client = new Account();
        client.RecordTypeId = clientRecordTypeId;
        client.Name = '(PL_TEST_RECORD) Test Client';
        client.Status__c = 'Active';
        client.Account_Number__c = 'PL Test Client Account#';
        client.Type = 'Customer';

        insert client;
        Program__c program = ControllingObject_TestUtils.createTestProgram(client);
        
        List<Vehicle__c> vehicleList = ControllingObject_TestUtils.createVehicles();
        List<Policy__c> policyList = ControllingObject_TestUtils.createPolicies(program);
        ControllingObject_TestUtils.createVehiclePolicies(policyList, vehicleList);
        Test.startTest();
            MobileIntegrationService.go('VINWarrantyCheck', null, null, null, null, null, vehicleList[0].Name, '100', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);          
            MobileIntegrationService.go('VINWarrantyCheck', null, null, null, null, null, '00jikj55', '100', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
            MobileIntegrationService.go('VINWarrantyCheck', null, null, null, null, null, '00jikj550000000000000000000000000000', '100', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
            MobileIntegrationService.go('VINWarrantyCheck', null, null, null, null, null, null, '100', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);             
        
        Test.stopTest();        
    }
    static testMethod void CoverageSummary() {
        Id clientRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Client').getRecordTypeId();
        //Client
        Account client = new Account();
        client.RecordTypeId = clientRecordTypeId;
        client.Name = '(PL_TEST_RECORD) Test Client';
        client.Status__c = 'Active';
        client.Account_Number__c = 'PL Test Client Account#';
        client.Type = 'Customer';

        insert client;
        Program__c program = ControllingObject_TestUtils.createTestProgram(client);
        
        List<Vehicle__c> vehicleList = ControllingObject_TestUtils.createVehicles();
        List<Policy__c> policyList = ControllingObject_TestUtils.createPolicies(program);
        ControllingObject_TestUtils.createVehiclePolicies(policyList, vehicleList);       
        Test.startTest();
            MobileIntegrationService.go('CoverageSummary', null, null, null, null, null, vehicleList[0].Name, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
            MobileIntegrationService.go('CoverageSummary', null, null, null, null, null, '010645hiy', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
            MobileIntegrationService.go('CoverageSummary', null, null, null, null, null, '010645hiy0000000000000000000000000000000', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);             
            MobileIntegrationService.go('CoverageSummary', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
        
        Test.stopTest();        
    }
    static testMethod void cancelRoadsideRequest() {
        CA__c ca = new CA__c();
        ca.XX_Status__c = '1';
        ca.Last_Case_Number__c = '1';
        insert ca;
        Case cs = new Case();       
        cs.Status = 'Driver On Location';
        cs.Last_Club_Update__c = Datetime.now();
        insert cs; 
        cs = [select CaseNumber from Case where id =: cs.id limit 1];       
        Test.startTest();
            MobileIntegrationService.go('cancelRoadsideRequest', null, null, null, null, null, null, null, cs.CaseNumber, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
            MobileIntegrationService.go('cancelRoadsideRequest', null, null, null, null, null, null, null, '45745jyh', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);           
        
        Test.stopTest();        
    }
    static testMethod void getServiceTrackerData() {
        CA__c ca = new CA__c();
        ca.XX_Status__c = '1';
        ca.Last_Case_Number__c = '1';
        insert ca;      
        Case cs = new Case();       
        cs.Status = 'Driver On Location';
        insert cs;      
        cs = [select CaseNumber from Case where id =: cs.id limit 1];
        Test.startTest();
            MobileIntegrationService.go('getServiceTrackerData', null, null, null, null, null, null, null, null, cs.CaseNumber, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);            
        Test.stopTest();        
    }
     static testMethod void RequestRoadsideAssistance() {
        CA__c cat = new CA__c();
        cat.XX_Status__c = '1';
        cat.Last_Case_Number__c = '1';
        insert cat;             
        Field_Translation__c ca = new Field_Translation__c();
        ca.Club__c = 'MOBILE';
        ca.Type__c = 'PH';
        ca.Output__c = 'MOBILE';
        ca.Name = 'MOBILE1';
        insert ca;      
        Field_Translation__c ca2 = new Field_Translation__c();
        ca2.Club__c = 'MOBILE';
        ca2.Type__c = 'T';
        ca2.Output__c = 'MOBILE';
        ca2.Name = 'MOBILE2';
        insert ca2;   
        Field_Translation__c ca3 = new Field_Translation__c();
        ca3.Club__c = 'MOBILE';
        ca3.Type__c = 'L';
        ca3.Output__c = 'MOBILE';
        ca3.Name = 'MOBILE3';
        insert ca3;                     
        Case cs = new Case();       
        cs.Status = 'Driver On Location';
        insert cs;      
        cs = [select CaseNumber from Case where id =: cs.id limit 1];       
        MobileIntegrationService.RequestRoadsideAssistanceClass cls = new MobileIntegrationService.RequestRoadsideAssistanceClass();
        cls.vinLookup = cs.CaseNumber;
        cls.callbackTypeCode = 'MOBILE'; 
        cls.breakdownLocationType  = 'LocationType';
        cls.tCode  = 'CP';
        cls.latitude  = '48.42462';
        cls.longitude  = '-123.353116';
        cls.streetName  = 'Baiker';
        cls.programName  = '916';  
        cls.callbackNumber = '123456789';
        cls.firstName = 'TestfirstName';
        cls.lastName = 'TestlastName';
        cls.odometer = '800';
        cls.crossStreet = 'crossBaiker';
        cls.streetNumber = '80';
        cls.city = 'Toronto';
        cls.stateCode = 'ON';
        cls.postalCode = 'q1w2e3';
        cls.breakdownLandmark = 'breakdownLandmark';
        cls.make = 'Ford';
        cls.model = 'F350';
        cls.color = 'Black';
        cls.year = '2008';
        cls.passengerCount = '200';
        cls.paceSetter = 'paceSetter';
        cls.towDestination = 'towDestination';
        cls.towDestinationAddr = 'towDestinationAddr';
        cls.towDestinationCity = 'City';
        cls.towDestinationState = 'NB';
        cls.TowDestLat = '48.42462';
        cls.TowDestLng = '-123.353116';
        cls.towOptions = '[{\"id\":\"a0Hm000000ClgVxEAJ\"},{\"id\":\"a0Hm000000ClgY9EAJ\"}]';
        MobileIntegrationServiceHelper.getParameters(cls);    
        Test.startTest();
            MobileIntegrationService.go(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, cls, null, null, null, null, null, null, null, null);             
        Test.stopTest();            
     }
    static testMethod void findClubAutoDealer() {
        Test.startTest();
            MobileIntegrationService.go('findClubAutoDealer', null, null, null, null, null, null, null, null, null, '', '48.42462', '-123.353116', '10', '10', '10', '10', '10', 'Name', null,null, null, null, null, null, null, null, null);             
        Test.stopTest();        
    } 
    static testMethod void MobileIntegrationServiceTest() {
        CA__c ca = new CA__c();
        ca.Google_Key__c = '232';
        ca.Google_Private_Key__c = 'ddd_fdf-34';
        insert ca;
        MobileIntegrationServiceHelper.reverse_geocode(1,2);
        MobileIntegrationServiceHelper.distanceBetween(1,2,3,4,5);  
        MobileIntegrationServiceHelper.geocode('5300 boulevard des Galeries, suite 305 - marchand QUÉBEC, Quebec G2K 2A2 Canada');  
        MobileIntegrationServiceHelper.KiaResponse kr = new MobileIntegrationServiceHelper.KiaResponse();   
        list<MobileIntegrationServiceHelper.Kiad> kList = new list<MobileIntegrationServiceHelper.Kiad>();
        MobileIntegrationServiceHelper.Kiad k = new MobileIntegrationServiceHelper.Kiad();
        k.id = '123';
        k.name = '123';
        k.street = '123';
        k.city = '123';
        k.state = '123';
        k.zip = '123';
        k.phone = '123';
        k.fax = '123';
        k.hours = '123';
        k.distance = '123';
        k.propType = '123';
        k.x = '123';
        k.y = '123';
        kList.add(k);       
        MobileIntegrationServiceHelper.Pois p = new MobileIntegrationServiceHelper.Pois();
        p.KIAD = kList; 
        kr.count = '1';
        kr.pois = p;
        kr.xMin = '123';
        kr.xMax = '123';
        kr.yMin = '123';
        kr.yMax = '123';
        MobileIntegrationServiceHelper.FindClubAutoDealerResponse ret = new MobileIntegrationServiceHelper.FindClubAutoDealerResponse();
        ret.status = '0';
        ret.message = '0';
        ret.kia_response = kr;      
        MobileIntegrationServiceHelper.RoadsideRequest rr = new MobileIntegrationServiceHelper.RoadsideRequest();
        rr.roadsideRequest = '123';
        rr.csiId = '123';
        MobileIntegrationServiceHelper.RequestRoadsideAssistanceResponse rrar = new MobileIntegrationServiceHelper.RequestRoadsideAssistanceResponse();
        rrar.status = 0;
        rrar.message = 'success';
        rrar.info = rr;
        
        MobileIntegrationServiceHelper.TowOption towOption = new MobileIntegrationServiceHelper.TowOption();
        towOption.id = '123';
        towOption.nearest = '123';
        towOption.selected = '123';
        towOption.estimatedDistance = '123';
        towOption.estimatedDuration = '123';

        MobileIntegrationServiceHelper.ServiceTrackerDataResponse stdr = new MobileIntegrationServiceHelper.ServiceTrackerDataResponse();
        stdr.csid = '123';
        stdr.CallState = '123';
        stdr.BreakdownLng = 1;
        stdr.BreakDownLat = 1;
        stdr.TowDestLng = 1;
        stdr.TowDestLat = 1;
        stdr.TowTruckLat = 1;
        stdr.TowTruckLng = 1;
        stdr.LastUpdateTimeStamp = Datetime.now();
        stdr.ETA = '123';
        MobileIntegrationServiceHelper.GetServiceTrackerDataResponse gstdr = new MobileIntegrationServiceHelper.GetServiceTrackerDataResponse();
        gstdr.status = '0';
        gstdr.message = '0';
        gstdr.ServiceTrackerData = stdr;
    }
}