global class GlobalClass {
    public static boolean firstRun = true;
    webService static void clearTowOptions(Id cid) {
        List<Tow_Option__c> opt = [Select Id from Tow_Option__c where Case__c=:cid];
        Database.delete(opt);
        if(!opt.isEmpty()) {
            Database.emptyRecycleBin(opt);
        }
    }
 
    private static String kHexChars = '0123456789abcdefABCDEF';
 
    global static String NewGuid() {

        String returnValue = '';
        Integer nextByte = 0;

        for (Integer i=0; i<16; i++) {

            if (i==4 || i==6 || i==8 || i==10) 
                returnValue += '-';

            nextByte = (Math.round(Math.random() * 255)-128) & 255;

            if (i==6) {
                nextByte = nextByte & 15;
                nextByte = nextByte | (4 << 4);
            }

            if (i==8) {
                nextByte = nextByte & 63;
                nextByte = nextByte | 128;
            }
                returnValue += getCharAtIndex(kHexChars, nextByte >> 4);
            returnValue += getCharAtIndex(kHexChars, nextByte & 15);
        }

        return returnValue;
    }
    
    global static String getCharAtIndex(String str, Integer index) {

        if (str == null) return null;

        if (str.length() <= 0) return str;    

        if (index == str.length()) return null;    

        return str.substring(index, index+1);
    }
    
    global static string getDatayyyyMMdd(DateTime dt)
    {
        string rtn = '';
        
        String sMonth = String.valueof(dt.month());
        String sDay = String.valueof(dt.day());
        if(sMonth.length()==1)
            sMonth = '0' + sMonth;
        if(sDay.length()==1)
            sDay = '0' + sDay;

         rtn = String.valueof(dt.year()) + sMonth + sDay ;
         return rtn;
    }
    
    global static string ScrambleText(string st)
    {
        string rtn = '';
        
        blob key = EncodingUtil.base64Decode('lTQelt/F4Bj6H7hdTPf0lw==');
        blob data = blob.valueOf(st);
        blob encrypted=crypto.encryptWithManagedIV('AES128', key,data);
        
        rtn = EncodingUtil.base64Encode(encrypted);
        return rtn;
    }
    
    
    global static string UnscrambleText(string st)
    {
        string rtn = '';
        
        blob key = EncodingUtil.base64Decode('lTQelt/F4Bj6H7hdTPf0lw==');
        
        
        blob data = EncodingUtil.base64Decode(st);
        blob decrypted = crypto.decryptWithManagedIV('AES128', key, data);
            
        
        rtn = decrypted.tostring();
        return rtn;
    
    }
}