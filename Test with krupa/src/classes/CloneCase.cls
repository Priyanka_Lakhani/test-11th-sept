public class CloneCase {

    Case thisCase;
    string cloneReason {get;set;}
    string serviceCall {get;set;}
    public Case clone { get; set; }
    transient String caseFields;
    public CloneCase(ApexPages.StandardController controller) {
        thisCase = (Case)controller.getRecord();
        cloneReason = ApexPages.currentPage().getParameters().get('CR');
        serviceCall = ApexPages.currentPage().getParameters().get('SC');
        system.debug('cr:' + cloneReason);
        system.debug('SC:' + serviceCall);
    }

    public PageReference init() {
        getCaseFields();
        String cquery = 'Select ' + caseFields + ' from Case where Id = \'' + thisCase.Id + '\'';
        thisCase = database.query(cquery);
        clone = thisCase.clone(false);
        for(Schema.FieldSetMember f : SObjectType.Case.FieldSets.Clear_Fields_on_Clone.getFields()) {
            try { clone.put(f.getFieldPath(), NULL); } catch(Exception e) {}
        }
        clone.Status=CA__c.getOrgDefaults().RE_Status__c;
        clone.OwnerId=userInfo.getUserId();
        if (String.isNotEmpty(thisCase.Club_Call_Number__c) && String.isNotEmpty(thisCase.Club__c))
          clone.IsRedispatch__c = true;
        else
          clone.IsRedispatch__c = false;
        clone.ParentId = thisCase.Id;
        clone.cloneReason__c = cloneReason;

        if (serviceCall == 'No') {
            clone.Non_Service_Call__c = true;
        }
        else if (serviceCall == 'Yes') {
            clone.Non_Service_Call__c = false;
        }

        insert clone;
        return new PageReference('/apex/TakeCall?Id='+clone.Id).setRedirect(TRUE);
    }
     /**
    * ───────────────────────────────────────────────────────────────────────────────────────────────┐
    *  Case Field API String: To pull all field api names for query
    * ───────────────────────────────────────────────────────────────────────────────────────────────┘
    */
    public void getCaseFields() {
        //FoldRegion
        caseFields='';
        for(Schema.SObjectField f : Case.SObjectType.getDescribe().fields.getMap().values()) {
            caseFields += (f.getDescribe().getName() + ',');
        }
        caseFields=caseFields.left(caseFields.length()-1);
    }
}