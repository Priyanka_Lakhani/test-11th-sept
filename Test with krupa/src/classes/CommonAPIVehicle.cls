@RestResource(urlMapping='/api/v1/vehiclebundle/*')
global class CommonAPIVehicle {

    @HttpPost
    global static CommonApiResponse doPost(CommonApiRequest bundle){
        CommonApiResponse response = new CommonApiResponse();
        String vpExternal = bundle.vehicle.Vehicle_External__c+'-'+bundle.policy.Policy_External__c;
		Vehicle_Policy__c vehiclePolicy = new Vehicle_Policy__c();
        vehiclePolicy.Vehicle_Policy_External__c = vpExternal;
        System.debug('here we gooooo ' + vpExternal);

        Savepoint sp = Database.setSavepoint();
        try{
            // upsert policy
            upsert bundle.policy Policy_External__c;
            
            // upsert contact
            bundle.contact.Policy__c = bundle.policy.Id;
        	bundle.contact.Contact_External__c = bundle.policy.Policy_External__c +'-1';
            upsert bundle.contact Contact_External__c;
            
            // upsert Vehicle
            upsert bundle.vehicle Vehicle_External__c;
            
            // upsert vehicle_policy__c
            vehiclePolicy.Vehicle__c = bundle.vehicle.Id;
        	vehiclePolicy.Policy__c = bundle.policy.Id;
            upsert vehiclePolicy Vehicle_Policy_External__c;
            
        }catch(DMLException e){
            Database.rollback(sp);
            System.debug('hahaha i am here - dml exception' + e.getMessage());
            response.statusCode = '500';
            response.message = e.getMessage();
            return response;
        }catch(Exception e){
            Database.rollback(sp);
            System.debug('hahaha i am here - string exception' + e.getMessage());
            response.statusCode = '500';
            response.message = e.getMessage();
            return response;
        }
        
        response.statusCode = '200';
        response.message = 'Success';
        
        return response;
    }
    
    
    global class CommonApiRequest{
        public Vehicle__c vehicle;
        public Policy__c Policy;
        public Contact contact;
    }
    
    global class CommonApiResponse{
        public String statusCode;
        public String message;
    }
    
}