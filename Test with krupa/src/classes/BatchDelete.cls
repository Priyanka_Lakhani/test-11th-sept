global class BatchDelete implements Database.Batchable<sObject>, Database.Stateful{
   public String query;

   global BatchDelete(string q){         
          query=q;
     }
    
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(query);
   }

   global void execute(Database.BatchableContext BC, List<sObject> scope){
      delete scope;
      //DataBase.emptyRecycleBin(scope);
   }

   global void finish(Database.BatchableContext BC){
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,
      TotalJobItems, CreatedBy.Email
      FROM AsyncApexJob WHERE Id =
      :BC.getJobId()];
       system.debug(a);

   }
}