global class SetLocationBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
    
    global String query;
    global CA__c settings;
    
    global SetLocationBatch(String q) {

        if(q == null){
            query = 'SELECT BillingCity,BillingCountry,BillingPostalCode,BillingState,BillingStreet,Location__Latitude__s,Location__Longitude__s,IncorrectLocation__c,isLocationUpdated__c FROM Account WHERE isLocationUpdated__c = false';
        }
        else{
            query = q;
        }

        settings = CA__c.getOrgDefaults();
        
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<Account> scope) {
        List<Account>accToUpdate = new List<Account>();
        for(Account a : scope){
            String address = '';
            if (a.BillingStreet != null)
                address += a.BillingStreet +', ';
            if (a.BillingCity != null)
                address += a.BillingCity +', ';
            if (a.BillingState != null)
                address += a.BillingState +' ';
            if (a.BillingPostalCode != null)
                address += a.BillingPostalCode +', ';
            if (a.BillingCountry != null)
                address += a.BillingCountry;

            address = EncodingUtil.urlEncode(address, 'UTF-8');
            Http h = new Http();
            HttpRequest req = new HttpRequest();
            req.setEndpoint('https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&sensor=false&key=' + settings.Google_Geocoder_Key__c);
            req.setMethod('GET');
            req.setTimeout(60000);
            HttpResponse res;
            
            res = h.send(req);
                
            system.debug('333333333333 ' + RES.GETBODY());
            
            JSONParser parser = JSON.createParser(res.getBody());
            double lat = null;
            double lon = null;
            while (parser.nextToken() != null) {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) &&
                    (parser.getText() == 'location')){
                       parser.nextToken(); // object start
                   while (parser.nextToken() != JSONToken.END_OBJECT){
                       String txt = parser.getText();
                       parser.nextToken();
                       if (txt == 'lat')
                           lat = parser.getDoubleValue();
                       else if (txt == 'lng')
                           lon = parser.getDoubleValue();
                   }

                }
            }


            if (lat != null){
                a.Location__Latitude__s = lat;
                a.Location__Longitude__s = lon;
                a.isLocationUpdated__c = true;
                a.IncorrectLocation__c = false;
                accToUpdate.add(a);
            }
        }
        update accToUpdate;
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}