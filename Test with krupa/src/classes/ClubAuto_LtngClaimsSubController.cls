/*
Author:Thamizh
Program:Claims Portal
Description: Controller for the claims submission component
Date:Sept 11 2018
Last Modified Date: March 11 2019
*/
public without sharing class ClubAuto_LtngClaimsSubController {
    @AuraEnabled
    public List < ClubAuto_Consent_Attributes__c > conslist {get;set;}
    
    @AuraEnabled
    public Expense_Claim__c claim {get;set;}
    
    @AuraEnabled
    public Boolean success{get;set;}
  
  @AuraEnabled
    public Boolean claimRecId{get;set;}
    
    @AuraEnabled
    public String ErrMsg{get;set;}
    
    @AuraEnabled
    public List<ContentDocument> doclist{get;set;}
    
    @AuraEnabled
    public static Map < String, String > getselectOptions(string fld) {
        Expense_Claim__c cl = new Expense_Claim__c();
        system.debug('cl --->' + cl);
        system.debug('fld --->' + fld);
        Map < String , String > allOpts = new Map < String , String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = cl.getSObjectType();
         
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
         
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
         
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
        fieldMap.get(fld).getDescribe().getPickListValues();
         
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allOpts.put(a.getLabel(), a.getValue());
        }
        system.debug('allOpts ---->' + allOpts);
        //allOpts.sort();
        return allOpts;
    }
    @AuraEnabled
    public static ClubAuto_LtngClaimsSubController instPage(String type, String subtype, String claimRecId) {
    /*Modified to pre populate the logged in user's email id on to the claimant email field*/
    String strQuery = '';
    String claimRcId = '';
    if(claimRecId != null){
      claimRcId = claimRecId;
    }
    system.debug('******claimRcId********'+claimRcId);
    User loggedUsr = [Select id, eMail from user where Id =:UserInfo.getUserId()];
    ClubAuto_LtngClaimsSubController pg = new ClubAuto_LtngClaimsSubController();
    if(claimRcId == ''){
      pg.claim = new Expense_Claim__c();
      pg.doclist = new List<ContentDocument>();
      pg.success = true;
      pg.ErrMsg = '';
      pg.claim.Claim_Status__c = 'Pending Submission';//Default value to Pending Submission
      pg.claim.Claimants_Email__c = loggedUsr.eMail;//Pre-populating the claimant email address with logged in users email
      //pg.claim.Claimants_First_Name__c = 'test';
    }else{
      strQuery = 'Select ' + getAllFields('Expense_Claim__c') + ' from Expense_Claim__c where Id = \''+claimRcId+'\'';
      List<Expense_Claim__c> expClaimList = new List<Expense_Claim__c>();
      expClaimList = Database.query(strQuery);
      if(expClaimList.size() > 0){
        for(Consent__c cons:[Select id, name, Certify__c, Claim__c, Consent_Message__c, Consent_Sub_Type__c, Consent_Type__c, Contact__c from Consent__c where Claim__c 
        =: claimRcId]){
          if(cons.Certify__c == true){
            
          }
        }
      }
      system.debug('******expClaimList********'+expClaimList[0]);
      system.debug('******Claimant_Province__c********'+expClaimList[0].Claimant_Province__c);
      pg.claim = expClaimList[0];
      pg.success = true;
      pg.ErrMsg = '';
    }
    pg.conslist = new List < ClubAuto_Consent_Attributes__c >();
    for(ClubAuto_Consent_Attributes__c conattr:[Select id, name, Consent_Type__c, Consent_SubType__c, Consent_Message__c from ClubAuto_Consent_Attributes__c 
    where Consent_Type__c =:type AND Consent_SubType__c  =:subtype]){
      pg.conslist.add(conattr);
    }
    if(pg.conslist.size() == 0){
      pg.success = false;
      pg.ErrMsg = 'Consent messages not configured';
    }
        return pg;
    }
    @AuraEnabled
    public static List<ContentDocument> retrieveDoc(String claimid) {
        List<ContentDocument> doclist = new List<ContentDocument>();
        List<Id> doclinkIdlist = new List<Id>();
        if(claimid != null && claimid !=''){
            for(ContentDocumentLink doclink : [Select id, ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId =:claimid]){
                doclinkIdlist.add(doclink.ContentDocumentId);
            }
        }
        if(doclinkIdlist.size() > 0){
            for(ContentDocument doc :[SELECT Id, ContentSize,FileExtension,FileType,Title, Description FROM ContentDocument where Id in:doclinkIdlist]){
                doclist.add(doc);
            }
        }
        return doclist;
    }
    @AuraEnabled
    public static List<ContentDocument> deleteAttachment(String recid, String claimid) {
        List<ContentDocument> doclist = new List<ContentDocument>();
        if(recid != null && recid != ''){
            List<ContentDocument> doclst = [select id from ContentDocument where Id =:recid];
            if(doclst.size() > 0){
                delete doclst;
            }
        }
        List<Id> doclinkIdlist = new List<Id>();
        if(claimid != null && claimid !=''){
            for(ContentDocumentLink doclink : [Select id, ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId =:claimid]){
                doclinkIdlist.add(doclink.ContentDocumentId);
            }
        }
        if(doclinkIdlist.size() > 0){
            for(ContentDocument doc :[SELECT Id, ContentSize,FileExtension,FileType,Title FROM ContentDocument where Id in:doclinkIdlist]){
                doclist.add(doc);
            }
        }
        return doclist;
    }
    @AuraEnabled
    public static String updAttachment(ContentDocument cont) {
        String msg = '';
        try{
            update cont;
            msg = 'Success';
        }Catch(Exception ex){
            msg = 'Error';
        }
        return msg;
    }
    @AuraEnabled
    public static ClubAuto_LtngClaimsSubController subitClaim(String caclaim, boolean consent, string conmes,String claimsid, String screen, String operation) {
        ClubAuto_LtngClaimsSubController pg = new ClubAuto_LtngClaimsSubController();
        pg.claim = new Expense_Claim__c();
        pg.doclist = new List<ContentDocument>();
        pg.success = true;
        pg.ErrMsg = '';
        
        String claimid = '';
        String contactid = '';
        String consid = '';
    Expense_Claim__c cl = new Expense_Claim__c();
        system.debug('******caclaim*****'+caclaim);
        List<User> usrlist = new List<User>();
        List<Vehicle__c> vinlist = new List<Vehicle__c>(); //Changed VIN look up reference to Vehicle object
    try{
      cl = (Expense_Claim__c)JSON.deserialize(caclaim, Expense_Claim__c.class);
      String usrid = userinfo.getuserid();
      usrlist = [select id, name, contactid from user where id = :usrid];
      if(usrlist.size() > 0){
        if(usrlist[0].contactid != null){
          contactid = usrlist[0].contactid;
        }
      }
      if(contactid != ''){
        cl.Claimant__c = contactid;
      }
      if(cl.VIN_Member_ID__c != ''){
        vinlist = [Select id, name, (Select Vehicle__c, Policy__c, Policy__r.recordtype.DeveloperName from Vehicle_Policies__r limit 1) from Vehicle__c where name =:cl.VIN_Member_ID__c]; //Changed VIN query to Vehicle object
      }
      if(vinlist.size() > 0){
        cl.Vehicle__c = vinlist[0].id; //Changed VIN look up reference to Vehicle object
        cl.VIN_Available__c = true;
        if(vinlist[0].Vehicle_Policies__r.size() > 0){
            cl.Vehicle_Policy__c = vinlist[0].Vehicle_Policies__r[0].Policy__c;
        }
      }
      if(claimsid != '' && claimsid != null){
        cl.Id = claimsid;
        List<Consent__c> cons = [select id, Claim__c from Consent__c where Claim__c = :claimsid];
        if(cons.size() > 0){
          consid = cons[0].Id;
        }
      }
      system.debug('******claim*****'+cl);
          if(screen == 'screen1'){
        if(operation == 'New'){
          cl.Claim_Status__c = 'Pending Submission';//Default value to Pending Submission when first screen is completed
        }
          }else{
            cl.Claim_Status__c = 'Submitted';//Default value to Submitted when second screen is submitted
          }
            upsert cl;
            Consent__c con = new Consent__c();
            con.Consent_Message__c = conmes;
            con.Consent_Type__c = 'Club Auto Claims Portal';
            con.Consent_Sub_Type__c = 'Claim Submission';
            con.Claim__c = cl.Id;
            if(contactid != ''){
                con.Contact__c = contactid;
            }
            if(consid != ''){
                con.id = consid;
            }
            con.Certify__c = consent;
            upsert con;
            pg.claim = cl;
        }catch(Exception ex){
            system.debug('******EXIT*****');
            pg.success = false;
            pg.ErrMsg = ex.getmessage();
            pg.claim = cl;
        }
        return pg;
    }
    /*Dependent picklist to work in lightning component*/
    @AuraEnabled 
    public static Map<String, String> getDependentOptions(string depfieldApiName, string contrfieldApiName, string contrlFieldValue) {
        String controllingField = contrfieldApiName.toLowerCase();
        String dependentField = depfieldApiName.toLowerCase();
    String ctrlFlValue = '';
    system.debug('**contrlFieldValue**'+contrlFieldValue);
    if(contrlFieldValue != null && contrlFieldValue != ''){
      ctrlFlValue = contrlFieldValue;
    }
    system.debug('**ctrlFlValue**'+ctrlFlValue);
        Expense_Claim__c cl = new Expense_Claim__c();
        Map<String,Map<String,String>> objResults = new Map<String,Map<String,String>>();
    Map<string,string> objDepValues = new Map<string,string>();
        
        Schema.sObjectType objType = cl.getSObjectType();
        if (objType==null){
            return objDepValues;
        }
        
        Map<String, Schema.SObjectField> objFieldMap = objType.getDescribe().fields.getMap();
        
        if (!objFieldMap.containsKey(controllingField) || !objFieldMap.containsKey(dependentField)){
            return objDepValues;     
        }
        
        Schema.SObjectField theField = objFieldMap.get(dependentField);
        Schema.SObjectField ctrlField = objFieldMap.get(controllingField);
        
        List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
        List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(theField.getDescribe().getPicklistValues());
        List<String> controllingValues = new List<String>();
        
        for (Schema.PicklistEntry ple : contrEntries) {
            String label = ple.getLabel();
      String value = ple.getValue();
            objResults.put(value, new Map<String,String>());
            controllingValues.add(value);
        }
        
        for (PicklistEntryWrapper plew : depEntries) {
            String label = plew.label;
            String value = plew.value;
            String validForBits = base64ToBits(plew.validFor);
            for (Integer i = 0; i < validForBits.length(); i++) {
                String bit = validForBits.mid(i, 1);
                if (bit == '1') {
                    objResults.get(controllingValues.get(i)).put(label,value);
                }
            }
        }
    system.debug('**objResults**'+objResults);
    if(objResults.size() > 0 && ctrlFlValue != ''){
      if(objResults.keyset().contains(ctrlFlValue)){
        objDepValues = objResults.get(ctrlFlValue);
      }
    }
    system.debug('**objDepValues**'+objDepValues);
        return objDepValues;
    }
    
    public static String decimalToBinary(Integer val) {
        String bits = '';
        while (val > 0) {
            Integer remainder = Math.mod(val, 2);
            val = Integer.valueOf(Math.floor(val / 2));
            bits = String.valueOf(remainder) + bits;
        }
        return bits;
    }
    
    public static String base64ToBits(String validFor) {
        if (String.isEmpty(validFor)) return '';
        
        String validForBits = '';
        
        for (Integer i = 0; i < validFor.length(); i++) {
            String thisChar = validFor.mid(i, 1);
            Integer val = base64Chars.indexOf(thisChar);
            String bits = decimalToBinary(val).leftPad(6, '0');
            validForBits += bits;
        }
        
        return validForBits;
    }
    
    private static final String base64Chars = '' +
        'ABCDEFGHIJKLMNOPQRSTUVWXYZ' +
        'abcdefghijklmnopqrstuvwxyz' +
        '0123456789+/';
    
    
    private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> PLEs) {
        return (List<PicklistEntryWrapper>)
            JSON.deserialize(JSON.serialize(PLEs), List<PicklistEntryWrapper>.class);
    }
    
    public class PicklistEntryWrapper{
        public String active {get;set;}
        public String defaultValue {get;set;}
        public String label {get;set;}
        public String value {get;set;}
        public String validFor {get;set;}
        public PicklistEntryWrapper(){            
        }
        
    }
  
  private static string getAllFields(String objectAPI){
    Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
    Map<String, Schema.SObjectField> fieldMap = schemaMap.get(objectAPI).getDescribe().fields.getMap();
    String strFields = '';
    for(String fieldName : fieldMap.keyset()){
      if(strFields == null || strFields == ''){
        strFields = fieldName;
      }else{
        strFields = strFields + ' , ' + fieldName;
      }
    }
    return strFields;
  }
  
}