public class FEDHandler implements ITrigger {
    
    public FEDHandler(){}
    
    public void bulkBefore(){}
    
    public void bulkAfter(){
        if(!Trigger.isDelete){
            List<Flow_Entity__c> feToUpdateList = new List<Flow_Entity__c>();
            Map<String, Integer> dependencyCountMap = new Map<String, Integer>();
            Set<String> dependencyChildSet = new Set<String>();
            for(sObject obj : Trigger.new){
                dependencyChildSet.add(((Flow_Entity_Dependency__c)obj).Dependency_Child__c);
            }
            List<Flow_Entity_Dependency__c> dependencyList = [SELECT Id, Dependency_Child__c FROM Flow_Entity_Dependency__c WHERE Dependency_Child__c IN :dependencyChildSet AND RecordType.Name = 'Layout'];
            Map<Id, Flow_Entity__c> relatedFlowEntityMap = new Map<Id, Flow_Entity__c>([SELECT Id , Dependency_Logic__c FROM Flow_Entity__c WHERE Id IN :dependencyChildSet]);
            for(Flow_Entity_Dependency__c fed : dependencyList){
                if(!dependencyCountMap.containsKey(fed.Dependency_Child__c)){
                    dependencyCountMap.put(fed.Dependency_Child__c, 0);
                }
                Integer temp = dependencyCountMap.get(fed.Dependency_Child__c);
                temp++;
                dependencyCountMap.put(fed.Dependency_Child__c, temp);
            }
            for(sObject obj : Trigger.new){
                Flow_Entity_Dependency__c entity = (Flow_Entity_Dependency__c)obj;
                String recordTypeName = Schema.SObjectType.Flow_Entity_Dependency__c.getRecordTypeInfosById().get(entity.RecordTypeId).getname();
                if(recordTypeName == 'Layout'){
                    if(dependencyCountMap.get(entity.Dependency_Child__c) > 1){
                        Flow_Entity__c adjustedFlowEntity = relatedFlowEntityMap.get(entity.Dependency_Child__c);
                        if(!String.isBlank(adjustedFlowEntity.Dependency_Logic__c)){
                            if(!String.isBlank(adjustedFlowEntity.Dependency_Logic__c)){
                                adjustedFlowEntity.Dependency_Logic__c = adjustedFlowEntity.Dependency_Logic__c + ' AND '+ dependencyCountMap.get(entity.Dependency_Child__c);
                                feToUpdateList.add(adjustedFlowEntity);
                            }
                        }
                    }
                }
            }
            update feToUpdateList;
        } else {
            List<Flow_Entity__c> feToUpdateList = new List<Flow_Entity__c>();
            Set<String> dependencyChildSet = new Set<String>();
            for(sObject obj : Trigger.old){
                dependencyChildSet.add(((Flow_Entity_Dependency__c)obj).Dependency_Child__c);
            }
            Map<Id, Flow_Entity__c> relatedFlowEntityMap = new Map<Id, Flow_Entity__c>([SELECT Id , Dependency_Logic__c FROM Flow_Entity__c WHERE Id IN :dependencyChildSet]);
            for(sObject obj : Trigger.old){
                Flow_Entity_Dependency__c entity = (Flow_Entity_Dependency__c)obj;
                if(Trigger.isDelete){
                    String recordTypeName = Schema.SObjectType.Flow_Entity_Dependency__c.getRecordTypeInfosById().get(entity.RecordTypeId).getname();
                    if(recordTypeName == 'Layout'){
                        Flow_Entity__c adjustedFlowEntity = relatedFlowEntityMap.get(entity.Dependency_Child__c);
                        if(!String.isBlank(adjustedFlowEntity.Dependency_Logic__c)){
                            adjustedFlowEntity.Dependency_Logic__c = '';
                            feToUpdateList.add(adjustedFlowEntity);
                        }
                    }
                }
            }
            update feToUpdateList;
        }
    }
    
    public void beforeInsert(SObject so){}
    
    public void beforeUpdate(SObject oldSo, SObject so){}
    
    public void beforeDelete(SObject so){
    
    }
    
    public void afterInsert(SObject so){
    }
    
    public void afterUpdate(SObject oldSo, SObject so){}
    
    public void afterDelete(SObject so){
    }
    
    public void andFinally(){}
}