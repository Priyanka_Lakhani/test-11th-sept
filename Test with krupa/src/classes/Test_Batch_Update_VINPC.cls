@isTest
public class Test_Batch_Update_VINPC {
    @testSetup 
    static void setup() {
        List<Account> accList = new List<Account>();
        List<Program__c> prodList = new List<Program__c>();
        List<VIN__c> vinList = new List<VIN__c>();
        Id rtClient = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Client' LIMIT 1].Id;
        Id rtDealership = [SELECT Id FROM RecordType WHERE SobjectType = 'Account' AND DeveloperName = 'Dealership' LIMIT 1].Id;
        Account client930 = new Account(
            RecordTypeId = rtClient,
            Name = 'Enterprise',
            Account_Number__c = '930',
            Status__c = 'Active',
            Type = 'Customer'
        );
        accList.add(client930);

        Account towDest930 = new Account(
            RecordTypeId = rtDealership,
            Name = 'test Dealership',
            Status__c = 'Active',
            Client__c = client930.Id,
            Type = 'Dealership / Tow Destination',
            BillingCountry = 'Canada',
            BillingStreet = '266 LABELLE BOUL',
            BillingCity = 'SAINTE-ROSE',
            BillingState = 'Quebec',
            BillingPostalCode = 'H7L3A2',
            Location__Latitude__s = 45.59971620,
            Location__Longitude__s = -73.78835360
        );
        accList.add(towDest930);
        insert accList;
        Program__c prog930 = new Program__c(
            Account__c = client930.Id,
            Description__c = 'Base',
            Type__c = 'Passthrough',
            Status__c = 'Active',
            Name = '930 - Enterprise [Base]',
            Program_Code__c = '930',
            T1__c = true,
            T3__c = true,
            T4__c = true,
            T5__c = true,
            T6__c = true,
            T6F__c= true,
            T6H__c = true,
            T7__c = true,
            T8__c = true,
            Unattended_Tow__c = true,
            CTI_Search_Terms__c = 'Enterprise,ERACHomeCity',
            Client_Name_for_Club__c = 'Enterprise',
            VIN_Help_Text__c = 'VIN',
            Max_Search_Results__c = 10,
            Max_Search_Results_Extra__c = 10,
            VIN_Field_Label__c = 'VIN #',
            Vehicle_Info_Provided__c = true,
            Member_Add_Available__c = true,
            OEM__c = true,
            Contact_Available__c = true,
            Tow_Dest_Available__c = true,
            Default_Tow_Option__c = 'Closest',
            Default_Tow_Distance__c = 20,
            Max_Tow_Distance__c = 100,
            Max_Tow_Distance_Absolute__c = 500,
            Client_Name_English__c = 'LGM MP Financial Services Inc.',
            Client_Name_French__c = 'Groupe Financier LGM',
            Service_Tracker_Enabled__c = true,
            Additional_Misc_Call_Reasons__c = 'test1,test2',
            Track_Call_History__c = true,
            AddTowReason__c = 'test1,test2',
            Call_Flow_License_Plate_Required__c = true,
            Call_Flow_Tow_Exchange_Required__c = true,
            Call_Flow_EHI__c = true
            //Base_Warranty_Service_Limit_Term__c
        );
        prodList.add(prog930);
        insert prodList;
        VIN__c vin930 = new VIN__c(
            Name = 'EHI11222223333344',
            UID__c = 'EHI11222223333344',
            Program__c = prog930.Id,
            Year__c = '2018',
            Make__c = 'Jaguar',
            Model__c = '111',
            Colour__c = 'GREY',
            Base_Warranty_Start__c = Date.today().addMonths(-3),
            Base_Warranty_End__c = Date.today().addMonths(3),
            Base_Warranty_Max_Odometer__c = 100000,
            Warranty_Term__c = 6,
            First_Name__c = 'Test',
            Last_Name__c = 'Test',
            Phone__c = '123123123',
            License__c = 'Test'
        );
        vinList.add(vin930);
        insert vinList;
    }
    static testMethod void test_schedulable_classes1() {
        Test.startTest();
            Batch_Update_VINPC vinpc = new Batch_Update_VINPC();
            Database.executebatch(vinpc,1);
        Test.stopTest();
    }
    static testMethod void test_schedulable_classes2() {
        Test.startTest();
            Schedule_BatchUpdateVINPC sched = new Schedule_BatchUpdateVINPC();
            String sch = '0 0 23 * * ?'; 
            system.schedule('test_updvinpc_sch', sch, sched); 
        Test.stopTest();
    }   
}