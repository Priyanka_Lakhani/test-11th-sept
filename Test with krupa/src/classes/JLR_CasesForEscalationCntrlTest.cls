@isTest
private class JLR_CasesForEscalationCntrlTest {
    
    @testSetup public static void setup(){

        CA__c settings = new CA__c(
            Last_Case_Number__c = '02785882'

            );
        insert settings;

         Account client = new Account(
            Name='Client', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Client').getRecordTypeId(),
            Account_Number__c='123'
        );
        insert client;
        
        Program__c prog = new Program__c(
            Account__c=client.Id,
            Program_Code__c='123',
            Description__c='Base',
            Type__c='Passthrough',
            CTI_Search_Terms__c='123',
            Status__c='Active',
            Additional_Misc_Call_Reasons__c='test',
            Call_Flow_JLR__c = true
        );
        insert prog;

        VIN__c vin = new VIN__c(
            Name='TEST1111',
            UID__c='TEST2222',
            Base_Warranty_Start__c=Date.today().addYears(-5),
            Base_Warranty_End__c=Date.today().addYears(1),
            Make__c='LAND ROVER',
            Model__c='R8',
            Program__c=prog.Id
        );
        insert vin;

        Account tow = new Account(
            Name='TEST Dealership', 
            RecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dealership / Tow Destination').getRecordTypeId(), 
            Client__c=client.Id,
            Location__Longitude__s=0.1, 
            Location__Latitude__s=0.1,
            R8_Dealership__c=TRUE
        );
        insert tow;

        Account club = new Account(
            Name = 'test',
            Account_Number__c = '999',
            ETA__c = 91,
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Dispatch Club').getRecordTypeId());
        insert club;

         Territory_Coverage__c terr = new Territory_Coverage__c(
            Club__c=club.Id,
            City__c='Richmond Hill',
            Province__c='Ontario',
            Province_Code__c='ON'
        );
        insert terr;

       Case c = new Case(
            Status = 'Dispatched',
            City__c = terr.City__c,
            Province__c = terr.Province__c,
            Breakdown_Location__Longitude__s = tow.Location__Longitude__s, 
            Breakdown_Location__Latitude__s = tow.Location__Latitude__s,
            VIN_Member_ID__c = vin.Name,
            Interaction_ID__c = 'INTERACTION111',
            vin__c = vin.id,
            Phone__c = '416-999-9999',
            Call_ID__c = 'ABCDD',
            trouble_code__c = 'Flatbed Tow',
            tow_reason__c = 'brake problem',
            program__c = prog.id,
            jlr__c = true,
            ETA__c = 100,
            DI_Status_Date_Time__c = Date.today()

        );

       insert c;
    }


    @isTest static void test_method_one() {
        JLR_CasesForEscalationCntrl ctrl = new JLR_CasesForEscalationCntrl();
    }

}